/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import ogm.tasks.service;
import vibe.service.main;

int main(string[] args) {
  return mainService!(TasksService)(args);
}
