/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.article.Test;

import vibe.data.json;
import vibe.core.log;

import crate.base;
import crate.mime;
import crate.error;
import std.conv;
import std.algorithm;
import std.array;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import ogm.crates.all;
import ogm.models.meta;
import ogm.meta;
import ogm.process;
import gis_collective.tasks.color;
import gis_collective.tasks.newsletter.emailMessage;

version(unittest) {
  import fluent.asserts;
  import ogm.test.fixtures;
}

struct ArticleTest {
  struct Request {
    ObjectId id;
    string userId;
  }

  enum type = "article.test";

  OgmCrates crates;
  IBroadcast broadcast;

  void main(Request request) {
    auto strId = request.id.to!string;

    auto article = crates.article.getItem(strId).and.exec.front;

    if(!article["type"].to!string.canFind("newsletter")) {
      info("The article " ~ strId ~ " must have the 'newsletter-article' type.");
      return;
    }

    auto userRange = crates.user.get
      .where("_id").equal(ObjectId.fromString(request.userId)).and.exec;

    if(userRange.empty) {
      info("The user " ~ strId ~ " does not exist.");
      return;
    }

    auto user = userRange.front;
    string email = user["email"].to!string;

    auto newsletter = crates.newsletter.getItem(article["relatedId"].to!string).and.exec.front;

    info("The newsletter article " ~ strId ~ " is being sent.");

    EmailMessage message;
    message.article = article;
    message.isTest = true;
    message.recipients = [email];
    message.newsletterId = newsletter["_id"].to!string;
    message.updateLinks(crates);

    broadcast.push("newsletter.email", message.serializeToJson);
  }
}
