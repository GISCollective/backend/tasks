/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.article.Publish;

import vibe.data.json;
import vibe.core.log;

import crate.base;
import crate.mime;
import crate.error;
import std.conv;
import std.algorithm;
import std.array;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import ogm.crates.all;
import ogm.models.meta;
import ogm.meta;
import ogm.process;
import gis_collective.tasks.color;
import gis_collective.tasks.newsletter.emailMessage;

version(unittest) {
  import fluent.asserts;
  import ogm.test.fixtures;
}

struct ArticlePublish {
  struct Request {
    ObjectId id;
  }

  size_t maxRecipients = 500;
  enum type = "article.publish";

  OgmCrates crates;
  IBroadcast broadcast;

  bool isNotSent(string articleId, string email) {
    const sentCount = crates.metric.get
      .where("name").equal(articleId).and
      .where("type").equal("newsletterSent").and
      .where("labels.email").equal(email).and
      .size;

    return sentCount == 0;
  }

  void main(Request request) {
    auto strId = request.id.to!string;

    auto article = crates.article.getItem(strId).and.exec.front;

    if(article["status"] != "pending") {
      info("The newsletter article " ~ strId ~ " does not have the 'pending' status.");
      return;
    }

    if(article["type"] != "newsletter-article") {
      info("The article " ~ strId ~ " must have the 'newsletter-article' type.");
      return;
    }

    auto newsletter = crates.newsletter.getItem(article["relatedId"].to!string).and.exec.front;

    string[] recipients;
    size_t start;

    article["status"] = "publishing";
    crates.article.updateItem(article);

    info("The newsletter article " ~ strId ~ " is being sent.");

    do {
      auto query = crates.newsletterEmail.get;

      auto or = query.or;

      or.where("list").arrayFieldContains("newsletter", newsletter["_id"].to!string).
      or.where("list").arrayFieldContains("newsletter", ObjectId(newsletter["_id"].to!string));

      recipients = query
        .skip(start)
        .limit(maxRecipients)
        .exec
        .map!(a => a["email"].to!string)
        .array;

      info("Got " ~ recipients.length.to!string ~ " recipients for " ~ strId ~ ".");

      start += recipients.length;

      if(recipients.length > 0) {
        EmailMessage message;
        message.article = article;
        message.recipients ~= recipients.filter!(a => this.isNotSent(strId, a)).array;
        message.newsletterId = newsletter["_id"].to!string;
        message.updateLinks(crates);

        if(message.recipients.length) {
          info("Sending " ~ message.recipients.length.to!string ~ " messages for " ~ strId ~ ".");
          broadcast.push("newsletter.email", message.serializeToJson);
        }
      }
    } while(recipients.length > 0);

    article["status"] = "sent";
    crates.article.updateItem(article);
  }
}
