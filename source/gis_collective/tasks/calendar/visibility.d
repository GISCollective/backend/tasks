/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.calendar.visibility;

import vibe.data.json;
import vibe.core.log;

import std.datetime;
import std.algorithm;
import std.array;

import crate.base;
import vibe.service.stats;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import ogm.crates.all;

version(unittest) {
  import fluent.asserts;
  import ogm.test.fixtures;
}

struct CalendarEventsVisibility {
  struct Message {
    ObjectId id;
  }

  enum type = "calendar.eventsVisibility";
  alias Model = Calendar;

  OgmCrates crates;
  IBroadcast broadcast;

  static Json[] processList(const ChangeSet changeSet, OgmCrates crates) {
    if(changeSet.model != Model.stringof) {
      return [];
    }

    if(changeSet.type != CrateChangeType.update) {
      return [];
    }

    return [ changeSet.itemId.toJson ];
  }


  void log(T...)(string message, T params) {
    logInfo("[%s %s] " ~ message, type, id, params);
  }

  void main(Message message) {
    auto calendar = crates.calendar.getItem(message.id.toString).and.exec.front;
    auto visibility = calendar["visibility"];

    auto events = crates.event.get
      .where("calendar").equal(message.id)
      .and.exec;

    foreach(Json event; events) {
      event["visibility"] = visibility;
      crates.event.updateItem(event);
    }
  }
}
