/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.event.generateOccurrences;

import vibe.data.json;
import vibe.core.log;

import std.datetime;
import std.algorithm;
import std.array;

import crate.base;
import vibe.service.stats;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import ogm.crates.all;
import ogm.middleware.event.EventOccurrencesMiddleware;

version(unittest) {
  import fluent.asserts;
  import ogm.test.fixtures;
}

struct EventGenerateOccurrences {
  struct Message {
    string id;
  }

  enum type = "Event.generateOccurrences";
  alias Model = Event;

  OgmCrates crates;

  void log(T...)(string message, T params) {
    logInfo("[%s %s] " ~ message, type, id, params);
  }

  void main(Message message) {
    auto event = crates.event.getItem(message.id).and.exec.front;

    auto middleware = new EventOccurrencesMiddleware(crates);
    middleware.addOccurrences(event);

    crates.event.updateItem(event);
  }
}
