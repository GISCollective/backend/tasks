/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.history.changeset;

import ogm.crates.all;
import vibe.data.json;

import crate.base;
import crate.json;
import crate.collection.notifications;

import std.datetime;
import std.conv;

ChangeSet toChangeSet(ref const Json change) {
  auto before = Json.emptyObject;
  auto after = Json.emptyObject;
  ChangeSet changeSet;

  if(change["before"].type == Json.Type.object) {
    before = change["before"].toFlatJson;

    if("_id" in change["before"]) {
      changeSet.itemId = ObjectId.fromJson(change["before"]["_id"]);
    }
  }

  if(change["after"].type == Json.Type.object) {
    after = change["after"].toFlatJson;

    if("_id" in change["after"]) {
      changeSet.itemId = ObjectId.fromJson(change["after"]["_id"]);
    }

    if("info" in change["after"] && "author" in change["after"]["info"]) {
      changeSet.author = change["after"]["info"]["author"].to!string;
    }
  }

  changeSet.removed = jsonDiff(before, after);
  changeSet.added = jsonDiff(after, before);
  changeSet.model = change["modelName"].to!string;
  changeSet.time = Clock.currTime;
  changeSet.type = CrateChangeType.update;

  if(change["type"] == "add") {
    changeSet.type = CrateChangeType.add;
  }

  if(change["type"] == "delete") {
    changeSet.type = CrateChangeType.delete_;
  }

  return changeSet;
}
