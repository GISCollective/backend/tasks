/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.history.task;

import ogm.crates.all;
import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import gis_collective.tasks.history.changeset;

import crate.base;
import crate.collection.notifications;
import crate.json;

import vibe.service.stats;
import vibe.data.json;
import vibe.core.log;
import vibe.core.core;

import std.conv;
import std.datetime;
import std.algorithm;
import std.array;
import std.traits;
import std.exception;

import described : Describe = describe, where;

alias TaskCheck = Json[] delegate(ChangeSet);

class HistoryTaskPool(Tasks...) {
  private {
    OgmCrates crates;
    IBroadcast broadcast;

    TaskCheck[string] tasks;

    static immutable string[] modelsWithHistory = [
      "Map",
      "MapFile",
      "Feature",
      "Article",
      "Team",
      "UserProfile",
      "BaseMap",
      "Icon",
      "IconSet",
      "Preference",
      "Issue",
      "Page",
      "Layout",
      "Campaign",
      "Space",
    ];
  }

  this(OgmCrates crates, IBroadcast broadcast) {
    this.crates = crates;
    this.broadcast = broadcast;
  }

  void log(T...)(string message, T params) {
    logInfo("[HistoryTaskPool] " ~ message, params);
  }

  TaskCheck wrap(T)(T[] delegate(const ChangeSet) f) {
    Json[] t(const ChangeSet change) {
      return f(change).map!(a => a.serializeToJson).array;
    }

    return &t;
  }

  ///
  auto add(T)(T task) {
    enum description = Describe!T;
    enum name = T.type;
    enforce(name !in tasks, "The `" ~ name ~ "` task is already added!");

    static foreach(method; description.methods.where.anyOfAttributes(["onChange"])) {
      static if(method.returns.name == "Json[]") {
        mixin(`tasks[name] = &task.` ~ method.name ~ `;`);
      } else {
        mixin(`tasks[name] = this.wrap(&task.` ~ method.name ~ `);`);
      }
    }

    return this;
  }

  void subscriber(const Json change) @trusted nothrow {
    auto begin = Clock.currTime;
    Stats.instance.inc("Task", ["type": "history", "action": "subscriber"]);

    try {
      auto changeSet = change.toChangeSet;
      Json[] list;

      logDebug("Got a new change: %s", changeSet.serializeToJson);
      if(changeSet.removed.length == 0 && changeSet.added.length == 0) {
        log("Ignoring the change in `%s` because there are no removed or added fields.", changeSet.serializeToJson);
        return;
      }

      foreach(name, check; tasks) {
        auto values = check(changeSet);

        foreach(value; values) {
          auto data = Json.emptyObject;
          data["id"] = value;
          data["model"] = changeSet.model;
          data["type"] = changeSet.type;

          this.broadcast.push(name, data);
        }
      }

      static foreach(Task; Tasks) static if(is(Task == struct)) {
        logDebug("Asking `%s` if can process the change.", Task.type);
        list = [];

        static if(__traits(hasMember, Task, "processList") && arity!(Task.processList) == 1) {
          list = Task.processList(changeSet);
        }

        static if(__traits(hasMember, Task, "processList") && arity!(Task.processList) == 2) {
          list = Task.processList(changeSet, crates);
        }

        if(list.length > 0) {
          log("`%s` can process `%s`", Task.type, list);

          foreach(item; list) {
            Json value;

            if(item.type == Json.Type.string) {
              value = Json.emptyObject;
              value["id"] = item;
            } else if(item.type == Json.Type.object) {
              value = item;
            } else {
              log("Trigger `%s` with `%s` was ignored.", Task.type, value);
              continue;
            }

            value["uid"] = value["id"];
            value["model"] = changeSet.model;
            value["type"] = changeSet.type;

            log("Trigger `%s` with `%s`", Task.type, value);
            this.broadcast.push(Task.type, value);
          }
        }
      }

      if(modelsWithHistory.canFind(changeSet.model)) {
        log("Add: %s", changeSet.serializeToJson);
        this.crates.changeSet.addItem(changeSet.serializeToJson);
      }
    } catch(Exception e) {
      error(e);
    }

    auto diff = Clock.currTime - begin;
    Stats.instance.avg("Task", diff.total!"nsecs", ["type": "history", "action": "duration"]);
  }
}
