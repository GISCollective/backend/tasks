/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.task;

import vibe.service.stats;

import vibe.core.log;
import vibe.data.json;

import ogm.crates.all;
import gis_collective.hmq.broadcast.base;
import gis_collective.tasks.error;
import gis_collective.tasks.GisTask;

import std.datetime;
import std.conv;
import std.traits;
import gis_collective.hmq.log;

import gis_collective.tasks.GisTask;

version(unittest) {
  import fluent.asserts;
  import ogm.test.fixtures;
  import gis_collective.hmq.broadcast.memory;
}

class Task(T : GisTask) {
  private {
    IBroadcast broadcast;
    T instance;
  }

  this(IBroadcast broadcast, T instance) {
    this.broadcast = broadcast;
    this.instance = instance;
  }

  void subscriber(const Json data) @trusted nothrow {
    auto begin = Clock.currTime;
    enum taskName = T.stringof;

    Json value;
    alias P = Parameters!(instance.start)[0];

    try {
      Stats.instance.inc("Task", ["type": taskName, "action": "start"]);
      logDebug("`%s`triggered: `%s`", taskName, value.to!string);
      enforce!TaskValidationException(data.type == Json.Type.object, "Can't handle `" ~ value.to!string ~ "`. Only objects are supported.");

      value = data["value"];
      auto taskValue = value.deserializeJson!P;

      instance.log(value, "Start");

      instance.start(taskValue);
    } catch(TaskValidationException e) {
      Stats.instance.inc("Task", ["type": taskName, "action": "validation error"]);

      try { instance.log(value, "Validation Error: " ~ e.message.to!string); } catch(Exception) {}
      instance.incError(value);

      error("[%s] Validation exception: %s", taskName, e.message);
    } catch(Exception e) {
      Stats.instance.inc("Task", ["type": taskName, "action": "error"]);

      try { instance.log(value, "Error: " ~ e.message.to!string); } catch(Exception) {}
      instance.incError(value);

      error("[%s] Exception: %s", taskName, e.toString);
    }

    auto diff = Clock.currTime - begin;
    Stats.instance.avg("Task", diff.total!"nsecs", ["type": taskName, "action": "duration"]);
    instance.done(value, diff);
  }
}

class Task(T) if(is(T == struct)) {

  private {
    OgmCrates crates;
    IBroadcast broadcast;
  }

  this(IBroadcast broadcast, OgmCrates crates) {
    this.broadcast = broadcast;
    this.crates = crates;
  }

  void subscriber(const Json value) @trusted nothrow {
    auto begin = Clock.currTime;

    T t;

    try {
      Stats.instance.inc("Task", ["type": T.type, "action": "start"]);
      logDebug("`%s`triggered: `%s`", T.type, value.to!string);
      enforce!TaskValidationException(value.type == Json.Type.object, "Can't handle `" ~ value.to!string ~ "`. Only objects are supported.");

      static if(__traits(hasMember, T, "id")) {
        t.id = value["id"].to!string;
      }

      static if(__traits(hasMember, T, "crates")) {
        t.crates = crates;
      }

      static if(__traits(hasMember, T, "value")) {
        t.value = value;
      }

      static if(__traits(hasMember, T, "broadcast")) {
        t.broadcast = this.broadcast;
      }

      static if(__traits(hasMember, T, "start")) t.start();

      static if(__traits(hasMember, T, "main")) {
        alias P = Parameters!(t.main)[0];
        P message = value.deserializeJson!P;

        t.main(message);
      }
    } catch(TaskValidationException e) {
      Stats.instance.inc("Task", ["type": T.type, "action": "validation error"]);
      error("[%s][%s] Validation exception: %s", T.stringof, T.type, e.message);
    } catch(Exception e) {
      static if(__traits(hasMember, T, "incError")) try { t.incError; } catch(Exception) {}
      error("[%s][%s] Exception: %s", T.stringof, T.type, e.toString);
    }

    static if(__traits(hasMember, T, "done")) {
      t.done();
    }

    auto diff = Clock.currTime - begin;
    Stats.instance.avg("Task", diff.total!"nsecs", ["type": T.type, "action": "duration"]);
  }
}

version(unittest) {
  struct TaskWithoutFields {
    enum type = "TaskWithoutFields";
    static bool started;

    void start() {
      started = true;
    }
  }

  struct TaskWithAllFields {
    enum type = "TaskWithAllFields";
    static {
      bool started;
      string id;
      OgmCrates crates;
      Json value;
      IBroadcast broadcast;
    }

    void start() {
      started = true;
    }
  }

  struct TaskWithError {
    enum type = "TaskWithError";
    static bool started;

    void start() {
      started = true;
      throw new Exception("There is a generic error");
    }
  }

  struct TaskWithErrorDoneAndLog {
    enum type = "TaskWithErrorDoneAndLog";

    static {
      bool started;
      bool finished;
      string logMessage;
    }

    void start() {
      started = true;
      throw new Exception("There is a generic error");
    }

    void log(string message) {
      logMessage = message;
    }

    void done() nothrow {
      finished = true;
    }
  }
}

/// It triggers a task with no fields
unittest {
  IBroadcast broadcast;

  auto task = new Task!TaskWithoutFields(broadcast, crates);
  task.subscriber(Json.emptyObject);

  TaskWithoutFields.started.should.equal(true);
}

/// It triggers a task with all fields
unittest {
  IBroadcast broadcast = new MemoryBroadcast;

  auto task = new Task!TaskWithAllFields(broadcast, crates);
  task.subscriber(`{"id":"some id","key":"value"}`.parseJsonString);

  TaskWithAllFields.started.should.equal(true);
  TaskWithAllFields.id.should.equal("some id");
  TaskWithAllFields.crates.should.equal(crates);
  TaskWithAllFields.value.should.equal(`{"id":"some id","key":"value"}`.parseJsonString);
  TaskWithAllFields.broadcast.should.equal(broadcast);
}

/// It does not fail when the task throws
unittest {
  IBroadcast broadcast = new MemoryBroadcast;

  auto task = new Task!TaskWithError(broadcast, crates);
  task.subscriber(`{"id":"some id","key":"value"}`.parseJsonString);

  TaskWithError.started.should.equal(true);
}
