/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.team.invitation;

import vibe.data.json;
import vibe.core.log;

import std.datetime;
import std.algorithm;
import std.array;
import std.conv;

import crate.base;
import vibe.service.stats;
import vibe.core.log;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import ogm.crates.all;
import ogm.models.team;
import ogm.models.message;
import ogm.message;

struct TeamInvitation {
  struct Request {
    string id;
    ObjectId team;
    Invitation invitation;
  }

  enum type = "team.invitation";
  alias Model = Team;

  OgmCrates crates;
  IBroadcast broadcast;

  static string serviceUrl;

  void main(Request request) {
    auto teamRange = crates.team.getItem(request.team.to!string).exec;

    if(teamRange.empty) {
      logInfo("The team %s was not found.", request.team);
      return;
    }

    auto team = teamRange.front;

    Message notification;
    notification.subject = "Invitation to join " ~ team["name"].to!string;
    notification.to = MessageRecipient("email", request.invitation.email);
    notification.uniqueKey = "team.invitation." ~ request.invitation.email ~ "." ~ request.team.to!string;
    notification.type = MessageType.message;
    notification.text = "Welcome aboard!\n\nYou're invited to join our " ~ request.invitation.role ~ " of the " ~ team["name"].to!string ~ " team.";
    notification.html = "<h1>Welcome aboard!</h1><p>You're invited to join our <b>" ~ request.invitation.role ~ "</b> of the <b>" ~ team["name"].to!string ~ "</b> team.</p>";
    notification.actions["Register now to get access"] = crates.getRegisterUrl(serviceUrl) ~ "/login/register";

    crates.message.send(notification, broadcast);
  }
}
