/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.team.delete_;

import vibe.data.json;
import vibe.core.log;

import std.datetime;
import std.algorithm;
import std.array;
import std.conv;

import crate.base;
import vibe.service.stats;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import ogm.crates.all;
import ogm.models.meta;
import ogm.meta;

version(unittest) {
  import fluent.asserts;
  import ogm.test.fixtures;
}

struct TeamDelete {
  struct Request {
    ObjectId id;
  }

  enum type = "Team.delete";
  alias Model = Team;

  OgmCrates crates;
  IBroadcast broadcast;

  static Json[] processList(const ChangeSet changeSet) {
    if(changeSet.model != Model.stringof) {
      return [];
    }

    if(changeSet.type != CrateChangeType.delete_) {
      return [];
    }

    return [ changeSet.itemId.toJson ];
  }

  void main(Request request) {
    auto size = crates.map.get
      .where("visibility.team").equal(request.id).and
      .size;

    logInfo("Removing `" ~ size.to!string ~ "` maps from team `" ~ request.id.toString ~ "`.");

    auto range = crates.map.get
      .where("visibility.team").equal(request.id).and.exec;

    auto deleteMapMessage = Json.emptyObject;

    foreach(map; range) {
      crates.map.deleteItem(map["_id"].to!string);

      deleteMapMessage["id"] = map["_id"];
      broadcast.push("map.delete", deleteMapMessage);
    }

    size = crates.meta.get
      .where("model").equal(Model.stringof).and
      .where("itemId").equal(request.id.toString).and
      .size;

    logInfo("Removing `" ~ size.to!string ~ "` metadata related to map `" ~ request.id.toString ~ "`.");

    range = crates.meta.get
      .where("model").equal(Model.stringof).and
      .where("itemId").equal(request.id.toString).and
      .exec;

    foreach(meta; range) {
      crates.meta.deleteItem(meta["_id"].to!string);
    }
  }
}
