/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.team.welcome;

import vibe.data.json;
import vibe.core.log;

import std.datetime;
import std.algorithm;
import std.array;
import std.conv;

import crate.base;
import vibe.service.stats;
import vibe.core.log;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import ogm.crates.all;
import ogm.models.team;
import ogm.models.message;
import ogm.message;

struct TeamWelcome {
  struct Request {
    string id;
    string role;
    ObjectId team;
    ObjectId user;
}

  enum type = "team.welcome";
  alias Model = Team;

  OgmCrates crates;
  IBroadcast broadcast;

  void main(Request request) {
    auto teamRange = crates.team.getItem(request.team.to!string).exec;

    if(teamRange.empty) {
      logInfo("The team %s was not found.", request.team);
      return;
    }

    auto team = teamRange.front;

    auto userRange = crates.user.getItem(request.user.to!string).exec;

    if(userRange.empty) {
      logInfo("The user %s was not found.", request.user);
      return;
    }

    auto user = userRange.front;

    Message notification;
    notification.subject = "Welcome to " ~ team["name"].to!string;
    notification.to = MessageRecipient("user", user["_id"].to!string);
    notification.uniqueKey = "team.welcome." ~ request.user.to!string ~ "." ~ request.team.to!string;
    notification.type = MessageType.message;
    notification.text = "Welcome aboard!\n\nYou were added to the " ~ request.role ~ " list of the " ~ team["name"].to!string ~ " team.";
    notification.html = "<h1>Welcome aboard!</h1><p>You were added to the <b>" ~ request.role ~ "</b> list of the <b>" ~ team["name"].to!string ~ "</b> team.</p>";

    crates.message.send(notification, broadcast);
  }
}
