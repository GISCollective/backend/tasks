/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.campaignAnswer.mapSync;


import geo.json;
import geo.geometries;
import geo.algorithm;

import vibe.data.json;
import vibe.core.log;

import std.datetime;
import std.algorithm;
import std.array;
import std.string;
import std.conv;
import std.math;
import std.exception;
import std.net.isemail;

import crate.base;
import vibe.service.stats;

import gis_collective.tasks.notifications.NotificationTask;
import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import ogm.crates.all;
import ogm.models.meta;
import ogm.models.feature;
import ogm.models.icon;
import ogm.models.picture;
import ogm.models.modelinfo;
import ogm.calendar;
import ogm.meta;
import ogm.message;

import ogm.features.fromCampaignAnswer;

struct CampaignAnswerMapSync {
  static string serviceUrl;

  struct Request {
    string id;
  }

  struct MessageData {
    string receiverName;
    string reviewUrl;
    string serviceName;
    string serviceUrl;
    string campaignName;
  }

  enum type = "CampaignAnswer.MapSync";
  alias Model = CampaignAnswer;

  OgmCrates crates;
  IBroadcast broadcast;

  static Json[] processList(const ChangeSet changeSet) {
    if(changeSet.model != CampaignAnswer.stringof) {
      return [];
    }

    if(changeSet.type != CrateChangeType.add) {
      return [];
    }

    return [ changeSet.itemId.toJson ];
  }

  void sendContributorNotification(Json answer, Json campaign) {
    string campaignId = campaign["_id"].to!string;
    string author = "";

    if(answer["info"].type == Json.Type.object && answer["info"]["author"].type == Json.Type.string) {
      author = answer["info"]["author"].to!string.strip;
    }

    if(author == "") {
      return;
    }

    auto message = messageFromArticle(crates.article, "notification-campaign-add-" ~ campaignId, "notification-campaign-add-default");
    message.uniqueKey = "notification-campaign-add-" ~ campaignId ~ "-" ~ answer["_id"].to!string;
    message.to.type = "email";

    if(answer["contributor"].type == Json.Type.object && answer["contributor"]["email"].type == Json.Type.string && answer["contributor"]["email"].to!string.isEmail.valid) {
      message.to.value = answer["contributor"]["email"].to!string.strip;
    } else try {
      message.to.value = crates.fetchEmail(author);
    } catch(Exception e) {
      error(e);
      return;
    }

    MessageData messageData;
    messageData.receiverName = crates.fetchName(author);
    messageData.serviceName = crates.getPreference("appearance.name");
    messageData.serviceUrl = CampaignAnswerMapSync.serviceUrl;
    messageData.campaignName = campaign["name"].to!string;
    messageData.reviewUrl = CampaignAnswerMapSync.serviceUrl ~ "/manage/survey-answers/" ~ answer["_id"].to!string;

    message.fill(messageData);

    crates.message.send(message, broadcast);
  }

  void sendTeamNotification(Json answer, Json campaign) {
    auto teamId = campaign["visibility"]["team"].to!string;
    auto team = crates.team.getItem(teamId).and.exec.front;

    auto message = messageFromArticle(crates.article, "notification-add-answer-team");
    message.uniqueKey = "notification-add-answer-team-" ~ answer["_id"].to!string;
    message.to.type = "team";
    message.to.value = teamId;

    MessageData messageData;
    messageData.receiverName = team["name"].to!string;
    messageData.serviceName = crates.getPreference("appearance.name");
    messageData.serviceUrl = CampaignAnswerMapSync.serviceUrl;
    messageData.campaignName = campaign["name"].to!string;
    messageData.reviewUrl = CampaignAnswerMapSync.serviceUrl ~ "/manage/survey-answers/" ~ answer["_id"].to!string;

    message.actions["Review it now"] = messageData.reviewUrl;

    message.fill(messageData);

    crates.message.send(message, broadcast);
  }

  void main(Request request) {
    string logPrefix = "[" ~ type ~ "][" ~ request.id ~ "]";

    auto answer = crates.campaignAnswer.getItem(request.id).and.exec.front;

    if("status" !in answer || answer["status"] == 0) {
      answer["status"] = cast(int) CampaignAnswerStatus.pending;
    }

    if(answer.hasFeature(crates)) {
      logInfo("%s The answer will not be copied, because it already has a matching feature.", logPrefix);
      return;
    }

    auto campaign = crates.campaign.getItem(answer["campaign"].to!string).and.exec.front;

    try {
      sendContributorNotification(answer, campaign);
    } catch(Exception e) {
      error(e);
    }

    try {
      sendTeamNotification(answer, campaign);
    } catch(Exception e) {
      error(e);
    }

    Json feature;
    try {
      feature = answer.fromCampaignAnswer(crates, broadcast);
    } catch(Exception e) {
      error(e);
    }

    if(feature.type == Json.Type.object) {
      answer["featureId"] = feature["_id"];
      crates.campaignAnswer.updateItem(answer);
    }
  }
}
