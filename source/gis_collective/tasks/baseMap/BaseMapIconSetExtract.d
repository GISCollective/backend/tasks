/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.baseMap.BaseMapIconSetExtract;

import crate.base;
import vibe.data.json;
import vibe.core.log;
import vibe.core.core;
import vibe.core.taskpool;

import vibe.service.stats;

import gis_collective.tasks.notifications.NotificationTask;
import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import ogm.crates.all;
import ogm.models.meta;
import ogm.models.feature;
import ogm.models.icon;
import ogm.models.picture;
import ogm.models.modelinfo;
import ogm.models.iconset;
import ogm.vt;
import ogm.meta;

import std.file;
import std.path;
import std.array;
import std.conv;
import std.datetime;
import std.algorithm;
import std.exception;

import ogm.process;

///
void extractImage(int width, int height, int x, int y, string file, string destination) {
  run("convert -crop " ~
      width.to!string ~ "x" ~ height.to!string ~ "+" ~ x.to!string ~ "+" ~ y.to!string ~ " " ~
      file ~
      " PNG32:" ~ destination);
}

void extractIcon_(string strData, string spritePath, string workingPath, string name) {
  auto data = strData.parseJsonString;

  auto w = data["width"].to!int;
  auto h = data["height"].to!int;
  auto x = data["x"].to!int;
  auto y = data["y"].to!int;

  extractImage(w, h, x, y, spritePath, buildPath(workingPath, name ~ ".png"));
}

struct IconSetExtract {
  OgmCrates crates;
  string spritePath;
  string workingPath;
  Json spriteData;
  Json baseMap;
  Json iconSet;

  void extractIcons() {
    workingPath.mkdirRecurse;

    foreach(string key, Json value; spriteData) {
      extractIcon(key);
    }
  }

  void extractIcon(string key) {
    extractIcon_(spriteData[key].to!string, spritePath, workingPath, key);
  }

  void ensureIconSet() {
    auto range = crates.iconSet.get
      .where("source.type").equal("BaseMap").and
      .where("source.modelId").equal(baseMap["_id"].to!string).and
      .exec;

    if(!range.empty) {
      iconSet = range.front;
      return;
    }

    IconSet iconSetModel;

    iconSetModel.name = baseMap["name"].to!string ~ " icons";
    iconSetModel.source.type = "BaseMap";
    iconSetModel.source.modelId = baseMap["_id"].to!string;
    iconSetModel.source.syncAt = Clock.currTime;

    iconSet = LazyIconSet.fromModel(iconSetModel).toJson;
    iconSet["visibility"]["team"] = baseMap["visibility"]["team"];

    crates.iconSet.addItem(iconSet);
  }

  Json upsertIcon(string name, Json data) {
    Json icon;
    auto range = crates.icon.get
      .where("name").equal(name).and
      .where("iconSet").equal(ObjectId.fromJson(iconSet["_id"])).and
      .exec;

    if(!range.empty) {
      icon = range.front;
    } else {
      Icon iconModel;

      iconModel.name = name;

      icon = LazyIcon.fromModel(iconModel).toJson;
      icon["iconSet"] = iconSet["_id"];

      info("The icon `%s` does not exist.", name);
      icon = crates.icon.addItem(icon);
    }

    auto measurements = IconMeasurements();

    try if("placeholder" in data) {
      measurements.content = data["placeholder"].deserializeJson!(int[]);
    } catch(Exception e) {
      error(e);
    }

    try if("content" in data) {
      measurements.content = data["content"].deserializeJson!(int[]);
    } catch(Exception e) {
      error(e);
    }

    try if("width" in data) {
      measurements.width = data["width"].to!int;
    } catch(Exception e) {
      error(e);
    }

    try if("height" in data) {
      measurements.height = data["height"].to!int;
    } catch(Exception e) {
      error(e);
    }

    try if("stretchX" in data) {
      measurements.stretchX = data["stretchX"].deserializeJson!(int[][]);
    } catch(Exception e) {
      error(e);
    }

    try if("stretchY" in data) {
      measurements.stretchY = data["stretchY"].deserializeJson!(int[][]);
    } catch(Exception e) {
      error(e);
    }

    icon["measurements"] = measurements.serializeToJson;
    icon["enforceSvg"] = true;

    icon["image"] = Json.emptyObject;
    icon["image"]["useParent"] = false;
    icon["image"]["value"] = upsertIconPicture(name);

    info("Updating `%s` to `%s`.", name, icon.to!string);
    icon = crates.icon.updateItem(icon);

    return icon;
  }

  string upsertIconPicture(string name) {
    auto path = buildPath(workingPath, name ~ ".png");

    return IconFile.fromLocalFile(path).toString;
  }

  void clean() {
    workingPath.rmdirRecurse;
  }
}

struct BaseMapIconSetExtract {
  struct Request {
    ObjectId id;
  }

  enum type = "BaseMap.iconSetExtract";
  alias Model = Icon;

  OgmCrates crates;
  string tmpFolder;

  static Json[] processList(const ChangeSet changeSet, OgmCrates crates) {
    if(changeSet.model == "BaseMap") {
      return [ changeSet.itemId.toJson ];
    }

    return [ ];
  }

  void main(Request request) {
    auto range = crates.baseMap.get.where("_id").equal(request.id).and.exec;

    if(range.empty) {
      info("The base map `" ~ request.id.to!string ~ "` does not exist.");
      return;
    }

    auto baseMap = range.front;

    auto layerRange = baseMap["layers"].byValue.filter!(a => a["type"] == "MapBox");

    if(layerRange.empty) {
      info("The base map `" ~ request.id.to!string ~ "` has no vector tile layer.");
      return;
    }

    info("Extracting icons from basemap layers.");
    IconSetExtract iconSetExtract;
    iconSetExtract.workingPath = buildPath(tempDir, request.id.to!string);
    iconSetExtract.workingPath.mkdirRecurse;
    iconSetExtract.spritePath = buildPath(iconSetExtract.workingPath, "sprite.png");
    iconSetExtract.baseMap = baseMap;
    iconSetExtract.crates = crates;
    info("Working path: " ~ iconSetExtract.workingPath);

    scope(exit) iconSetExtract.clean();

    info("Downloading the mapbox base map.");
    auto style = downloadMapBoxBaseMap(layerRange.front);

    info("Downloading the sprite data.");
    iconSetExtract.spriteData = getRemoteJson(style["sprite"].getSpriteUrl!"json");

    info("Downloading the sprite image to: %s", iconSetExtract.spritePath);
    downloadFile(style["sprite"].getSpriteUrl!"png", iconSetExtract.spritePath);

    info("Downloaded ", iconSetExtract.spritePath.getSize / 1024, "kb");

    enforce(iconSetExtract.spritePath.getSize > 0, "Can't work with empty sprites.");

    info("Extracting icons from the sprite imge.");
    iconSetExtract.extractIcons;

    info("Ensure that the icon set exists.");
    iconSetExtract.ensureIconSet;

    foreach (string iconName, value; iconSetExtract.spriteData) {
      info("Ensure that the icon `" ~ iconName ~ "` exists.");
      iconSetExtract.upsertIcon(iconName, value);
    }
  }
}