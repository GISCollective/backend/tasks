/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.GisTask;

public import gis_collective.hmq.broadcast.base;

import ogm.crates.all;
import ogm.models.meta;

import vibe.data.json;
import vibe.core.log;

import crate.base;
import crate.lazydata.base;

import std.string;
import std.datetime;
import std.exception;
import std.conv;
import std.algorithm;
import std.array;

///
class GisTask {

  private IBroadcast _broadcast;

  protected {
    OgmCrates crates;
    size_t errors;
    string type;
  }

  ///
  this(OgmCrates crates, IBroadcast broadcast, string type) {
    this.crates = crates;
    this.type = type;
    this._broadcast = broadcast;
  }

  /// Logs the message to the default logger
  void log(Json value, string message) nothrow {
    logInfo(`[%s][%s] %s`, type, value, message);
  }

  /// Send a message to the queue
  void broadcast(string channel, Json message) {
    enforce(message.type == Json.Type.object, "The message must be an object.");
    enforce(channel != "", "The channel must not be an empty string.");
    enforce(_broadcast !is null, "The broadcast does not exist.");

    this._broadcast.push(channel, message);
  }

  ///
  void incError(Json value) nothrow {
    errors++;
  }

  ///
  void done(Json value, Duration duration) nothrow {
    errors++;

    try {
      log(value, "Done in " ~ duration.to!string);
    } catch(Exception) {
      log(value, "Done in unknown time.");
    }
  }

  ///
  Json resolve(string model, string id) @trusted {
    assert(model in crateGetters, "The crateGetters[`" ~ model ~ "`] is not set!");

    try {
      return crateGetters[model](id).exec.front;
    } catch(Exception e) {
      logError("Can't fetch `%s` with id `%s. %s.`", model, id, e.message);

      return Json(null);
    }
  }

  Meta[] getMetaList(string type, string itemId) {
    return crates.meta.get
      .where("type").equal(type).and
      .where("itemId").equal(itemId).and
      .exec
      .map!(a => a.deserializeJson!Meta)
      .array;
  }

  /// Receive a typed item from the database
  bool exists(T)(string id) {
    auto data = resolve(T.stringof, id);
    return data.type == Json.Type.object;
  }

  /// Receive a typed item from the database
  T get(T)(string id) {
    auto data = resolve(T.stringof, id);

    return Lazy!T(data, &resolve).toType;
  }

  /// Update a typed item in the database
  void update(T)(const T item) {
    auto data = Lazy!T.fromModel(item).toJson;

    enum name = T.stringof[0..1].toLower ~ T.stringof[1..$];

    mixin(`crates.` ~ name ~ `.updateItem(data);`);
  }

  /// Add a typed item in the database
  void add(T)(const T item) {
    auto data = Lazy!T.fromModel(item).toJson;
    enum name = T.stringof[0..1].toLower ~ T.stringof[1..$];

    mixin(`crates.` ~ name ~ `.addItem(data);`);
  }

  /// Add a typed item in the database
  void deleteItem(T)(const T item) {
    enum name = T.stringof[0..1].toLower ~ T.stringof[1..$];

    mixin(`crates.` ~ name ~ `.deleteItem(item._id.toString);`);
  }
}

/// Attribute used to tag methods that shold trigger
/// when there is a new changeset
struct OnChange {}

/// ditto
OnChange onChange() { return OnChange(); }