/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.map.MapSprite;

import gis_collective.hmq.log;
import gis_collective.tasks.iconSet.IconSetSprite;
import crate.base;

import std.algorithm;
import std.array;
import std.conv;
import std.functional;

import ogm.crates.all;
import ogm.models.changeset;

import vibe.data.json;

struct MapSpriteGenerator {
  OgmCrates crates;
  string linkId;
  ObjectId[] iconSetIds;
  int spriteSize = 64;
  Picture[] sprites;

  void prepare() {
    auto pictures = crates.picture.get
      .where("meta.link.modelId").equal(linkId).and
      .where("meta.link.model").equal("Map").and
      .where("meta.link.type").equal("sprite").and
      .exec;

    foreach (picture; pictures) {
      crates.picture.deleteItem(picture["_id"].to!string);
    }
  }

  void main() {
    auto resolver = toDelegate(&itemResolver);

    iconSetIds = iconSetIds.filter!(a => a.isValid).array;

    auto icons = crates.icon.get
      .where("iconSet").anyOf(iconSetIds)
      .and.exec.array.map!(a => LazyIcon.fromJson(a, resolver).toType).array;

    foreach (ref icon; icons) {
      if(!icon.styles.hasCustomStyle) {
        icon.styles.types = icon.iconSet.styles;
      }
    }

    auto colors = [
      "rgba(181,196,237,.8)",
      "rgba(139,163,228,.8)",
      "rgba(98,130,218,.8)",
      "rgba(57,97,208,.8)",
      "rgba(41,76,173,.8)",
      "rgba(31,58,132,.8)",
      "rgba(21,40,91,.8)",
      "rgba(12,22,49,.8)",
      "rgba(12,22,49,.8)",
      "rgba(12,22,49,.8)",
    ];

    auto borderColors = [
      "#3961d0",
      "#3961d0",
      "#3961d0",
      "#3961d0",
      "#3961d0",
      "#3961d0",
      "#3961d0",
      "#3961d0",
      "#3961d0",
      "#3961d0",
    ];

    foreach (i; 0..10) {
      Icon icon;

      icon.name = "group-" ~ i.to!string;
      icon.styles.types.site.shape = "hexagon";
      icon.styles.types.site.backgroundColor = colors[i];
      icon.styles.types.site.borderColor = borderColors[i];
      icons ~= icon;
    }

    foreach (sizeFactor; 1..3) {
      auto sprite = Sprite(icons, sizeFactor, spriteSize, linkId);
      sprite.generate();
      scope(exit) sprite.clean;

      auto picture = Picture();
      picture.name = linkId ~ " sprite @" ~ sizeFactor.to!string;
      picture.picture = PictureFile.fromLocalFile(sprite.path);
      picture.meta.link = Json.emptyObject;
      picture.meta.link["model"] = "Map";
      picture.meta.link["modelId"] = linkId;
      picture.meta.link["type"] = "sprite";
      picture.meta.link["size"] = sizeFactor;
      picture.meta.data = sprite.info;

      auto result = crates.picture.addItem(picture.serializeToJson);

      picture._id = ObjectId(result["_id"]);

      sprites ~= picture;
    }
  }
}

struct MapSprite {
  struct Request {
    ObjectId id;
  }

  enum type = "Map.sprite";
  alias Model = Map;

  OgmCrates crates;
  string tmpFolder;

  static Json[] processList(const ChangeSet changeSet, OgmCrates crates) {
    if(changeSet.model == "Map" && changeSet.type == CrateChangeType.delete_) {
      return [ changeSet.itemId.toJson ];
    }

    if(changeSet.model == "Map") {
      Json[] results;

      auto range = crates.map.get
        .where("_id").equal(changeSet.itemId).and
        .exec;

      return range
        .filter!(a => a["iconSets"].type != Json.Type.object || a["iconSets"]["useCustomList"] == true)
        .map!(a => a["_id"]).array;
    }

    if(changeSet.model == "Icon") {
      ObjectId[] results;

      auto range = crates.icon.get
        .where("_id").equal(changeSet.itemId)
        .and.exec;

      foreach(icon; range) {
        results ~= ObjectId.fromJson(icon["iconSet"]);
      }

      if("iconSet" in changeSet.added) {
        results ~= ObjectId.fromJson(changeSet.added["iconSet"]);
      }

      if("iconSet" in changeSet.removed) {
        results ~= ObjectId.fromJson(changeSet.removed["iconSet"]);
      }

      auto mapRange = crates.map.get
        .where("iconSets.list").anyOf(results)
        .and.exec;

      return mapRange
        .filter!(a => a["iconSets"].type != Json.Type.object || a["iconSets"]["useCustomList"] == true)
        .map!(a => a["_id"]).array;
    }

    if(changeSet.model == "IconSet") {
      Json[] results;

      auto range = crates.map.get
        .where("iconSets.list").arrayContains(changeSet.itemId)
        .and.exec;

      return range
        .filter!(a => a["iconSets"].type != Json.Type.object || a["iconSets"]["useCustomList"] == true)
        .map!(a => a["_id"]).array;
    }

    return [ ];
  }

  void main(Request request) {
    info("Building the sprite for the map: `" ~ request.id.toString ~ "` is disabled. Skipping for now.");
  }

  void main_(Request request) {
    info("Building the sprite for the map: `" ~ request.id.toString ~ "`.");

    auto generator = MapSpriteGenerator(crates, request.id.to!string);
    generator.prepare();

    auto size = crates.map.get
      .where("_id").equal(request.id)
      .and.size;

    if(size == 0) {
      info("Map does not exist. Can not generate a sprite for the map: `" ~ request.id.toString ~ "`.");
      return;
    }

    auto mapJson = crates.map.get
      .where("_id").equal(request.id)
      .and.exec.front;

    auto resolver = toDelegate(&itemResolver);

    Map map;

    try {
      map = LazyMap.fromJson(mapJson, resolver).toType;
    } catch(Exception e) {

      error("Can't generate sprice for invalid map %s: %s", request.id.to!string, e.message);

      debug {
        import std.stdio;
        writeln(e);
      }

      return;
    }

    if(!map.iconSets.useCustomList) {
      info("The map is using the default icons. Can not generate a sprite for the map: `" ~ request.id.toString ~ "`.");
      return;
    }

    auto iconSetIds = map.iconSets.list.map!(a => a._id).array;

    generator.iconSetIds = iconSetIds;
    generator.main();

    map.sprites.small = generator.sprites[0];
    map.sprites.large = generator.sprites[1];

    crates.map.updateItem(LazyMap.fromModel(map).toJson);

    info("Done building the sprite for the map: `" ~ request.id.toString ~ "`.");
  }
}

struct MapSpriteDefault {
  struct Request {
    ObjectId id;
  }

  enum type = "Map.spriteDefault";
  alias Model = Icon;

  OgmCrates crates;
  string tmpFolder;

  static Json[] processList(const ChangeSet changeSet, OgmCrates crates) {
    if(changeSet.model == "Icon") {
      ObjectId[] results;

      auto range = crates.icon.get
        .where("_id").equal(changeSet.itemId)
        .and.exec;

      foreach(icon; range) {
        results ~= ObjectId.fromJson(icon["iconSet"]);
      }

      if("iconSet" in changeSet.added) {
        results ~= ObjectId.fromJson(changeSet.added["iconSet"]);
      }

      if("iconSet" in changeSet.removed) {
        results ~= ObjectId.fromJson(changeSet.removed["iconSet"]);
      }

      auto iconSetRange = crates.iconSet.get
        .where("_id").anyOf(results).and
        .where("visibility.isDefault").equal(true)
        .and.exec;

      return iconSetRange.map!(a => a["_id"]).array;
    }

    if(changeSet.model == "IconSet") {
      Json[] results;

      auto range = crates.iconSet.get
        .where("_id").arrayContains(changeSet.itemId).and
        .where("visibility.isDefault").equal(true)
        .and.exec;

      return range
        .map!(a => a["_id"]).array;
    }

    return [ ];
  }

  void main(Request request) {
    info("Building the default map sprite.");
    auto generator = MapSpriteGenerator(crates, "default");
    generator.prepare();

    auto resolver = toDelegate(&itemResolver);

    auto iconSetIds = crates.iconSet.get
      .where("visibility.isDefault").equal(true).and
      .withProjection(["_id"]).and
      .exec.map!(a => ObjectId.fromJson(a["_id"])).array;

    generator.iconSetIds = iconSetIds;
    generator.main();
  }
}

struct MapSpriteAll {
  struct Request {
    ObjectId id;
  }

  enum type = "Map.spriteAll";
  alias Model = Icon;

  OgmCrates crates;
  string tmpFolder;

  static Json[] processList(const ChangeSet changeSet, OgmCrates crates) {
    if(changeSet.model == "Icon" || changeSet.model == "IconSet") {
      return [ changeSet.itemId.toJson ];
    }

    return [ ];
  }

  void main(Request request) {
    info("Building the sprite with all icons.");
    auto generator = MapSpriteGenerator(crates, "_");
    generator.prepare();

    auto iconSetsJson = crates.iconSet.get
        .where("visibility.isDefault").equal(true)
        .and.exec;

    generator.iconSetIds = iconSetsJson.map!(a => ObjectId.fromJson(a["_id"])).array;

    generator.main();
  }
}