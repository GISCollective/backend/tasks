/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.map.MapMask;

import geo.json;
import geo.geometries;

import vibe.data.json;
import vibe.core.log;

import std.datetime;
import std.algorithm;
import std.array;
import std.conv;
import std.math;
import std.exception;

import crate.base;
import vibe.service.stats;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import ogm.crates.all;
import ogm.models.meta;
import ogm.meta;

version(unittest) {
  import fluent.asserts;
  import ogm.test.fixtures;
}

struct MapMask {
  struct Request {
    ObjectId id;
  }

  enum type = "Map.Mask";
  alias Model = Map;

  OgmCrates crates;
  IBroadcast broadcast;

  static Json[] processList(const ChangeSet changeSet) {
    if(changeSet.model != Model.stringof) {
      return [];
    }

    if(changeSet.type == CrateChangeType.delete_) {
      return [];
    }

    return [ changeSet.itemId.toJson ];
  }

  void main(Request request) {
    string logPrefix = "[" ~ type ~ "][" ~ request.id.toString ~ "]";

    auto featureRange = crates.feature.get
      .where("maps").arrayContains(request.id).and
      .withProjection(["_id"])
      .exec;

    foreach(feature; featureRange) {
      auto message = Json.emptyObject;
      message["id"] = feature["_id"];

      broadcast.push("Feature.Mask", message);
    }
  }
}
