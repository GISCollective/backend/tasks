/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.map.info;

import vibe.data.json;
import vibe.core.log;

import std.datetime;
import std.algorithm;
import std.array;

import crate.base;
import vibe.service.stats;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import ogm.crates.all;
import ogm.models.meta;
import ogm.meta;

version(unittest) {
  import fluent.asserts;
  import ogm.test.fixtures;
}

struct MapInfo {
  enum type = "Map.info";
  alias Model = Map;

  string id;
  OgmCrates crates;
  IBroadcast broadcast;

  static Json[] processList(const ChangeSet changeSet, OgmCrates crates) {
    if(changeSet.model == "Feature") {
      Json[] result;

      if(changeSet.anyIsChanged("visibility", "contributors", "info.author", "info.originalAuthor")) {
        result = crates.feature.getItem(changeSet.itemId.toString).exec.front["maps"].deserializeJson!(Json[]);
      }

      foreach(key, val; changeSet.added) {
        if(key.startsWith("maps[")) {
          result ~= val;
        }
      }

      foreach(key, val; changeSet.removed) {
        if(key.startsWith("maps[")) {
          result ~= val;
        }
      }

      return result.uniq.array;
    }

    if(changeSet.model != Model.stringof) {
      return [];
    }

    if(changeSet.model == Model.stringof && changeSet.type == CrateChangeType.delete_) {
      return [];
    }

    return [ changeSet.itemId.toJson ];
  }

  void log(T...)(string message, T params) {
    logInfo("[%s %s] " ~ message, type, id, params);
  }

  void start() {
    scope metaQuery = new MetaQuery("Map", type, crates);
    auto meta = metaQuery.get(id);

    auto map = crates.map.getItem(id).exec.front;
    long changeIndex;

    if("info" in map && "changeIndex" in map["info"]) {
      changeIndex = map["info"]["changeIndex"].to!long;
    }

    auto objectId = ObjectId.fromString(id);

    /// total features, public features, total contributors
    meta.changeIndex = changeIndex;
    meta.data = Json.emptyObject;
    meta.data["totalFeatures"] = crates.feature.get
          .where("maps").arrayContains(objectId).and
          .size;

    meta.data["publicFeatures"] = crates.feature.get
          .where("maps").arrayContains(objectId).and
          .where("visibility").equal(1).and
          .size;

    auto contributors = crates.feature.get
          .where("maps").arrayContains(objectId).and
          .exec.map!(a => getContributors(a))
          .joiner.array.sort.uniq.array;

    log("contributors: %s", contributors);
    meta.data["totalContributors"] = contributors.count;

    log("result: %s", meta.data.to!string);

    crates.meta.updateItem(meta.serializeToJson);
  }
}

Json[] getContributors(Json feature) {
  string[] result;

  if(feature["contributors"].type == Json.Type.array) {
    result = feature["contributors"].deserializeJson!(string[]);
  }

  if("info" in feature && "author" in feature["info"]) {
    result ~= feature["info"]["author"].to!string;
  }

  if("info" in feature && "originalAuthor" in feature["info"]) {
    result ~= feature["info"]["originalAuthor"].to!string;
  }

  return result.filter!(a => a != "").map!(a => Json(a)).array;
}
