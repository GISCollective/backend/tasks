/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.map.MapIcons;

import vibe.data.json;
import vibe.core.log;

import std.datetime;
import std.algorithm;
import std.array;

import crate.base;
import vibe.service.stats;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import ogm.crates.all;
import ogm.models.meta;
import ogm.meta;

version(unittest) {
  import fluent.asserts;
  import ogm.test.fixtures;
}

struct MapIcons {
  struct MapIconsMessage {
    string id;
  }

  enum type = "Map.icons";
  alias Model = Map;

  OgmCrates crates;
  IBroadcast broadcast;

  static Json[] processList(const ChangeSet changeSet, OgmCrates crates) {
    if(changeSet.model == "Feature") {
      Json[] result;

      foreach(key, val; changeSet.added) {
        if(key.startsWith("maps[") || key.startsWith("icons")) {
          result = crates.feature.getItem(changeSet.itemId.toString).exec.front["maps"].deserializeJson!(Json[]);
        }
      }

      foreach(key, val; changeSet.removed) {
        if(key.startsWith("maps[") || key.startsWith("icons")) {
          result = crates.feature.getItem(changeSet.itemId.toString).exec.front["maps"].deserializeJson!(Json[]);
        }
      }

      foreach(key, val; changeSet.added) {
        if(key.startsWith("maps[")) {
          result ~= val;
        }
      }

      foreach(key, val; changeSet.removed) {
        if(key.startsWith("maps[")) {
          result ~= val;
        }
      }

      return result.uniq.array;
    }

    if(changeSet.model != Model.stringof) {
      return [];
    }

    if(changeSet.model == Model.stringof && changeSet.type == CrateChangeType.delete_) {
      return [];
    }

    return [ changeSet.itemId.toJson ];
  }

  void log(T...)(string message, T params) {
    logInfo("[%s %s] " ~ message, type, id, params);
  }

  void main(MapIconsMessage mapIconsMessage) {
    const id = mapIconsMessage.id;

    scope metaQuery = new MetaQuery("Map", type, crates);
    auto meta = metaQuery.get(id);

    auto map = crates.map.getItem(id).exec.front;
    long changeIndex;

    if("info" in map && "changeIndex" in map["info"]) {
      meta.changeIndex = map["info"]["changeIndex"].to!long;
    }
    meta.data = Json.emptyObject;

    auto objectId = ObjectId.fromString(id);
    auto features = crates.feature.get
          .where("maps").arrayContains(objectId).and.exec;


    string[] icons;
    foreach (feature; features) {
      if(feature["icons"].type == Json.Type.array) {
        icons ~= feature["icons"].deserializeJson!(string[]);
        icons = icons.sort.uniq.array;
      }
    }

    meta.data["icons"] = icons.serializeToJson;

    crates.meta.updateItem(meta.serializeToJson);
  }
}
