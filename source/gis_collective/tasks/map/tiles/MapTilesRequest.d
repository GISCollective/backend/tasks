/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.map.tiles.MapTilesRequest;

import gis_collective.tasks.GisTask;
import ogm.crates.all;

import crate.base;

import geo.xyz;
import geo.json;
import geo.algorithm;

import std.algorithm;
import std.array;
import std.datetime;
import std.conv;

import vibe.core.log;
import vibe.data.json;
import vibe.service.stats;

import ogm.models.meta;

///
struct TileProcessRequest {
  ///
  string mapId;

  ///
  size_t changeIndex;

  /// The z/x/y path to the tile
  ZXY path;
}

/// Increase the change index for all maps attacheet to a site
class MapTilesRequest : GisTask {

  ///
  this(OgmCrates crates, IBroadcast broadcast) {
    super(crates, broadcast, "MapTilesRequest");
  }

  /// Returns the list of maps that should generate the tiles
  @onChange
  string[] processList(const ChangeSet changeSet) {
    if(changeSet.model != "Map") {
      return [];
    }

    return [ changeSet.itemId.toString ];
  }

  /// Trigger the task that removes old tiles and updates the metadata
  void triggerCheck(string mapId) {
    auto message = Json.emptyObject;
    message["value"] = mapId;

    this.broadcast("MapTilesCheck", message);
  }

  void send(TileProcessRequest request) {
    auto message = Json.emptyObject;
    message["value"] = request.serializeToJson;

    this.broadcast("MapTiles", message);
  }

  string[] getTileHashes(Point point) {
    auto currentTiles = strTilesWithEdgesCheck(point.coordinates, 0, 17);

    debug logDebug("`%s` is on `%s`", point.coordinates.to!string, currentTiles);

    return currentTiles;
  }

  string[] getTileHashes(LineString line) {
    auto currentTiles = strTiles(line.coordinates, 0, 17);
    debug logDebug("`%s` is on `%s`", line.coordinates.to!string, currentTiles);

    return currentTiles;
  }

  string[] getTileHashes(MultiLineString lines) {
    string[] currentTiles;

    foreach(coordinates; lines.coordinates) {
      currentTiles ~= strTiles(coordinates, 0, 17);
      debug logDebug("`%s` is on `%s`", coordinates.to!string, currentTiles);
    }

    return currentTiles;
  }

  string[] getTileHashes(Polygon polygon) {
    string[] currentTiles;

    currentTiles ~= strTiles(polygon.coordinates, 0, 17);
    debug logDebug("`%s` is on `%s`", polygon.coordinates.to!string, currentTiles);

    return currentTiles;
  }

  string[] getTileHashes(MultiPolygon polygons) {
    string[] currentTiles;

    foreach (coordinates; polygons.coordinates) {
      currentTiles ~= strTiles(coordinates, 0, 17);
      debug logDebug("`%s` is on `%s`", coordinates.to!string, currentTiles);
    }

    return currentTiles.sort.uniq.array;
  }

  string[] getTileHashes(Json feature) {
    auto geometry = GeoJsonGeometry.fromJson(feature["position"]);

    if(geometry.type == "Point") {
      return getTileHashes(geometry.to!Point);
    }

    if(geometry.type == "LineString") {
      return getTileHashes(geometry.to!LineString);
    }

    if(geometry.type == "MultiLineString") {
      return getTileHashes(geometry.to!MultiLineString);
    }

    if(geometry.type == "Polygon") {
      return getTileHashes(geometry.to!Polygon);
    }

    if(geometry.type == "MultiPolygon") {
      return getTileHashes(geometry.to!MultiPolygon);
    }

    return [];
  }

  /// Create the sites layer for the map tiles
  void start(string mapId) {
    string[] tileHashes;

    auto map = this.get!Map(mapId);

    auto r = crates.feature.get.where("maps").anyOf([ ObjectId.fromString(mapId) ]).and.exec;

    foreach(feature; r) {
      tileHashes ~= getTileHashes(feature);
      tileHashes = tileHashes.sort.uniq.array;
    }

    log(Json(mapId), "Updating `" ~ tileHashes.length.to!string ~ "` tiles: " ~ tileHashes.to!string);

    auto now = Clock.currTime;
    now.fracSecs = 0.msecs;

    Meta meta;
    meta.type = "MapTiles";
    meta.model = "Map";
    meta.itemId = map._id.toString;
    meta.changeIndex = map.info.changeIndex;
    meta.data = Json.emptyObject;
    meta.data["layer"] = "sites";
    meta.data["createdOn"] = now.toISOExtString;
    meta.data["processedTileCount"] = 0;
    meta.data["tiles"] = tileHashes.serializeToJson;

    bool exists;
    foreach(item; this.getMetaList("MapTiles", mapId)) {
      if(item.changeIndex == map.info.changeIndex) {
        meta._id = item._id;
        exists = true;
      }
    }

    if(exists) {
      Stats.instance.inc("MapTilesRequest", [ "action": "UpdateMetadata", "map": mapId ]);
      this.update(meta);
    } else {
      Stats.instance.inc("MapTilesRequest", [ "action": "CreateMetadata", "map": mapId ]);
      this.add(meta);
    }

    string[] missing;

    foreach(tileHash; tileHashes) {
      if(!this.tileExists(tileHash, map.info.changeIndex, mapId)) {
        Stats.instance.inc("MapTilesRequest", [ "action": "GenerateTile", "map": mapId ]);
        auto request = TileProcessRequest(mapId, map.info.changeIndex, ZXY.fromDotNotation(tileHash));
        send(request);
      } else {
        Stats.instance.inc("MapTilesRequest", [ "action": "IgnoreExistingTile", "map": mapId ]);
      }
    }

    log(Json(mapId), "Missing tiles: " ~ missing.to!string);

    this.triggerCheck(mapId);
  }

  bool tileExists(string hash, size_t changeIndex, string mapId) {
    auto path = ZXY.fromDotNotation(hash);

    return this.crates.tileLayer.get
      .where("mapId").equal(mapId).and
      .where("path.z").equal(path.z).and
      .where("path.x").equal(path.x).and
      .where("path.y").equal(path.y).and
      .where("changeIndex").equal(changeIndex).and
        .size > 0;
  }
}
