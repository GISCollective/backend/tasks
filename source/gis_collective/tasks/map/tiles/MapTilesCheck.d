/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.map.tiles.MapTilesCheck;

import gis_collective.tasks.GisTask;
import ogm.crates.all;

import geo.xyz;
import geo.json;
import geo.algorithm;

import std.algorithm;
import std.array;
import std.datetime;
import std.conv;

import vibe.data.json;
import vibe.service.stats;

import ogm.models.meta;


/// Removes old tiles and updates the MapTiles metadata
class MapTilesCheck : GisTask {

  ///
  this(OgmCrates crates, IBroadcast broadcast) {
    super(crates, broadcast, "MapTilesCheck");
  }

  void triggerMapTilesRequest(string mapId) {
    auto message = Json.emptyObject;
    message["value"] = mapId;

    Stats.instance.inc("MapTilesCheck", [ "action": "MapTilesRequest", "map": mapId ]);
    broadcast("MapTilesRequest", message);
  }

  ///
  void removeDuplicates(ZXY path, Map map) {
    auto r = crates.tileLayer.get
      .where("mapId").equal(map._id.toString).and
      .where("layerName").equal("sites").and
      .where("path.z").equal(path.z).and
      .where("path.x").equal(path.x).and
      .where("path.y").equal(path.y).and
      .exec
      .map!(a => a.deserializeJson!TileLayer);

    bool[string] found;
    foreach(tile; r) {
      string type = tile.layerName;

      if(tile.changeIndex != map.info.changeIndex || type in found) {
        try {
          log(map._id.toJson, "Removing duplicate for `" ~ tile.layerName ~ "` at `" ~ path.to!string ~
            "`. tile index: " ~ tile.changeIndex.to!string ~ " map index: " ~ map.info.changeIndex.to!string);
          Stats.instance.inc("MapTilesCheck", [ "action": "RemoveDuplicate", "map": map._id.toString ]);
          crates.tileLayer.deleteItem(tile._id.toString);
        } catch(Exception) {}
      } else {
        found[type] = true;
      }
    }
  }

  /// Validates the sites layer of map
  void removeMeta(string mapId) {
    auto metaList = this.getMetaList("MapTiles", mapId);

    foreach(meta; metaList) {
      this.deleteItem!Meta(meta);
    }

    auto r = crates.tileLayer.get.where("mapId").equal(mapId).and.exec;

    foreach (item; r) {
      crates.tileLayer.deleteItem(item["_id"].to!string);
    }
  }

  /// Validates the sites layer of map
  void start(string mapId) {
    if(!this.exists!Map(mapId)) {
      this.removeMeta(mapId);
      return;
    }

    auto map = this.get!Map(mapId);

    auto metaList = this.getMetaList("MapTiles", mapId);

    bool lastMetaExist;
    Meta lastMeta;

    foreach(meta; metaList) {
      if(meta.changeIndex != map.info.changeIndex) {
        crates.meta.deleteItem(meta._id.toString);
      } else {
        lastMetaExist = true;
        lastMeta = meta;
      }
    }

    if(!lastMetaExist) {
      Stats.instance.inc("MapTilesCheck", [ "action": "MissingMeta", "map": mapId ]);

      triggerMapTilesRequest(mapId);
      return;
    }

    auto tilePaths = lastMeta.data["tiles"].deserializeJson!(string[]).array;

    auto r = crates.tileLayer.get
      .where("mapId").equal(mapId).and
      .where("layerName").equal("sites").and
      .exec;

    size_t processedTileCount;

    ZXY[] processedTiles;

    foreach(jsonTile; r) {
      TileLayer tile;

      try {
        tile = jsonTile.deserializeJson!TileLayer;
      } catch(Exception) {
        log(Json(mapId), "Failed deserializing data.");
        Stats.instance.inc("MapTilesCheck", [ "action": "DeserializationError", "map": mapId ]);
        try crates.tileLayer.deleteItem(jsonTile["_id"].to!string); catch(Exception e) { }
        continue;
      }

      if(!tilePaths.canFind(tile.path.toDotNotation)) {
        log(Json(mapId), "Found invalid path.");
        Stats.instance.inc("MapTilesCheck", [ "action": "InvalidPath", "map": mapId ]);
        try crates.tileLayer.deleteItem(tile._id.toString); catch(Exception e) { }
        continue;
      }

      if(tile.changeIndex == map.info.changeIndex) {
        Stats.instance.inc("MapTilesCheck", [ "action": "ProcessedTileCount", "map": mapId ]);
        processedTileCount++;
        processedTiles ~= tile.path;
      }
    }

    auto processedTilesHash = processedTiles.map!(a => a.toDotNotation).array;
    auto missing = tilePaths.filter!(a => !processedTilesHash.canFind(a)).array;

    log(Json(mapId), "Found `" ~ processedTileCount.to!string ~ "/" ~ tilePaths.length.to!string ~ "` processed tiles.");
    log(Json(mapId), "Missing tiles: `" ~ missing.to!string);

    foreach(path; processedTiles) {
      this.removeDuplicates(path, map);
    }

    lastMeta.data["processedTileCount"] = processedTileCount;
    lastMeta.data["waitingCheck"] = false;
    crates.meta.updateItem(lastMeta.serializeToJson);

    Stats.instance.inc("MapTilesCheck", [ "action": "UpdatedMeta", "map": mapId ]);

    if("createdOn" in lastMeta.data) {
      auto createdOn = SysTime.fromISOExtString(lastMeta.data["createdOn"].to!string);

      if(Clock.currTime - createdOn > 2.hours && processedTileCount != tilePaths.length) {
        log(Json(mapId), "The meta was created `" ~ (Clock.currTime - createdOn).toString ~ "` ago and not all tiles are generated. Triger a new request.");
        triggerMapTilesRequest(mapId);
      }
    }
  }
}