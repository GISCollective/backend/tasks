/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.map.tiles.MapTiles;

import gis_collective.tasks.GisTask;
import ogm.crates.all;

import geo.xyz;
import geo.json;
import geo.algorithm;
import geo.epsg;
import geo.conv;

import std.algorithm;
import std.array;
import std.datetime;
import std.conv;

import vibe.data.json;
import vibe.service.stats;

import ogm.models.meta;

import gis_collective.tasks.map.tiles.MapTilesRequest;
import gis_collective.tasks.map.tiles.FeatureContainer;

/// Increase the change index for all maps attached to a site
class MapTiles : GisTask {

  ///
  this(OgmCrates crates, IBroadcast broadcast) {
    super(crates, broadcast, "MapTiles");
  }

  /// Trigger the task that removes old tiles and updates the metadata
  void triggerCheck(string mapId, size_t changeIndex) {
    auto metaList = this.getMetaList("MapTiles", mapId);

    if(metaList.length == 0) {
      Stats.instance.inc("MapTiles", [ "action": "MissingMeta", "map": mapId ]);
      return;
    }

    auto message = Json.emptyObject;
    message["value"] = mapId;
    message["uid"] = mapId;

    Stats.instance.inc("MapTiles", [ "action": "MapTilesCheck", "map": mapId ]);
    this.broadcast("MapTilesCheck", message);
  }

  /// ditto
  void triggerCheck(Map map) {
    triggerCheck(map._id.toString, map.info.changeIndex);
  }

  /// Create the sites layer for the map tiles
  void start(TileProcessRequest request) {
    auto map = this.get!Map(request.mapId);

    if(map.info.changeIndex != request.changeIndex) {
      Stats.instance.inc("MapTiles", [ "action": "ChangeIndexMissmatch", "map": request.mapId ]);
      return;
    }

    auto exists = crates.tileLayer.get
      .where("mapId").equal(request.mapId).and
      .where("layerName").equal("sites").and
      .where("path.z").equal(request.path.z).and
      .where("path.x").equal(request.path.x).and
      .where("path.y").equal(request.path.y).and
      .where("changeIndex").equal(request.changeIndex).and
      .size;

    if(exists > 0) {
      Stats.instance.inc("MapTiles", [ "action": "AlreadyExists", "map": request.mapId ]);
      return;
    }

    auto container = FeatureContainer(map, request.path);

    auto points = request.path.getTilePolygon(-0.2);

    PositionT!double[] tmp =
      points
      .map!(a => PositionT!double.fromArray([a.longitude, a.latitude]))
      .array;

    tmp = rewind(tmp);

    double[][] polygon = tmp.map!(a => a.toList).array;

    auto selector = crates.feature.get
        .where("maps").anyOf([ map._id ]).and;

    if(request.path.z > 0) {
      selector = selector.where("position").intersectsPolygon([ polygon ]).and;
      log(request.serializeToJson, "Adding features inside the tile polygon: `" ~ polygon.to!string ~ "`");
    }

    try {
      auto range = selector.exec;

      foreach(feature; range) {
        PropertyTable properties;
        properties["_id"] = feature["_id"].to!string;
        properties["name"] = feature["name"].to!string;
        properties["visibility"] = feature["visibility"].to!string;
        properties["icons"] = feature["icons"].byValue.map!(a => a.to!string).join(",");

        try {
          container.add(GeoJsonGeometry.fromJson(feature["position"]), properties);
          Stats.instance.inc("MapTiles", [ "action": "AddFeature", "map": request.mapId ]);
        } catch(Exception e) {
          log(request.serializeToJson, "There was an error adding geometry. " ~ e.message.to!string);
          Stats.instance.inc("MapTiles", [ "action": "AddError", "map": request.mapId ]);
        }
      }
    } catch(Exception e) {
      log(request.serializeToJson, "There was an error generating the tile. " ~ e.message.to!string);
      Stats.instance.inc("MapTiles", [ "action": "AddError", "map": request.mapId ]);
    }

    Stats.instance.inc("MapTiles", [ "action": "New", "map": request.mapId ]);

    this.add!TileLayer(container.tileLayer);

    triggerCheck(map);
  }
}
