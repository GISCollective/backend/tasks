/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.map.tiles.FeatureContainer;

import ogm.models.map;
import ogm.models.tileLayer;

import std.conv;
import std.algorithm;

import vibe.data.json;

import geo.conv;
import geo.xyz;
import geo.epsg;
import geo.json;
import geo.algorithm;

version(unittest) {
  import fluent.asserts;
}

struct FeatureContainer {
  TileLayer tileLayer;
  ZXY path;

  enum acceptedLineArea = 0.00005;
  enum acceptedPolygonArea = 0.001;
  enum polygonLineArea = 0.5;

  private {
    enum size = 4096;
    enum border = 5;

    BBox!double bbox;
    BBox!int bboxTile;

    double bboxArea;
  }

  this(Map map, ZXY path) {
    this.bbox = path.toBBox(-0.2);
    this.bboxTile = BBox!int(-1, 4097, -1, 4097);
    bboxArea = ZXY(path.z, 0, 0).toBBox.area;

    tileLayer.changeIndex = map.info.changeIndex;
    tileLayer.mapId = map._id.toString;
    tileLayer.teamId = map.visibility.team._id.toString;
    tileLayer.isReady = true;
    tileLayer.isPublic = map.visibility.isPublic;
    tileLayer.extent = 4096;
    tileLayer.path = path;
    tileLayer.layerName = "sites";
    tileLayer.geometries = Json.emptyArray;

    this.path = path;
  }

  Json createGeometry(T)(T points, string type) {
    auto geometry = Json.emptyObject;
    geometry["points"] = points.serializeToJson;
    geometry["type"] = type;

    return geometry;
  }

  void add(Point point, PropertyTable properties) {
    if(!bbox.contains(point.coordinates)) {
      return;
    }

    if("type" !in properties) {
      properties["type"] = "point";
    }

    auto tilePoint = point.coordinates.toVectorTileCoordinates(path.x, path.y, path.z, tileLayer.extent);

    tileLayer.geometries ~= createGeometry([ tilePoint[0], tilePoint[1] ], "point");
    tileLayer.properties ~= properties;
  }

  void add(MultiPoint points, PropertyTable properties) {
    foreach(coordinates; points.coordinates) {
      add(Point(coordinates), properties);
    }
  }

  void add(LineString line, PropertyTable properties) {
    auto lineBox = BBox!double(line.coordinates);
    auto area = lineBox.area / bboxArea;

    version(unittest) {} else debug {
      properties["relativeArea"] = area.to!string;
      properties["bboxArea"] = bboxArea.to!string;
      properties["lineArea"] = lineBox.area.to!string;
    }

    if("type" !in properties) {
      properties["type"] = "line";
    }

    foreach(coordinates; line.coordinates.lineIntersection(bbox)) {
      auto tileLine = coordinates
        .toVectorTileCoordinates(path.x, path.y, path.z, tileLayer.extent, 3);

      if(area > acceptedLineArea) {
        tileLayer.geometries ~= createGeometry(tileLine, "line");
        tileLayer.properties ~= properties;
      }
    }

    addLineMarkers(line.coordinates.toList, properties);
  }

  void add(MultiLineString line, PropertyTable properties) {
    foreach(lineCoordinates; line.coordinates) {
      add(LineString(lineCoordinates), properties);
    }
  }

  void add(Polygon polygon, PropertyTable properties) {
    if(polygon.coordinates.length == 0 || polygon.coordinates[0].length == 0) {
      return;
    }

    if("type" !in properties) {
      properties["type"] = "polygon";
    }

    const polygonBox = BBox!double(polygon.coordinates[0]);
    const area = polygonBox.area / bboxArea;

    version(unittest) {} else debug {
      properties["relativeArea"] = area.to!string;
      properties["bboxArea"] = bboxArea.to!string;
      properties["polygonArea"] = polygonBox.area.to!string;
    }

    if(area < acceptedPolygonArea) {
      PropertyTable pointProperties;
      foreach(key, val; properties) {
        pointProperties[key] = val;
      }

      pointProperties["type"] = "polygonMarker";
      add(Point(PositionT!double.fromArray(polygonBox.center)), pointProperties);
    }

    if(area >= acceptedPolygonArea && area < polygonLineArea) {
      const coordinates = polygon.coordinates[0].intersection(bbox);
      int[2][][] tilePolygon = coordinates
        .toVectorTileCoordinates(path.x, path.y, path.z, tileLayer.extent, 3)
        .to!(int[2][][]);

      tileLayer.geometries ~= createGeometry(tilePolygon, "polygon");
      tileLayer.properties ~= properties;
    }

    if(area >= polygonLineArea) {
      PropertyTable lineProperties;
      foreach(key, val; properties) {
        lineProperties[key] = val;
      }

      lineProperties["type"] = "polygonBorder";
      add(LineString(polygon.coordinates[0]), lineProperties);
    }
  }

  void add(MultiPolygon polygon, PropertyTable properties) {
    foreach(coordinates; polygon.coordinates) {
      add(Polygon(coordinates), properties);
    }
  }

  void add(GeoJsonGeometry geometry, PropertyTable properties) {
    if(geometry.type == "Point") {
      this.add(geometry.to!Point, properties);
    }

    if(geometry.type == "MultiPoint") {
      this.add(geometry.to!MultiPoint, properties);
    }

    if(geometry.type == "LineString") {
      this.add(geometry.to!LineString, properties);
    }

    if(geometry.type == "MultiLineString") {
      this.add(geometry.to!MultiLineString, properties);
    }

    if(geometry.type == "Polygon") {
      this.add(geometry.to!Polygon, properties);
    }

    if(geometry.type == "MultiPolygon") {
      this.add(geometry.to!MultiPolygon, properties);
    }
  }

  void addLineMarkers(double[][] points, PropertyTable lineProperties) {
    auto lineLen = points.distance;
    double interval = bbox.dx;

    if(lineLen < bbox.dx) {
      interval = lineLen / 2;
    } else if(lineLen < bbox.dx * 2) {
      interval = lineLen / 3;
    }

    PropertyTable properties;
    foreach(key, val; lineProperties) {
      properties[key] = val;
    }

    if("type" !in properties) {
      properties["type"] = "line";
    }

    properties["type"] ~= "Marker";

    foreach(coordinates; points.lineSplit(interval)) {
      add(Point(PositionT!double.fromArray(coordinates)), properties);
    }
  }
}

/// Adding a line should be converted to a point on zoomlevels where the line is less than 40 points
unittest {
  auto line = LineString([
      PositionT!double.fromArray([ 13.450833004640478, 52.41379927147025 ]),
      PositionT!double.fromArray([ 13.451198285661233, 52.41313880961721 ])
  ]);

  Map map;
  auto container = FeatureContainer(map, ZXY(0,0,0));
  PropertyTable properties;

  container.add(line, properties);
  container.tileLayer.geometries.should.equal(`[
    { "type":"point","points": [ 2201, 1345 ] }
  ]`.parseJsonString);
}

/// Adding a long line should add point markers over the line
unittest {
  auto line = LineString([
    PositionT!double.fromArray([ 13.41328688344344, 52.51689312637657 ]),
    PositionT!double.fromArray([ 13.52172241827204, 52.48608886620525 ]),
    PositionT!double.fromArray([ 13.57051840894491, 52.45746558075007 ]),
    PositionT!double.fromArray([ 13.71148460422209, 52.44535012458942 ])
  ]);

  Map map;
  auto container = FeatureContainer(map, ZXY(10,550,335));
  PropertyTable properties;

  container.add(line, properties);
  container.tileLayer.geometries.should.equal(`[
    {"type":"line","points":[[628,3443],[1891,4032],[2459,4580],[4102,4811]]},
    {"type":"point","points":[2320,4445]},
    {"type":"point","points":[4102,4811]}
  ]`.parseJsonString);
}

/// Adding a long line that crosses the tile corner
unittest {
  auto line = LineString([
    PositionT!double.fromArray([ 13.41328688344344, 52.51689312637657 ]),
    PositionT!double.fromArray([ 13.52172241827204, 52.48608886620525 ]),
    PositionT!double.fromArray([ 13.57051840894491, 52.45746558075007 ]),
    PositionT!double.fromArray([ 13.71148460422209, 52.44535012458942 ])
  ]);

  Map map;
  auto container = FeatureContainer(map, ZXY(12,2201,1344));
  PropertyTable properties;

  container.add(line, properties);
  container.tileLayer.geometries.should.equal(`[
    {"type":"line","points":[[4915,420],[3469,-254],[4915,1138]]},
    {"type":"point","points":[3884,146]}
  ]`.parseJsonString);
}

/// Adding a long line that crosses the tile
unittest {
  auto line = LineString([
    PositionT!double.fromArray([ 13.41328688344344, 52.51689312637657 ]),
    PositionT!double.fromArray([ 13.52172241827204, 52.48608886620525 ]),
    PositionT!double.fromArray([ 13.57051840894491, 52.45746558075007 ]),
    PositionT!double.fromArray([ 13.71148460422209, 52.44535012458942 ])
  ]);

  Map map;
  auto container = FeatureContainer(map, ZXY(12,2201,1343));
  PropertyTable properties;

  container.add(line, properties);
  container.tileLayer.geometries.should.equal(`[
    {"type":"line","points":[[-820,1841],[3469,3842],[-820,-292]]},
    {"type":"point","points":[3884,4242]}]`.parseJsonString);
}
