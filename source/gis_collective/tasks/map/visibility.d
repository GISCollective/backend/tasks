/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.map.visibility;

import vibe.data.json;
import vibe.core.log;

import std.datetime;
import std.algorithm;
import std.array;

import crate.base;
import vibe.service.stats;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import ogm.crates.all;
import ogm.models.meta;
import ogm.features.visibility;
import ogm.meta;

version(unittest) {
  import fluent.asserts;
  import ogm.test.fixtures;
}

struct MapVisibility {
  struct Message {
    ObjectId id;
  }

  enum type = "Map.visibility";
  alias Model = Map;

  OgmCrates crates;
  IBroadcast broadcast;

  static Json[] processList(const ChangeSet changeSet, OgmCrates crates) {
    if(changeSet.model != Model.stringof) {
      return [];
    }

    return [ changeSet.itemId.toJson ];
  }

  void log(T...)(string message, T params) {
    logInfo("[%s %s] " ~ message, type, id, params);
  }

  void main(Message message) {
    auto features = crates.feature.get
      .where("maps").arrayContains(message.id)
      .and.exec;

    foreach(Json feature; features) {
      feature["computedVisibility"] = crates.getComputedVisibility(feature);
      crates.feature.updateItem(feature);
    }
  }
}
