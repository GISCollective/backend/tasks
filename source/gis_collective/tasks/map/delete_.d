/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.map.delete_;

import vibe.data.json;
import vibe.core.log;

import std.datetime;
import std.algorithm;
import std.array;
import std.conv;

import crate.base;
import vibe.service.stats;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import ogm.crates.all;
import ogm.models.meta;
import ogm.meta;

version(unittest) {
  import fluent.asserts;
  import ogm.test.fixtures;
}

struct MapDelete {
  struct Request {
    ObjectId id;
  }

  enum type = "Map.delete";
  alias Model = Map;

  OgmCrates crates;
  IBroadcast broadcast;

  static Json[] processList(const ChangeSet changeSet) {
    if(changeSet.model != Model.stringof) {
      return [];
    }

    if(changeSet.type != CrateChangeType.delete_) {
      return [];
    }

    return [ changeSet.itemId.toJson ];
  }

  void main(Request request) {
    auto size = crates.feature.get
      .where("maps").arrayContains(request.id).and
      .size;

    logInfo("Removing `" ~ size.to!string ~ "` features from map `" ~ request.id.toString ~ "`.");

    auto range = crates.feature.get
      .where("maps").arrayContains(request.id).and.exec;

    auto mapId = request.id.toString;
    Json deleteFeatureMessage = Json.emptyObject;

    foreach(feature; range) {
      if(feature["maps"].length == 1) {
        crates.feature.deleteItem(feature["_id"].to!string);

        deleteFeatureMessage["id"] = feature["_id"];
        broadcast.push("feature.delete", deleteFeatureMessage);

        continue;
      }

      string[] maps = feature["maps"].deserializeJson!(string[]);
      feature["maps"] = maps.filter!(a => a != mapId).array.serializeToJson;

      crates.feature.updateItem(feature);
    }

    size = crates.meta.get
      .where("model").equal(Model.stringof).and
      .where("itemId").equal(mapId).and
      .size;

    logInfo("Removing `" ~ size.to!string ~ "` metadata related to map `" ~ request.id.toString ~ "`.");

    range = crates.meta.get
      .where("model").equal(Model.stringof).and
      .where("itemId").equal(mapId).and
      .exec;

    foreach(meta; range) {
      crates.meta.deleteItem(meta["_id"].to!string);
    }
  }
}
