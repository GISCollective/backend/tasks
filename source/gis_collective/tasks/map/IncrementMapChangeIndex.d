/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.map.IncrementMapChangeIndex;

import gis_collective.tasks.GisTask;
import ogm.crates.all;

import vibe.core.log;
import vibe.data.json;

import std.algorithm;
import std.conv;
import std.array;
import std.string;
import crate.base;
import crate.lazydata.base;

version(unittest) {
  import ogm.test.fixtures;
  import fluent.asserts;
}

/// Increase the change index for all maps attacheet to a site
class IncrementMapChangeIndex : GisTask {

  ///
  this(OgmCrates crates, IBroadcast broadcast = null) {
    super(crates, broadcast, typeof(this).stringof);
  }

  /// Returns the list of items that has to be processed
  @onChange
  string[] processList(const ChangeSet changeSet) {
    if(changeSet.model != "Feature") {
      return [];
    }

    auto range = crates.feature.get.where("_id").equal(changeSet.itemId).and.exec;

    if(range.empty) {
      return changeSet.removed.byKeyValue
        .filter!(a => a.key.indexOf("maps[") == 0)
        .map!(a => a.value.clone.to!string)
        .array;
    }

    return range.front["maps"].byValue.map!(a => a.to!string).array;
  }

  ///
  void start(string id) {
    auto map = this.get!Map(id);

    map.info.changeIndex++;

    this.update(map);
  }
}
