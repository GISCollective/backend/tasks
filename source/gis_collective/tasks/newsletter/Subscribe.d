/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.newsletter.Subscribe;

import vibe.data.json;
import vibe.core.log;

import crate.base;
import crate.mime;
import crate.error;
import std.conv;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import ogm.crates.all;
import ogm.models.meta;
import ogm.meta;
import ogm.process;
import gis_collective.tasks.color;
import gis_collective.tasks.newsletter.emailMessage;

version(unittest) {
  import fluent.asserts;
  import ogm.test.fixtures;
}

struct NewsletterSubscribe {
  struct Request {
    ObjectId id;
    string email;
  }

  enum type = "newsletter.subscribe";

  OgmCrates crates;
  IBroadcast broadcast;

  static Json[] processList(const ChangeSet changeSet, OgmCrates crates) {
    return [ ];
  }

  void main(Request request) {
    auto strId = request.id.to!string;

    auto newsletter = crates.newsletter.getItem(strId);
    auto welcomeMessageRange = getWelcomeMessageRange(crates, strId);

    if(welcomeMessageRange.empty) {
      info("The newsletter " ~ strId ~ " does not have a welcome message.");
      return;
    }

    EmailMessage message;
    message.article = welcomeMessageRange.front;
    message.recipients ~= request.email;
    message.newsletterId = strId;
    message.updateLinks(crates);

    broadcast.push("newsletter.email", message.serializeToJson);
  }
}
