/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.newsletter.emailMessage;

import vibe.data.json;
import crate.json;
import std.string;
import std.algorithm;
import std.array;
import std.conv;
import ogm.markdown;
import crate.json;
import ogm.crates.all;

version(unittest) {
  import ogm.test.fixtures;
  import fluent.asserts;
}

string[] extractUrls(string text) {
  string[] result;

  const pieces = text.split("href");

  foreach(value; pieces) {
    auto withoutSpaces = value
      .filter!(a => a != ' ')
      .filter!(a => a != '\n')
      .filter!(a => a != '\t')
      .map!(a => a == '\'' ? '"' : a)
      .array;

    if(withoutSpaces.startsWith(`="`)) {
      auto last = withoutSpaces[2..$].indexOf(`"`) + 2;

      if(last < 5) {
        continue;
      }

      auto url = withoutSpaces[2..last].toLower.to!string;

      if(result.canFind(url)) {
        continue;
      }

      if(url.startsWith("https://") || url.startsWith("http://")) {
        result ~= url;
      }
    }
  }

  return result;
}

/// extractUrls returns an empty list for an empty string
unittest {
  expect("".extractUrls).to.equal([]);
}

/// extractUrls returns a link when it contains one
unittest {
  expect(`<a href="https://test.com"></a>`.extractUrls).to.equal(["https://test.com"]);
  expect(`<a href='https://test.com'></a>`.extractUrls).to.equal(["https://test.com"]);
}

/// extractUrls returns two links when it contains two
unittest {
  expect(`<a href="https://test.com"></a>some<a href="http://other.com"></a>`.extractUrls)
    .to
    .equal(["https://test.com", "http://other.com"]);
}

/// extractUrls ignores duplicated links
unittest {
  expect(`<a href="https://test.com"></a>some<a href="http://other.com"></a>other<a href="https://test.com">`.extractUrls)
    .to
    .equal(["https://test.com", "http://other.com"]);
}

/// extractUrls ignores white spaces
unittest {
  expect(`<a href = " https://test.com "> </a>`.extractUrls).to.equal(["https://test.com"]);
  expect(`<a href   =  'https://test.com ' ></a>`.extractUrls).to.equal(["https://test.com"]);
}

/// extractUrls ignores invalid hrefs
unittest {
  expect(`<a href   =  https://test.com ></a>`.extractUrls).to.equal([]);
  expect(`<a href   =  https://test.com ></a>`.extractUrls).to.equal([]);
  expect(`<a href   = "  "></a>`.extractUrls).to.equal([]);
  expect(`<a href   = "`.extractUrls).to.equal([]);
  expect(`<a href   = "some link"></a>`.extractUrls).to.equal([]);
}

struct EmailMessage {
  string[] recipients;
  string newsletterId;
  string articleId;
  string subject;
  bool isTest;

  Json content = Json.emptyObject;

  void article(Json article) {
    articleId = article["_id"].to!string;

    if(article.exists("subject")) {
      subject = article["subject"].to!string.strip;
    }

    if(subject == "" && article.exists("title")) {
      subject = article["title"].to!string.strip;
    }

    if(article.exists("content")) {
      if(article["content"].type == Json.Type.string) {
        content = article["content"].to!string.blocksFromMarkdown;
      }

      if(article["content"].type == Json.Type.object) {
        content = article["content"];
      }
    }
  }

  string[] links() {
    string[] result;

    if(!content.exists("blocks")) {
      return result;
    }

    foreach(block; content["blocks"]) {
      if(block.exists("data.text")) {
        result ~= extractUrls(block["data"]["text"].to!string);
      }
    }

    return result;
  }

  void updateLinks(OgmCrates crates) {
    auto linkMap = crates.createRedirects(links(), articleId);

    foreach(block; content["blocks"]) {
      if(block.exists("data.text")) {
        auto text = block["data"]["text"].to!string;

        foreach(string key, value; linkMap) {
          text = text.replace(key, value);
        }

        block["data"]["text"] = text;
      }
    }
  }
}

/// it uses the article name when it has no subject
unittest {
  auto message = EmailMessage();

  message.article = `{
    "title": "some title"
  }`.parseJsonString;

  expect(message.subject).to.equal("some title");
}

/// it uses the article subject when it is set
unittest {
  auto message = EmailMessage();

  message.article = `{
    "subject": "some subject",
    "title": "some title"
  }`.parseJsonString;

  expect(message.subject).to.equal("some subject");
}

/// it sets an empty string when the title and subject are not set
unittest {
  auto message = EmailMessage();

  message.article = `{}`.parseJsonString;

  expect(message.subject).to.equal("");
}

/// it sets an empty object when the content is not set
unittest {
  auto message = EmailMessage();

  message.article = `{}`.parseJsonString;

  expect(message.content.type).to.equal(Json.Type.object);
}

/// it sets the article content when set
unittest {
  auto message = EmailMessage();

  message.article = `{
    "content": {
      "blocks": [{
        "type": "paragraph",
        "data": {
          "text": "some paragraph"
        }
      }]
    }
  }`.parseJsonString;

  expect(message.content).to.equal(`{
    "blocks": [{
      "type": "paragraph",
      "data": {
        "text": "some paragraph"
      }
    }]
  }`.parseJsonString);
}

/// it converts an md text to a json object
unittest {
  auto message = EmailMessage();

  message.article = `{
    "content": "some paragraph"
  }`.parseJsonString;

  expect(message.content).to.equal(`{
    "blocks": [{
      "type": "paragraph",
      "data": {
        "text": "some paragraph"
      }
    }]
  }`.parseJsonString);
}

/// it can extract a link from the message
unittest {
  auto message = EmailMessage();

  message.article = `{
    "content": { "blocks": [{
      "type": "paragraph",
      "data": {
        "text": "If the <a href=\"https://giscollective.com\">information</a> we provide is not useful."
      }
    }]}
  }`.parseJsonString;

  expect(message.links).to.equal([ "https://giscollective.com" ]);
}

/// it uses the right domain in links
unittest {
  setupTestData();

  auto space = `{
    "visibility": {
      "team": "000000000000000000000001", "isDefault": true, "isPrivate": false
    },
    "domain": "giscollective.com",
  }`.parseJsonString;
  crates.space.addItem(space);

  auto message = EmailMessage();

  message.article = `{
    "content": { "blocks": [{
      "type": "paragraph",
      "data": {
        "text": "If the <a href=\"https://example.com\">information</a> we provide is not useful."
      }
    }]}
  }`.parseJsonString;

  message.updateLinks(crates);

  expect(message.content["blocks"][0]["data"]["text"].to!string)
    .to.equal(`If the <a href="https://giscollective.com/link-redirect?id=000000000000000000000001">information</a> we provide is not useful.`);
}

///
string createRedirect(OgmCrates crates, string link, string articleId) {
  auto space = crates.space.get.where("visibility.isDefault").equal(true).and.exec.front;
  Json record;

  auto range = crates.articleLink.get
    .where("url").equal(link).and
    .where("articleId").equal(articleId).and
    .exec;

  if(range.empty) {
    record = Json.emptyObject;
    record["url"] = link;
    record["articleId"] = articleId;

    record = crates.articleLink.addItem(record);
  } else {
    record = range.front;
  }

  return "https://" ~ space["domain"].to!string ~ "/link-redirect?id=" ~ record["_id"].to!string;
}

///
string[string] createRedirects(OgmCrates crates, string[] links, string articleId) {
  string[string] maps;

  foreach(link; links) {
    maps[link] = createRedirect(crates, link, articleId);
  }

  return maps;
}

/// createRedirects creates db records for a link
unittest {
  setupTestData();
  createTestDefaultSpace();

  const maps = crates.createRedirects(["http://example.com"], "1");

  expect(maps.keys).to.equal(["http://example.com"]);
  expect(maps["http://example.com"]).to.equal("https://giscollective.com/link-redirect?id=000000000000000000000001");

  const record = crates.articleLink.get.where("url").equal("http://example.com").and.exec.front;

  record["_id"].to!string.should.equal(`000000000000000000000001`);
  record["articleId"].to!string.should.equal(`1`);
  record["url"].to!string.should.equal(`http://example.com`);
}

/// createRedirects does not create a db record for a link when it exists
unittest {
  setupTestData();
  createTestDefaultSpace();

  auto link = `{
    "url": "http://example.com",
    "articleId": "1"
  }`.parseJsonString;
  crates.articleLink.addItem(link);

  const maps = crates.createRedirects(["http://example.com"], "1");

  expect(maps.keys).to.equal(["http://example.com"]);
  expect(maps["http://example.com"]).to.equal("https://giscollective.com/link-redirect?id=000000000000000000000001");

  const record = crates.articleLink.get.where("url").equal("http://example.com").and.exec.front;

  record["_id"].to!string.should.equal(`000000000000000000000001`);
  record["articleId"].to!string.should.equal(`1`);
  record["url"].to!string.should.equal(`http://example.com`);
}
