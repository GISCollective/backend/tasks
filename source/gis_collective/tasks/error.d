/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.error;

public import std.exception;

/// A generic task exception
/// given when an unexpected condition was encountered and no more specific message is suitable.
class TaskException : Exception {
  this(string msg = null, Throwable next = null) @safe {
    super(msg, next);
  }

  this(string msg, string file, size_t line, Throwable next = null) @safe {
    super(msg, file, line, next);
  }
}

/// Exception triggered when a task can not be processed because the data is invalid
class TaskValidationException : Exception
{
  this(string msg = null, Throwable next = null) @safe {
    super(msg, next);
  }

  this(string msg, string file, size_t line, Throwable next = null) @safe {
    super(msg, file, line, next);
  }
}
