/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.feature.FeatureItemSplit;

import gis_collective.tasks.task;
import gis_collective.hmq.log;
import ogm.crates.all;
import ogm.meta;

import vibe.core.log;
import vibe.data.json;

import std.algorithm;
import std.conv;
import std.array;
import crate.base;

version(unittest) {
  import ogm.test.fixtures;
  import fluent.asserts;
}

/// This is a task that gets a feature and create an unique id for each attribute list item
struct FeatureItemSplit {
  enum type = "Feature.itemSplit";
  alias Model = Feature;

  string id;
  OgmCrates crates;

  static Json[] processList(const ChangeSet changeSet) {
    if(changeSet.model != Model.stringof) {
      return [];
    }

    if(!changeSet.isChanged("attributes")) {
      return [];
    }

    return [ changeSet.itemId.toJson ];
  }

  void start() {
    foreach(item; crates.meta.get.where("itemId").equal(id).and.where("type").equal(type).and.exec) {
      try {
        crates.meta.deleteItem(item["_id"].to!string);
      } catch(Exception e) {
        error(e);
      }
    }

    auto featureRange = crates.feature.get().where("_id").equal(ObjectId.fromString(id)).and.exec;

    if(featureRange.empty) {
      return;
    }

    auto feature = featureRange.front;

    if(feature["icons"].length == 0) {
      return;
    }

    if(feature["maps"].length == 0) {
      return;
    }

    scope metaQuery = new MetaQuery(Model.stringof, type, crates);

    ObjectId[] iconIds = (cast(Json[]) feature["icons"]).map!(a => ObjectId.fromJson(a)).array;

    auto idBuilder = IdListBuilder(feature["attributes"]);
    idBuilder.setIcons(crates.icon.get.where("_id").containsAny(iconIds).and.exec.array);

    size_t changeIndex;

    try {
      changeIndex = feature["info"]["changeIndex"].to!size_t;
    } catch(Exception e) {
      logError("Can't get the change index for feature `%s`", feature["_id"]);
    }

    Meta meta;
    foreach(size_t index, item; idBuilder.lists) {
      meta = metaQuery.get(id, true);

      meta.changeIndex = changeIndex;
      meta.data = Json.emptyObject;
      meta.data["id"] = id ~ "_" ~ index.to!string;
      meta.data["icons"] = item;
      meta.data["maps"] = feature["maps"];
      crates.meta.updateItem(meta.serializeToJson);
      logDiagnostic("Updated feature item split meta: %s", meta.serializeToJson);
    }
  }
}

struct IdListBuilder {
  Json attributes;
  Json[] lists;

  void setIcons(Json[] icons) {
    lists ~= Json(icons.map!(a => a["_id"]).array);

    long[string] attributeLists;
    foreach(string key, val; attributes) {
      if(val.type == Json.Type.array && val.length > 1) {
        attributeLists[key] = val.length;
      }
    }

    foreach(ref Json icon; icons) {
      string name = icon["name"].to!string;

      if(name !in attributeLists) {
        continue;
      }

      auto listCount = attributeLists[name];

      foreach(index; 1..listCount) {
        if(lists.length <= index) {
          lists ~= Json.emptyArray;
        }

        lists[index].appendArrayElement(icon["_id"]);
      }
    }
  }
}

version(unittest) {
  import ogm.models.icon;

  Team team1;
  Icon icon1;
  IconSet iconSet1;
  Map map1;
  Picture cover;
  OgmCrates crates;

  void prepareTestData() {
    crates = createCrates;

    IconSettings.files = crates.iconFiles;
    IconSettings.chunks = crates.iconChunks;

    cover._id = ObjectId.fromString("2");
    cover.picture = new PictureFile;
    crates.picture.addItem(cover.serializeToJson);

    icon1._id = ObjectId.fromString("1");
    icon1.iconSet = iconSet1;
    icon1.category = "category";
    icon1.subcategory = "subcategory";
    icon1.name = "name1";
    icon1.image = OptionalIconImage(false, IconFile.fromString("data:text/plain;base64,dGhpcyBpcyBhIHRleHQgZmlsZQ=="));
    createIcon(icon1);

    auto team1 = Team(ObjectId.fromString("1"), "team1", Json(""), [], [], [], [], true, cover);
    createTeam(team1);

    iconSet1._id = ObjectId.fromString("1");
    iconSet1.name = "set1";
    iconSet1.visibility.team = team1;
    iconSet1.visibility.isPublic = true;
    iconSet1.visibility.isDefault = true;
    createIconSet(iconSet1);

    map1 = createMap(Map(ObjectId.fromString("1"), "map1", Json(""),
      Visibility(team1, true), Polygon([[[-70, 30], [-70, 50], [-80, 50], [-80, 30], [-70,30]]]),
      cover, IconSets(true, [iconSet1]), BaseMaps(), "", SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-02-01T00:00:00Z"),
      ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z"))));
  }
}

/// It should not split a site with no icons
unittest {
  prepareTestData;

  createFeature(
    Feature(ObjectId.fromString("1"),
        [map1],
        "site1",
        Json("description of site1"),
        GeoJsonGeometry.fromJson(`{ "type": "LineString", "coordinates": [[1.5, 1.5], [2,2], [5,5]] }`.parseJsonString),
        ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z"))));

  auto featureItemSplit = FeatureItemSplit("000000000000000000000001", crates);
  featureItemSplit.start;
  featureItemSplit.start;

  crates.meta.get
    .where("type")
    .equal(FeatureItemSplit.type)
    .and.exec.array
    .length.should.equal(0);
}

/// It should not split a site with no maps
unittest {
  prepareTestData;

  auto feature = Feature(ObjectId.fromString("1"),
        [],
        "site1",
        Json("description of site1"),
        GeoJsonGeometry.fromJson(`{ "type": "LineString", "coordinates": [[1.5, 1.5], [2,2], [5,5]] }`.parseJsonString),
        ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z")));
  feature.icons ~= icon1;

  createFeature(feature);

  auto featureItemSplit = FeatureItemSplit("000000000000000000000001", crates);
  featureItemSplit.start;
  featureItemSplit.start;

  crates.meta.get
    .where("type")
    .equal(FeatureItemSplit.type)
    .and.exec.array
    .length.should.equal(0);
}

/// It should create one meta item for a site with one icon and no attributes
unittest {
  prepareTestData;

  auto feature = Feature(ObjectId.fromString("1"),
    [map1],
    "site1",
    Json("description of site1"),
    GeoJsonGeometry.fromJson(`{ "type": "LineString", "coordinates": [[1.5, 1.5], [2,2], [5,5]] }`.parseJsonString),
    ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z")));
  feature.icons ~= icon1;
  feature.info.changeIndex = 1;

  createFeature(feature);

  auto featureItemSplit = FeatureItemSplit("000000000000000000000001", crates);
  featureItemSplit.start;

  auto result = crates.meta.get
    .where("type")
    .equal(FeatureItemSplit.type)
    .and.exec.array;

  result.length.should.equal(1);
  result[0].should.equal(`{
    "_id": "000000000000000000000001",
    "type": "Feature.itemSplit",
    "model": "Feature",
    "itemId": "000000000000000000000001",
    "changeIndex": 1,
    "data": {
      "id": "000000000000000000000001_0",
      "maps": ["000000000000000000000001"],
      "icons": ["000000000000000000000001"]
    }
  }`.parseJsonString);
}

/// It should create two meta items for a site with a site attr list with two items
unittest {
  prepareTestData;

  auto feature = Feature(ObjectId.fromString("1"),
    [map1],
    "site1",
    Json("description of site1"),
    GeoJsonGeometry.fromJson(`{ "type": "LineString", "coordinates": [[1.5, 1.5], [2,2], [5,5]] }`.parseJsonString),
    ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z")));

  feature.icons ~= icon1;
  feature.attributes["name1"] = Json([Json.emptyObject, Json.emptyObject]);

  createFeature(feature);

  auto featureItemSplit = FeatureItemSplit("000000000000000000000001", crates);
  featureItemSplit.start;

  auto result = crates.meta.get
    .where("type")
    .equal(FeatureItemSplit.type)
    .and.exec.array;

  result.length.should.equal(2);
  result[0].should.equal(`{
    "_id": "000000000000000000000001",
    "type": "Feature.itemSplit",
    "model": "Feature",
    "itemId": "000000000000000000000001",
    "changeIndex": 0,
    "data": {
      "id": "000000000000000000000001_0",
      "maps": ["000000000000000000000001"],
      "icons": ["000000000000000000000001"]
    }
  }`.parseJsonString);

  result[1].should.equal(`{
    "_id": "000000000000000000000002",
    "type": "Feature.itemSplit",
    "model": "Feature",
    "itemId": "000000000000000000000001",
    "changeIndex": 0,
    "data": {
      "id": "000000000000000000000001_1",
      "maps": ["000000000000000000000001"],
      "icons": ["000000000000000000000001"]
    }
  }`.parseJsonString);
}

/// It should remove the metadata if the site does not exist
unittest {
  prepareTestData;

  crates.meta.addItem(`{
    "_id": "0000000000000000000000001",
    "type": "Feature.itemSplit",
    "model": "Feature",
    "itemId": "000000000000000000000100",
    "changeIndex": 0,
    "data": { }
  }`.parseJsonString);

  auto featureItemSplit = FeatureItemSplit("000000000000000000000100", crates);
  featureItemSplit.start;

  auto result = crates.meta.get
    .where("type")
    .equal(FeatureItemSplit.type)
    .and.exec.array;

  result.length.should.equal(0);
}

/// It should process attributes add inside sites
unittest {
  ChangeSet change;
  change.model = "Feature";
  change.itemId = ObjectId.generate;
  change.added["attributes"] = "";

  FeatureItemSplit.processList(change).should.equal([change.itemId.toJson]);
}

/// It should process attributes remove inside sites
unittest {
  ChangeSet change;
  change.model = "Feature";
  change.itemId = ObjectId.generate;
  change.removed["attributes"] = "";

  FeatureItemSplit.processList(change).should.equal([change.itemId.toJson]);
}

/// It should process attributes properties remove inside sites
unittest {
  ChangeSet change;
  change.model = "Feature";
  change.itemId = ObjectId.generate;
  change.removed["attributes.type"] = "";

  FeatureItemSplit.processList(change).should.equal([change.itemId.toJson]);
}

/// It should not process when area is not changed
unittest {
  ChangeSet change;
  change.model = "Feature";
  change.itemId = ObjectId.generate;

  FeatureItemSplit.processList(change).should.equal([]);
}

/// It should not process changes for other models
unittest {
  ChangeSet change;
  change.model = "Map";
  change.itemId = ObjectId.generate;

  FeatureItemSplit.processList(change).should.equal([]);
}
