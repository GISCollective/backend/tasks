/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.feature.notifications.NearbyFeatureUpdateNotificationTask;

import std.array;
import std.functional;
import std.range;
import std.algorithm;
import std.string;
import std.conv;
import gis_collective.tasks.error;

import ogm.crates.all;
import ogm.models.mapFile;
import ogm.models.feature;
import ogm.models.meta;
import ogm.http.request;
import ogm.meta;

import vibe.data.json;
import vibe.data.bson;
import vibe.stream.wrapper;
import vibe.mail.smtp;
import vibeauth.mail.base;
import vibeauth.mail.vibe;

import crate.base;
import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import gis_collective.tasks.mapFile.deserializer.base;

import vibe.core.log;

struct NearbyFeatureUpdateNotificationTask {
  enum type = "feature.nearbyUpdateNotification";
  alias Model = Feature;

  /// the message that needs to be processed
  string id;

  OgmCrates crates;
  IBroadcast broadcast;

  static Json[] processList(const ChangeSet changeSet) {
    if(changeSet.model != Model.stringof) {
      return [ ];
    }

    return [ changeSet.itemId.toJson ];
  }

  string[] getNearbyTeams(double lon, double lat) {
    return crates.map.get.where("area")
      .containsPoint(lon, lat)
      .and.exec
      .map!(a => a["visibility"]["team"].to!string)
      .array;
  }

  string[] getUserIds(string teamId) {
    auto team = crates.team.getItem(teamId).exec.front;

    string[] userStrings;

    userStrings ~= team["guests"].deserializeJson!(string[]);
    userStrings ~= team["leaders"].deserializeJson!(string[]);
    userStrings ~= team["members"].deserializeJson!(string[]);
    userStrings ~= team["owners"].deserializeJson!(string[]);

    string[] userIds;

    foreach(userString; userStrings) {
      userIds ~= crates.user.get
        .where("email").equal(userString).and
        .withProjection(["_id"]).and
        .exec
        .map!(a => a["_id"].to!string)
        .array;
    }

    return userIds;
  }

  void start() {
    string[] userIds;

    auto feature = crates.feature.getItem(this.id).exec.front;
    auto position = feature["position"];

    if(position["type"] != "Point") {
      logInfo("The feature is not a point: %s", position);
      return;
    }

    auto featureName = feature["name"].to!string;

    auto coordinates = position["coordinates"].deserializeJson!(double[]);

    auto nearbyTeams = getNearbyTeams(coordinates[0], coordinates[1]);
    foreach(team; nearbyTeams) {
      auto newIds = getUserIds(team);
      foreach(id; newIds) {
        if(!canFind(userIds, id)) {
          userIds ~= id;
        }
      }
    }

    auto outputValue = Json.emptyObject;
    outputValue["subject"] = "Updates in the area of your map";
    outputValue["textMessage"] = "Here's what has updated: " ~ featureName;

    foreach(id; userIds) {
      outputValue["userId"] = id;
      broadcast.push("notifications.message", outputValue);
    }
  }

}
