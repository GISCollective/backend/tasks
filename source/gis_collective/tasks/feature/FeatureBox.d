/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.feature.FeatureBox;

import geo.json;
import geo.geometries;
import geo.algorithm;
import geo.algo.simplify;
import geo.algo.cut;
import geo.conv;

import vibe.data.json;
import vibe.core.log;

import std.datetime;
import std.algorithm;
import std.array;
import std.conv;
import std.math;
import std.exception;

import crate.base;
import crate.error;
import vibe.service.stats;
import vibe.core.core;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import ogm.crates.all;
import ogm.features.simplify;

import geo.xyz;

version(unittest) {
  import fluent.asserts;
  import ogm.test.fixtures;
}

struct FeatureBox {
  struct Request {
    ObjectId id;
  }

  string logPrefix;
  enum type = "Feature.Box";
  alias Model = Feature;

  OgmCrates crates;
  IBroadcast broadcast;

  static Json[] processList(const ChangeSet changeSet) {
    if(changeSet.model != Model.stringof) {
      return [];
    }

    if(changeSet.type == CrateChangeType.delete_) {
      return [];
    }

    return [ changeSet.itemId.toJson ];
  }

  void main(Request request) {
    logPrefix = "[" ~ type ~ "][" ~ request.id.toString ~ "]";

    auto featureRange = crates.feature.getItem(request.id.to!string).and.exec;
    enforce(!featureRange.empty, "There is no feature with id `" ~ request.id.to!string ~ "`");

    auto feature = featureRange.front;
    auto mapIds = ObjectId.fromJsonList(feature["maps"]);

    auto maskedMaps = crates.map.get
      .where("_id").anyOf(mapIds).and
      .where("mask.isEnabled").equal(true).and
      .size;

    if(maskedMaps > 0) {
      logInfo("%s Done. Box is not needed because the feature is masked.", logPrefix);
      return;
    }

    auto result = feature["position"].featureBox;

    if(result.to!string == feature["positionBox"].to!string) {
      logInfo("%s Done. Update is not needed.", logPrefix);
      return;
    }

    feature["positionBox"] = result;

    crates.feature.updateItem(feature);

    logInfo("%s Done", logPrefix);
  }
}
