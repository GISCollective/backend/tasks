/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.feature.AttributesFix;

import vibe.data.json;
import vibe.core.log;

import std.datetime;
import std.algorithm;
import std.array;
import std.conv;

import crate.base;
import vibe.service.stats;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import ogm.crates.all;
import ogm.models.meta;
import ogm.meta;

version(unittest) {
  import fluent.asserts;
  import ogm.test.fixtures;
}

struct AttributesFix {
  struct Request {
    ObjectId id;
  }

  enum type = "Feature.AttributesFix";
  alias Model = Feature;

  OgmCrates crates;
  IBroadcast broadcast;

  static Json[] processList(const ChangeSet changeSet, OgmCrates crates) {
    if(changeSet.model != Model.stringof && changeSet.model != "Icon") {
      return [];
    }

    if(changeSet.model == "Icon" && changeSet.type == CrateChangeType.add) {
      return [];
    }

    if(changeSet.model == "Icon") {
      return crates.feature
        .get
        .where("icons")
        .arrayContains(changeSet.itemId)
        .and
        .withProjection(["_id"])
        .exec
        .map!(a => a["_id"])
        .array;
    }

    if(changeSet.type == CrateChangeType.delete_) {
      return [];
    }

    return [ changeSet.itemId.toJson ];
  }

  void main(Request request) {
    string logPrefix = "[" ~ type ~ "][" ~ request.id.toString ~ "]";
    auto feature = crates.feature.getItem(request.id.to!string).and.exec.front;
    auto before = feature.clone;

    ObjectId[] iconIds = feature["icons"].byValue.map!(a => ObjectId.fromJson(a)).array;
    Json[] existingIcons = crates.icon.get.where("_id").anyOf(iconIds).and.withProjection(["_id", "name", "allowMany", "otherNames"]).exec.array;

    this.fixIconList(feature, existingIcons.map!(a => a["_id"]).array);

    if(feature["attributes"].type != Json.Type.object) {
      feature["attributes"] = Json.emptyObject;
    }

    string[] invalidKeys;

    foreach(string key, group; feature["attributes"]) {
      if(group.type != Json.Type.object && group.type != Json.Type.array) {
        invalidKeys ~= key;
      }
    }

    foreach(key; invalidKeys) {
      feature["attributes"].remove(key);
    }

    foreach(icon; existingIcons) {
      auto allowMany = icon["allowMany"].to!bool;
      auto name = icon["name"].to!string;

      if(feature["attributes"][name].type != Json.Type.object && feature["attributes"][name].type != Json.Type.array) {
        fixOtherNames(feature["attributes"], name, icon["otherNames"].deserializeJson!(string[]));
      }

      if(name in feature["attributes"]) {
        fixManyAttributes(feature["attributes"], name, allowMany);
      }
    }

    crates.feature.updateItem(feature);
    notify(before, feature);
  }

  void fixOtherNames(ref Json attributes, string name, string[] otherNames) {
    foreach(otherName; otherNames) {
      if(otherName in attributes) {
        attributes[name] = attributes[otherName];
        attributes.remove(otherName);
        return;
      }
    }
  }

  void fixIconList(ref Json feature, Json[] existingIconIds) {
    Json[] matchedIds;
    foreach(id; feature["icons"]) {
      if(existingIconIds.canFind(id)) {
        matchedIds ~= id;
      }
    }

    feature["icons"] = matchedIds.serializeToJson;
  }

  void fixManyAttributes(ref Json attributes, string groupName, bool allowMany) {
    auto group = attributes[groupName];

    if(group.type == Json.Type.array) {
      Json[] validElements;

      foreach(element; group) {
        if(element.type == Json.Type.object) {
          validElements ~= element;
        }
      }

      attributes[groupName] = validElements.serializeToJson;
      group = attributes[groupName];
    }

    if(allowMany && group.type == Json.Type.object) {
      attributes[groupName] = Json.emptyArray;
      attributes[groupName].appendArrayElement(group);
    }

    if(!allowMany && group.type == Json.Type.array && group.length == 1) {
      attributes[groupName] = group[0];
    }
  }

  void notify(Json before, Json after) {
    if(broadcast is null) return;

    scope channel = "Feature.change";

    auto change = CrateChange(Model.stringof, CrateChangeType.update, before, after);

    broadcast.push(channel, change.serializeToJson);
  }
}
