/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.feature.FeatureSimplify;

import geo.json;
import geo.geometries;
import geo.algorithm;
import geo.algo.simplify;
import geo.algo.cut;
import geo.conv;

import vibe.data.json;
import vibe.core.log;

import std.datetime;
import std.algorithm;
import std.array;
import std.conv;
import std.math;
import std.exception;

import crate.base;
import crate.error;
import vibe.service.stats;
import vibe.core.core;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import ogm.crates.all;
import ogm.features.simplify;

import geo.xyz;

version(unittest) {
  import fluent.asserts;
  import ogm.test.fixtures;
}

struct FeatureSimplify {
  struct Request {
    ObjectId id;
  }

  string logPrefix;
  enum type = "Feature.Simplify";
  alias Model = Feature;

  OgmCrates crates;
  IBroadcast broadcast;

  static Json[] processList(const ChangeSet changeSet) {
    if(changeSet.model != Model.stringof) {
      return [];
    }

    if(changeSet.type == CrateChangeType.delete_) {
      return [];
    }

    return [ changeSet.itemId.toJson ];
  }

  void simplify(ref Json feature) {
    auto deleteRange = crates.simpleFeature.get.where("featureId").equal(feature["_id"].to!string).and.withProjection(["_id"]).exec;

    bool skip;
    logInfo("%s deleting existing geometries", logPrefix);
    foreach(item; deleteRange) {
      if(skip) {
        continue;
      }

      try {
        crates.simpleFeature.deleteItem(item["_id"].to!string);
      } catch(CrateNotFoundException e) {
        logError("%s %s", logPrefix, e.msg);
        skip = true;
      }
    }

    if(skip) {
      logWarn("%s Skip. Another task might also run in the same time.", logPrefix);
      return;
    }

    FeatureDecorators decorators;

    if("decorators" in feature && feature["decorators"] != null) {
      decorators = feature["decorators"].deserializeJson!FeatureDecorators;
    }

    auto geometry = GeoJsonGeometry.fromJson(feature["position"]);

    geo.geometries.Polygon polygon;
    geo.geometries.MultiPolygon multiPolygon;
    size_t pointCount;

    if(geometry.type == "Polygon") {
      polygon = geometry.to!(geo.geometries.Polygon);
      pointCount = polygon.countPoints;
    }

    if(geometry.type == "MultiPolygon") {
      multiPolygon = geometry.to!(geo.geometries.MultiPolygon);
      pointCount = multiPolygon.countPoints;
    }

    if(pointCount > 1000) {
      logWarn("%s Skip. Skip geometry with %s points.", logPrefix, pointCount);
      return;
    }

    foreach_reverse(uint zoom; 0..17) {
      auto tiles = geometry.tileIndexes(zoom);

      if(tiles.length == 0) {
        logInfo("%s no tiles to be generated on zoom %s.", logPrefix, zoom);
        continue;
      }

      const tileBox = toBBox(zoom, tiles[0][0], tiles[0][1], 0.1);

      if(geometry.type == "Polygon") {
        logInfo("%s simplify Polygon for zoom %s", logPrefix, zoom);
        polygon = polygon.simplify(tileBox.dx / 4_000);

        if(polygon.coordinates.length == 0) {
          return;
        }
      }

      if(geometry.type == "MultiPolygon") {
        logInfo("%s simplify MultiPolygon for zoom %s", logPrefix, zoom);
        multiPolygon = multiPolygon.simplify(tileBox.dx / 4_000);

        if(multiPolygon.coordinates.length == 0) {
          return;
        }
      }

      logInfo("%s generating tiles on zoom %s. %s estimated tiles to be generated.", logPrefix, zoom, tiles.length);
      auto begin = Clock.currTime;
      size_t tileCount;

      auto simpleFeature = Json.emptyObject;

      simpleFeature["featureId"] = feature["_id"];
      simpleFeature["maps"] = feature["maps"];
      simpleFeature["icons"] = feature["icons"];
      simpleFeature["computedVisibility"] = feature["computedVisibility"];
      simpleFeature["_ver"] = 1;
      simpleFeature["z"] = zoom;

      if("info" in feature) {
        simpleFeature["changeIndex"] = feature["info"]["changeIndex"];
      }

      string type = geometry.type;

      bool enforceLine = (type == "Polygon" || type == "MultiPolygon") && tiles.length > 10;

      size_t i;
      foreach(tile; tiles) {
        simpleFeature["x"] = tile[0];
        simpleFeature["y"] = tile[1];

        auto key = zoom.to!string ~ "/" ~ tile[0].to!string ~ "/" ~ tile[1].to!string;

        if(type == "LineString") {
          simpleFeature["position"] = simplifyGeometry(zoom, tile[0], tile[1], geometry.to!(geo.geometries.LineString), decorators);
        }

        if(type == "MultiLineString") {
          simpleFeature["position"] = simplifyGeometry(zoom, tile[0], tile[1], geometry.to!(geo.geometries.MultiLineString), decorators);
        }

        if(type == "Polygon" && !enforceLine) {
          auto coordinates = polygon.cut(zoom, tile[0], tile[1]).coordinates;

          if(coordinates.length) {
            simpleFeature["position"] = Json.emptyObject;
            simpleFeature["position"]["type"] = "Polygon";
            simpleFeature["position"]["coordinates"] = coordinates.serializeToJson;
          }
        }

        if(type == "Polygon" && enforceLine) {
          simpleFeature["position"] = simplifyGeometry(zoom, tile[0], tile[1], geo.geometries.MultiLineString(polygon.coordinates), decorators);
        }

        if(type == "MultiPolygon" && !enforceLine) {
          auto coordinates = multiPolygon.cut(zoom, tile[0], tile[1]).coordinates;

          if(coordinates.length) {
            simpleFeature["position"] = Json.emptyObject;
            simpleFeature["position"]["type"] = "MultiPolygon";
            simpleFeature["position"]["coordinates"] = coordinates.serializeToJson;
          }
        }

        if(type == "MultiPolygon" && enforceLine) {
          simpleFeature["position"] = `{ "type": "MultiLineString", "coordinates": [] }`.parseJsonString;

          foreach (polygonCoordinates; multiPolygon.coordinates) {
            auto result = simplifyGeometry(zoom, tile[0], tile[1], geo.geometries.MultiLineString(polygonCoordinates), decorators);

            if(result.type != Json.Type.object || "coordinates" !in result) {
              continue;
            }

            foreach (line; result["coordinates"]) {
              if(line.length) {
                simpleFeature["position"]["coordinates"].appendArrayElement(line);
              }
            }
          }

          if(simpleFeature["position"]["coordinates"].length == 0) {
            simpleFeature["position"] = Json();
          }
        }

        if("position" in simpleFeature) {
          try {
            crates.simpleFeature.addItem(simpleFeature);
            tileCount++;
          } catch(Exception e) {
            logWarn("%s[%s/%s/%s] invalid feature transformation: %s\n%s", logPrefix, zoom, tile[0], tile[1], geometry.type, e.to!string);

            debug {
              import std.stdio, core.stdc.stdlib : exit;
              writeln(simpleFeature);
              exit(1);
            }
          }
        }

        i++;

        if(i % 300) {
          yield;
        }
      }

      logInfo("%s %s tiles generated on zoom %s in %s.", logPrefix, tileCount, zoom, Clock.currTime - begin);
    }
  }

  void main(Request request) {
    logPrefix = "[" ~ type ~ "][" ~ request.id.toString ~ "]";

    auto featureRange = crates.feature.getItem(request.id.to!string).and.exec;
    enforce(!featureRange.empty, "There is no feature with id `" ~ request.id.to!string ~ "`");

    auto feature = featureRange.front;
    auto mapIds = ObjectId.fromJsonList(feature["maps"]);

    auto maskedMaps = crates.map.get
      .where("_id").anyOf(mapIds).and
      .where("mask.isEnabled").equal(true).and
      .size;

    if(maskedMaps > 0) {
      logInfo("%s Done. Box is not needed because the feature is masked.", logPrefix);
      return;
    }

    auto indexedMaps = crates.map.get
      .where("_id").anyOf(mapIds).and
      .where("isIndex").equal(true).and
      .size;

    if(indexedMaps > 0) {
      logInfo("%s Done. Box is not needed because the feature is on an indexed map.", logPrefix);
      return;
    }

    simplify(feature);

    logInfo("%s Done", logPrefix);
  }
}
