/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.feature.IndexedFeature;

import geo.json;
import geo.geometries;

import vibe.data.json;
import vibe.core.log;

import std.datetime;
import std.algorithm;
import std.array;
import std.conv;
import std.math;
import std.exception;

import crate.base;
import vibe.service.stats;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import ogm.crates.all;
import ogm.models.meta;
import ogm.meta;
import std.string;

version(unittest) {
  import fluent.asserts;
  import ogm.test.fixtures;
}

struct IndexedFeature {
  struct Request {
    string id;
    @optional string[] removedMaps;
  }

  enum type = "Feature.Indexed";
  alias Model = Feature;

  OgmCrates crates;
  IBroadcast broadcast;

  static Json[] processList(const ChangeSet changeSet) {
    if(changeSet.model != Model.stringof) {
      return [];
    }

    auto removedMaps = changeSet.removed.byKeyValue.filter!(a => a.key.indexOf("maps[") == 0);

    if(removedMaps.empty) {
      return [ changeSet.itemId.toJson ];
    }

    auto request = Json.emptyObject;

    request["id"] = changeSet.itemId.toJson;
    request["removedMaps"] = removedMaps.map!(a => a.value).array.serializeToJson;

    return [ request ];
  }

  void main(Request request) {
    string logPrefix = "[" ~ type ~ "][" ~ request.id ~ "]";

    auto maps = request.removedMaps;
    try {
      auto feature = crates.feature.getItem(request.id).and.exec.front;
      auto featureMaps = feature["maps"].byValue.map!(a => a.to!string).array;
      maps = join([maps, featureMaps]);
    } catch(Exception e) {
      error(e);
    }

    auto maskedMaps = crates.map.get.where("mask.map").anyOf(maps).and.withProjection(["_id"]).exec;

    foreach(maskedMap; maskedMaps) {
      auto message = Json.emptyObject;
      message["id"] = maskedMap["_id"];

      broadcast.push("Map.Mask", message);
    }
  }
}
