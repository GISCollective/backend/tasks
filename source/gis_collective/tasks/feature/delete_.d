/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.feature.delete_;

import vibe.data.json;
import vibe.core.log;

import std.datetime;
import std.algorithm;
import std.array;
import std.conv;

import crate.base;
import vibe.service.stats;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import ogm.crates.all;
import ogm.models.meta;
import ogm.meta;

version(unittest) {
  import fluent.asserts;
  import ogm.test.fixtures;
}

struct FeatureDelete {
  struct Request {
    ObjectId id;
  }

  enum type = "Feature.delete";
  alias Model = Feature;

  OgmCrates crates;
  IBroadcast broadcast;

  static Json[] processList(const ChangeSet changeSet) {
    if(changeSet.model != Model.stringof) {
      return [];
    }

    if(changeSet.type != CrateChangeType.delete_) {
      return [];
    }

    return [ changeSet.itemId.toJson ];
  }

  void main(Request request) {
    auto logPrefix = "[" ~ type ~ "][" ~ request.id.toString ~ "]";

    auto size = crates.meta.get
      .where("model").equal(Model.stringof).and
      .where("itemId").equal(request.id).and
      .size;

    logInfo("%s Removing `" ~ size.to!string ~ "` metadata related to feature `" ~ request.id.toString ~ "`.");

    auto range = crates.meta.get
      .where("model").equal(Model.stringof).and
      .where("itemId").equal(request.id).and
      .exec;

    foreach(meta; range) {
      crates.meta.deleteItem(meta["_id"].to!string);
    }

    auto deleteRange = crates.simpleFeature.get.where("featureId").equal(request.id).and.withProjection(["_id"]).exec;

    logInfo("%s deleting existing simple geometries", logPrefix);
    foreach(item; deleteRange) {
      try {
        crates.simpleFeature.deleteItem(item["_id"].to!string);
      } catch(Exception e) {
        logInfo("%s can't delete simple geometry %s", logPrefix, item["_id"]);
      }
    }
  }
}
