/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.feature.SoundsVisibility;

import vibe.data.json;
import vibe.core.log;

import std.datetime;
import std.algorithm;
import std.array;
import std.conv;

import crate.base;
import vibe.service.stats;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import ogm.crates.all;
import ogm.models.meta;
import ogm.meta;

version(unittest) {
  import fluent.asserts;
  import ogm.test.fixtures;
}

struct SoundsVisibility {
  struct Request {
    ObjectId id;
  }

  enum type = "Feature.SoundsVisibility";
  alias Model = Feature;

  OgmCrates crates;
  IBroadcast broadcast;

  static Json[] processList(const ChangeSet changeSet) {
    if(changeSet.model != Model.stringof) {
      return [];
    }

    if(changeSet.type == CrateChangeType.delete_) {
      return [];
    }

    return [ changeSet.itemId.toJson ];
  }

  void main(Request request) {
    string logPrefix = "[" ~ type ~ "][" ~ request.id.toString ~ "]";
    auto feature = crates.feature.getItem(request.id.to!string).and.exec.front;

    if("sounds" !in feature || feature["sounds"].type != Json.Type.array || feature["sounds"].length == 0) {
      logInfo("%s The feature `%s` has no sounds.", logPrefix, request.id.toString);
      return;
    }

    if("maps" !in feature || feature["maps"].type != Json.Type.array || feature["maps"].length == 0) {
      logInfo("%s The feature `%s` has no maps.", logPrefix, request.id.toString);
      return;
    }

    auto map = crates.map.getItem(feature["maps"][0].to!string).and.exec.front;

    auto soundIds = feature["sounds"].type == Json.Type.array ? feature["sounds"].byValue.map!(a => ObjectId.fromJson(a)).array : [];
    auto sounds = crates.sound.get.where("_id").anyOf(soundIds).and.exec;

    foreach (sound; sounds) {
      sound["visibility"] = map["visibility"];
      sound["visibility"]["isPublic"] = feature["visibility"] == 1;

      crates.sound.updateItem(sound);
    }
  }
}
