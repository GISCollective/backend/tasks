/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.feature.FeatureDecorator;

import std.array;
import std.functional;
import std.range;
import std.algorithm;
import std.datetime;
import std.conv;

import ogm.crates.all;
import ogm.models.feature;

import vibe.data.json;
import vibe.data.bson;
import vibe.stream.wrapper;

import crate.base;
import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;

import vibe.core.log;
import crate.json;
import crate.base;
import ogm.icons.TaxonomyTree;
import geo.algorithm;
import crate.json;

version(unittest) {
  import std.string;
  import fluent.asserts;
  import ogm.test.fixtures;
  import gis_collective.hmq.broadcast.memory;
}

struct FeatureDecorator {
  enum type = "Feature.decorators";
  alias Model = Feature;

  OgmCrates crates;
  string _id;

  struct Request {
    string id;
  }

  void log(T...)(string message, T params) {
    logInfo("[%s %s] " ~ message, type, _id, params);
  }

  static Json[] processList(const ChangeSet changeSet, OgmCrates crates) {
    if(changeSet.model != Model.stringof && changeSet.model != "Icon") {
      return [];
    }

    if(changeSet.model == "Icon" && changeSet.type == CrateChangeType.add) {
      return [];
    }

    if(changeSet.model == "Icon") {
      return crates.feature
        .get
        .where("icons")
        .arrayContains(changeSet.itemId)
        .and
        .withProjection(["_id"])
        .exec
        .map!(a => a["_id"])
        .array;
    }

    if(changeSet.type == CrateChangeType.delete_) {
      return [];
    }

    return [ changeSet.itemId.toJson ];
  }

  void main(Request request) {
    this._id = request.id;
    auto iconsTree = new TaxonomyTree(&crates.icon.get, &crates.iconSet.getItem);

    string logPrefix = "[" ~ type ~ "][" ~ request.id ~ "]";
    FeatureDecorators decorators;
    Json feature;
    Json icon;

    try {
      feature = crates.feature.getItem(request.id).and.exec.front;

      if(feature["icons"].type != Json.Type.array || feature["icons"].length == 0) {
        log("Ignoring feature with no icons.");
        return;
      }

      icon = iconsTree.getIcon(feature["icons"][0].to!string);
    } catch(Exception e) {
      error(e);
      return;
    }

    if(icon.type == Json.Type.null_ || icon.to!string == "null") {
      log("The first icon is null");
      return;
    }

    if(feature["decorators"].type == Json.Type.object) {
      decorators = feature["decorators"].deserializeJson!FeatureDecorators;
    }

    if(!decorators.useDefault) {
      return;
    }

    if(feature["info"].type != Json.Type.object) {
      feature["info"] = `{}`.parseJsonString;
    }

    if(feature["info"]["changeIndex"].type != Json.Type.int_) {
      feature["info"]["changeIndex"] = 0;
    }

    decorators.changeIndex = feature["info"]["changeIndex"].to!size_t;

    if("minZoom" in icon && icon["minZoom"].type == Json.Type.int_) {
      decorators.minZoom = icon["minZoom"].to!uint;
    } else {
      log("Invalid icon.minZoom value icon._id = %s", icon["_id"]);
    }

    if("maxZoom" in icon && icon["maxZoom"].type == Json.Type.int_) {
      decorators.maxZoom = icon["maxZoom"].to!uint;
    } else {
      log("Invalid icon.maxZoom value icon._id = %s", icon["_id"]);
    }

    if("keepWhenSmall" in icon && icon["keepWhenSmall"].type == Json.Type.bool_) {
      decorators.keepWhenSmall = icon["keepWhenSmall"].to!bool;
    } else {
      log("Invalid icon.keepWhenSmall value icon._id = %s", icon["_id"]);
    }

    auto flatIcon = icon.toFlatJson;

    auto requiredFlags = ["styles.types.lineMarker.isVisible", "styles.types.polygonMarker.isVisible", "styles.types.polygonLabel.isVisible", "styles.types.polygon.showAsLineAfterZoom"];

    foreach (flag; requiredFlags) {
      if(flag!in flatIcon) {
        flatIcon[flag] = false;
      }
    }

    if(flatIcon["styles.types.lineMarker.isVisible"].type == Json.Type.bool_) {
      decorators.showLineMarkers = flatIcon["styles.types.lineMarker.isVisible"].to!bool;
    } else {
      log("Invalid styles.types.lineMarker.isVisible value icon._id = %s", icon["_id"]);
    }

    if("styles.types.polygon.showAsLineAfterZoom" in flatIcon && flatIcon["styles.types.polygon.showAsLineAfterZoom"].type == Json.Type.int_) {
      decorators.showAsLineAfterZoom = flatIcon["styles.types.polygon.showAsLineAfterZoom"].to!uint;
    } else {
      log("Invalid styles.types.polygon.showAsLineAfterZoom value icon._id = %s", icon["_id"]);
    }

    bool addCenter = flatIcon["styles.types.polygonMarker.isVisible"] == true || flatIcon["styles.types.polygonLabel.isVisible"] == true;

    if(!addCenter) {
      decorators.center = [];
    }

    if("position" in feature && feature["position"].type == Json.Type.object) {
      if(addCenter && feature["position"]["type"] == "Polygon") {
        auto polygon = feature["position"]["coordinates"].deserializeJson!(double[][][]);

        decorators.center = polygon[0].visualCenter;
      }

      if(addCenter && feature["position"]["type"] == "MultiPolygon") {
        auto polygon = feature["position"]["coordinates"].deserializeJson!(double[][][][]);

        decorators.center = polygon[0][0].visualCenter;
      }
    }

    feature["decorators"] = decorators.serializeToJson;
    crates.feature.updateItem(feature);
  }
}