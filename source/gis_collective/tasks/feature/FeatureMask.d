/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.feature.FeatureMask;

import geo.json;
import geo.geometries;
import geo.algorithm;

import vibe.data.json;
import vibe.core.log;

import std.datetime;
import std.algorithm;
import std.array;
import std.conv;
import std.math;
import std.exception;

import crate.base;
import vibe.service.stats;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import ogm.crates.all;
import ogm.models.meta;
import ogm.meta;
import ogm.features.simplify;

version(unittest) {
  import fluent.asserts;
  import ogm.test.fixtures;
}

struct FeatureMask {
  struct Request {
    ObjectId id;
  }

  enum type = "Feature.Mask";
  alias Model = Feature;

  OgmCrates crates;
  IBroadcast broadcast;

  private string getMaskingDecimalsCount() {
    auto range = this.crates.preference.get.where("name").equal("locationServices.maskingPrecision").and.exec;

    if(range.empty) {
      return "0";
    }

    return range.front["value"].to!string;
  }

  private double getMaskingDecimalPrecision() {
    double result = 1;
    auto decimalsCount = getMaskingDecimalsCount().to!int;

    if(decimalsCount == 0) {
      return result;
    }

    try {
      for(int i = 0; i < decimalsCount; ++i) {
        result = result / 10;
      }
    } catch (Exception e) {
      logError("Can't get the masking precision value : %s", e.toString);
    }
    return result;
  }

  static Json[] processList(const ChangeSet changeSet) {
    if(changeSet.model != Model.stringof) {
      return [];
    }

    if(changeSet.type == CrateChangeType.delete_) {
      return [];
    }

    return [ changeSet.itemId.toJson ];
  }

  void main(Request request) {
    string logPrefix = "[" ~ type ~ "][" ~ request.id.toString ~ "]";

    auto featureRange = crates.feature.getItem(request.id.to!string).and.exec;
    enforce(!featureRange.empty, "There is no feature with id `" ~ request.id.to!string ~ "`");

    auto feature = featureRange.front;
    auto jsonGeometry = feature["unmasked"].type == Json.Type.object ? feature["unmasked"] : feature["position"];
    auto maskType = "";

    auto geometry = GeoJsonGeometry.fromJson(jsonGeometry);
    auto mapIds = ObjectId.fromJsonList(feature["maps"]);

    auto maskedMaps = crates.map.get
      .where("_id").anyOf(mapIds).and
      .where("mask.isEnabled").equal(true).and
      .exec
      .filter!(a => a["mask"].type == Json.Type.object)
      .filter!(a => a["mask"]["map"].type == Json.Type.string)
      .map!(a => ObjectId.fromJson(a["mask"]["map"]))
      .array;

    if(maskedMaps.length > 0) {
      auto bbox = BBox!double(geometry);
      auto center = bbox.center;

      logInfo("%s Add mask to feature with center at %s", logPrefix, center);

      auto maskRange = crates.feature.get
        .where("maps").anyOf(maskedMaps).and
        .where("position").containsPoint(center[0], center[1]).and
        .limit(1)
        .exec;

      if(!maskRange.empty) {
        auto mask = maskRange.front;
        auto maskGeometry = GeoJsonGeometry.fromJson(mask["position"]);

        if(maskGeometry.type == "Polygon") {
          auto coordinates = maskGeometry.coordinates.deserializeJson!(Position[][]);

          if(coordinates.length > 0) {
            center = coordinates[0].centroid.toList;
            logInfo("%s masking using the mask BBOX centroid: %s", logPrefix, center);
          } else {
            center = [ double.nan, double.nan ];
          }
        } else {
          center = BBox!double(maskGeometry).center;
          logInfo("%s masking using the mask BBOX center: %s", logPrefix, center);
        }

        maskType = "index map";
      }

      if(maskRange.empty || center[0] == double.nan || center[1] == double.nan) {
        auto roundingUnit = getMaskingDecimalPrecision();
        center = center.map!(a => a.quantize!floor(roundingUnit)).array;

        logInfo("%s masking using the feature BBOX center truncation: %s", logPrefix, center);
        maskType = getMaskingDecimalsCount();
      }

      feature["unmasked"] = jsonGeometry.clone;
      feature["position"] = Json.emptyObject;
      feature["position"]["type"] = "Point";
      feature["position"]["coordinates"] = Json.emptyArray;
      feature["position"]["coordinates"].appendArrayElement(Json(center[0]));
      feature["position"]["coordinates"].appendArrayElement(Json(center[1]));

      if("attributes" !in feature) {
        feature["attributes"] = Json.emptyObject;
      }

      if("mask" !in feature["attributes"]) {
        feature["attributes"]["mask"] = Json.emptyObject;
      }
      feature["attributes"]["mask"]["type"] = maskType;

      feature["unmaskedBox"] = feature["unmasked"].featureBox;
      feature["positionBox"] = feature["position"].featureBox;

      crates.feature.updateItem(feature);
    } else if(feature["unmasked"].type == Json.Type.object) {
      logInfo("%s Removing mask from feature.", logPrefix);
      feature["position"] = feature["unmasked"];
      feature["positionBox"] = feature["position"].featureBox;

      feature.remove("unmasked");
      feature.remove("unmaskedBox");

      if("attributes" in feature && "mask") {
        feature["attributes"].remove("mask");
      }

      crates.feature.updateItem(feature);
    }

    logInfo("%s Done");
  }
}
