/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.feature.update;

import std.array;
import std.functional;
import std.range;
import std.algorithm;
import std.datetime;
import std.conv;
import ogm.features.visibility;

import ogm.crates.all;
import ogm.models.feature;

import vibe.data.json;
import vibe.data.bson;
import vibe.stream.wrapper;

import crate.base;
import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;

import vibe.core.log;
import crate.json;
import crate.base;

version(unittest) {
  import std.string;
  import fluent.asserts;
  import ogm.test.fixtures;
  import gis_collective.hmq.broadcast.memory;
}

struct FeatureUpdate {
  enum type = "Feature.update";
  alias Model = Feature;

  Json author;
  Json value;
  OgmCrates crates;
  IBroadcast broadcast;

  private {
    string _id;
  }

  void log(T...)(string message, T params) {
    logInfo("[%s %s] " ~ message, type, _id, params);
  }

  void start() {
    bool exists;

    if("_id" in value) {
      _id = "new";

      if(isObjectId(value["_id"])) {
        exists = crates.feature.get.where("_id").equal(ObjectId.fromString(value["_id"].to!string)).and.size > 0;
      }

      if(exists) {
        _id = value["_id"].to!string;
      }

      log("Feature exists? %s", exists);
    }

    if("info" in value && "author" in value["info"]) {
      author = value["info"]["author"];
    }

    if(exists) {
      auto originalItem = crates.feature.getItem(value["_id"].to!string).exec.front;
      value["info"] = originalItem["info"];
      value = mix(originalItem.clone, value);

      if(value == originalItem) {
        log("The item matches the previous one. No UPDATE!");
        return;
      }
    } else {
      value.remove("info");
      value.remove("_id");
    }

    if("name" in value && value["name"].type != Json.Type.string) {
      value["name"] = value["name"].to!string;
    }

    if("description" in value && value["description"].type != Json.Type.string) {
      value["description"] = value["description"].to!string;
    }

    if("visibility" in value && value["visibility"].type != Json.Type.int_) {
      value["visibility"] = value["visibility"].to!string.to!int;
    }

    value["computedVisibility"] = crates.getComputedVisibility(value);

    auto feature = LazyFeature.fromModel(LazyFeature(value, toDelegate(&itemResolver)).toType);
    auto jsonFeature = feature.toJson;

    if(author.type == Json.Type.string) {
      jsonFeature["info"]["author"] = author;
      Json[] contributors = cast(Json[]) jsonFeature["contributors"];
      contributors ~= author;

      jsonFeature["contributors"] = Json(contributors.uniq.array);

      if(!exists) {
        jsonFeature["info"]["originalAuthor"] = author;
      }
    }

    auto now = Clock.currTime.toUTC;
    now.fracSecs = Duration.zero;

    if(exists) {
      size_t nextIndex;
      try {
        nextIndex = jsonFeature["info"]["changeIndex"].to!ulong;
      } catch(Exception e) {
        error(e);
      }

      jsonFeature["info"]["changeIndex"] = nextIndex + 1;
    } else {
      jsonFeature["info"]["createdOn"] = now.toISOExtString;
    }

    jsonFeature["info"]["lastChangeOn"] = now.toISOExtString;

    if(exists) {
      log("Feature Update: %s", jsonFeature);
      crates.feature.updateItem(jsonFeature);
    } else {
      log("Feature add: %s", jsonFeature);
      crates.feature.addItem(jsonFeature);
    }
  }
}

version(unittest) {

  Json receivedValue;
  bool isReceived;

  void beforeTest() {
    setupTestData();

    receivedValue = Json();
    isReceived = false;

    crateGetters["Map"] = &crates.map.getItem;
    crateGetters["Team"] = &crates.team.getItem;
    crateGetters["Picture"] = &crates.picture.getItem;
    crateGetters["IconSet"] = &crates.iconSet.getItem;
    crateGetters["Icon"] = &crates.icon.getItem;
  }
}

/// It should add a missing feature
unittest {
  beforeTest();

  FeatureUpdate featureUpdate;
  featureUpdate.crates = crates;

  featureUpdate.value = `{
    "_id": "000000000000000000000011",
    "contributors": [],
    "maps": [ "000000000000000000000001" ],
    "name": "site1",
    "visibility": 1,
    "icons": [ "000000000000000000000001" ],
    "description": "description of site1",
    "attributes": {},
    "position": {
      "type": "Point",
      "coordinates": [ 1.5, 1.5 ]
    },
    "pictures": []}`.parseJsonString;

  auto before = crates.feature.get.size;
  featureUpdate.start;

  crates.feature.get.size.should.equal(before + 1);

  auto item = crates.feature.get.skip(before).exec.front;

  item.should.equal(parseJsonString(`{
  "_id": "000000000000000000000005",
  "info": {
    "changeIndex": 0,
    "createdOn": "` ~ item["info"]["createdOn"].to!string ~ `",
    "lastChangeOn": "` ~ item["info"]["createdOn"].to!string ~ `",
    "originalAuthor": "",
    "author": ""
  },
  "decorators": {
    "showAsLineAfterZoom": 99,
    "useDefault": true,
    "changeIndex": 0,
    "maxZoom": 20,
    "minZoom": 0,
    "showLineMarkers": false,
    "keepWhenSmall": true,
    "center": []
  },
  "source": {
    "type": "",
    "remoteId": "",
    "modelId": "",
    "syncAt": "0001-01-01T00:00:00+00:00"
  },
  "computedVisibility":{"isDefault":false,"isPublic":true,"team":"000000000000000000000001"},
  "contributors": [],
  "isLarge": false,
  "maps": [ "000000000000000000000001" ],
  "name": "site1",
  "visibility": 1,
  "icons": [ "000000000000000000000001" ],
  "description": "description of site1",
  "attributes": {},
  "position": { "type": "Point", "coordinates": [1.5, 1.5] },
  "sounds": [],
  "pictures": []}`));
}

/// It should add a site with an invalid id
unittest {
  beforeTest();

  FeatureUpdate featureUpdate;
  featureUpdate.crates = crates;

  featureUpdate.value = `{
    "_id": "3f62d7ef-027e-434a-b2de-13558ae2074b",
    "contributors": [],
    "maps": [ "000000000000000000000001" ],
    "name": "site1",
    "visibility": 1,
    "icons": [ "000000000000000000000001" ],
    "description": "description of site1",
    "attributes": {},
    "decorators": {
      "showAsLineAfterZoom": 99,
      "useDefault": true,
      "changeIndex": 0,
      "maxZoom": 20,
      "minZoom": 0,
      "showLineMarkers": false,
      "keepWhenSmall": true,
      "center": []
    },
    "position": {
      "type": "Point",
      "coordinates": [ 1.5, 1.5 ]
    },
    "pictures": []}`.parseJsonString;

  auto before = crates.feature.get.size;
  featureUpdate.start;

  crates.feature.get.size.should.equal(before + 1);

  auto item = crates.feature.get.skip(before).exec.front;

  item.should.equal(parseJsonString(`{
    "_id": "000000000000000000000005",
    "info": {
      "changeIndex": 0,
      "createdOn": "` ~ item["info"]["createdOn"].to!string ~ `",
      "lastChangeOn": "` ~ item["info"]["createdOn"].to!string ~ `",
      "originalAuthor": "",
      "author": ""
    },
    "decorators": {
      "showAsLineAfterZoom": 99,
      "useDefault": true,
      "changeIndex": 0,
      "maxZoom": 20,
      "minZoom": 0,
      "showLineMarkers": false,
      "keepWhenSmall": true,
      "center": []
    },
    "computedVisibility":{"isDefault":false,"isPublic":true,"team":"000000000000000000000001"},
    "contributors": [],
    "isLarge": false,
    "maps": [ "000000000000000000000001" ],
    "name": "site1",
    "visibility": 1,
    "icons": [ "000000000000000000000001" ],
    "description": "description of site1",
    "attributes": {},
    "source": {
      "type": "",
      "remoteId": "",
      "modelId": "",
      "syncAt": "0001-01-01T00:00:00+00:00"
    },
    "position": { "type": "Point", "coordinates": [1.5, 1.5] },
    "sounds": [],
    "pictures": []}`));
}

/// It should add a missing site and update the author if it's set
unittest {
  beforeTest();

  FeatureUpdate featureUpdate;
  featureUpdate.crates = crates;

  featureUpdate.value = `{
    "info": {
      "author": "000000000000000000000001"
    },
    "decorators": {
      "showAsLineAfterZoom": 99,
      "useDefault": true,
      "changeIndex": 0,
      "maxZoom": 20,
      "minZoom": 0,
      "showLineMarkers": false,
      "keepWhenSmall": true,
      "center": []
    },
    "contributors": [],
    "maps": [ "000000000000000000000001" ],
    "name": "site1",
    "visibility": 1,
    "icons": [ "000000000000000000000001" ],
    "description": "description of site1",
    "attributes": {},
    "position": {
      "type": "Point",
      "coordinates": [ 1.5, 1.5 ]
    },
    "pictures": []}`.parseJsonString;

  auto before = crates.feature.get.size;
  featureUpdate.start;

  crates.feature.get.size.should.equal(before + 1);

  auto item = crates.feature.get.skip(before).exec.front;

  item.should.equal(parseJsonString(`{
  "_id": "000000000000000000000005",
  "info": {
    "changeIndex": 0,
    "createdOn": "` ~ item["info"]["createdOn"].to!string ~ `",
    "lastChangeOn": "` ~ item["info"]["createdOn"].to!string ~ `",
    "originalAuthor": "000000000000000000000001",
    "author": "000000000000000000000001"
  },
  "decorators": {
    "showAsLineAfterZoom": 99,
    "useDefault": true,
    "changeIndex": 0,
    "maxZoom": 20,
    "minZoom": 0,
    "showLineMarkers": false,
    "keepWhenSmall": true,
    "center": []
  },
  "source": {
    "type": "",
    "remoteId": "",
    "modelId": "",
    "syncAt": "0001-01-01T00:00:00+00:00"
  },
  "computedVisibility":{"isDefault":false,"isPublic":true,"team":"000000000000000000000001"},
  "contributors": ["000000000000000000000001"],
  "isLarge": false,
  "maps": [ "000000000000000000000001" ],
  "name": "site1",
  "visibility": 1,
  "icons": [ "000000000000000000000001" ],
  "description": "description of site1",
  "attributes": {},
  "position": { "type": "Point", "coordinates": [1.5, 1.5] },
  "sounds": [],
  "pictures": []}`));
}

/// It should update an existing site
unittest {
  beforeTest();

  FeatureUpdate featureUpdate;
  featureUpdate.crates = crates;

  featureUpdate.value = `{
    "_id": "000000000000000000000001",
    "contributors": [],
    "maps": [ "000000000000000000000001" ],
    "name": "site1",
    "visibility": 1,
    "icons": [ "000000000000000000000001" ],
    "description": "other description of site1",
    "attributes": {},
    "decorators": {
      "showAsLineAfterZoom": 99,
      "useDefault": true,
      "changeIndex": 0,
      "maxZoom": 20,
      "minZoom": 0,
      "showLineMarkers": false,
      "keepWhenSmall": true,
      "center": []
    },
    "position": {
      "type": "Point",
      "coordinates": [ 1.5, 1.5 ]
    },
    "pictures": []}`.parseJsonString;

  auto before = crates.feature.get.size;
  auto originalItem = crates.feature.getItem("000000000000000000000001").exec.front;

  featureUpdate.start;

  crates.feature.get.size.should.equal(before);

  auto item = crates.feature.getItem("000000000000000000000001").exec.front;

  item.should.equal(parseJsonString(`{
  "_id": "000000000000000000000001",
  "info": {
    "changeIndex": 1,
    "createdOn": "` ~ originalItem["info"]["createdOn"].to!string ~ `",
    "lastChangeOn": "` ~ item["info"]["lastChangeOn"].to!string ~ `",
    "originalAuthor": "000000000000000000000001",
    "author": ""
  },
  "contributors": [],
  "isLarge": false,
  "maps": [ "000000000000000000000001" ],
  "name": "site1",
  "computedVisibility":{"isDefault":false,"isPublic":true,"team":"000000000000000000000001"},
  "visibility": 1,
  "icons": [ "000000000000000000000001" ],
  "description": "other description of site1",
  "decorators": {
    "showAsLineAfterZoom": 99,
    "useDefault": true,
    "changeIndex": 0,
    "maxZoom": 20,
    "minZoom": 0,
    "showLineMarkers": false,
    "keepWhenSmall": true,
    "center": []
  },
  "source": {
    "type": "",
    "remoteId": "",
    "modelId": "",
    "syncAt": "0001-01-01T00:00:00+00:00"
  },
  "attributes": {
    "name1": {
      "program": "Luni-Vineri:9-18",
      "phone": "123456",
      "max number of people": "23",
      "kids-friendly": "true",
      "type of food": "international",
      "price": "11.4" }},
  "position": { "type": "Point", "coordinates": [1.5, 1.5] },
  "positionBox":{"type":"Point","coordinates":[1.5,1.5]},
  "sounds": [],
  "pictures": []}`));
}

/// It should update an existing site with a numeric name
unittest {
  beforeTest();

  FeatureUpdate featureUpdate;
  featureUpdate.crates = crates;

  featureUpdate.value = `{
    "_id": "000000000000000000000001",
    "contributors": [],
    "maps": [ "000000000000000000000001" ],
    "name": 1,
    "visibility": 1,
    "icons": [ "000000000000000000000001" ],
    "description": "other description of site1",
    "attributes": {},
    "position": {
      "type": "Point",
      "coordinates": [ 1.5, 1.5 ]
    },
    "pictures": []}`.parseJsonString;

  auto before = crates.feature.get.size;
  auto originalItem = crates.feature.getItem("000000000000000000000001").exec.front;

  featureUpdate.start;

  crates.feature.get.size.should.equal(before);

  auto item = crates.feature.getItem("000000000000000000000001").exec.front;

  item["name"].should.equal(Json("1"));
}

/// It should update an existing site with string visibility
unittest {
  beforeTest();

  FeatureUpdate featureUpdate;
  featureUpdate.crates = crates;

  featureUpdate.value = `{
    "_id": "000000000000000000000001",
    "contributors": [],
    "maps": [ "000000000000000000000001" ],
    "name": "test",
    "visibility": 1,
    "icons": [ "000000000000000000000001" ],
    "description": "other description of site1",
    "attributes": {},
    "position": {
      "type": "Point",
      "coordinates": [ 1.5, 1.5 ]
    },
    "pictures": []}`.parseJsonString;

  auto before = crates.feature.get.size;
  auto originalItem = crates.feature.getItem("000000000000000000000001").exec.front;

  featureUpdate.start;

  crates.feature.get.size.should.equal(before);

  auto item = crates.feature.getItem("000000000000000000000001").exec.front;

  item["visibility"].should.equal(Json(1));
}

/// It should update an existing site with string isPublished
unittest {
  beforeTest();

  FeatureUpdate featureUpdate;
  featureUpdate.crates = crates;

  featureUpdate.value = `{
    "_id": "000000000000000000000001",
    "contributors": [],
    "maps": [ "000000000000000000000001" ],
    "name": "test",
    "visibility": 1,
    "icons": [ "000000000000000000000001" ],
    "description": 1,
    "attributes": {},
    "position": {
      "type": "Point",
      "coordinates": [ 1.5, 1.5 ]
    },
    "pictures": []}`.parseJsonString;

  auto before = crates.feature.get.size;
  auto originalItem = crates.feature.getItem("000000000000000000000001").exec.front;

  featureUpdate.start;

  crates.feature.get.size.should.equal(before);

  auto item = crates.feature.getItem("000000000000000000000001").exec.front;

  item["description"].should.equal(Json("1"));
}

/// It should add the author for existing sites
unittest {
  beforeTest();

  FeatureUpdate featureUpdate;
  featureUpdate.crates = crates;

  featureUpdate.value = `{
    "_id": "000000000000000000000001",
    "info": {
      "author": "000000000000000000000001"
    },
    "decorators": {
      "showAsLineAfterZoom": 99,
      "useDefault": true,
      "changeIndex": 0,
      "maxZoom": 20,
      "minZoom": 0,
      "showLineMarkers": false,
      "keepWhenSmall": true,
      "center": []
    },
    "contributors": [],
    "maps": [ "000000000000000000000001" ],
    "name": "site1",
    "visibility": 1,
    "icons": [ "000000000000000000000001" ],
    "description": "other description of site1",
    "attributes": {},
    "position": {
      "type": "Point",
      "coordinates": [ 1.5, 1.5 ]
    },
    "pictures": []}`.parseJsonString;

  auto before = crates.feature.get.size;
  auto originalItem = crates.feature.getItem("000000000000000000000001").exec.front;

  featureUpdate.start;

  crates.feature.get.size.should.equal(before);

  auto item = crates.feature.getItem("000000000000000000000001").exec.front;

  item.should.equal(parseJsonString(`{
  "_id": "000000000000000000000001",
  "info": {
    "changeIndex": 1,
    "createdOn": "` ~ originalItem["info"]["createdOn"].to!string ~ `",
    "lastChangeOn": "` ~ item["info"]["lastChangeOn"].to!string ~ `",
    "originalAuthor": "000000000000000000000001",
    "author": "000000000000000000000001"
  },
  "computedVisibility":{"isDefault":false,"isPublic":true,"team":"000000000000000000000001"},
  "contributors": [ "000000000000000000000001" ],
  "isLarge": false,
  "maps": [ "000000000000000000000001" ],
  "name": "site1",
  "visibility": 1,
  "icons": [ "000000000000000000000001" ],
  "description": "other description of site1",
  "source": {
    "type": "",
    "remoteId": "",
    "modelId": "",
    "syncAt": "0001-01-01T00:00:00+00:00"
  },
  "attributes": {
    "name1": {
      "program": "Luni-Vineri:9-18",
      "phone": "123456",
      "max number of people": "23",
      "kids-friendly": "true",
      "type of food": "international",
      "price": "11.4" }},
  "decorators": {
    "showAsLineAfterZoom": 99,
    "useDefault": true,
    "changeIndex": 0,
    "maxZoom": 20,
    "minZoom": 0,
    "showLineMarkers": false,
    "keepWhenSmall": true,
    "center": []
  },
  "position": { "type": "Point", "coordinates": [1.5, 1.5] },
  "positionBox":{"type":"Point","coordinates":[1.5,1.5]},
  "sounds": [],
  "pictures": []}`));
}

/// It should not update an existing site if there is no change
unittest {
  beforeTest();

  FeatureUpdate featureUpdate;
  featureUpdate.crates = crates;
  auto originalItem = crates.feature.getItem("000000000000000000000001").exec.front;

  featureUpdate.value = originalItem;
  auto before = crates.feature.get.size;

  featureUpdate.start;
  crates.feature.get.size.should.equal(before);

  auto item = crates.feature.getItem("000000000000000000000001").exec.front;
  item.should.equal(originalItem);
}