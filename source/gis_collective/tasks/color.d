/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.color;

import std.string;
import std.algorithm;
import std.conv;
import std.array;
import std.format;


version(unittest) {
  import fluent.asserts;
}

struct Color {
  double red = 0;
  double green = 0;
  double blue = 0;
  double opacity = 0;

  this(string color) {
    auto k = color.toLower;

    if(k in predefinedColors) {
      readHex(predefinedColors[k]);
    }

    if(color[0] == '#' && color.length >= 4) {
      readHex(color);
    }

    if(color.toLower.indexOf("rgb") == 0) {
      readRgb(color);
    }
  }

  void readHex(string color) {
    string r = "0";
    string g = "0";
    string b = "0";

    if(color.length == 4) {
      r = color[1..2] ~ color[1..2];
      g = color[2..3] ~ color[2..3];
      b = color[3..4] ~ color[3..4];
    }

    if(color.length == 7) {
      r = color[1..3];
      g = color[3..5];
      b = color[5..7];
    }

    auto spec = singleSpec("%X");
    red = r.unformatValue!int(spec);
    green = g.unformatValue!int(spec);
    blue = b.unformatValue!int(spec);
    opacity = 1;
  }

  void readRgb(string color) {
    auto start = color.indexOf("(") + 1;
    auto end = color.indexOf(")");

    const pieces = color[start..end].split(",").map!(a => a.strip.to!double).array;

    if(pieces.length > 0) {
      red = pieces[0];
    } else {
      red = 0;
    }

    if(pieces.length > 1) {
      green = pieces[1];
    } else {
      green = 0;
    }

    if(pieces.length > 2) {
      blue = pieces[2];
    } else {
      blue = 0;
    }

    if(pieces.length > 3) {
      opacity = pieces[3];
    } else {
      opacity = 1;
    }
  }

  string cssColor() {
    if(red == 0 && green == 0 && blue == 0 && opacity == 0) {
      return "transparent";
    }

    if(opacity == 1) {
      return "rgb(" ~ red.to!string ~ ", " ~ green.to!string ~ ", " ~ blue.to!string ~ ")";
    }

    if(opacity != 1) {
      return "rgba(" ~ red.to!string ~ ", " ~ green.to!string ~ ", " ~ blue.to!string ~ ", " ~ opacity.to!string ~ ")";
    }

    return "";
  }

  string hexColor() {
    return "#" ~ toHex(red.to!int) ~ toHex(green.to!int) ~ toHex(blue.to!int);
  }
}

string toHex(int value) {
  return format("%x", value).rightJustify(2, '0');
}

/// an empty color should be transparent
unittest {
  Color().cssColor.should.equal("transparent");
  Color().hexColor.should.equal("#000000");
  Color().opacity.should.equal(0);
  Color().red.should.equal(0);
  Color().green.should.equal(0);
  Color().blue.should.equal(0);
}

/// it can parse a rgb color
unittest {
  auto color = Color("rgb(1,2,3)");
  color.cssColor.should.equal("rgb(1, 2, 3)");
  color.hexColor.should.equal("#010203");
  color.opacity.should.equal(1);
  color.red.should.equal(1);
  color.green.should.equal(2);
  color.blue.should.equal(3);
}

/// it can parse a rgba color
unittest {
  auto color = Color("rgba(201,202,203,0.5)");
  color.cssColor.should.equal("rgba(201, 202, 203, 0.5)");
  color.hexColor.should.equal("#c9cacb");
  color.opacity.should.equal(0.5);
  color.red.should.equal(201);
  color.green.should.equal(202);
  color.blue.should.equal(203);
}

/// it sets an invalid rgba as black
unittest {
  auto color = Color("rgba()");
  color.cssColor.should.equal("rgb(0, 0, 0)");
  color.hexColor.should.equal("#000000");
  color.opacity.should.equal(1);
  color.red.should.equal(0);
  color.green.should.equal(0);
  color.blue.should.equal(0);
}

/// can parse a hex color with 6 chars
unittest {
  auto color = Color("#aabbcc");
  color.cssColor.should.equal("rgb(170, 187, 204)");
  color.hexColor.should.equal("#aabbcc");
  color.opacity.should.equal(1);
  color.red.should.equal(170);
  color.green.should.equal(187);
  color.blue.should.equal(204);
}

/// can parse a hex color with 3 chars
unittest {
  auto color = Color("#fff");
  color.cssColor.should.equal("rgb(255, 255, 255)");
  color.hexColor.should.equal("#ffffff");
  color.opacity.should.equal(1);
  color.red.should.equal(255);
  color.green.should.equal(255);
  color.blue.should.equal(255);
}

/// can parse a named color
unittest {
  auto color = Color("burlywood");
  color.cssColor.should.equal("rgb(222, 184, 135)");
  color.hexColor.should.equal("#deb887");
  color.opacity.should.equal(1);
  color.red.should.equal(222);
  color.green.should.equal(184);
  color.blue.should.equal(135);
}

enum string[string] predefinedColors = [
  "aliceblue": "#f0f8ff",
  "antiquewhite": "#faebd7",
  "aqua": "#00ffff",
  "aquamarine": "#7fffd4",
  "azure": "#f0ffff",
  "beige": "#f5f5dc",
  "bisque": "#ffe4c4",
  "black": "#000000",
  "blanchedalmond": "#ffebcd",
  "blue": "#0000ff",
  "blueviolet": "#8a2be2",
  "brown": "#a52a2a",
  "burlywood": "#deb887",
  "cadetblue": "#5f9ea0",
  "chartreuse": "#7fff00",
  "chocolate": "#d2691e",
  "coral": "#ff7f50",
  "cornflowerblue": "#6495ed",
  "cornsilk": "#fff8dc",
  "crimson": "#dc143c",
  "cyan": "#00ffff",
  "darkblue": "#00008b",
  "darkcyan": "#008b8b",
  "darkgoldenrod": "#b8860b",
  "darkgray": "#a9a9a9",
  "darkgreen": "#006400",
  "darkgrey": "#a9a9a9",
  "darkkhaki": "#bdb76b",
  "darkmagenta": "#8b008b",
  "darkolivegreen": "#556b2f",
  "darkorange": "#ff8c00",
  "darkorchid": "#9932cc",
  "darkred": "#8b0000",
  "darksalmon": "#e9967a",
  "darkseagreen": "#8fbc8f",
  "darkslateblue": "#483d8b",
  "darkslategray": "#2f4f4f",
  "darkslategrey": "#2f4f4f",
  "darkturquoise": "#00ced1",
  "darkviolet": "#9400d3",
  "deeppink": "#ff1493",
  "deepskyblue": "#00bfff",
  "dimgray": "#696969",
  "dimgrey": "#696969",
  "dodgerblue": "#1e90ff",
  "firebrick": "#b22222",
  "floralwhite": "#fffaf0",
  "forestgreen": "#228b22",
  "fuchsia": "#ff00ff",
  "gainsboro": "#dcdcdc",
  "ghostwhite": "#f8f8ff",
  "goldenrod": "#daa520",
  "gold": "#ffd700",
  "gray": "#808080",
  "green": "#008000",
  "greenyellow": "#adff2f",
  "grey": "#808080",
  "honeydew": "#f0fff0",
  "hotpink": "#ff69b4",
  "indianred": "#cd5c5c",
  "indigo": "#4b0082",
  "ivory": "#fffff0",
  "khaki": "#f0e68c",
  "lavenderblush": "#fff0f5",
  "lavender": "#e6e6fa",
  "lawngreen": "#7cfc00",
  "lemonchiffon": "#fffacd",
  "lightblue": "#add8e6",
  "lightcoral": "#f08080",
  "lightcyan": "#e0ffff",
  "lightgoldenrodyellow": "#fafad2",
  "lightgray": "#d3d3d3",
  "lightgreen": "#90ee90",
  "lightgrey": "#d3d3d3",
  "lightpink": "#ffb6c1",
  "lightsalmon": "#ffa07a",
  "lightseagreen": "#20b2aa",
  "lightskyblue": "#87cefa",
  "lightslategray": "#778899",
  "lightslategrey": "#778899",
  "lightsteelblue": "#b0c4de",
  "lightyellow": "#ffffe0",
  "lime": "#00ff00",
  "limegreen": "#32cd32",
  "linen": "#faf0e6",
  "magenta": "#ff00ff",
  "maroon": "#800000",
  "mediumaquamarine": "#66cdaa",
  "mediumblue": "#0000cd",
  "mediumorchid": "#ba55d3",
  "mediumpurple": "#9370db",
  "mediumseagreen": "#3cb371",
  "mediumslateblue": "#7b68ee",
  "mediumspringgreen": "#00fa9a",
  "mediumturquoise": "#48d1cc",
  "mediumvioletred": "#c71585",
  "midnightblue": "#191970",
  "mintcream": "#f5fffa",
  "mistyrose": "#ffe4e1",
  "moccasin": "#ffe4b5",
  "navajowhite": "#ffdead",
  "navy": "#000080",
  "oldlace": "#fdf5e6",
  "olive": "#808000",
  "olivedrab": "#6b8e23",
  "orange": "#ffa500",
  "orangered": "#ff4500",
  "orchid": "#da70d6",
  "palegoldenrod": "#eee8aa",
  "palegreen": "#98fb98",
  "paleturquoise": "#afeeee",
  "palevioletred": "#db7093",
  "papayawhip": "#ffefd5",
  "peachpuff": "#ffdab9",
  "peru": "#cd853f",
  "pink": "#ffc0cb",
  "plum": "#dda0dd",
  "powderblue": "#b0e0e6",
  "purple": "#800080",
  "rebeccapurple": "#663399",
  "red": "#ff0000",
  "rosybrown": "#bc8f8f",
  "royalblue": "#4169e1",
  "saddlebrown": "#8b4513",
  "salmon": "#fa8072",
  "sandybrown": "#f4a460",
  "seagreen": "#2e8b57",
  "seashell": "#fff5ee",
  "sienna": "#a0522d",
  "silver": "#c0c0c0",
  "skyblue": "#87ceeb",
  "slateblue": "#6a5acd",
  "slategray": "#708090",
  "slategrey": "#708090",
  "snow": "#fffafa",
  "springgreen": "#00ff7f",
  "steelblue": "#4682b4",
  "tan": "#d2b48c",
  "teal": "#008080",
  "thistle": "#d8bfd8",
  "tomato": "#ff6347",
  "turquoise": "#40e0d0",
  "violet": "#ee82ee",
  "wheat": "#f5deb3",
  "white": "#ffffff",
  "whitesmoke": "#f5f5f5",
  "yellow": "#ffff00",
  "yellowgreen": "#9acd32"
];