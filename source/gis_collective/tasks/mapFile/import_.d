/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.mapFile.import_;

import std.array;
import std.functional;
import std.range;
import std.algorithm;
import std.string;
import std.conv;
import std.datetime;
import std.stdio;

import gis_collective.tasks.error;

import ogm.batch.log;
import ogm.crates.all;
import ogm.models.mapFile;
import ogm.models.meta;
import ogm.models.modelinfo;
import ogm.http.request;
import ogm.meta;

import vibe.data.json;
import vibe.data.bson;
import vibe.stream.wrapper;

import crate.base;
import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import gis_collective.tasks.mapFile.deserializer.base;

import vibe.core.log;

struct ImportMessage {
  string id;
  string user;
  string mapId;
}

struct MapFileImport {
  enum type = "mapFile.import";
  enum logLimit = 10_000;

  alias Model = MapFile;

  OgmCrates crates;
  IBroadcast broadcast;
  Json[string] cacheValues;
  IBatchLog log;

  private {
    long runIndex;

    string messages;
    Meta mapFileMeta;
    string fileId;

    SysTime now;
    BatchJob job;
    RunEntry currentRun;

    bool isCancel;
  }

  bool isAdmin(string userId) {
    auto selector = crates.user.get;
    selector.where("_id").equal(ObjectId.fromString(userId));
    selector.where("scopes").arrayContains("admin");

    return selector.size > 0;
  }

  string email(string userId) {
    auto selector = crates.user.get;
    selector.where("_id").equal(ObjectId.fromString(userId));

    return selector.exec.front["email"].to!string;
  }

  UserSession session(string userId) {
    VibeContext *context = new VibeContext();
    context.addField("user_id", userId);
    context.addField("email", email(userId));

    if(isAdmin(userId)) {
      context.addField("isAdmin", "true");
    }

    auto requestUserData = RequestUserData(context, VibeParams());

    return requestUserData.session(crates);
  }

  void updateFeatureOwnership(ObjectId mapId, ref Json feature) {
    bool exists;

    if("_id" in feature) {
      exists = crates.feature.get.where("_id").equal(ObjectId.fromJson(feature["_id"])).and.size > 0;

      if(exists) {
        log("Already exists by id");
        auto originalFeature = crates.feature.getItem(feature["_id"].to!string).exec.front;
        feature["info"] = originalFeature["info"];
        feature["contributors"] = originalFeature["contributors"];

        return;
      } else {
        feature.remove("_id");
      }
    }

    if(feature["name"] != "") {
      exists = crates.feature.get
        .where("name").equal(feature["name"].to!string).and
        .where("maps").arrayContains(mapId).and
        .size > 0;

      if(exists) {
        auto originalFeatures = crates.feature.get
          .where("name").equal(feature["name"].to!string).and
          .where("maps").arrayContains(mapId).and
          .withProjection(["_id", "info", "contributors"])
          .exec.array;

        feature["_id"] = originalFeatures[0]["_id"];
        feature["info"] = originalFeatures[0]["info"];
        feature["contributors"] = originalFeatures[0]["contributors"];
      }
    }
  }

  void prepareJobRun() {
    scope metaQuery = new MetaQuery("MapFile", type, crates);
    mapFileMeta = metaQuery.get(fileId);

    runIndex = -1;

    foreach(index, run; job.runHistory) {
      enforce!TaskValidationException(run.status != BatchJobStatus.running, "The job is already running");

      if(run.status == BatchJobStatus.scheduled) {
        runIndex = index;
        break;
      }
    }

    if(runIndex == -1) {
      runIndex = job.runHistory.length;
      currentRun.runId = job._id.to!string ~ "." ~ runIndex.to!string;
      job.runHistory ~= currentRun;
    }

    if(mapFileMeta.data.type == Json.Type.object && "total" in mapFileMeta.data) {
      currentRun.total = mapFileMeta.data["total"].to!size_t;
    }

    currentRun.time = now;
    currentRun.status = BatchJobStatus.running;
  }

  bool canBegin() {
    bool exists = crates.batchJob.get.where("name").equal(type ~ " " ~ fileId).and.size > 0;

    if(!exists) {
      broadcast.push("mapFile.analyze", ["id": fileId].serializeToJson);
      return false;
    }

    now = Clock.currTime;
    now.fracSecs = 0.msecs;

    job = LazyBatchJob(crates.batchJob.get.where("name").equal(type ~ " " ~ fileId).and.exec.array[0], toDelegate(&itemResolver)).toType;

    if(log is null) {
      log = new CrateBatchLog(crates.log, job._id.toString);
    }

    prepareJobRun();

    updateMeta();
    return true;
  }

  void checkState() {
    auto storedJob = crates.batchJob.get.where("name").equal(type ~ " " ~ fileId).and.exec.array[0];

    if(storedJob["runHistory"].length > runIndex) {
      string status = storedJob["runHistory"][runIndex]["status"].to!string;

      isCancel = status == "cancelled" || status == "timeout";
      enforce(!isCancel, "The import status was changed: " ~ storedJob["runHistory"][runIndex]["status"].to!string);
    }
  }

  void updateMeta() nothrow {
    try {
      if(runIndex >= job.runHistory.length) {
        return;
      }

      now = Clock.currTime;
      now.fracSecs = 0.msecs;

      currentRun.ping = now;
      currentRun.duration = (currentRun.ping - currentRun.time).total!"seconds";

      if(currentRun.processed > currentRun.total) {
        currentRun.total = currentRun.processed;
      }

      job.runHistory[runIndex] = currentRun;

      crates.batchJob.updateItem(LazyBatchJob.fromModel(job).toJson);
    } catch(Exception err) {
      error(err);
    }
  }

  void main(ImportMessage importMessage) {
    fileId = importMessage.id;

    if(!canBegin()) {
      return;
    }

    log("Extracting the map file record.");

    auto mapFile = getFile(crates, importMessage.id);
    auto mapId = mapFile.map._id;
    auto deserializer = mapFile.file.deserialize;

    log("The file `" ~ mapFile.file.fileName ~ "` has content type: `" ~ mapFile.file.contentType ~ "`");

    enforce!TaskValidationException(deserializer !is null, "Could not find a deserializer for this content type: `" ~ mapFile.file.contentType ~ "`");

    auto userSession = this.session(importMessage.user);
    log("Using the user session for `" ~ importMessage.user ~ "`");

    Json maps = [ importMessage.mapId ].serializeToJson;
    Json feature = Json.emptyObject;

    string[string] fields;

    foreach (field; mapFile.options.fields) {
      fields[field.key] = field.destination;
    }

    size_t index;
    foreach(item; deserializer) {
      try {
        index++;
        extractFields(item, fields, feature);
        updateFeatureOwnership(mapId, feature);

        feature["icons"] = updateItems("icons", feature["icons"], crates.icon);
        feature["info"]["lastChangeOn"] = now.toISOExtString;
        feature["info"]["author"] = importMessage.user;
        feature["maps"] = maps;

        if("originalAuthor" !in feature["info"] || feature["info"]["originalAuthor"] == "") {
          feature["info"]["originalAuthor"] = importMessage.user;
        }

        if("_id" in feature) {
          crates.feature.updateItem(feature);
          log("[" ~ currentRun.processed.to!string ~ "] Updated: " ~ feature.to!string);
        } else {
          crates.feature.addItem(feature);
          log("[" ~ currentRun.processed.to!string ~ "] Added: " ~ feature.to!string);
        }

        currentRun.processed++;
      } catch(Exception e) {
        debug writeln(e);
        error(`[%s] error: %s`, index, e.message.to!string);
        log.error(`[` ~ index.to!string ~ `] ` ~ e.message.to!string);
        currentRun.errors++;
      }

      if(index % 50 == 0) {
        checkState;
        updateMeta;
      }
    }

    currentRun.status = BatchJobStatus.success;
    updateMeta();
  }

  Json[] updateItems(string name, Json items, CrateAccess crate) {
    if(items.type != Json.Type.array) {
      return [];
    }

    Json[] result;

    foreach(item; items) {
      auto value = updateItem(name, item, crate);

      if(value.type != Json.Type.null_ && value.type != Json.Type.undefined) {
        result ~= value;
      }
    }

    return result;
  }

  Json updateItem(string name, Json value, CrateAccess crate) {
    if(value.type == Json.Type.array || value.type == Json.Type.object) {
      return Json();
    }

    if(value == "") {
      return Json();
    }

    string key = name ~ "." ~ value.to!string;

    if(key in cacheValues) {
      return cacheValues[key];
    }

    auto query = crate.get;

    try {
      auto idRange = crate.get.withProjection(["_id"])
        .where("_id").equal(ObjectId.fromString(value.to!string)).and.exec;

      if(!idRange.empty) {
        return idRange.front["_id"];
      }
    } catch(Exception e) { }

    auto range = crate.get.withProjection(["_id"]).where("name").like(value.to!string).and.exec;

    if(range.empty) {
      cacheValues[key] = Json();
    } else {
      cacheValues[key] = range.front["_id"];
    }

    return cacheValues[key];
  }
}
