/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.mapFile.analyze;

import std.array;
import std.functional;
import std.range;
import std.algorithm;
import std.string;
import std.conv;


import ogm.crates.all;
import ogm.models.mapFile;
import ogm.models.feature;
import ogm.models.meta;
import ogm.http.request;
import ogm.meta;

import vibe.data.json;
import vibe.data.bson;
import vibe.stream.wrapper;

import crate.base;
import crate.json;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import gis_collective.tasks.error;
import gis_collective.tasks.mapFile.deserializer.base;
import gis_collective.tasks.mapFile.import_;
import gis_collective.tasks.GisTask;
import ogm.batch.log;

import ogm.models.mapFile;
import vibe.core.log;

struct MapFileAnalyzeRequest {
  string id;
}

struct MapFileAnalyze {
  enum type = "mapFile.analyze";
  alias Model = MapFile;

  string id;

  OgmCrates crates;
  MetaQuery metaQuery;

  IBatchLog log;

  @onChange
  Json[] processList(ChangeSet changeSet) {
    if(changeSet.type != CrateChangeType.add || changeSet.model != Model.stringof) {
      return [];
    }

    return [ changeSet.itemId.toJson ];
  }

  void main(MapFileAnalyzeRequest message) {
  }

/*
  void setMeta(string id, size_t total, bool isValid) {
    if(metaQuery is null) {
      metaQuery = new MetaQuery("MapFile", "mapFile.import", crates);
    }

    auto mapFileMeta = metaQuery.get(id);
    mapFileMeta.data = Json.emptyObject;
    mapFileMeta.data["total"] = total;
    mapFileMeta.data["isValid"] = isValid;

    crates.meta.updateItem(mapFileMeta.serializeToJson);
  }

  string getBatchJobId(MapFile mapFile) {
    auto jobKey = MapFileImport.type ~ " " ~ mapFile._id.toString;

    if(crates.batchJob.get.where("name").equal(jobKey).and.size == 0) {
      BatchJob job;
      job.visibility = mapFile.map.visibility;
      job.name = jobKey;

      auto jsonJob = LazyBatchJob.fromModel(job).toJson;
      crates.batchJob.addItem(jsonJob);
    }

    return crates.batchJob.get.where("name").equal(jobKey)
      .and.withProjection([ "_id" ]).exec.array[0]["_id"].to!string;
  }

  void main(MapFileAnalyzeRequest message) {
    auto mapFile = getFile(crates, message.id);
    auto deserializer = mapFile.file.deserialize;
    auto batchJobId = getBatchJobId(mapFile);

    if(log is null) {
      log = new CrateBatchLog(crates.log, batchJobId);
    }

    log.info("The file `" ~ message.id ~ "` has the content type `" ~ mapFile.file.contentType ~ "`.");
    log.info("The BatchJob id is `" ~ batchJobId ~ "`.");

    if(deserializer is null) {
      log.info("The file can not be deserialzied.");
      setMeta(message.id, 0, false);
      return;
    }

    size_t total;
    setMeta(message.id, total, true);

    string[string] fields;

    foreach(item; deserializer) {
      total++;

      foreach(string key, value; item) {
        if(key != "properties" && key !in fields) {
          fields[key] = guessTransformedKey(key);
          mapFile.options.fields ~= FieldMapping(key, guessTransformedKey(key));

          log.info("Found the key `" ~ key ~ "`. Mapping it to: " ~ fields[key]);
        }
      }
    }

    crates.mapFile.updateItem(LazyMapFile.fromModel(mapFile).toJson);

    setMeta(message.id, total, true);
  }*/
}
