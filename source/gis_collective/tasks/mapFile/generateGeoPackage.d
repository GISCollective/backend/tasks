/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.mapFile.generateGeoPackage;

import geo.geopackage.file;
import geo.geopackage.layer;

import ogm.crates.all;
import ogm.models.mapFile;
import ogm.models.map;

import vibe.data.json;
import vibe.core.file;
import vibe.stream.wrapper;

import crate.base;

import std.functional;
import std.path;
import std.file;
import std.uuid;
import std.uni;
import std.algorithm;

version(unittest) {
  import tests.fixtures;
  import fluent.asserts;
}

struct GenerateGeoPackageRequest {
  string id;
  string uuid;
  string timestamp;
}

struct GenerateGeoPackage {
  enum type = "mapFile.generateGeoPackage";
  alias Model = MapFile;

  OgmCrates crates;

  GeoPackage geoPackage;

  string[] addedLayers;
  Json[string] iconCache;
  GpkgLayer[string] layers;

  string getUniqName() {
    string name;

    do {
      name = randomUUID.toString ~ ".gpkg";
    } while(name.exists);

    return name;
  }

  GpkgColumn toColumn(Json attribute) {
    string type;

    switch(attribute["type"].to!string) {
      case "integer":
        type = "INTEGER";
        break;

      case "decimal":
        type = "REAL";
        break;

      default:
        type = "TEXT";
    }

    return GpkgColumn(attribute["name"].to!string, type);
  }

  GpkgLayer createIconLayer(string type, string iconId) {
    if(iconId !in iconCache) {
      iconCache[iconId] = crates.icon.getItem(iconId).exec.front;
    }

    Json icon = iconCache[iconId];

    string name = icon["name"].to!string;

    auto geometryColumn = GpkgColumn("geometry", type.toUpper);
    GpkgColumn[] columns = [];

    columns ~= GpkgColumn("_id", "TEXT");
    columns ~= GpkgColumn("name", "TEXT");
    columns ~= GpkgColumn("description", "TEXT");
    columns ~= GpkgColumn("visibility", "TEXT");

    foreach (attribute; icon["attributes"]) {
      columns ~= toColumn(attribute);
    }

    auto key = name ~ " " ~ type;

    if(addedLayers.canFind(key)) {
      key ~= " " ~ iconId;
    }

    addedLayers ~= key;
    return geoPackage.geometryLayers.create(key, 4326, 0, 0, geometryColumn, columns);
  }

  void addFeature(Json feature, string iconId) {
    string geometryType = feature["position"]["type"].to!string;
    string key = iconId ~ geometryType;

    if(key !in layers) {
      layers[key] = createIconLayer(geometryType, iconId);
    }

    string[string] attributes;
    attributes["_id"] = feature["_id"].to!string;
    attributes["name"] = feature["name"].to!string;
    attributes["description"] = feature["description"].to!string;
    attributes["visibility"] = feature["visibility"].to!string;

    string[] allowedFields = layers[key].fields;
    string[] names = [ iconCache[iconId]["name"].to!string, iconId ];

    if("otherNames" in iconCache[iconId]) {
      names ~= iconCache[iconId]["otherNames"].deserializeJson!(string[]);
    }

    foreach(name; names) {
      if(name !in feature["attributes"]) {
        continue;
      }

      foreach(string k, value; feature["attributes"][name]) {
        if(allowedFields.canFind(k) && k !in attributes) {
          attributes[k] = value.to!string;
        }
      }
    }

    layers[key].add(feature["position"], attributes);
  }

  void main(GenerateGeoPackageRequest message) {
    Map map = LazyMap(crates.map.getItem(message.id).exec.front, (&itemResolver).toDelegate).toType;

    auto fileName = getUniqName();
    scope(exit) fileName.remove;

    geoPackage = new GeoPackage(fileName);
    geoPackage.spatialRefSys.setupDefault;

    auto features = crates.feature.get.where("maps").arrayContains(map._id).and.exec;

    foreach(feature; features) {
      foreach (icon; feature["icons"]) {
        auto iconId = icon.to!string;
        addFeature(feature, iconId);
      }
    }

    geoPackage.close;

    MapFile mapFile;
    mapFile.map = map;
    mapFile.file = new MapFileResource();
    mapFile.file.fileName = fileName;
    mapFile.file.contentType = "application/geopackage+vnd.sqlite3";
    mapFile.file.read(createProxyStream(openFile(fileName, FileMode.read)));
    mapFile.file.update;

    auto serializedFile = mapFile.serializeToJson;
    serializedFile["map"] = message.id;
    serializedFile["options"]["uuid"] = message.uuid;

    crates.mapFile.addItem(serializedFile);
  }
}
