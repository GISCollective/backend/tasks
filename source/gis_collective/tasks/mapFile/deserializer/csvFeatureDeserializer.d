/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.mapFile.deserializer.csvFeatureDeserializer;

import ogm.test.fixtures;
import ogm.models.feature;
import std.range;
import std.algorithm;
import std.csv;
import crate.collection.csv;
import crate.base;
import vibe.core.log;

version(unittest) {
  import fluent.asserts;
  import ogm.test.fixtures;
}

class CsvFeatureDeserializer : InputRange!Json {
  private {
    InputRange!(string[string]) data;
    Head[string] header;
  }

  this(T)(T data) {
    this.data = csvReader!(string[string], Malformed.throwException)(data, null).inputRangeObject;
  }

  bool empty() {
    return data.empty;
  }

  Json front() {
    auto obj = Json.emptyObject;
    auto item = data.front;

    foreach(string key, string value; item) {
      obj[key] = value;
    }

    return obj;
  }

  void popFront() {
    data.popFront;
  }

  Json moveFront() {
    auto oldFront = front;
    popFront;

    return oldFront;
  }

  int opApply(scope int delegate(Json) dg) {
    int result;

    while(!this.empty) {
      auto item = front;

      result = dg(item);
      popFront;
    }

    return result;
  }

  int opApply(scope int delegate(size_t, Json) dg) {
    int result;
    size_t i;

    while(!this.empty) {
      auto item = front;

      result = dg(i, item);
      i++;
      popFront;
    }

    return result;
  }
}

/// it should convert to a new valid site
unittest {
  setupTestData();
  string csvData = `_id,maps,name,description,position.lon,position.lat,info.createdOn,info.lastChangeOn,info.changeIndex,info.author,info.originalAuthor,pictures,icons,isPublished,contributors,attributes` ~ "\n" ~
    `,000000000000000000000001,site1,description of site1,1.5,1.5,2015-01-01T00:00:00Z,2015-01-01T00:00:00Z,0,,,,000000000000000000000001,true,,"{""name1"":{""program"":""Luni-Vineri:9-18"",""phone"":""123456"",""kids-friendly"":""true"",""type of food"":""international"",""price"":""11.4"",""max number of people"":""23""}}"` ~ "\n";

  auto deserialize = new CsvFeatureDeserializer(csvData);
  deserialize.empty.should.equal(false);
  deserialize.front.should.equal(`{
    "info.originalAuthor": "",
    "info.changeIndex": "0",
    "info.lastChangeOn": "2015-01-01T00:00:00Z",
    "maps": "000000000000000000000001",
    "info.author": "",
    "name": "site1",
    "_id": "",
    "contributors": "",
    "isPublished": "true",
    "icons": "000000000000000000000001",
    "description": "description of site1",
    "attributes": "{\"name1\":{\"program\":\"Luni-Vineri:9-18\",\"phone\":\"123456\",\"kids-friendly\":\"true\",\"type of food\":\"international\",\"price\":\"11.4\",\"max number of people\":\"23\"}}",
    "pictures": "",
    "position.lon": "1.5",
    "info.createdOn": "2015-01-01T00:00:00Z",
    "position.lat": "1.5"
  }`.parseJsonString);

  deserialize.popFront;
  deserialize.empty.should.equal(true);
}

/// it should convert lists of icons,maps and pictures to a new valid site
unittest {
  setupTestData();
  string csvData = `_id,maps,name,description,position.lon,position.lat,info.createdOn,info.lastChangeOn,info.changeIndex,info.author,info.originalAuthor,pictures,icons,isPublished,contributors,attributes` ~ "\n" ~
    `,000000000000000000000001;000000000000000000000002,site1,description of site1,1.5,1.5,2015-01-01T00:00:00Z,2015-01-01T00:00:00Z,0,,,000000000000000000000001;000000000000000000000002,000000000000000000000001;000000000000000000000002,true,,"{""name1"":{""program"":""Luni-Vineri:9-18"",""phone"":""123456"",""kids-friendly"":""true"",""type of food"":""international"",""price"":""11.4"",""max number of people"":""23""}}"` ~ "\n";

  auto deserialize = new CsvFeatureDeserializer(csvData);
  deserialize.empty.should.equal(false);
  deserialize.front.should.equal(`{
    "info.originalAuthor": "",
    "info.changeIndex": "0",
    "info.lastChangeOn": "2015-01-01T00:00:00Z",
    "maps": "000000000000000000000001;000000000000000000000002",
    "info.author": "",
    "name": "site1",
    "_id": "",
    "contributors": "",
    "isPublished": "true",
    "icons": "000000000000000000000001;000000000000000000000002",
    "description": "description of site1",
    "attributes": "{\"name1\":{\"program\":\"Luni-Vineri:9-18\",\"phone\":\"123456\",\"kids-friendly\":\"true\",\"type of food\":\"international\",\"price\":\"11.4\",\"max number of people\":\"23\"}}",
    "pictures": "000000000000000000000001;000000000000000000000002",
    "position.lon": "1.5",
    "info.createdOn": "2015-01-01T00:00:00Z",
    "position.lat": "1.5"
  }`.parseJsonString);

  deserialize.popFront;
  deserialize.empty.should.equal(true);
}

/// it should convert to a valid site with id
unittest {
  setupTestData();
  string csvData = `_id,maps,name,description,position.lon,position.lat,info.createdOn,info.lastChangeOn,info.changeIndex,info.author,info.originalAuthor,pictures,icons,isPublished,contributors,attributes` ~ "\n" ~
    `000000000000000000000011,000000000000000000000001,site1,description of site1,1.5,1.5,2015-01-01T00:00:00Z,2015-01-01T00:00:00Z,0,,,,000000000000000000000001,true,,"{""name1"":{""program"":""Luni-Vineri:9-18"",""phone"":""123456"",""kids-friendly"":""true"",""type of food"":""international"",""price"":""11.4"",""max number of people"":""23""}}"` ~ "\n";

  auto deserialize = new CsvFeatureDeserializer(csvData);
  deserialize.empty.should.equal(false);
  deserialize.front.should.equal(`{
    "info.originalAuthor": "",
    "info.changeIndex": "0",
    "info.lastChangeOn": "2015-01-01T00:00:00Z",
    "maps": "000000000000000000000001",
    "info.author": "",
    "name": "site1",
    "_id": "000000000000000000000011",
    "contributors": "",
    "isPublished": "true",
    "icons": "000000000000000000000001",
    "description": "description of site1",
    "attributes": "{\"name1\":{\"program\":\"Luni-Vineri:9-18\",\"phone\":\"123456\",\"kids-friendly\":\"true\",\"type of food\":\"international\",\"price\":\"11.4\",\"max number of people\":\"23\"}}",
    "pictures": "",
    "position.lon": "1.5",
    "info.createdOn": "2015-01-01T00:00:00Z",
    "position.lat": "1.5"
  }`.parseJsonString);

  deserialize.popFront;
  deserialize.empty.should.equal(true);
}

/// it should convert to a valid site with id when is published is uppercased
unittest {
  setupTestData();
  string csvData = `_id,maps,name,description,position.lon,position.lat,info.createdOn,info.lastChangeOn,info.changeIndex,info.author,info.originalAuthor,pictures,icons,isPublished,contributors,attributes` ~ "\n" ~
    `000000000000000000000011,000000000000000000000001,site1,description of site1,1.5,1.5,2015-01-01T00:00:00Z,2015-01-01T00:00:00Z,0,,,,000000000000000000000001,TRUE,,"{""name1"":{""program"":""Luni-Vineri:9-18"",""phone"":""123456"",""kids-friendly"":""true"",""type of food"":""international"",""price"":""11.4"",""max number of people"":""23""}}"` ~ "\n";

  auto deserialize = new CsvFeatureDeserializer(csvData);
  deserialize.empty.should.equal(false);
  deserialize.front.should.equal(`{
    "info.originalAuthor": "",
    "info.changeIndex": "0",
    "info.lastChangeOn": "2015-01-01T00:00:00Z",
    "maps": "000000000000000000000001",
    "info.author": "",
    "name": "site1",
    "_id": "000000000000000000000011",
    "contributors": "",
    "isPublished": "TRUE",
    "icons": "000000000000000000000001",
    "description": "description of site1",
    "attributes": "{\"name1\":{\"program\":\"Luni-Vineri:9-18\",\"phone\":\"123456\",\"kids-friendly\":\"true\",\"type of food\":\"international\",\"price\":\"11.4\",\"max number of people\":\"23\"}}",
    "pictures": "",
    "position.lon": "1.5",
    "info.createdOn": "2015-01-01T00:00:00Z",
    "position.lat": "1.5"
  }`.parseJsonString);

  deserialize.popFront;
  deserialize.empty.should.equal(true);
}

/// it should resolve icon and map names
unittest {
  setupTestData();
  string csvData = `_id,maps,name,description,position.lon,position.lat,info.createdOn,info.lastChangeOn,info.changeIndex,info.author,info.originalAuthor,pictures,icons,isPublished,contributors,attributes` ~ "\n" ~
    `000000000000000000000011,map1,site1,description of site1,1.5,1.5,2015-01-01T00:00:00Z,2015-01-01T00:00:00Z,0,,,,name1,true,,"{""name1"":{""program"":""Luni-Vineri:9-18"",""phone"":""123456"",""kids-friendly"":""true"",""type of food"":""international"",""price"":""11.4"",""max number of people"":""23""}}"` ~ "\n";

  auto deserialize = new CsvFeatureDeserializer(csvData);
  deserialize.empty.should.equal(false);
  deserialize.front.should.equal(`{
    "info.originalAuthor": "",
    "info.changeIndex": "0",
    "info.lastChangeOn": "2015-01-01T00:00:00Z",
    "maps": "map1",
    "info.author": "",
    "name": "site1",
    "_id": "000000000000000000000011",
    "contributors": "",
    "isPublished": "true",
    "icons": "name1",
    "description": "description of site1",
    "attributes": "{\"name1\":{\"program\":\"Luni-Vineri:9-18\",\"phone\":\"123456\",\"kids-friendly\":\"true\",\"type of food\":\"international\",\"price\":\"11.4\",\"max number of people\":\"23\"}}",
    "pictures": "",
    "position.lon": "1.5",
    "info.createdOn": "2015-01-01T00:00:00Z",
    "position.lat": "1.5"
  }`.parseJsonString);

  deserialize.popFront;
  deserialize.empty.should.equal(true);
}

Json splitList(Json value) {
  if(value.type == Json.Type.array) {
    return value;
  }

  if(value.type == Json.Type.string) {
    return Json(value.to!string.split(";").map!(a => Json(a)).array);
  }

  return Json();
}