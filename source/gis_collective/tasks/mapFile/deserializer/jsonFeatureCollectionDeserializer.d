/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.mapFile.deserializer.jsonFeatureCollectionDeserializer;

import ogm.test.fixtures;
import ogm.models.feature;

import gis_collective.tasks.mapFile.deserializer.jsonParser;
import gis_collective.hmq.log;

import std.range;
import std.algorithm;
import std.math;
import std.conv;
import std.exception;

import crate.base;

import vibe.core.log;
import vibe.core.stream;
import vibe.stream.memory;

version(unittest) {
  import fluent.asserts;
  import ogm.test.fixtures;
}

class JsonFeatureCollectionDeserializer : InputRange!Json {
  private {
    InputStream data;

    bool foundFeatures;
    bool readingFeature;

    ubyte[] buffer;
    string text;

    static size_t bufferSize = 1024 * 20;

    JsonParser arrayParser;

    string objectString;
    size_t objectBeginIndex;

    Json[] pendingObjects;
    size_t parsedItems;
  }

  this(InputStream data) {
    this.data = data;
    buffer.reserve(bufferSize);

    arrayParser.onArrayObjectBegin = &this.onFeatureObjectBegin;
    arrayParser.onArrayObjectEnd = &this.onFeatureObjectEnd;

    findFeatureList();

    if(readingFeature) {
      objectString = text[objectBeginIndex..$].array.to!string;
      objectBeginIndex = 0;
    }

    readNext();
  }

  private bool onArrayBegin(string key, size_t index) {
    foundFeatures = true;
    text = text[index..$];

    arrayParser.read(text);

    return false;
  }

  private void findFeatureList() {
    JsonParser parser;
    parser.onArrayBegin = &onArrayBegin;

    while(data.dataAvailableForRead && !foundFeatures) {
      buffer.length = min(data.leastSize, bufferSize);
      data.read(buffer);
      text = cast(string) buffer;

      parser.read(text);
    }
  }

  private bool onFeatureObjectBegin(size_t index) {
    readingFeature = true;
    objectBeginIndex = index;
    return true;
  }

  private bool onFeatureObjectEnd(size_t index) {
    readingFeature = false;
    parsedItems++;

    objectString ~= text[objectBeginIndex..index+1];

    try {
      auto obj = objectString.parseJsonString;
      objectString = "";

      if("properties" !in obj) {
        obj["properties"] = Json.emptyObject;
      }

      if("name" !in obj["properties"]) {
        obj["properties"]["name"] = "feature " ~ parsedItems.to!string;
      }

      pendingObjects ~= obj;
    } catch(Exception e) {
      if(objectString.length > 200) {
        error("Can't deserialize value: " ~ objectString[0..50] ~ " [...] " ~ objectString[$-50..$]);
      } else {
        error("Can't deserialize value: " ~ objectString);
      }
    }

    return true;
  }

  private void readNext() {
    while(pendingObjects.length == 0 && data.dataAvailableForRead) {
      buffer.length = min(data.leastSize, bufferSize);

      data.read(buffer);
      text = cast(string) buffer;

      arrayParser.read(text);

      if(readingFeature) {
        string copy = text[objectBeginIndex..$].array.to!string;
        objectString ~= copy;
        objectBeginIndex = 0;
      }
    }
  }

  ///
  Json front() {
    enforce(pendingObjects.length > 0, "There is nothing left to read.");
    auto item = Json.emptyObject;

    auto obj = pendingObjects[pendingObjects.length - 1];

    enum fields = [ "_id", "name", "maps", "description", "info", "pictures", "icons", "isPublished", "contributors"];
    item["position"] = obj["geometry"];

    foreach(string key, value; obj["properties"]) {
      if(key !in item) {
        item[key] = value;
      }
    }

    return item;
  }

  ///
  Json moveFront() {
    assert(false, "not implemented.");
  }

  ///
  void popFront() {
    if(pendingObjects.length > 0) {
      pendingObjects.length = pendingObjects.length - 1;
    }

    if(pendingObjects.length == 0) {
      readNext;
    }
  }

  ///
  bool empty() {
    if(pendingObjects.length > 0) {
      return false;
    }

    return true;
  }

  int opApply(scope int delegate(Json) dg) {
    int result;

    while(!this.empty) {
      auto item = front;

      result = dg(item);
      popFront;
    }

    return result;
  }

  int opApply(scope int delegate(size_t, Json) dg) {
    int result;
    size_t i;

    while(!this.empty) {
      auto item = front;

      result = dg(i, item);
      i++;
      popFront;
    }

    return result;
  }
}

/// It should find a feature
unittest {
  auto jsonData = createMemoryStream(cast(ubyte[]) `{ "type": "FeatureCollection", "features": [{
    "type": "Feature",
    "properties": {},
    "geometry": {
      "type": "Point",
      "coordinates": [
        1.5,1.5
  ]}}]}`);

  auto deserializer = new JsonFeatureCollectionDeserializer(jsonData);

  deserializer.empty.should.equal(false);
  deserializer.front.should.equal(`{
    "name":"feature 1",
    "position":{"type":"Point","coordinates":[1.5,1.5]}
  }`.parseJsonString);


  deserializer.popFront;
  deserializer.empty.should.equal(true);
}

/// It should find two features by parsing two long items
unittest {
  auto item = `{
    "type": "Feature",
    "properties": {},
    "geometry": {
      "type": "Polygon",
      "coordinates": [[]]
    }
  }`.parseJsonString;

  auto expectedItem = `{
    "name": "",
    "position": {
      "type": "Polygon",
      "coordinates": [[]]
    }
  }`.parseJsonString;

  foreach(i; 0..100) {
    item["properties"]["key" ~ i.to!string] = "value " ~ i.to!string;
    expectedItem["key" ~ i.to!string] = "value " ~ i.to!string;
  }

  foreach(i; 0..800) {
    item["geometry"]["coordinates"][0] ~= [ 1.707006470000067, 42.5027814740001 ].serializeToJson;
    expectedItem["position"]["coordinates"][0] ~= [ 1.707006470000067, 42.5027814740001 ].serializeToJson;
  }

  auto jsonData = createMemoryStream(cast(ubyte[]) (`{ "type": "FeatureCollection", "features": [` ~ item.to!string ~ `, ` ~ item.to!string ~ `]}`));

  auto deserializer = new JsonFeatureCollectionDeserializer(jsonData);

  deserializer.empty.should.equal(false);
  expectedItem["name"] = "feature 1";
  deserializer.front.should.equal(expectedItem);

  deserializer.popFront;
  deserializer.empty.should.equal(false);
  expectedItem["name"] = "feature 2";
  deserializer.front.should.equal(expectedItem);

  deserializer.popFront;
  deserializer.empty.should.equal(true);
}

/// It should find a feature using a buffer of 2 chars
unittest {

  auto jsonData = createMemoryStream(cast(ubyte[]) `{ "type": "FeatureCollection", "features": [{
    "type": "Feature",
    "properties": {},
    "geometry": {
      "type": "Point",
      "coordinates": [
        1.5,1.5
  ]}}]}`);

  JsonFeatureCollectionDeserializer.bufferSize = 2;
  scope(exit) JsonFeatureCollectionDeserializer.bufferSize = 1024 * 20;

  auto deserializer = new JsonFeatureCollectionDeserializer(jsonData);

  deserializer.empty.should.equal(false);
  deserializer.front.should.equal(`{
    "name":"feature 1",
    "position":{"type":"Point","coordinates":[1.5,1.5]}
  }`.parseJsonString);


  deserializer.popFront;
  deserializer.empty.should.equal(true);
}
