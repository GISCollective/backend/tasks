/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.mapFile.deserializer.base;

import gis_collective.tasks.mapFile.deserializer.csvFeatureDeserializer;
import gis_collective.tasks.mapFile.deserializer.jsonFeatureCollectionDeserializer;

import vibe.data.json;
import vibe.stream.wrapper;

import std.range.interfaces;
import std.functional;
import std.string;
import std.array;
import std.algorithm;
import std.conv;
import std.math;
import std.datetime;

import geo.wkt;
import geo.json;

import ogm.crates.all;
import ogm.models.mapFile;

import crate.base;
import crate.collection.csv;

version(unittest) {
  import fluent.asserts;
}

MapFile getFile(OgmCrates crates, string id) {
  auto range = crates.mapFile.getItem(id).exec;

  return LazyMapFile(range.front, (&itemResolver).toDelegate).toType;
}

InputRange!Json deserialize(MapFileResource file) {
  InputRange!Json deserializer;

  if(file.contentType == "text/csv" || file.fileName.toLower.endsWith(".csv")) {
    auto csvRange = streamInputRange(file.inputStream).array.assumeUTF;
    deserializer = new CsvFeatureDeserializer(csvRange);
  }

  if(file.contentType == "application/geo+json" || file.fileName.toLower.endsWith(".geojson")) {
    deserializer = new JsonFeatureCollectionDeserializer(file.inputStream);
  }

  return deserializer;
}

///
string guessTransformedKey(string key) {
  auto lowerKey = key.toLower;

  static foreach(member; __traits(allMembers, Feature)) {
    if(lowerKey == member.toLower) {
      return member;
    }
  }

  if(lowerKey == "id") {
    return "_id";
  }

  if(lowerKey.startsWith("info.")) {
    return "";
  }

  if(lowerKey == "maps.name") {
    return "maps";
  }

  if(lowerKey == "position.lon" || lowerKey == "position.lat") {
    return lowerKey;
  }

  if(lowerKey == "lat" || lowerKey == "latitude") {
    return "position.lat";
  }

  if(lowerKey == "lon" || lowerKey == "longitude") {
    return "position.lon";
  }

  if(lowerKey == "position.wkt" || lowerKey == "position.geojson") {
    return "position";
  }

  if(lowerKey == "pictures.name") {
    return "pictures";
  }

  if(lowerKey == "icons.name") {
    return "icons";
  }

  if(key.indexOf("attributes.") != -1) {
    key = key.replace("attributes.", "");
  }

  if(key.strip == "") {
    return "";
  }

  auto pieces = key.split(".").filter!(a => a.strip != "").array;

  if(pieces.length == 0) {
    return "";
  }

  if(pieces.length == 1) {
    return "attributes.other." ~ pieces[0];
  }

  return "attributes." ~ pieces[0] ~ "." ~ pieces[1..$].join("_");
}

/// It returns the key when it is a field name
unittest {
  static foreach(member; __traits(allMembers, Feature)) {
    guessTransformedKey(member).should.equal(member);
  }
}

/// It returns the key when it is a field name with a different case
unittest {
  static foreach(member; __traits(allMembers, Feature)) {
    guessTransformedKey(member.toUpper).should.equal(member);
  }
}

/// It matches the id field with _id
unittest {
  guessTransformedKey("id").should.equal("_id");
  guessTransformedKey("ID").should.equal("_id");
}

/// It matches the csv output keys with the feature members
unittest {
  guessTransformedKey("maps.name").should.equal("maps");
  guessTransformedKey("lon").should.equal("position.lon");
  guessTransformedKey("lat").should.equal("position.lat");
  guessTransformedKey("position.lon").should.equal("position.lon");
  guessTransformedKey("position.lat").should.equal("position.lat");
  guessTransformedKey("position.wkt").should.equal("position");
  guessTransformedKey("position.geoJson").should.equal("position");
  guessTransformedKey("pictures.name").should.equal("pictures");
  guessTransformedKey("icons.name").should.equal("icons");
  guessTransformedKey("visibility").should.equal("visibility");
  guessTransformedKey("attributes.Wetlands.attr1").should.equal("attributes.Wetlands.attr1");
  guessTransformedKey("attributes.attr1").should.equal("attributes.other.attr1");
  guessTransformedKey("attr1").should.equal("attributes.other.attr1");
  guessTransformedKey("a.b.c").should.equal("attributes.a.b_c");
}

///
void extractFields(const ref Json item, ref Json feature) {
  string[string] fieldMapping;

  extractFields(item, fieldMapping, feature);
}

///
void extractFields(const ref Json item, const ref string[string] fieldMapping, ref Json result) {
  auto now = Clock.currTime;
  now.fracSecs = 0.msecs;

  result.remove("_id");
  result["name"] = "";
  result["description"] = "";
  result["maps"] = Json.emptyArray;
  result["info"] = Json.emptyObject;
  result["info"]["createdOn"] = now.toISOExtString;
  result["info"]["lastChangeOn"] = result["info"]["createdOn"];
  result["info"]["changeIndex"] = 0;
  result["info"]["author"] = "@unknown";

  result["pictures"] = Json.emptyArray;
  result["sounds"] = Json.emptyArray;
  result["icons"] = Json.emptyArray;
  result["contributors"] = Json.emptyArray;
  result["visibility"] = 0;
  result["attributes"] = Json.emptyObject;
  result["position"] = GeoJsonGeometry.nullPoint.toJson;

  foreach(string key, value; item) {
    if(value.to!string.strip == "") continue;
    if(value.type == Json.Type.object && value.length == 0) continue;

    string mappedKey;

    if(key in fieldMapping) {
      mappedKey = fieldMapping[key];
    } else {
      mappedKey = guessTransformedKey(key);
    }

    if(mappedKey.strip == "") {
      continue;
    }

    if(mappedKey == "maps" || mappedKey == "icons" || mappedKey == "contributors" || mappedKey == "pictures") {
      result.appendValue(mappedKey, value);
    }

    if(mappedKey == "name" || mappedKey == "description" || mappedKey == "_id") {
      result[mappedKey] = value.to!string;
    }

    if(mappedKey == "visibility") {
      result["visibility"] = value.to!int;
    }

    if(mappedKey == "attributes" && value.to!string.strip != "") {
      result["attributes"].setAttribute(key, value);
    }

    if(mappedKey == "position.lon") {
      result.setPoint(0, value);
    }

    if(mappedKey == "position.lat") {
      result.setPoint(1, value);
    }

    if(mappedKey == "position") {
      result.setPosition(value);
    }

    if(mappedKey.indexOf("attributes.") == 0) {
      result.setAttribute(mappedKey, value);
    }
  }
}

/// It should add all feature fields when the object is empty
unittest {
  auto item = Json.emptyObject;

  auto now = Clock.currTime;
  now.fracSecs = 0.msecs;

  string strJson = `{
    "info": {
      "createdOn": "` ~ now.toISOExtString ~ `",
      "lastChangeOn": "` ~ now.toISOExtString ~ `",
      "changeIndex": 0,
      "author": "@unknown"
    },
    "maps": [],
    "contributors": [],
    "name": "",
    "visibility": 0,
    "icons": [],
    "description": "",
    "attributes": {},
    "position": {
      "type": "Point",
      "coordinates": [ 0., 0. ]
    },
    "pictures": [],
    "sounds": []
  }`;

  auto feature = Json.emptyObject;
  extractFields(item, feature);

  feature.should.equal(strJson.parseJsonString);
}

/// It should add a map if a string is set
unittest {
  auto item = Json.emptyObject;
  item["maps"] = "some id";

  auto feature = Json.emptyObject;
  extractFields(item, feature);

  feature["maps"].should.equal(`[ "some id" ]`.parseJsonString);
}

/// It should add an icon if a string is set
unittest {
  auto item = Json.emptyObject;
  item["icons"] = "some id";

  auto feature = Json.emptyObject;
  extractFields(item, feature);

  feature["icons"].should.equal(`[ "some id" ]`.parseJsonString);
}

/// It should add contributors if a string is set
unittest {
  auto item = Json.emptyObject;
  item["contributors"] = "some id";

  auto feature = Json.emptyObject;
  extractFields(item, feature);

  feature["contributors"].should.equal(`[ "some id" ]`.parseJsonString);
}

/// It should add pictures if a string is set
unittest {
  auto item = Json.emptyObject;
  item["pictures"] = "some id";

  auto feature = Json.emptyObject;
  extractFields(item, feature);

  feature["pictures"].should.equal(`[ "some id" ]`.parseJsonString);
}

/// It should set the id, name and description
unittest {
  auto item = Json.emptyObject;
  item["name"] = "name";
  item["description"] = "description";
  item["_id"] = "id";

  auto feature = Json.emptyObject;
  extractFields(item, feature);

  feature["name"].to!string.should.equal("name");
  feature["description"].to!string.should.equal("description");
  feature["_id"].to!string.should.equal("id");
}

/// It should set the coordinates
unittest {
  auto item = Json.emptyObject;
  item["position.lon"] = 1.;
  item["position.lat"] = 2.;

  auto feature = Json.emptyObject;
  extractFields(item, feature);

  feature["position"].should.equal(`{
    "type": "Point",
    "coordinates": [ 1., 2. ]
  }`.parseJsonString);
}

/// It should set the position using wkt strings
unittest {
  auto item = Json.emptyObject;
  item["position"] = "LINESTRING(30.1234 10.1234, 30.2 50.4)";

  auto feature = Json.emptyObject;
  extractFields(item, feature);

  feature["position"].should.equal(`{
    "type": "LineString",
    "coordinates": [ [ 30.1234, 10.1234 ], [ 30.2, 50.4 ]]
  }`.parseJsonString);
}

/// It should set the visibility field to 1
unittest {
  auto item = Json.emptyObject;
  item["visibility"] = 1;

  auto feature = Json.emptyObject;
  extractFields(item, feature);

  feature["visibility"].to!int.should.equal(1);
}

/// It should set an attribute
unittest {
  auto item = Json.emptyObject;
  item["key"] = "value";

  auto feature = Json.emptyObject;
  extractFields(item, feature);

  feature["attributes"].should.equal(`{
    "other": {
      "key": "value"
    }
  }`.parseJsonString);
}

/// It should ignore empty string attributes
unittest {
  auto item = Json.emptyObject;
  item["attributes"] = "";

  auto feature = Json.emptyObject;
  extractFields(item, feature);

  feature["attributes"].should.equal(Json.emptyObject);
}

/// It should set a specific attribute
unittest {
  auto item = Json.emptyObject;
  item["key"] = "value";
  string[string] fields = ["key": "attributes.key"];

  auto feature = Json.emptyObject;
  extractFields(item, fields, feature);

  feature["attributes"].should.equal(`{
    "key": "value"
  }`.parseJsonString);
}

///
void appendValue(ref Json item, string key, Json value) {
  if(key !in item || item[key].type != Json.Type.array) {
    item[key] = Json.emptyArray;
  }

  if(value.type == Json.Type.string) {
    auto pieces = value.to!string.split(";").map!(a => Json(a.strip)).array;

    foreach(piece; pieces) {
      item[key].appendArrayElement(piece);
    }

    return;
  }

  if(value.type == Json.Type.array) {
    item[key] ~= value;
  } else {
    item[key].appendArrayElement(value);
  }
}

/// It should add a string value to the array
unittest {
  Json item = Json.emptyObject;

  item.appendValue("maps", Json("Test"));

  item.should.equal(`{
    "maps": [ "Test" ]
  }`.parseJsonString);
}

/// It should split a string value by ; and add the pieces
unittest {
  Json item = Json.emptyObject;

  item.appendValue("maps", Json("Test1;Test2"));

  item.should.equal(`{
    "maps": [ "Test1", "Test2" ]
  }`.parseJsonString);
}

/// It should add a string list value to the array
unittest {
  Json item = Json.emptyObject;

  item.appendValue("maps", `[ "Test1","Test2" ]`.parseJsonString);

  item.should.equal(`{
    "maps": [ "Test1","Test2" ]
  }`.parseJsonString);
}

/// It should combine values with arrays
unittest {
  Json item = Json.emptyObject;

  item.appendValue("maps", `[ "Test1","Test2" ]`.parseJsonString);
  item.appendValue("maps", Json("Test3"));
  item.appendValue("maps", `[ "Test4","Test5" ]`.parseJsonString);
  item.appendValue("maps", Json("Test6"));

  item.should.equal(`{
    "maps": [ "Test1","Test2", "Test3","Test4", "Test5", "Test6" ]
  }`.parseJsonString);
}

///
void setAttribute(ref Json json, string key, Json value) {
  auto dotIndex = key.indexOf(".");
  auto arrayIndex = key.indexOf("[");

  if(dotIndex == -1 && arrayIndex == -1) {
    json[key] = value;
    return;
  }

  if(arrayIndex == 0) {
    if(json.type != Json.Type.array) {
      json = Json.emptyArray;
    }

    auto arrayEndIndex = key.indexOf("]");
    size_t index = key[arrayIndex+1..arrayEndIndex].to!size_t;

    while(json.length <= index) {
      json ~= Json(null);
    }

    json[index].setAttribute(key[arrayEndIndex+1..$], value);

    return;
  }

  if(dotIndex == 0) {
    json = Json.emptyObject;
    json.setAttribute(key[1..$], value);
    return;
  }

  long cutIndex = 0;

  if(dotIndex > arrayIndex && arrayIndex != -1) {
    cutIndex = arrayIndex;
  } else {
    cutIndex = dotIndex;
  }


  if(cutIndex > 0) {
    string nextKey = key[0..cutIndex];

    if(json[nextKey].type != Json.Type.object) {
      json[nextKey] = Json.emptyObject;
    }

    json[nextKey].setAttribute(key[cutIndex..$], value);
  }
}

/// it should set an attribute by key
unittest {
  auto item = Json.emptyObject;

  item.setAttribute("key", Json("value"));

  item.should.equal(`{
    "key": "value"
  }`.parseJsonString);
}

/// it should set a nested attribute by key
unittest {
  auto item = Json.emptyObject;

  item.setAttribute("level1.level2", Json("value"));

  item.should.equal(`{
    "level1": {
      "level2": "value"
    }
  }`.parseJsonString);
}

/// it should set an array value
unittest {
  auto item = Json.emptyObject;

  item.setAttribute("level1[0].level2", Json("value"));

  item.should.equal(`{
    "level1": [{
      "level2": "value"
    }]
  }`.parseJsonString);
}

/// it should set a nested array value
unittest {
  auto item = Json.emptyObject;

  item.setAttribute("level1[0][1].level2", Json("value"));

  item.should.equal(`{
    "level1": [[null, {
      "level2": "value"
    }]]
  }`.parseJsonString);
}

void setPoint(ref Json json, long index, Json value) {
  import std.stdio;

  if("position" !in json) {
    return;
  }

  if("type" !in json["position"] || json["position"]["type"] != "Point") {
    return;
  }

  if("coordinates" !in json["position"]) {
    json["position"]["coordinates"] = `[0., 0.]`.parseJsonString;
  }

  json["position"]["coordinates"][index] = value.to!double;
}

void setPosition(ref Json json, Json value) {
  string strVal = value.to!string;

  try {
    json["position"] = strVal.parseWKT.toJson;
    return;
  } catch(Exception) {}

  try {
    json["position"] = strVal.parseJsonString;
    return;
  } catch(Exception) {}
}

/// it should set a wkt position
unittest {
  auto item = Json.emptyObject;

  item.setPosition(Json("LINESTRING(30.1234 10.1234, 30.2 50.4)"));

  item.should.equal(`{
    "position": {
      "type": "LineString",
      "coordinates": [ [ 30.1234, 10.1234 ], [ 30.2, 50.4 ]]
    }
  }`.parseJsonString);
}

/// it should set a geojson position
unittest {
  auto item = Json.emptyObject;

  item.setPosition(Json(`{ "type": "Point", "coordinates": [ 1., 2. ] }`));

  item.should.equal(`{
    "position": { "type": "Point", "coordinates": [ 1., 2. ] }
  }`.parseJsonString);
}

/// it should set a deserialized geojson
unittest {
  auto item = Json.emptyObject;

  item.setPosition(`{ "type": "Point", "coordinates": [ 1.1, 2.1 ] }`.parseJsonString);

  item.should.equal(`{
    "position": { "type": "Point", "coordinates": [ 1.1, 2.1 ] }
  }`.parseJsonString);
}
