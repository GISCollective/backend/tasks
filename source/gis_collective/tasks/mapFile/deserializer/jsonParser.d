/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.mapFile.deserializer.jsonParser;

version(unittest) {
  import fluent.asserts;
}

struct JsonParser {
  bool isReadingKey;
  bool hasKey;
  long paranthesisIndex;

  ValueType readingValueType;
  ValueType parserType;

  enum ValueType {
    none,
    unknown,
    plain,
    array,
    object
  }

  string key;
  string value;

  size_t extraIndex;
  size_t eventCount;

  bool delegate(string, string) onKeyValue;

  bool delegate(string, size_t) onArrayBegin;
  bool delegate(string, size_t) onArrayEnd;

  bool delegate(size_t) onArrayObjectBegin;
  bool delegate(size_t) onArrayObjectEnd;

  void read(const string text) {
    extraIndex = 0;
    eventCount = 0;

    if(parserType == ValueType.object) {
      readObject(text);
      return;
    }

    if(parserType == ValueType.array) {
      readArray(text);
      return;
    }

    foreach(index, ch; text) {
      if(ch == '{') {
        extraIndex = index + 1;
        parserType = ValueType.object;
        readObject(text[index+1..$]);
        return;
      }

      if(ch == '[') {
        extraIndex = index + 1;
        parserType = ValueType.array;
        readArray(text[index+1..$]);
        return;
      }
    }
  }

  void readObject(const string text) {
    foreach(index, ch; text) {
      if(readingValueType == ValueType.none) {
        if(ch == '"' && !isReadingKey) {
          isReadingKey = true;
          key = "";
          continue;
        }

        if(ch == '"' && isReadingKey) {
          isReadingKey = false;
          hasKey = true;
          continue;
        }

        if(isReadingKey) {
          key ~= ch;
        }

        if(ch == ':' && hasKey) {
          readingValueType = ValueType.unknown;
          continue;
        }

        continue;
      }

      if(readingValueType == ValueType.unknown) {
        if(ch == '"') {
          readingValueType = ValueType.plain;
          continue;
        }

        if(ch == '[') {
          readingValueType = ValueType.array;

          if(!raise!"onArrayBegin"(key, index)) return;
          continue;
        }

        continue;
      }

      if(ch == ']' && readingValueType == ValueType.array) {
        readingValueType = ValueType.unknown;
        hasKey = false;

        if(!raise!"onArrayEnd"(key, index)) return;
        continue;
      }

      if(ch == '"' && readingValueType == ValueType.plain) {
        readingValueType = ValueType.none;
        hasKey = false;

        if(!raise!"onKeyValue"(key, value)) return;
        continue;
      }

      if(readingValueType == ValueType.plain) {
        value ~= ch;
      }
    }
  }

  void readArray(const string text) {
    foreach(index, ch; text) {
      if(readingValueType == ValueType.none) {
        if(ch == '{') {
          paranthesisIndex = 0;
          readingValueType = ValueType.object;

          if(!raise!"onArrayObjectBegin"(extraIndex+index)) return;
          continue;
        }
      }

      if(readingValueType == ValueType.object) {
        if(ch == '{') {
          paranthesisIndex++;
        }

        if(ch == '}') {
          paranthesisIndex--;
        }

        if(ch == '}' && paranthesisIndex < 0) {
          readingValueType = ValueType.none;
          if(!raise!"onArrayObjectEnd"(extraIndex+index)) return;
          continue;
        }
      }
    }
  }

  bool raise(string method, Params...)(Params params) {
    eventCount++;
    mixin("if(" ~ method ~ " is null) return true;");
    mixin("return " ~ method ~ "(params);");
  }
}

/// it should find an empty object inside an array
unittest {
  size_t begin;
  size_t end;

  bool onArrayObjectBegin(size_t index) {
    begin = index;
    return true;
  }

  bool onArrayObjectEnd(size_t index) {
    end = index;
    return true;
  }

  JsonParser parser;
  parser.onArrayObjectBegin = &onArrayObjectBegin;
  parser.onArrayObjectEnd = &onArrayObjectEnd;

  parser.read("[{}]");

  begin.should.equal(1);
  end.should.equal(2);
}

/// it should find an object with nested objects inside an array
unittest {
  size_t begin;
  size_t end;

  bool onArrayObjectBegin(size_t index) {
    begin = index;
    return true;
  }

  bool onArrayObjectEnd(size_t index) {
    end = index;
    return true;
  }

  JsonParser parser;
  parser.onArrayObjectBegin = &onArrayObjectBegin;
  parser.onArrayObjectEnd = &onArrayObjectEnd;

  parser.read(`[{ "key": { "other": {} } }]`);

  begin.should.equal(1);
  end.should.equal(26);
}

/// it should find an object for the string [{
unittest {
  size_t begin;

  bool onArrayObjectBegin(size_t index) {
    begin = index;
    return true;
  }

  JsonParser parser;
  parser.onArrayObjectBegin = &onArrayObjectBegin;

  parser.read(`[`);
  parser.read(` {`);

  begin.should.equal(1);
}

/// it should parse an object by chunks
unittest {
  size_t begin;
  size_t end;

  bool onArrayObjectBegin(size_t index) {
    begin = index;
    return true;
  }

  bool onArrayObjectEnd(size_t index) {
    end = index;
    return true;
  }

  JsonParser parser;
  parser.onArrayObjectBegin = &onArrayObjectBegin;
  parser.onArrayObjectEnd = &onArrayObjectEnd;

  parser.read(`[{"type":"Feature","properties": {},"geometry": {"type": "Point","coordinates": [[1.5,1.5], `);
  begin.should.equal(1);
  end.should.equal(0);

  begin = 0;
  end = 0;

  parser.read(`[1.5,1.5]`);
  begin.should.equal(0);
  end.should.equal(0);

  parser.read(`]}}]`);
  begin.should.equal(0);
  end.should.equal(2);
}
