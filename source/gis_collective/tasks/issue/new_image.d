/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

module gis_collective.tasks.issue.new_image;

import gis_collective.hmq.broadcast.base;
import gis_collective.tasks.error;

import crate.base;

import ogm.crates.all;
import ogm.models.meta;
import ogm.meta;

import vibe.data.json;
import vibe.core.log;

import vibe.service.stats;

import std.algorithm;
import std.array;
import std.string;
import std.datetime;
import std.exception;

version(unittest) {
  import fluent.asserts;
  import ogm.test.fixtures;
}

struct IssueStatus {
  enum type = "Issue.newImageStatus";
  alias Model = Issue;

  string id;
  OgmCrates crates;
  IBroadcast broadcast;

  static Json[] processList(const ChangeSet changeSet) {
    if(changeSet.model != Model.stringof) {
      return [];
    }

    if(changeSet.added.length == 0 && changeSet.removed.length == 0) {
      return [];
    }

    if("status" !in changeSet.added) {
      return [];
    }

    if(changeSet.added["status"] != "resolved") {
      return [];
    }

    if("processed" in changeSet.added && changeSet.added["processed"].to!string == "true") {
      return [];
    }

    return [ changeSet.itemId.toJson ];
  }

  void log(T...)(string message, T params) {
    logInfo("[%s %s] " ~ message, type, id, params);
  }

  void start() {
    auto range = crates.issue.get.where("_id").equal(ObjectId.fromString(id)).and.exec;
    enforce!TaskValidationException(!range.empty, "Tried to process issue `" ~ id ~ "` but it does not exist.");

    auto issue = range.front;

    if(issue["status"] == "open") {
      log("Ignoring because it's open.");
      return;
    }

    if(issue["processed"] == true) {
      log("Ignoring because it was already processed.");
      return;
    }

    enforce!TaskValidationException(issue["type"] == "newImage", "Tried to update issue `" ~ id ~ " but it has `" ~ issue["type"].to!string ~ "` type instead of `newImage`.");

    issue["processed"] = true;

    if("file" !in issue) {
      log("Updating issue.");
      crates.issue.updateItem(issue);

      throw new TaskValidationException("Tried to process issue `" ~ id ~ "` with no file.");
    }

    log("Get the picture file.");
    auto issueFileId = issue["file"].to!string;
    auto creationDate = SysTime.fromISOExtString(issue["creationDate"].to!string);

    log("Looking for the feature `%s`", issue["feature"]);
    auto featureRange = crates.feature.get.where("_id").equal(ObjectId.fromJson(issue["feature"])).and.exec;

    if(featureRange.empty) {
      log("Updating issue.");
      crates.issue.updateItem(issue);

      throw new TaskValidationException("Feature `" ~ issue["feature"].to!string ~ "` referenced by issue `" ~ id ~ "` does not exist.");
    }

    auto feature = featureRange.front;
    auto issueFile = new IssueFile(issueFileId);

    Picture picture;
    picture.owner = issue["author"].to!string;
    picture.name = issue["title"].to!string;

    if("attributions" in issue) {
      picture.meta.attributions = issue["attributions"].to!string;
    }

    picture.picture = new PictureFile();
    picture.picture.fileName = issueFile.fileName;
    picture.picture.contentType = issueFile.contentType;
    picture.picture.read(issueFile.inputStream);

    log("Updating feature picture.");
    auto jsonPicture = crates.picture.addItem(picture.serializeToJson);
    feature["pictures"] ~= jsonPicture["_id"];

    log("Updating feature `%s`.", issue["feature"]);
    crates.feature.updateItem(feature);

    log("Updating issue.");
    crates.issue.updateItem(issue);
  }
}
