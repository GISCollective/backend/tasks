/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.searchMeta.SearchMetaTask;

import crate.base;
import crate.json;
import crate.lazydata.base;
import geo.json;
import gis_collective.hmq.log;
import gis_collective.tasks.GisTask;
import ogm.crates.all;
import ogm.markdown;
import ogm.models.visibility;
import ogm.calendar;
import std.algorithm;
import std.array;
import std.conv;
import std.math;
import std.datetime;
import std.string;
import stemmer.cleaner;
import stemmer.english;
import vibe.core.log;
import vibe.data.json;

struct PrivateAttribute {
  string iconName;
  string attrName;

  bool match(string key) {
    auto index1 = key.indexOf(iconName);
    auto index2 = key.indexOf(attrName);

    if(index1 == -1 || index2 == -1) {
      return false;
    }

    return index1 < index2;
  }
}

struct SearchMetaTask {
  OgmCrates crates;
  EnStemmer stemmer = new EnStemmer;
  string logPrefix;

  void getArticleKeywords(string text, ref string[] keywords) {
    auto range = text.clean
      .split(" ")
      .map!(a => stemmer.get(a.toLower));

    foreach (string keyword; range) {
      if(!keywords.canFind(keyword) && !isNumeric(keyword)) {
        keywords ~= keyword;
      }
    }
  }

  void getKeywords(Json obj, Json iconIds, ref string[] keywords) {
    auto iconIdList = iconIds.deserializeJson!(string[]).map!(a => ObjectId.fromString(a)).array;

    auto icons = crates.icon.get.where("_id").anyOf(iconIdList).and.exec;

    PrivateAttribute[] privateAttributes;

    foreach (icon; icons) {
      foreach (attribute; icon["attributes"]) {
        if(attribute["isPrivate"] == true) {
          privateAttributes ~= PrivateAttribute(icon["name"].to!string, attribute["name"].to!string);
        }
      }
    }

    auto values = obj.toFlatJson;

    foreach (string key, Json value; values) {
      if(privateAttributes.canFind!(a => a.match(key))) {
        continue;
      }

      if(value.type == Json.Type.string) {
        getArticleKeywords(value.to!string, keywords);
      }
    }
  }

  void getKeywords(Json[string] obj, ref string[] keywords) {
    foreach (string key, Json value; obj) {
      getArticleKeywords(value, keywords);
    }
  }

  void getIconNamesToKeywords(Json iconList, ref string[] keywords) {
    auto icons = crates.icon.get.where("_id").anyOf(ObjectId.fromJsonList(iconList)).and.exec.array;

    foreach(iconName; icons.map!(a => a["name"])) {
      getArticleKeywords(iconName.to!string, keywords);
    }
  }

  void getMapNamesToKeywords(Json mapList, ref string[] keywords) {
    auto maps = crates.map.get.where("_id").anyOf(ObjectId.fromJsonList(mapList)).and.exec.array;

    foreach(mapName; maps.map!(a => a["name"])) {
      getArticleKeywords(mapName.to!string, keywords);
    }
  }

  void getArticleKeywords(Json article, ref string[] keywords) {
    if(article.type == Json.Type.string) {
      getArticleKeywords(article.to!string, keywords);
    }

    if(article.type == Json.Type.object && "blocks" in article) {
      foreach(Json block; article["blocks"]) {
        if("data" !in block || block["type"] == "image") {
          continue;
        }

        auto values = block["data"].toFlatJson;
        getKeywords(values, keywords);
      }
    }
  }

  string[] getArticleKeywords(Json article) {
    string[] keywords;

    getArticleKeywords(article, keywords);

    return keywords;
  }

  void removeMeta(string modelName, ObjectId id) {
    logInfo("%s Removing meta", logPrefix);

    auto recordRange = crates.searchMeta
      .get.where("relatedId").equal(id.to!string)
      .and.where("relatedModel").equal(modelName)
      .and.exec;

    import std.stdio;
    writeln(modelName, " ", id);

    foreach (Json record; recordRange) {
      crates.searchMeta.deleteItem(record["_id"].to!string);
    }

    logInfo("%s Done", logPrefix);
  }

  void addFeatureData(ref Json feature, ref SearchMeta searchMeta) {
    if("maps" in feature) {
      searchMeta.feature.maps = feature["maps"].deserializeJson!(string[]);
      if(searchMeta.feature.maps.length) {
        auto map = crates.map.getItem(searchMeta.feature.maps[0]).and.exec.front;
        searchMeta.feature.isGeoCoding = map["isIndex"] == true;
      }
    }

    if(feature["icons"].type == Json.Type.array) {
      searchMeta.icons = feature["icons"].byValue.map!(a => Icon(ObjectId.fromJson(a))).array;
    }

    if("position" in feature) {
      auto geometry = feature["position"].deserializeJson!GeoJsonGeometry;
      searchMeta.feature.centroid = geometry.centroid;
      searchMeta.feature.area = abs(geometry.area).to!size_t;
      searchMeta.feature.distance = abs(geometry.metricDistance).to!size_t;
    }
  }

  void getCampaignData(Json mapRelation, ref SearchMeta searchMeta) {
    if(mapRelation["isEnabled"] != true) {
      return;
    }

    auto mapId = mapRelation["map"].to!string;

    getMapData(mapId, searchMeta);
  }

  void getMapData(string mapId, ref SearchMeta searchMeta) {
    if(mapId == "") {
      return;
    }

    searchMeta.feature.maps = [mapId];

    auto map = crates.map.getItem(mapId).and.exec.front;

    try {
      auto geometry = map["area"].deserializeJson!GeoJsonGeometry;
      searchMeta.feature.centroid = geometry.centroid;
      searchMeta.feature.area = abs(geometry.area).to!size_t;
      searchMeta.feature.distance = abs(geometry.metricDistance).to!size_t;
    } catch (Exception e) {
      info(map["area"].to!string);
      error(e);
    }
  }

  Visibility getVisibility(Json record) {
    if("visibility" in record && record["visibility"].type == Json.Type.object) {
      try {
        return Lazy!Visibility.fromJson(record["visibility"], &itemResolver).toType;
      } catch(Exception) {
        return Visibility();
      }
    }

    if("maps" in record) {
      if(record["maps"].length == 0) {
        return Visibility();
      }

      auto mapRange = crates.map.get.where("_id").equal(ObjectId.fromJson(record["maps"][0])).and.exec;

      if(mapRange.empty) {
        return Visibility();
      }

      return Lazy!Visibility.fromJson(mapRange.front["visibility"], &itemResolver).toType;
    }

    if("iconSet" in record && "iconSets" !in record) {
      auto setRange = crates.iconSet.get.where("_id").equal(ObjectId.fromJson(record["iconSet"])).and.exec;

      if(setRange.empty) {
        return Visibility();
      }

      return Lazy!Visibility.fromJson(setRange.front["visibility"], &itemResolver).toType;
    }

    return Visibility();
  }

  void updateMeta(string modelName, string id) {
    this.removeMeta(modelName, ObjectId(id));

    logInfo("%s Updating metadata", logPrefix);

    auto record = itemResolver(modelName, id);

    Json article;

    if("description" in record) {
      article = record["description"];
    }

    if("about" in record) {
      article = record["about"];
    }

    if("article" in record) {
      article = record["article"];
    }

    if("content" in record) {
      article = record["content"];
    }

    if(article.type == Json.Type.string) {
      article = blocksFromMarkdown(article.to!string);
    }

    string firstParagraph;

    if(article.type == Json.Type.object && "blocks" in article) {
      foreach(Json block; article["blocks"].byValue) {
        if(block["type"] == "paragraph") {
          firstParagraph = block["data"]["text"].to!string;
          break;
        }
      }
    }

    SearchMeta result;

    if("name" in record) {
      result.title = record["name"].to!string;
    }

    if(record["pictures"].type == Json.Type.array && record["pictures"].length > 0) {
      result.cover = Picture(ObjectId.fromJson(record["pictures"][0]));
    }

    if(record["cover"].type == Json.Type.string) {
      result.cover = Picture(ObjectId.fromJson(record["cover"]));
    }

    if(record["logo"].type == Json.Type.string) {
      result.cover = Picture(ObjectId.fromJson(record["logo"]));
    }

    if("title" in record) {
      result.title = record["title"].to!string;
    }

    auto lastChangeOn = SysCalendar.instance.now;

    try {
      if(record["info"].type == Json.Type.object) {
        lastChangeOn = SysTime.fromISOExtString(record["info"]["lastChangeOn"].to!string);
      }
    } catch(Exception e) {
      error(e);
    }

    result.description = firstParagraph;
    result.relatedModel = modelName;
    result.relatedId = id;
    result.lastChangeOn = lastChangeOn;

    result.visibility = getVisibility(record);

    getArticleKeywords(record["name"], result.keywords);
    getArticleKeywords(article, result.keywords);

    if(modelName == "Feature") {
      getKeywords(record["attributes"], record["icons"], result.keywords);
      getIconNamesToKeywords(record["icons"], result.keywords);
      getMapNamesToKeywords(record["maps"], result.keywords);

      try {
        addFeatureData(record, result);
      } catch(Exception e) {
        error(e);
      }
    }

    if(modelName == "Campaign") {
      getCampaignData(record["map"], result);
    }

    if(modelName == "Map") {
      getMapData(record["_id"].to!string, result);
    }

    if(result.visibility.isPublic && "visibility" in record && record["visibility"].type != Json.Type.object) {
      result.visibility.isPublic = record["visibility"] == 1;
    }

    if(modelName == "Team") {
      result.visibility.isPublic = record["isPublic"] == true;
      result.visibility.team._id = ObjectId.fromJson(record["_id"]);
    }

    if(modelName == "Article" && record["categories"].type == Json.Type.array) {
      result.categories = record["categories"].deserializeJson!(string[]);
    }

    auto lazyMeta = Lazy!SearchMeta.fromModel(result);

    crates.searchMeta.addItem(lazyMeta.toJson);

    logInfo("%s Done", logPrefix);
  }
}

struct RecordChangeMessage {
  string id;
  string model;

  @optional:
    string type;
}

struct UpdateSearchMetaTask {
  enum type = "searchMeta.update";

  OgmCrates crates;

  @onChange
  static Json[] processList(ChangeSet changeSet) {
    if(changeSet.type == CrateChangeType.delete_) {
      return [];
    }

    if(![ "Team", "Icon", "IconSet", "Map", "Feature", "Campaign", "Article", "Event" ].canFind(changeSet.model)) {
      return [];
    }

    return [ changeSet.itemId.toJson ];
  }

  void main(RecordChangeMessage message) {
    SearchMetaTask task;
    task.crates = crates;
    task.logPrefix = "[" ~ type ~ "][" ~ message.model ~ "][" ~ message.id ~ "]";

    task.updateMeta(message.model, message.id);
  }
}

struct RemoveSearchMetaTask {
  enum type = "searchMeta.remove";

  OgmCrates crates;

  @onChange
  static Json[] processList(ChangeSet changeSet) {
    if(changeSet.type != CrateChangeType.delete_) {
      return [];
    }

    if(![ "Team", "Icon", "IconSet", "Map", "Feature", "Campaign", "Article", "Event" ].canFind(changeSet.model)) {
      return [];
    }

    return [ changeSet.itemId.toJson ];
  }

  void main(RecordChangeMessage message) {
    SearchMetaTask task;
    task.crates = crates;
    task.logPrefix = "[" ~ type ~ "][" ~ message.model ~ "][" ~ message.id ~ "]";

    task.removeMeta(message.model, ObjectId.fromString(message.id));
  }
}
