/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.notifications.admin;

import gis_collective.tasks.GisTask;
import crate.base;

import std.datetime;
import std.functional;

import ogm.crates.all;
import ogm.meta;
import ogm.contentBlocks;
import vibeauth.data.usermodel;
import vibe.data.json;

import gis_collective.hmq.broadcast.base;
import handlebars.tpl;
import gis_collective.tasks.notifications.NotificationTask;
import gis_collective.hmq.log;
import crate.auth.usercollection;
import ogm.message;
import ogm.calendar;

enum subjects = [
  "Map-add": "There is a new map!",
  "Map-delete": "A map was deleted!",
  "Campaign-add": "There is a new survey!",
  "Campaign-delete": "A survey was deleted!",
  "IconSet-add": "There is a new icon set!",
  "IconSet-delete": "An icon set was deleted!",
  "Calendar-add": "There is a new calendar!",
  "Calendar-delete": "A calendar was deleted!",
  "Newsletter-add": "There is a new newsletter!",
  "Newsletter-delete": "A newsletter was deleted!",
  "Space-add": "There is a new space!",
  "Space-delete": "A space was deleted!",
  "Team-add": "There is a new team!",
  "Team-delete": "A team was deleted!"
];

enum niceModelName = [
  "Map": "map",
  "Campaign": "survey",
  "IconSet": "icon set",
  "Calendar": "calendar",
  "Newsletter": "newsletter",
  "Space": "space",
  "Team": "team",
];

struct NotificationsAdmin {
  enum type = "notifications.admin";

  OgmCrates crates;
  IBroadcast broadcast;

  static string serviceUrl;

  struct Request {
    string id;
    string model;
    string type;

    @optional:
      string name;
      string teamId;
      string author;
  }

  static Json[] processList(const ChangeSet changeSet) {
    if(changeSet.type == CrateChangeType.update || changeSet.type == CrateChangeType.snapshot) {
      return [];
    }

    if(changeSet.model !in niceModelName) {
      return [];
    }

    auto value = Json.emptyObject;
    value["id"] = changeSet.itemId.toJson;

    if("name" in changeSet.added) {
      value["name"] = changeSet.added["name"];
    }

    if("name" in changeSet.removed) {
      value["name"] = changeSet.removed["name"];
    }

    if("visibility.team" in changeSet.added) {
      value["teamId"] = changeSet.added["visibility.team"];
    }

    if("visibility.team" in changeSet.removed) {
      value["teamId"] = changeSet.removed["visibility.team"];
    }

    value["author"] = changeSet.author;

    return [ value ];
  }

  void main(Request request) {
    Message notification;
    notification.subject = subjects[request.model ~ "-" ~ request.type];
    notification.uniqueKey = "notification-admin-" ~ request.model ~ "-" ~ request.type ~ "-" ~ request.id;
    notification.type = MessageType.admin;

    string author = "Someone";
    string team = "unknown team";

    try {
      team = crates.team.getItem(request.teamId).and.exec.front["name"].to!string;
    } catch(Exception) {
      team = "unknown team";
    }

    try {
      auto profile = crates.userProfile.getItem(request.author).and.exec.front;
      author = profile["firstName"].to!string ~ " " ~ profile["lastName"].to!string;
    } catch(Exception) {
      author = "Someone";
    }

    if(request.type == "add" && request.model != "Team") {
      notification.text = author ~ " added a new " ~ niceModelName[request.model] ~ ": " ~ request.name ~ " to " ~ team ~ ".";
    }

    if(request.type == "delete" && request.model != "Team") {
      notification.text = author ~ " deleted a " ~ niceModelName[request.model] ~ ": " ~ request.name ~ " from " ~ team ~ ".";
    }

    if(request.type == "add" && request.model == "Team") {
      notification.text = author ~ " added a new " ~ niceModelName[request.model] ~ ": " ~ request.name ~ ".";
    }

    if(request.type == "delete" && request.model == "Team") {
      notification.text = author ~ " deleted a " ~ niceModelName[request.model] ~ ": " ~ request.name ~ ".";
    }

    if(request.model == "Team") {
      request.teamId = request.id;
    }

    notification.html = notification.text;

    auto url = crates.getRegisterUrl(serviceUrl);

    if(request.author != "") {
      notification.actions["View profile"] = url ~ "/browse/profiles/" ~ request.author;
    }

    if(request.teamId != "") {
      notification.actions["View team dashboard"] = url ~ "/manage/dashboards/" ~ request.teamId;
    }

    crates.message.send(notification, broadcast);
  }
}
