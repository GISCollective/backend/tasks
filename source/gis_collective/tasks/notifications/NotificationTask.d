/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.notifications.NotificationTask;

import gis_collective.tasks.GisTask;
import crate.base;

import std.datetime;
import std.functional;

import ogm.crates.all;
import ogm.meta;
import ogm.contentBlocks;
import vibeauth.data.usermodel;
import vibe.data.json;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import handlebars.tpl;

struct NotificationTask_ {
  OgmCrates crates;
  IBroadcast broadcast;

  string subject;
  string textContent;
  string htmlContent;

  Json user = Json.emptyObject;
  Json profile = Json.emptyObject;

  void articleSlug(string slug) {
    auto article = crates.article.get
      .where("slug").equal(slug)
      .and.exec;

    if(article.empty) {
      return;
    }

    auto content = article.front["content"];

    subject = article.front["title"].to!string;
    textContent = content.toTextContent(true);
    htmlContent = content.toHtmlContent(true);
  }

  void userId(string id) {
    user = Json.emptyObject;
    profile = Json.emptyObject;

    if(!isObjectId(id)) {
      return;
    }

    try {
      auto userRange = crates.user.get
        .where("_id").equal(ObjectId.fromString(id))
        .and.exec;

      if(!userRange.empty) {
        user = userRange.front;
      }

      auto profileRange = crates.userProfile.get
        .where("_id").equal(ObjectId.fromString(id))
        .and.exec;

      if(!profileRange.empty) {
        profile = profileRange.front;
      } else {
        profile["firstName"] = "---";
      }
    } catch(Exception e) {
      error(e);
    }
  }

  string getPreference(string name) {
    auto range = crates.preference.get
      .where("name").equal(name)
      .and.exec;

    if(range.empty) {
      return "---";
    }

    return range.front["value"].to!string;
  }

  void send(T)(string[] to, T controller) {
    auto notification = Json.emptyObject;
    auto text = render(textContent, controller);

    if(text == "") {
      return;
    }

    notification["to"] = to.serializeToJson;
    notification["subject"] = render(subject, controller);
    notification["text"] = text;
    notification["html"] = render(htmlContent, controller);

    broadcast.push("notifications.message", notification);
  }
}