/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.notifications.email;

import std.array;
import std.functional;
import std.range;
import std.algorithm;
import std.string;
import std.conv;
import std.net.isemail;
import gis_collective.tasks.error;

import ogm.crates.all;
import ogm.models.mapFile;
import ogm.models.feature;
import ogm.models.meta;
import ogm.http.request;
import ogm.meta;

import vibe.data.json;
import vibe.data.bson;
import vibe.stream.wrapper;
import vibe.mail.smtp;
import vibeauth.mail.base;
import vibeauth.mail.vibe;

import crate.base;
import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import gis_collective.tasks.mapFile.deserializer.base;

import vibe.core.log;

struct EmailTask {
  enum type = "notifications.message";

  OgmCrates crates;
  MailQueue mailQueue;

  void main(SmtpMessage message) {
    auto configuration = EmailConfiguration();
    configuration.smtp = new SMTPConfig(crates);

    if(mailQueue is null) {
      mailQueue = new VibeMailQueue(configuration);
    }

    string[] to;

    foreach(email; message.to) {
      if(email == "" || email.startsWith("@")) {
        error("There is an invalid user id: `" ~ email ~ "`");
        continue;
      }

      if(!isEmail(email)) {
        email = crates.user.getItem(email).exec.front["email"].to!string;
      }

      to ~= [ email ];
    }

    if(to.length == 0) {
      return;
    }

    message.to = to;
    message.from = configuration.smtp.from;
    mailQueue.addMessage(message);
  }
}

class SMTPConfig : ISMTPConfig {

  private {
    OgmCrates crates;
  }

  this(OgmCrates crates) {
    this.crates = crates;
  }

  private string get(string key)() {
    return this.crates.preference.get.where("name").equal("secret.smtp." ~ key).and.exec.front["value"].to!string;
  }

  string authType() {
    return this.get!"authType";
  }

  string connectionType() {
    return this.get!"connectionType";
  }

  string tlsValidationMode() {
    return this.get!"tlsValidationMode";
  }

  string tlsVersion() {
    return this.get!"tlsVersion";
  }

  string host() {
    return this.get!"host";
  }

  ushort port() {
    string value;

    try {
      value = this.get!"port".to!string;

      return value.to!ushort;
    } catch(Exception e) {
      logError("Can't get the smpt port value '%s': %s", value, e.toString);
    }

    return 465;
  }

  string localname() {
    return this.get!"localname";
  }

  string password() {
    return this.get!"password";
  }

  string username() {
    return this.get!"username";
  }

  string from() {
    return this.get!"from";
  }
}
