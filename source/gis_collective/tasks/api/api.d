/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.tasks.api;

import std.exception;
import std.meta;

import gis_collective.tasks.history.task;
import gis_collective.tasks.mapFile.import_;
import gis_collective.tasks.mapFile.analyze;
import gis_collective.tasks.mapFile.generateGeoPackage;
import gis_collective.tasks.feature.update;
import gis_collective.tasks.task;

import ogm.crates.all;
import crate.base;

import gis_collective.hmq.broadcast.http;

import vibe.core.log;
import vibe.service.configuration.db;

import gis_collective.tasks.article.Publish;
import gis_collective.tasks.article.Test;
import gis_collective.tasks.baseMap.BaseMapIconSetExtract;
import gis_collective.tasks.calendar.visibility;
import gis_collective.tasks.campaignAnswer.mapSync;
import gis_collective.tasks.event.generateOccurrences;
import gis_collective.tasks.feature.AttributesFix;
import gis_collective.tasks.feature.delete_;
import gis_collective.tasks.feature.FeatureBox;
import gis_collective.tasks.feature.FeatureDecorator;
import gis_collective.tasks.feature.FeatureItemSplit;
import gis_collective.tasks.feature.FeatureMask;
import gis_collective.tasks.feature.FeatureSimplify;
import gis_collective.tasks.feature.IndexedFeature;
import gis_collective.tasks.feature.SoundsVisibility;
import gis_collective.tasks.iconSet.delete_;
import gis_collective.tasks.iconSet.IconSetSprite;
import gis_collective.tasks.issue.new_image;
import gis_collective.tasks.map.delete_;
import gis_collective.tasks.map.IncrementMapChangeIndex;
import gis_collective.tasks.map.info;
import gis_collective.tasks.map.MapIcons;
import gis_collective.tasks.map.MapMask;
import gis_collective.tasks.map.MapSprite;
import gis_collective.tasks.map.tiles.MapTiles;
import gis_collective.tasks.map.tiles.MapTilesCheck;
import gis_collective.tasks.map.tiles.MapTilesRequest;
import gis_collective.tasks.map.visibility;
import gis_collective.tasks.newsletter.Subscribe;
import gis_collective.tasks.notifications.admin;
import gis_collective.tasks.notifications.email;
import gis_collective.tasks.searchMeta.SearchMetaTask;
import gis_collective.tasks.space.ForwardTasks;
import gis_collective.tasks.team.delete_;
import gis_collective.tasks.user.ChangedPassword;
import gis_collective.tasks.user.Contribution;
import gis_collective.tasks.user.ForgotPassword;
import gis_collective.tasks.user.TestNotification;
import gis_collective.tasks.user.UserActivate;
import gis_collective.tasks.user.UserNew;
import gis_collective.tasks.team.welcome;
import gis_collective.tasks.team.invitation;

import ogm.tasks.configuration;

alias AllTasks = AliasSeq!(
  FeatureUpdate, AttributesFix, FeatureDecorator, FeatureSimplify,
  BaseMapIconSetExtract,
  IssueStatus,
  UserContribution, TestNotification, ForgotPassword, ChangedPassword, UserActivate, UserNew,
  SoundsVisibility, FeatureMask, IndexedFeature, FeatureBox,
  CampaignAnswerMapSync,
  UpdateSearchMetaTask, RemoveSearchMetaTask,
  ArticlePublish, ArticleTest, NewsletterSubscribe,
  IconSetDelete, IconSetSprite, MapSpriteDefault, MapSpriteAll, MapSprite,
  MapDelete, MapInfo, MapIcons, MapMask, MapVisibility, GenerateGeoPackage,
  ForwardTasksSpaceTask,
  TeamDelete,
  CalendarEventsVisibility,
  EventGenerateOccurrences,
  NotificationsAdmin, TeamWelcome, TeamInvitation
);

alias ExternalTasks = AliasSeq!(MapFileAnalyze, MapFileImport);

///
void setupApi(HttpBroadcast broadcast, OgmCrates crates, MainApiConfiguration configuration) {
  crateGetters["Map"] = &crates.map.getItem;
  crateGetters["Team"] = &crates.team.getItem;
  crateGetters["Picture"] = &crates.picture.getItem;
  crateGetters["IconSet"] = &crates.iconSet.getItem;
  crateGetters["BaseMap"] = &crates.baseMap.getItem;
  crateGetters["Icon"] = &crates.icon.getItem;
  crateGetters["Feature"] = &crates.feature.getItem;
  crateGetters["Campaign"] = &crates.campaign.getItem;
  crateGetters["Article"] = &crates.article.getItem;
  crateGetters["Event"] = &crates.event.getItem;

  MapFileSettings.files = crates.mapFiles;
  MapFileSettings.chunks = crates.mapChunks;

  PictureFileSettings.files = crates.pictureFiles;
  PictureFileSettings.chunks = crates.pictureChunks;

  IconSettings.files = crates.iconFiles;
  IconSettings.chunks = crates.iconChunks;

  IssueFileSettings.files = crates.issueFiles;
  IssueFileSettings.chunks = crates.issueChunks;

  auto historyTaskPool = new HistoryTaskPool!AllTasks(crates, broadcast);

  static foreach(T; AllTasks) static if(is(T == struct)) {{
    logInfo("Setup `%s` task", T.stringof);
    auto task = new Task!T(broadcast, crates);
    broadcast.register(T.type, &task.subscriber);

    static if(__traits(hasMember, T, "serviceUrl")) {
      T.serviceUrl = configuration.general.serviceUrl;
    }
  }}

  static foreach(T; AllTasks) static if(is(T == class)) {{
    auto instance = new T(crates, broadcast);
    historyTaskPool.add(instance);

    auto task = new Task!T(broadcast, instance);
    broadcast.register(T.stringof, &task.subscriber);

    static if(__traits(hasMember, T, "serviceUrl")) {
      T.serviceUrl = configuration.general.serviceUrl;
    }
  }}

  static foreach (T; ExternalTasks) {
    historyTaskPool.add(T());
  }

  bool isSuccess;
  static foreach(Model; CratesWithNotification) {
    logInfo("Setting up `" ~ Model.stringof ~ "` change notifications.");
    isSuccess = broadcast.register(Model.stringof ~ ".change", &historyTaskPool.subscriber);
    enforce(isSuccess, "Could not setup the `" ~ Model.stringof ~ ".change` subscriber.");
  }
}
