/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.tasks.configuration;

import std.file;
import std.path;
import std.exception;
import std.conv;
import std.datetime;

import vibeauth.authenticators.OAuth2;
import vibeauth.mail.base;

import vibe.http.server;
import vibe.http.fileserver;
import vibe.data.json;

import vibe.service.configuration.general;
import vibe.service.configuration.http;

/// The service configuration
struct MainApiConfiguration {
  /// Configurations for all services
  GeneralConfig general;

  /// The http configuration
  HTTPConfig http;

  ///
  HMQConfig hmq;
}

struct HMQConfig {
  string url;
  @optional string subscriberUrl;
}