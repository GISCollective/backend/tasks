/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.tasks.service;

import std.file;
import std.path;
import std.exception;
import std.datetime;
import std.conv;

import ogm.tasks.api;
import ogm.tasks.configuration;

import ogm.crates.all;

import gis_collective.hmq.lifecycle;

import vibe.service.base;
import vibe.service.webservice;
import vibe.service.configuration.http;
import vibe.service.configuration.db;
import vibe.service.stats;

import vibe.http.server;
import vibe.http.fileserver;
import vibe.http.router;
import vibe.db.mongo.client;
import vibe.db.mongo.mongo;
import vibe.db.mongo.settings;
import vibe.core.core;
import vibe.core.log;

import gis_collective.hmq.queue.mongo;
import gis_collective.hmq.queue.memory;
import gis_collective.hmq.queue.base;
import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.broadcast.http;

import crate.collection.notifications;

///
class TasksService : WebService!MainApiConfiguration {
  private {
    OgmCrates crates;
    HttpBroadcast broadcast;
    HttpRegistration registration;
  }

  void shutdown() nothrow {
    logInfo("Shutting down the LifeCycle...");
    LifeCycle.instance.shutdown;
  }

  void push(CrateChange change) @safe {}

  IQueue!Json queueFactory(string name) nothrow @trusted {
    try {
      auto configurations = this.configuration.general.dbConfiguration;
      auto dbName = configurations.getMongoConfig.configuration["database"].to!string;

      auto client = configurations.getMongoClient;
      auto collection = client.getCollection(dbName ~ "._hmq_tasks_" ~ name);

      return new MongoQueue!Json(collection);
    } catch(Exception e) {
      return new MemoryQueue!Json();
    }
  }

  void registerTimer() nothrow @safe {
    try {
      this.registration.flush();
    } catch(Exception e) {
      error(e);
    }
  }

  /// The main service logic
  int main() {
    scope(exit) this.statsTimer();

    auto router = new URLRouter;
    router.setupStats();
    log("Added stats routes.");

    if(this.configuration.hmq.subscriberUrl == "") {
      string port;

      if(this.configuration.http.port != 80) {
        port = ":" ~ this.configuration.http.port.to!string;
      }

      this.configuration.hmq.subscriberUrl = "http://" ~ this.configuration.http.hostName ~ port ~ "/";
    }

    log("The subscriber url is `" ~ this.configuration.hmq.subscriberUrl ~ "`.");

    BroadcastConfig broadcastConfig;
    broadcastConfig.hmqUrl = this.configuration.hmq.url;
    broadcastConfig.localUrl = this.configuration.hmq.subscriberUrl;
    broadcastConfig.router = router;
    broadcastConfig.queueFactory = &this.queueFactory;

    this.registration = new HttpRegistration(broadcastConfig);


    auto timer = createTimer(&this.registerTimer);
    timer.rearm(2.seconds, true);

    this.crates = this.configuration.general.dbConfiguration.setupOgmCrates;
    log("Db connection ready.");

    runTask({
      HttpRegistration.instance = this.registration;
      this.broadcast = new HttpBroadcast(broadcastConfig);

      try {
        broadcast.setupApi(crates, this.configuration);
        router.rebuild;

        log("Api setup ready.");
      } catch(Exception e) {
        logError(e.message);
        exitEventLoop(true);
      }
    });

    auto config = this.configuration.http.toVibeConfig;
    config.serverString = this.info.name;

    logInfo("The maximum accepted request size is %s kb", config.maxRequestSize / 1024);
    logInfo("The server string is `%s`", config.serverString);

    setupWorkerThreads(1);
    listenHTTP(config, router);

    return runEventLoop();
  }
}
