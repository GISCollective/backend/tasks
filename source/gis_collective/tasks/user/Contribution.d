/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.user.Contribution;

import gis_collective.tasks.GisTask;
import crate.base;

import std.datetime;

import ogm.crates.all;
import ogm.meta;
import vibeauth.data.usermodel;
import vibe.data.json;

struct UserContributionRequest {
  string id;
}

struct UserContribution {
  enum type = "user.contribution";
  alias Model = UserModel;

  Json[string] mapCache;

  OgmCrates crates;
  SysTime time;

  @onChange
  static Json[] processList(const ChangeSet changeSet) {
    if(changeSet.model == "Feature" || changeSet.model == "Site" || changeSet.model == "Map") {
      return [ Json(changeSet.author) ];
    }

    if(changeSet.model == "UserProfile") {
      return [ changeSet.itemId.toJson ];
    }

    return [ ];
  }

  void main(UserContributionRequest message) {
    if(time.year < 1000) {
      time = Clock.currTime;
    }

    auto range = crates.meta.get
      .where("type").equal(type).and
      .where("itemId").equal(message.id).and.exec;

    foreach(entry; range) {
      crates.meta.deleteItem(entry["_id"].to!string);
    }

    if(message.id == "@anonymous") {
      return;
    }

    auto profileRange = crates.userProfile.get.where("_id").equal(ObjectId.fromString(message.id)).and.exec;

    if(profileRange.empty) {
      return;
    }

    auto profile = profileRange.front;
    bool showPrivateContributions = profile["showPrivateContributions"].to!bool;

    Meta meta;
    meta.type = type;
    meta.model = Model.stringof;
    meta.itemId = message.id;
    meta.data = `{
      "public": [],
      "private": {}
    }`.parseJsonString;

    size_t total;

    auto selector = crates.changeSet.get
      .where("author").equal(message.id).and
      .and;

    selector.or
      .where("model").equal("Site").or
      .where("model").equal("Feature");

    range = selector.exec;

    size_t[string][string] contributions;
    size_t[string] empty;

    contributions[""] = empty.dup;

    const now = cast(Date) time;

    foreach(change; range) {
      const date = cast(Date) SysTime.fromISOExtString(change["time"].to!string).toUTC;
      const strDate = date.toISOExtString;
      auto diff = now - date;

      if(diff > 365.days) {
        continue;
      }

      string privateMapId = "";

      if(!showPrivateContributions) {
        privateMapId = getPrivateMapId(change["itemId"].to!string);
      }

      if(privateMapId == "_") {
        continue;
      }

      if(privateMapId !in contributions) {
        contributions[privateMapId] = empty.dup;
      }

      contributions[privateMapId][strDate]++;
      total++;
    }

    foreach(string mapId, list; contributions) {
      foreach (strDate, count; list) {
        if(mapId == "") {
          meta.data["public"] ~= createContribution(strDate, count);
        } else {
          if(mapId !in meta.data["private"]) {
            meta.data["private"][mapId] = Json.emptyArray;
          }

          meta.data["private"][mapId] ~= createContribution(strDate, count);
        }
      }
    }

    if(total > 0) {
      crates.meta.addItem(meta.serializeToJson);
    }
  }

  string getPrivateMapId(string id) {
    auto featureRange = crates.feature.get
      .where("_id").equal(ObjectId.fromString(id)).and.exec;

    if(featureRange.empty) {
      return "_";
    }

    auto feature = featureRange.front;

    if(feature["maps"].length == 0) {
      return "";
    }

    string mapId = feature["maps"][0].to!string;

    if("visibility" !in feature || feature["visibility"] == 0) {
      return mapId;
    }

    if(mapId !in mapCache) {
      auto mapRange = crates.map.get
        .where("_id").equal(ObjectId.fromString(mapId)).and.exec;

      if(mapRange.empty) {
        return "_";
      }

      mapCache[mapId] = mapRange.front;
    }

    if("visibility" !in mapCache[mapId] || !mapCache[mapId]["visibility"]["isPublic"]) {
      return mapId;
    }

    return "";
  }
}


Json createContribution(string strDate, size_t count) {
  auto contribution = Json.emptyObject;

  auto date = Date.fromISOExtString(strDate);
  contribution["year"] = date.year;
  contribution["week"] = date.isoWeek;
  contribution["day"] = cast(ubyte) date.dayOfWeek;
  contribution["count"] = count;

  return contribution;
}