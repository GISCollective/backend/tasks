/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.user.ChangedPassword;

import gis_collective.tasks.GisTask;
import crate.base;

import std.datetime;
import std.functional;

import ogm.crates.all;
import ogm.meta;
import ogm.contentBlocks;
import vibeauth.data.usermodel;
import vibe.data.json;

import gis_collective.hmq.broadcast.base;
import handlebars.tpl;
import gis_collective.tasks.notifications.NotificationTask;
import crate.auth.usercollection;
import ogm.message;
import ogm.calendar;

struct ChangedPasswordRequest {
  string id;
}

struct ChangedPassword {
  enum type = "user.changedPassword";
  alias Model = UserModel;

  OgmCrates crates;
  IBroadcast broadcast;

  static string serviceUrl;

  struct MessageData {
    string receiverName = "---";
    string serviceName = "GISCollective";
    string serviceUrl = "";
  }

  void main(ChangedPasswordRequest message) {
    auto range = crates.user.get.where("_id").equal(ObjectId(message.id)).and.exec;

    if(range.empty) {
      return;
    }

    auto notification = messageFromArticle(crates.article, "notification-changed-password");
    notification.uniqueKey = "notification-changed-password-" ~ message.id ~ "-" ~ SysCalendar.instance.now.toISOExtString;
    notification.to.type = "user";
    notification.to.value = message.id;

    auto messageData = MessageData();
    messageData.receiverName = crates.fetchName(message.id);
    messageData.serviceName = crates.getPreference("appearance.name");
    messageData.serviceUrl = crates.getRegisterUrl(serviceUrl);

    notification.fill(messageData);
    crates.message.send(notification, broadcast);
  }
}
