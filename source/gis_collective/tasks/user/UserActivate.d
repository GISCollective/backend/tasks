/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.user.UserActivate;

import gis_collective.tasks.GisTask;
import crate.base;

import std.datetime;
import std.functional;

import ogm.crates.all;
import ogm.meta;
import ogm.contentBlocks;
import vibeauth.data.usermodel;
import vibe.data.json;

import gis_collective.hmq.broadcast.base;
import handlebars.tpl;
import gis_collective.tasks.notifications.NotificationTask;
import crate.auth.usercollection;
import ogm.message;
import ogm.calendar;
import gis_collective.hmq.log;

struct UserActivateRequest {
  string id;
}

struct UserActivate {
  enum type = "user.activate";
  alias Model = UserModel;

  OgmCrates crates;
  IBroadcast broadcast;

  static string serviceUrl;

  struct MessageData {
    string receiverName = "---";
    string serviceName = "GISCollective";
    string serviceUrl = "";
    string email = "";
    string token = "";
    string activateLink = "";
  }

  void main(UserActivateRequest message) {
    auto range = crates.user.get.where("_id").equal(ObjectId(message.id)).and.exec;

    if(range.empty) {
      return;
    }

    auto notification = messageFromArticle(crates.article, "notification-user-activate");
    notification.uniqueKey = "notification-user-activate-" ~ message.id ~ "-" ~ SysCalendar.instance.now.toISOExtString;
    notification.to.type = "user";
    notification.to.value = message.id;

    string email = crates.fetchEmail(message.id);
    auto userCollection = new UserCrateCollection([], crates.user);
    auto user = userCollection[email];

    if(user.isActive) {
      info(`[user.activate] The user ` ~ email ~ ` is already activated.`);
      return;
    }

    auto token = userCollection.createToken(email, SysCalendar.instance.now.toUTC + 2.days, [], "activation");

    auto messageData = MessageData();
    messageData.receiverName = crates.fetchName(message.id);
    messageData.email = email;
    messageData.serviceName = crates.getPreference("appearance.name");
    messageData.serviceUrl = crates.getRegisterUrl(serviceUrl);
    messageData.token = token.name;
    messageData.activateLink = crates.getRegisterUrl(serviceUrl) ~ "/login/activate?email=" ~ email ~ "&token=" ~ token.name;

    notification.actions["Confirm email"] = messageData.activateLink;

    notification.fill(messageData);
    crates.message.send(notification, broadcast);
  }
}
