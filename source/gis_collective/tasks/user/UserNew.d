/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.user.UserNew;

import gis_collective.tasks.GisTask;
import crate.base;

import std.datetime;
import std.functional;

import ogm.crates.all;
import ogm.meta;
import ogm.contentBlocks;
import vibeauth.data.usermodel;
import vibe.data.json;

import gis_collective.hmq.broadcast.base;
import handlebars.tpl;
import gis_collective.tasks.notifications.NotificationTask;
import gis_collective.hmq.log;
import crate.auth.usercollection;
import ogm.message;
import ogm.calendar;

struct UserNewRequest {
  string id;
}

struct UserNew {
  enum type = "user.new";
  alias Model = UserModel;

  OgmCrates crates;
  IBroadcast broadcast;

  static string serviceUrl;

  struct MessageData {
    string receiverName = "---";
    string serviceName = "GISCollective";
    string serviceUrl = "";
    string userEmail = "";
    string userId = "";
    string personName = "";
  }

  void main(UserNewRequest message) {
    auto range = crates.user.get.where("_id").equal(ObjectId(message.id)).and.exec;

    if(range.empty) {
      return;
    }

    auto notification = messageFromArticle(crates.article, "notification-user-new");
    notification.uniqueKey = "notification-user-new-" ~ message.id;
    notification.type = MessageType.admin;

    string email = crates.fetchEmail(message.id);
    auto userCollection = new UserCrateCollection([], crates.user);

    auto messageData = MessageData();
    messageData.personName = crates.fetchName(message.id);
    messageData.userEmail = email;
    messageData.userId = message.id;
    messageData.serviceName = crates.getPreference("appearance.name");
    messageData.serviceUrl = crates.getRegisterUrl(serviceUrl);

    notification.actions["View profile"] = crates.getRegisterUrl(serviceUrl) ~ "/browse/profiles/" ~ message.id;

    notification.fill(messageData);
    crates.message.send(notification, broadcast);
  }
}
