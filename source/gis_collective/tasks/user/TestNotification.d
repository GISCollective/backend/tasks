/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.user.TestNotification;

import gis_collective.tasks.GisTask;
import crate.base;

import std.datetime;
import std.functional;

import ogm.message;

import ogm.calendar;
import ogm.crates.all;
import ogm.meta;
import ogm.contentBlocks;
import vibeauth.data.usermodel;
import vibe.data.json;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;

struct TestNotificationRequest {
  string id;
}

struct TestNotification {
  enum type = "user.testNotification";
  alias Model = UserModel;
  OgmCrates crates;

  IBroadcast broadcast;

  struct MessageData {
    string receiverName = "---";
  }

  void main(TestNotificationRequest message) {
    auto testMessage = messageFromArticle(crates.article, "notification-test");
    testMessage.uniqueKey = "testMessage_" ~ message.id ~ "_" ~ SysCalendar.instance.now.toISOExtString;
    testMessage.type = MessageType.message;
    testMessage.to.type = "user";
    testMessage.to.value = message.id;
    testMessage.useGenericTemplate = true;

    auto messageData = MessageData();
    messageData.receiverName = "---";

    auto range = crates.user.get.where("_id").equal(ObjectId(message.id)).and.exec;

    if(range.empty) {
      return;
    }

    try {
      auto profile = crates.userProfile.getItem(message.id).and.exec.front;
      messageData.receiverName = profile["firstName"].to!string;
    } catch(Exception e) {
      error(e);
    }

    testMessage.fill(messageData);

    crates.message.send(testMessage, broadcast);

    testMessage.type = MessageType.admin;
    crates.message.send(testMessage, broadcast);
  }
}
