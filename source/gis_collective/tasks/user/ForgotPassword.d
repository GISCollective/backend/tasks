/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.user.ForgotPassword;

import gis_collective.tasks.GisTask;
import crate.base;

import std.datetime;
import std.functional;

import ogm.crates.all;
import ogm.meta;
import ogm.contentBlocks;
import vibeauth.data.usermodel;
import vibe.data.json;

import gis_collective.hmq.broadcast.base;
import handlebars.tpl;
import gis_collective.tasks.notifications.NotificationTask;
import crate.auth.usercollection;
import ogm.message;
import ogm.calendar;

struct ForgotPasswordRequest {
  string id;
}

struct ForgotPassword {
  enum type = "user.forgot";
  alias Model = UserModel;

  OgmCrates crates;
  IBroadcast broadcast;

  static string serviceUrl;

  struct MessageData {
    string receiverName = "---";
    string serviceName = "GISCollective";
    string serviceUrl = "";
    string email = "";
    string token = "";
    string resetLink = "";
  }

  void main(ForgotPasswordRequest message) {
    auto range = crates.user.get.where("_id").equal(ObjectId(message.id)).and.exec;

    if(range.empty) {
      return;
    }

    auto notification = messageFromArticle(crates.article, "notification-user-forgot");
    notification.uniqueKey = "notification-user-forgot-" ~ message.id ~ "-" ~ SysCalendar.instance.now.toISOExtString;
    notification.to.type = "user";
    notification.to.value = message.id;

    string email = crates.fetchEmail(message.id);
    auto userCollection = new UserCrateCollection([], crates.user);
    auto token = userCollection.createToken(email, SysCalendar.instance.now.toUTC + 2.days, [], "passwordReset");

    auto messageData = MessageData();
    messageData.receiverName = crates.fetchName(message.id);
    messageData.email = email;
    messageData.serviceName = crates.getPreference("appearance.name");
    messageData.serviceUrl = crates.getRegisterUrl(serviceUrl);
    messageData.token = token.name;
    messageData.resetLink = crates.getRegisterUrl(serviceUrl) ~ "/login/reset?email=" ~ email ~ "&token=" ~ token.name;

    notification.actions["Reset password"] = messageData.resetLink;

    notification.fill(messageData);
    crates.message.send(notification, broadcast);
  }
}
