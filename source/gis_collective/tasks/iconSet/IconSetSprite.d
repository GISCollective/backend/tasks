/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.iconSet.IconSetSprite;

import vibe.data.json;
import vibe.core.log;

import std.ascii;
import std.process;
import std.exception;
import std.random;
import std.datetime;
import std.algorithm;
import std.path;
import std.utf;
import std.array;
import std.conv;
import std.file;
import std.functional : toDelegate;
import std.math;
import std.digest.md;

import crate.base;
import crate.mime;
import crate.error;

import vibe.service.stats;
import vibe.core.file;
import vibe.core.stream;
import vibe.core.core;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import ogm.crates.all;
import ogm.models.meta;
import ogm.meta;
import ogm.process;
import gis_collective.tasks.color;

version(unittest) {
  import fluent.asserts;
  import ogm.test.fixtures;
}

static immutable localImageCache = buildPath(".", "sprites-cache");

static this() {
  if(!localImageCache.exists) {
    mkdirRecurse(localImageCache);
  }
}

///
void convert(string file, string destination, int width, int height) {
  run("rsvg-convert" ~
        " -w " ~ width.to!string ~
        " -h " ~ height.to!string ~
        " -f png" ~
        " " ~ file ~
        " -o " ~ destination);
}

auto runIM(string command) {
  version(unittest) {

  } else {
    return run(command);
  }
}

///
void resizeImage(string file, string destination, int width, int height) {
  runIM("convert -density 1200" ~
    " -resize " ~ width.to!string ~ "x" ~ height.to!string ~
    " -gravity center " ~
    " -extent " ~ width.to!string ~ "x" ~ height.to!string ~
    " -background transparent" ~
    " " ~ file ~
    " PNG32:" ~ destination);
}

void pasteIcon(string iconFile, string canvasFile, uint x, uint y, uint width, uint height) {
  runIM("composite" ~
    " -geometry "~ width.to!string ~ "x" ~ height.to!string ~ "+" ~ x.to!string ~ "+" ~ y.to!string ~
    " " ~ iconFile ~
    " " ~ canvasFile~
    " " ~ canvasFile);
}

///
void overlay(string[] files, string destination) {
  runIM("convert " ~ files.join(" ") ~
    " -gravity center -composite" ~
    " -background transparent" ~
    " PNG32:" ~ destination);
}

///
void montage(string files, string result, int size, int itemsPerLine, int lines) {
  runIM("montage " ~ files ~
    " -tile " ~ itemsPerLine.to!string ~ "x" ~ lines.to!string ~
    " -geometry " ~ size.to!string ~ "x" ~ size.to!string ~ `\>` ~
    " -background transparent" ~
    " PNG32:" ~ result);
}

void createCanvas(string file, uint width, uint height, string color) {
  run("convert " ~
    " -size " ~ width.to!string ~ "x" ~ height.to!string ~
    " canvas:" ~ color ~
    " PNG32:" ~ file);
}

void crop(string file, uint x, uint y, uint width, uint height) {
  run("convert " ~
    " " ~ file ~
    " -crop " ~ width.to!string ~ "x" ~ height.to!string ~ "-" ~ x.to!string ~ "-" ~ y.to!string ~ "\\!" ~
    " +repage" ~
    " " ~ file);
}

struct SpriteIconPaths {
  string tmpFolder;
  size_t index;
  double sizeFactor;
  string contentType;

  string strSize() {
    return "@" ~ sizeFactor.to!string;
  }

  string tmpFilePath() {
    return buildPath(tmpFolder, index.to!string  ~ ".tmp" ~ strSize ~ contentType.toExtension);
  }

  string svgShapePath() {
    return buildPath(tmpFolder, index.to!string ~ ".shape" ~ strSize ~ ".svg");
  }

  string pngShapePath() {
    return buildPath(tmpFolder, index.to!string ~ ".shape" ~ strSize ~ ".png");
  }

  string resultFilePath() {
    return buildPath(tmpFolder, index.to!string ~ strSize ~ ".png");
  }
}

string points(double width, double radius, double count) {
  string result;

  double angle = 180 / count;

  foreach(i; 0..(count + 1)) {
    double angleDeg = (angle*2) * i - angle;
    double angleRad = PI / 180 * angleDeg;

    result ~= (width/2 + radius * cos(angleRad)).to!string ~ "," ~
      (width/2 + radius * sin(angleRad)).to!string ~ " ";
  }

  return result;
}

struct SpriteIcon {
  SpriteIconPaths paths;
  string tmpFolder;
  Icon icon;
  double sizeFactor;
  int slotSize;
  uint x;
  uint y;
  size_t index;

  this(string tmpFolder, Icon icon, double sizeFactor, int slotSize, uint x, uint y, size_t index) {
    this.tmpFolder = tmpFolder;
    this.icon = icon;
    this.sizeFactor = sizeFactor;
    this.slotSize = slotSize;
    this.x = x;
    this.y = y;
    this.index = index;

    auto image = icon.image.value;
    string contentType;

    if(image !is null) {
      contentType = image.contentType;
    }

    paths = SpriteIconPaths(tmpFolder, index, sizeFactor, contentType);

    if(!localImageCache.exists) {
      localImageCache.mkdirRecurse;
    }
  }

  PointStyleProperties style() {
    return icon.styles.types.site;
  }

  IconFile image() {
    return icon.image.value;
  }

  string[string] shapeVars() {
    string[string] result;

    double size = style.size * sizeFactor;
    auto center = size / 2;
    auto radius = center - (style.borderWidth.to!double / 2);
    auto middle = size / 2;

    auto squareSize = size - style.borderWidth;
    auto squarePosition = style.borderWidth.to!double / 2;
    auto pentaPoints = points(squareSize, squareSize / 2, 5);
    auto hexaPoints = points(squareSize, squareSize / 2, 6);

    auto fill = Color(style.backgroundColor);
    auto stroke = Color(style.borderColor);

    result["size"] = size.to!string;
    result["center"] = center.to!string;
    result["radius"] = radius.to!string;
    result["middle"] = middle.to!string;
    result["squareSize"] = squareSize.to!string;
    result["squarePosition"] = squarePosition.to!string;
    result["hexaPoints"] = hexaPoints.to!string;
    result["pentaPoints"] = pentaPoints.to!string;
    result["style.borderWidth"] = style.borderWidth.to!string;
    result["stroke.hexColor"] = stroke.hexColor.to!string;
    result["stroke.opacity"] = stroke.opacity.to!string;
    result["fill.hexColor"] = fill.hexColor.to!string;
    result["fill.opacity"] = fill.opacity.to!string;

    return result;
  }

  string shapeHash() {
    auto result = this.shapeVars();

    return md5Of(result.byKeyValue.map!(a => a.key ~ "." ~ a.value).array.sort.join(".")).dup.toHexString;
  }

  string shapeSvg(string type)() {
    string svg = import(type ~ ".template.svg");

    auto result = this.shapeVars();

    svg = svg.replace("{{size}}", result["size"]);
    svg = svg.replace("{{center}}", result["center"]);
    svg = svg.replace("{{radius}}", result["radius"]);
    svg = svg.replace("{{middle}}", result["middle"]);

    svg = svg.replace("{{squareSize}}", result["squareSize"]);
    svg = svg.replace("{{squarePosition}}", result["squarePosition"]);

    svg = svg.replace("{{hexaPoints}}", result["hexaPoints"]);
    svg = svg.replace("{{pentaPoints}}", result["pentaPoints"]);

    svg = svg.replace("{{strokeWidth}}", result["style.borderWidth"]);
    svg = svg.replace("{{strokeColor}}", result["stroke.hexColor"]);
    svg = svg.replace("{{strokeOpacity}}", result["stroke.opacity"]);

    svg = svg.replace("{{fillColor}}", result["fill.hexColor"]);
    svg = svg.replace("{{fillOpacity}}", result["fill.opacity"]);

    return svg;
  }

  int imageSize() {
    return to!int(style.size);
  }

  uint width() {
    uint size = imageSize;

    if(icon.measurements.width) {
      size = icon.measurements.width;
    }

    if(sizeFactor > 0) {
      size = to!uint(size.to!double * sizeFactor);
    }

    if(size > slotSize) {
      size = slotSize;
    }

    return size;
  }

  uint height() {
    uint size = imageSize;

    if(icon.measurements.height) {
      size = icon.measurements.height;
    }

    if(sizeFactor > 0) {
      size = to!uint(size.to!double * sizeFactor);
    }

    if(size > slotSize) {
      size = slotSize;
    }

    return size;
  }

  string generateShape() {
    auto md5 = new MD5Digest();
    string content;

    switch(style.shape) {
      case "circle":
        content = shapeSvg!"circle";
        break;

      case "square":
        content = shapeSvg!"square";
        break;

      case "hexagon":
        content = shapeSvg!"hexagon";
        break;

      case "pentagon":
        content = shapeSvg!"pentagon";
        break;

      default:
        return "";
    }

    auto cachedFile = buildPath(localImageCache,
      style.shape ~ "_" ~ this.shapeHash ~ "_" ~
      this.width.to!string ~ "_" ~ this.height.to!string ~
      ".png");

    if(!cachedFile.exists) {
      std.file.write(paths.svgShapePath, content);
      convert(paths.svgShapePath, cachedFile, this.width, this.height);
    }

    cachedFile.copy(paths.pngShapePath);

    return paths.pngShapePath;
  }

  string ensureLocalImage() {
    if(image is null) {
      return "";
    }

    auto cachedFile = buildPath(localImageCache, icon._id.to!string ~ paths.contentType.toExtension);

    if(!cachedFile.exists) {
      auto file = cachedFile.openFile(FileMode.createTrunc);
      logInfo("Downloading icon file to: `" ~ cachedFile ~ "`.");

      pipe(image.inputStream, file);
    }

    return cachedFile;
  }

  void generate() {
    auto md5 = new MD5Digest();
    auto shapePath = generateShape();

    auto cachedFile = buildPath(localImageCache,
      "result_" ~ icon._id.to!string ~ "_" ~ style.shape ~ "_" ~ this.shapeHash ~
      ".png");

    if(!cachedFile.exists) {
      auto hasShape = shapePath != "";

      double padding = 0;

      if(hasShape) {
        padding = imageSize * 0.3;
      }

      if(image is null) {
        resizeImage(shapePath, cachedFile, this.width, this.height);
      }

      if(image !is null) {
        auto localImage = ensureLocalImage;
        localImage.copy(paths.tmpFilePath);

        int iconSize = to!int((imageSize - padding) * sizeFactor);

        if(image.contentType == "image/svg+xml") {
          convert(paths.tmpFilePath, cachedFile, iconSize, iconSize);
        } else {
          resizeImage(paths.tmpFilePath, cachedFile, iconSize, iconSize);
        }

        overlay([shapePath, cachedFile], cachedFile);
      }
    }

    if(cachedFile.exists) {
      logInfo(`Using cached file as result: %s`, cachedFile);
    }

    if(cachedFile.exists) {
      cachedFile.copy(paths.resultFilePath);
    }
  }

  Json indexData() {
    auto record = Json.emptyObject;

    record["width"] = this.width;
    record["height"] = this.height;
    record["x"] = this.x;
    record["y"] = this.y;
    record["pixelRatio"] = 1;

    return record;
  }
}

struct Sprite {
  Icon[] icons;
  double sizeFactor;
  int spriteSize;
  string linkId;

  Json info;
  string tmpFolder;

  this(Icon[] icons, double sizeFactor, int spriteSize, string linkId) {
    this.icons = icons;
    this.sizeFactor = sizeFactor;
    this.spriteSize = spriteSize;
    this.linkId = linkId;

    info = Json.emptyObject;
    info["index"] = Json.emptyObject;
    info["size"] = Json.emptyObject;

    auto id = letters.byCodeUnit.randomSample(20).to!string;
    tmpFolder = tempDir.buildPath(id);
    tmpFolder.createDirectory;
  }

  void clean() {
    tmpFolder.rmdirRecurse;
  }

  uint canvasSize() {
    return 4096;
  }

  string path() {
    return buildPath(tmpFolder, "result.png");
  }

  void generate() {
    logInfo("Building the sprite in: `" ~ tmpFolder ~ "`.");

    uint x = 0;
    uint y = 0;
    uint height = 0;

    string files;

    createCanvas(this.path, this.canvasSize, this.canvasSize, "transparent");

    foreach(index, icon; icons) {
      auto image = icon.image.value;

      auto spriteIcon = SpriteIcon(tmpFolder, icon, sizeFactor, spriteSize, x, y, index);

      if(x + spriteIcon.width >= canvasSize) {
        x = 0;
        y += height;
        height = 0;
      }

      try {
        spriteIcon.generate;
        x += spriteIcon.width;
        height = max(height, spriteIcon.height);

        pasteIcon(spriteIcon.paths.resultFilePath, this.path, spriteIcon.x, spriteIcon.y, spriteIcon.width, spriteIcon.height);
      } catch(Exception e) {
        error("Can't generate the sprite for icon %s %s %s", icon._id, icon.name, e.message);

        debug {
          import std.stdio;
          writeln(e);
        }

        continue;
      }

      auto key = icon._id.to!string;
      if(key == "") {
        key = icon.name;
      }

      info["index"][key] = spriteIcon.indexData;
      info["index"][icon.name] = spriteIcon.indexData;

      files ~= spriteIcon.paths.resultFilePath ~ " ";

      logInfo("Icon `%s` added to the sprite. %s icons left for %s.", icon.name, icons.length - index, linkId);
    }

    crop(this.path, 0, 0, canvasSize, y + height);
    info["size"]["width"] = canvasSize;
    info["size"]["height"] = y + height;
  }
}

struct IconSetSprite {
  struct Request {
    ObjectId id;
  }

  enum type = "IconSet.sprite";
  alias Model = IconSet;

  OgmCrates crates;
  IBroadcast broadcast;
  string tmpFolder;
  int spriteSize = 64;

  static Json[] processList(const ChangeSet changeSet, OgmCrates crates) {
    if(changeSet.model == "Icon") {
      Json[] results;
      auto range = crates.icon.get
        .where("_id").equal(changeSet.itemId)
        .and.exec;

      foreach(icon; range) {
        results ~= icon["iconSet"];
      }

      if("iconSet" in changeSet.added) {
        results ~= changeSet.added["iconSet"];
      }

      if("iconSet" in changeSet.removed) {
        results ~= changeSet.removed["iconSet"];
      }

      return results.sort.uniq.array;
    }

    if(changeSet.model != Model.stringof) {
      return [];
    }

    return [ changeSet.itemId.toJson ];
  }

  void main(Request request) {
    auto pictures = crates.picture.get
      .where("meta.link.modelId").equal(request.id.to!string).and
      .where("meta.link.model").equal("IconSet").and
      .where("meta.link.type").equal("sprite").and
      .and.exec;

    foreach (picture; pictures) {
      crates.picture.deleteItem(picture["_id"].to!string);
    }

    auto size = crates.iconSet.get
      .where("_id").equal(request.id)
      .and.size;

    if(size == 0) {
      logInfo("Icon set does not exist. Can not generate a sprite for the icon set: `" ~ request.id.toString ~ "`.");
      return;
    }

    auto iconSetJson = crates.iconSet.get
      .where("_id").equal(request.id)
      .and.exec.front;

    auto resolver = toDelegate(&itemResolver);

    IconSet iconSet;

    try {
      iconSet = LazyIconSet.fromJson(iconSetJson, resolver).toType;
    } catch(Exception e) {
      error("The icon set %s is invalid. Can't generate the sprites.", request.id);

      debug {
        import std.stdio;
        writeln(e);
      }
      return;
    }

    auto icons = crates.icon.get
      .where("iconSet").equal(request.id)
      .and.exec.map!(a => LazyIcon.fromJson(a, resolver).toType).array;

    foreach (ref icon; icons) {
      if(!icon.styles.hasCustomStyle) {
        icon.styles.types = iconSet.styles;
      }
    }

    Picture[] sprites;

    foreach (sizeFactor; 1..3) {
      auto sprite = Sprite(icons, sizeFactor, spriteSize, request.id.toString);
      sprite.generate();
      scope(exit) sprite.clean;

      auto picture = Picture();
      picture.name = iconSet.name ~ " sprite @" ~ sizeFactor.to!string;
      picture.picture = PictureFile.fromLocalFile(sprite.path);
      picture.meta.link = Json.emptyObject;
      picture.meta.link["model"] = "IconSet";
      picture.meta.link["modelId"] = iconSet._id.toString;
      picture.meta.link["type"] = "sprite";
      picture.meta.link["size"] = sizeFactor;
      picture.meta.data = sprite.info;

      auto result = crates.picture.addItem(picture.serializeToJson);

      picture._id = ObjectId(result["_id"]);

      sprites ~= picture;
    }

    iconSet.sprites.small = sprites[0];
    iconSet.sprites.large = sprites[1];

    crates.iconSet.updateItem(LazyIconSet.fromModel(iconSet).toJson);
  }
}
