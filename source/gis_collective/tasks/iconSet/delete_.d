/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.iconSet.delete_;

import vibe.data.json;
import vibe.core.log;

import std.datetime;
import std.algorithm;
import std.array;
import std.conv;

import crate.base;
import vibe.service.stats;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import ogm.crates.all;
import ogm.models.meta;
import ogm.meta;

version(unittest) {
  import fluent.asserts;
  import ogm.test.fixtures;
}

struct IconSetDelete {
  struct Request {
    ObjectId id;
  }

  enum type = "IconSet.delete";
  alias Model = IconSet;

  OgmCrates crates;
  IBroadcast broadcast;

  static Json[] processList(const ChangeSet changeSet) {
    if(changeSet.model != Model.stringof) {
      return [];
    }

    if(changeSet.type != CrateChangeType.delete_) {
      return [];
    }

    return [ changeSet.itemId.toJson ];
  }

  void main(Request request) {
    auto size = crates.iconSet.get
      .where("_id").equal(request.id)
      .and.size;

    if(size > 0) {
      logInfo("Can not remove the icons for set `" ~ request.id.toString ~ "` because it exists.");
      return;
    }

    auto icons = crates.icon.get
      .where("iconSet").equal(request.id)
      .and.exec.array;

    logInfo("Removing `" ~ icons.length.to!string ~ "` icons related to icon set `" ~ request.id.toString ~ "`.");

    foreach(icon; icons) {
      crates.icon.deleteItem(icon["_id"].to!string);
    }
  }
}
