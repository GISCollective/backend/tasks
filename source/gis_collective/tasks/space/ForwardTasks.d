/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module gis_collective.tasks.space.ForwardTasks;

import crate.base;
import vibe.data.json;
import gis_collective.tasks.GisTask;
import ogm.models.changeset;


struct ForwardTasksSpaceMessage {
  Json id;
}

struct ForwardTasksSpaceTask {
  enum type = "space.forward";

  IBroadcast broadcast;

  @onChange
  static Json[] processList(ChangeSet changeSet) {
    if(changeSet.model != "Space") {
      return [];
    }

    return [ changeSet.itemId.toJson ];
  }

  void main(ForwardTasksSpaceMessage message) {
    auto jsonMessage = message.serializeToJson;

    broadcast.push("space.domainSetup", jsonMessage);
    broadcast.push("space.style", jsonMessage);
  }
}
