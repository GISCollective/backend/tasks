/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.fixtures;

public import fluent.asserts : should;
public import trial.discovery.spec;

public import ogm.crates.all;
public import ogm.test.fixtures;
public import gis_collective.hmq.broadcast.base;

import vibe.core.log;

shared static this() {
  foreach(logger; getLoggers()) {
    deregisterLogger(logger);
  }
}
