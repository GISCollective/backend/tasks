/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.http;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;

import crate.collection.notifications;

import ogm.models.changeset;
/*
alias suite = Spec!({
  URLRouter router;

  describe("with a task http service", {
    HttpBroadcast subscriber;

    beforeEach({
      setupTestData();
      router = new URLRouter;

      subscriber = new HttpBroadcast(router, "", "");
    });

    it("should refuse invalid tasks", {
      Json data = "{}".parseJsonString;
      router
        .request
        .post("/")
        .send(data)
        .expectStatusCode(400)
        .expectHeader("Content-Type", "application/json")
        .end((Response response) => {
          response.bodyJson.should.equal(`{"errors": [{
            "title": "Invalid task",
            "description": "The provided task is not valid",
            "status": 400 }]}`.parseJsonString);
        });
    });

    it("should accept a change set", {
      ChangeSet changeSet;

      changeSet._id = "000000000000000000000001";
      changeSet.model = "Test";
      changeSet.time = SysTime.fromISOExtString("2019-06-02T01:26:54Z");
      changeSet.author = "@bogdan";
      changeSet.itemId = "000000000000000000000001";
      changeSet.added["field1"] = "random value";
      changeSet.removed["field2"] = "another random value";

      Json data = parseJsonString(`{ "changeSet": ` ~ changeSet.serializeToJson.to!string ~ ` }`);

      size_t called;
      void testHandler(const Json change) @safe {
        called++;
        change.deserializeJson!ChangeSet.should.equal(changeSet);
      }

      subscriber.register("test", &testHandler);

      router
        .request
        .post("/test")
        .send(data)
        .expectStatusCode(200)
        .expectHeader("Content-Type", "application/json")
        .end((Response response) => {
          response.bodyJson.should.equal(`{}`.parseJsonString);
          called.should.equal(1);
        });
    });

    it("should not accept invalid change sets", {

      Json data = parseJsonString(`{ "changeSet": {} }`);

      size_t called;
      void testHandler(const Json change) @safe {
        called++;
      }

      subscriber.register("test", &testHandler);

      router
        .request
        .post("/test")
        .send(data)
        .expectStatusCode(400)
        .expectHeader("Content-Type", "application/json")
        .end((Response response) => {
          response.bodyJson.should.equal(`{ "errors": [{
            "description": "The provided task is not valid",
            "title": "Invalid task",
            "status": 400
          }]}`.parseJsonString);
          called.should.equal(0);
        });
    });

    it("should accept a change set", {
      CrateChange crateChange;

      crateChange.modelName = "Test";
      crateChange.before = Json.emptyObject;
      crateChange.after = Json.emptyObject;

      Json data = parseJsonString(`{ "crateChange": ` ~ crateChange.serializeToJson.to!string ~ ` }`);

      size_t called;
      void testHandler(const Json change) @trusted {
        called++;

        ChangeSet tmp = change.deserializeJson!ChangeSet;


        tmp._id = "0";
        tmp.itemId = "0";

        tmp.serializeToJson.should.equal(parseJsonString(`{
          "time": "` ~ tmp.time.toISOExtString ~ `",
          "itemId": "000000000000000000000000",
          "_id": "000000000000000000000000",
          "removed": {},
          "added": {},
          "type": "add",
          "author": "",
          "model": "Test"
        }`));
      }

      subscriber.register("test", &testHandler);

      router
        .request
        .post("/test")
        .send(data)
        .expectStatusCode(200)
        .expectHeader("Content-Type", "application/json")
        .end((Response response) => {
          response.bodyJson.should.equal(`{}`.parseJsonString);
          called.should.equal(1);
        });
    });

    it("should accept a task trigger", {
      Json data = parseJsonString(`{ "trigger": { "model": "Test", "task": "some-task", "_id": "000000000000000000000001" } }`);

      size_t called;
      string id;
      void testHandler(const Json _id) @trusted {
        called++;
        id = _id.to!string;
      }

      subscriber.registerTrigger("test", &testHandler);

      router
        .request
        .post("/test")
        .send(data)
        .expectStatusCode(200)
        .expectHeader("Content-Type", "application/json")
        .end((Response response) => {
          response.bodyJson.should.equal(`{}`.parseJsonString);
          called.should.equal(1);
          id.should.equal("000000000000000000000001");
        });
    });
  });
});
*/