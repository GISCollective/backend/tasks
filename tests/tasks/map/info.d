/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.map.info;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;

import crate.collection.notifications;

import ogm.models.changeset;
import gis_collective.tasks.map.info;


alias suite = Spec!({
  URLRouter router;

  describe("The Map.info task", {
    describe("Processing list", {
      ChangeSet changeSet;

      beforeEach({
        setupTestData();

        changeSet = ChangeSet();
        changeSet.model = "Map";
        changeSet.itemId = ObjectId.fromString("1");
      });

      it("should process any changes in fields inside maps", {
        changeSet.model = "Map";
        changeSet.removed["area"] = "";

        MapInfo.processList(changeSet, crates).should.equal([changeSet.itemId.toJson]);
      });

      it("should not process a map delete", {
        changeSet.model = "Map";
        changeSet.type = CrateChangeType.delete_;

        MapInfo.processList(changeSet, crates).should.equal([ ]);
      });

      it("should process any changes inside features maps", {
        changeSet.model = "Feature";
        changeSet.added["maps[1]"] = `"1"`.parseJsonString;
        changeSet.added["maps[2]"] = `"2"`.parseJsonString;

        changeSet.removed["maps[1]"] = `"3"`.parseJsonString;
        changeSet.removed["maps[2]"] = `"4"`.parseJsonString;

        MapInfo.processList(changeSet, crates).should.contain([Json("1"),Json("2"),Json("3"),Json("4")]);
      });

      it("should process feature maps when the visibility is changed", {
        changeSet.model = "Feature";
        changeSet.added["visibility"] = 1;
        changeSet.removed["visibility"] = 0;

        MapInfo.processList(changeSet, crates).should.equal([Json("000000000000000000000001")]);
      });

      it("should process feature maps when the contributors are changed", {
        changeSet.model = "Feature";
        changeSet.added["contributors"] = `[]`.parseJsonString;
        changeSet.removed["contributors"] = `[]`.parseJsonString;

        MapInfo.processList(changeSet, crates).should.equal([Json("000000000000000000000001")]);
      });

      it("should process feature maps when the author are changed", {
        changeSet.model = "Feature";
        changeSet.added["info.author"] = `""`.parseJsonString;
        changeSet.removed["info.author"] = `""`.parseJsonString;

        MapInfo.processList(changeSet, crates).should.equal([Json("000000000000000000000001")]);
      });

      it("should process feature maps when the original author is changed", {
        changeSet.model = "Feature";
        changeSet.added["info.originalAuthor"] = `""`.parseJsonString;
        changeSet.removed["info.originalAuthor"] = `""`.parseJsonString;

        MapInfo.processList(changeSet, crates).should.equal([Json("000000000000000000000001")]);
      });

      it("should not process changes in the Picture model", {
        changeSet.model = "Picture";

        MapInfo.processList(changeSet, crates).should.equal([]);
      });
    });

    describe("Metadata", {
      it("should count all features, public feature and contributors", {
        setupTestData();

        MapInfo("000000000000000000000001", crates).start;

        crates.meta.get.exec.front.should.equal(`{
          "_id": "000000000000000000000001",
          "type": "Map.info",
          "itemId": "000000000000000000000001",
          "changeIndex": 0,
          "model": "Map",
          "data": {
            "publicFeatures": 1,
            "totalFeatures": 1,
            "totalContributors": 1
          }
        }`.parseJsonString);
      });

      it("should count all features, public feature and contributors when there is a feature with missing data", {
        setupTestData();

        auto feature = crates.feature.getItem("000000000000000000000001").exec.front;
        feature["contributors"] = null;
        crates.feature.updateItem(feature);

        MapInfo("000000000000000000000001", crates).start;

        crates.meta.get.exec.front.should.equal(`{
          "_id": "000000000000000000000001",
          "type": "Map.info",
          "itemId": "000000000000000000000001",
          "changeIndex": 0,
          "model": "Map",
          "data": {
            "publicFeatures": 1,
            "totalFeatures": 1,
            "totalContributors": 1
          }
        }`.parseJsonString);
      });
    });
  });
});