/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.map.MapSprite;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;
import std.path;
import std.file;

import vibe.core.file;
import vibe.stream.memory;

import crate.collection.notifications;

import ogm.models.meta;
import ogm.models.changeset;

import gis_collective.tasks.map.MapSprite;
import gis_collective.hmq.broadcast.memory;


alias suite = Spec!({
  URLRouter router;

  describe("The MapSprite task", {
    beforeEach({
      if("sprites-cache".exists) {
        rmdirRecurse("sprites-cache");
      }

      setupTestData();
    });

    describe("Processing list", {
      ChangeSet changeSet;

      beforeEach({
        changeSet = ChangeSet();
        changeSet.model = "Map";
        changeSet.itemId = ObjectId.generate;
      });

      describe("when there is a map with a custom icon set list", {
        Json mapId;

        beforeEach({
          auto map = crates.map.getItem("000000000000000000000001").exec.front;
          map["iconSets"]["useCustomList"] = true;
          map["iconSets"]["list"] = "000000000000000000000001";
          crates.map.updateItem(map);

          mapId = map["_id"];
        });

        it("should process add changes inside the Maps", {
          changeSet.model = "Map";
          changeSet.type = CrateChangeType.add;
          changeSet.itemId = ObjectId.fromJson(mapId);

          MapSprite.processList(changeSet, crates).should.equal([ changeSet.itemId.toJson ]);
        });

        it("should process update changes inside the Maps", {
          changeSet.model = "Map";
          changeSet.type = CrateChangeType.update;
          changeSet.itemId = ObjectId.fromJson(mapId);

          MapSprite.processList(changeSet, crates).should.equal([ changeSet.itemId.toJson ]);
        });

        it("should process changes inside IconSet", {
          changeSet.model = "IconSet";
          changeSet.itemId = ObjectId.fromString("000000000000000000000001");
          changeSet.type = CrateChangeType.delete_;

          MapSprite.processList(changeSet, crates).should.contain(mapId);
        });

        it("should process changes inside Icon", {
          changeSet.model = "Icon";
          changeSet.itemId = ObjectId.fromString("000000000000000000000001");
          changeSet.type = CrateChangeType.delete_;

          MapSprite.processList(changeSet, crates).should.contain(mapId);
        });
      });

      describe("when there is a map with the default icon set list", {
        Json mapId;

        beforeEach({
          auto map = crates.map.getItem("000000000000000000000001").exec.front;
          map["iconSets"]["useCustomList"] = false;
          map["iconSets"]["list"] = "000000000000000000000001";
          crates.map.updateItem(map);

          mapId = map["_id"];
        });

        it("should process add changes inside the Maps", {
          changeSet.model = "Map";
          changeSet.type = CrateChangeType.add;
          changeSet.itemId = ObjectId.fromJson(mapId);

          MapSprite.processList(changeSet, crates).should.equal([ ]);
        });

        it("should process update changes inside the Maps", {
          changeSet.model = "Map";
          changeSet.type = CrateChangeType.update;
          changeSet.itemId = ObjectId.fromJson(mapId);

          MapSprite.processList(changeSet, crates).should.equal([ ]);
        });

        it("should not process changes inside IconSet", {
          changeSet.model = "IconSet";
          changeSet.itemId = ObjectId.fromString("000000000000000000000001");
          changeSet.type = CrateChangeType.delete_;

          MapSprite.processList(changeSet, crates).should.not.contain(mapId);
        });

        it("should not process changes inside Icon", {
          changeSet.model = "Icon";
          changeSet.itemId = ObjectId.fromString("000000000000000000000001");
          changeSet.type = CrateChangeType.delete_;

          MapSprite.processList(changeSet, crates).should.not.contain(mapId);
        });
      });
    });

    describe("The task", {
      MapSprite task;
      ubyte[] svgIconContent;
      ubyte[] pngIconContent;

      beforeEach({
        task = MapSprite();

        crateGetters["IconSet"] = &crates.iconSet.getItem;
        crateGetters["Team"] = &crates.team.getItem;
        crateGetters["Picture"] = &crates.picture.getItem;

        IconSettings.files = crates.iconFiles;
        IconSettings.chunks = crates.iconChunks;
        PictureFileSettings.files = crates.pictureFiles;
        PictureFileSettings.chunks = crates.pictureChunks;

        svgIconContent = cast(ubyte[]) (buildPath("tests", "fixtures", "icon.svg").read);
        pngIconContent = cast(ubyte[]) (buildPath("tests", "fixtures", "icon.png").read);

        setMockFile("image/svg+xml", svgIconContent.dup);

        auto icon1 = crates.icon.getItem("000000000000000000000001").and.exec.front;
        icon1["image"]["value"] = "000000000000000000000001";
        icon1["iconSet"] = "000000000000000000000001";
        crates.icon.updateItem(icon1);

        foreach(int i; 1..10) {
          auto otherIcon = crates.icon.getItem("000000000000000000000002").and.exec.front;
          otherIcon["image"]["value"] = "000000000000000000000002";
          otherIcon["iconSet"] = "000000000000000000000001";
          crates.icon.addItem(otherIcon);
        }
      });

      it("does nothing when there is no map", {
        auto request = MapSprite.Request(ObjectId.fromString("0000000000000000000000a1"));

        task.crates = crates;
        task.main_(request);

        crates.picture.get
          .where("meta.link.modelId").equal("0000000000000000000000a1").and
          .where("meta.link.type").equal("sprite").and
          .and.size.should.equal(0);
      });

      it("deletes the sprites of a missing iconset", {
        auto picture = Picture();
        picture.meta.link = Json.emptyObject;
        picture.meta.link["model"] = "Map";
        picture.meta.link["modelId"] = "0000000000000000000000a1";
        picture.meta.link["type"] = "sprite";

        auto request = MapSprite.Request(ObjectId.fromString("0000000000000000000000a1"));

        task.crates = crates;
        task.main_(request);

        crates.picture.get
          .where("meta.link.modelId").equal("0000000000000000000000a1").and
          .where("meta.link.type").equal("sprite").and
          .and.size.should.equal(0);
      });

      it("builds the sprite when the set exists", {
        auto request = MapSprite.Request(ObjectId.fromString("000000000000000000000001"));

        task.crates = crates;
        task.main_(request);

        auto pictures = crates.picture.get
          .where("meta.link.modelId").equal("000000000000000000000001").and
          .where("meta.link.model").equal("Map").and
          .where("meta.link.type").equal("sprite").and
          .and.exec.array;

        pictures.length.should.equal(2);
        pictures[0]["meta"]["link"].should.equal(`{
          "type": "sprite",
          "size": 1,
          "modelId": "000000000000000000000001",
          "model": "Map"
        }`.parseJsonString);

        pictures[0]["meta"]["data"].should.equal(`{
          "size": { "height": 21, "width": 4096 },
          "index": {
            "group-0": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 210 },
            "name1": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 0 },
            "000000000000000000000001": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 0 },
            "000000000000000000000005": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 21 },
            "000000000000000000000006": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 42 },
            "000000000000000000000007": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 63 },
            "000000000000000000000008": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 84 },
            "000000000000000000000009": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 105 },
            "000000000000000000000010": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 126 },
            "000000000000000000000011": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 147 },
            "000000000000000000000012": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 168 },
            "000000000000000000000013": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 189 },
            "name2": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 189 },
            "group-1": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 231 },
            "group-2": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 252 },
            "group-3": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 273 },
            "group-4": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 294 },
            "group-5": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 315 },
            "group-6": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 336 },
            "group-7": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 357 },
            "group-8": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 378 },
            "group-9": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 399 }
          }
        }`.parseJsonString);

        pictures[1]["meta"]["link"].should.equal(`{
          "type": "sprite",
          "size": 2,
          "modelId": "000000000000000000000001",
          "model": "Map"
        }`.parseJsonString);

        pictures[1]["meta"]["data"].should.equal(`{
          "size": { "height": 42, "width": 4096 },
          "index": {
            "group-0": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 420 },
            "name1": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 0 },
            "000000000000000000000011": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 294 },
            "000000000000000000000005": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 42 },
            "000000000000000000000008": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 168 },
            "name2": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 378 },
            "000000000000000000000012": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 336 },
            "group-6": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 672 },
            "group-7": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 714 },
            "group-5": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 630 },
            "000000000000000000000001": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 0 },
            "000000000000000000000006": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 84 },
            "group-4": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 588 },
            "000000000000000000000010": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 252 },
            "group-2": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 504 },
            "group-9": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 798 },
            "group-1": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 462 },
            "000000000000000000000013": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 378 },
            "group-3": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 546},
            "000000000000000000000009": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 210 },
            "group-8": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 756 },
            "000000000000000000000007": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 126 }
          }
        }`.parseJsonString);
      });

      it("clears the sprites before they are regenerated", {
        auto request = MapSprite.Request(ObjectId.fromString("000000000000000000000001"));

        task.crates = crates;
        task.main_(request);
        task.main_(request);

        auto pictures = crates.picture.get
          .where("meta.link.modelId").equal("000000000000000000000001").and
          .where("meta.link.type").equal("sprite").and
          .and.exec.array;

        pictures.length.should.equal(2);
      });

      it("adds the sprite pictures to the map", {
        auto request = MapSprite.Request(ObjectId.fromString("000000000000000000000001"));

        task.crates = crates;
        task.main_(request);

        auto map = crates.map.getItem("000000000000000000000001").and.exec.front;

        map["sprites"].should.equal(`{ "small": "000000000000000000000003", "large": "000000000000000000000004" }`.parseJsonString);
      });
    });
  });

  describe("The MapSpriteDefault task", {
    beforeEach({
      setupTestData();
    });

    describe("Processing list", {
      ChangeSet changeSet;

      beforeEach({
        changeSet = ChangeSet();
        changeSet.model = "Map";
        changeSet.itemId = ObjectId.generate;
      });

      it("should not process add changes inside the Maps", {
        changeSet.model = "Map";
        changeSet.type = CrateChangeType.add;

        MapSpriteDefault.processList(changeSet, crates).should.equal([ ]);
      });

      describe("when there is a default icon set", {
        Json setId;

        beforeEach({
          auto set = crates.iconSet.getItem("000000000000000000000001").exec.front;
          set["visibility"]["isDefault"] = true;
          crates.iconSet.updateItem(set);

          setId = set["_id"];
        });

        it("should process add changes inside the IconSet", {
          changeSet.model = "IconSet";
          changeSet.type = CrateChangeType.add;
          changeSet.itemId = ObjectId.fromJson(setId);

          MapSpriteDefault.processList(changeSet, crates).should.equal([ changeSet.itemId.toJson ]);
        });

        it("should process update changes inside the IconSet", {
          changeSet.model = "IconSet";
          changeSet.type = CrateChangeType.update;
          changeSet.itemId = ObjectId.fromJson(setId);

          MapSpriteDefault.processList(changeSet, crates).should.equal([ changeSet.itemId.toJson ]);
        });

        it("should process changes inside Icon", {
          changeSet.model = "Icon";
          changeSet.itemId = ObjectId.fromString("000000000000000000000001");
          changeSet.type = CrateChangeType.delete_;

          MapSpriteDefault.processList(changeSet, crates).should.contain(setId);
        });
      });

      describe("when there is no default icon set", {
        Json setId;

        beforeEach({
          auto set = crates.iconSet.getItem("000000000000000000000001").exec.front;
          set["visibility"]["isDefault"] = false;
          crates.iconSet.updateItem(set);

          setId = set["_id"];
        });

        it("should not process add changes inside the IconSet", {
          changeSet.model = "IconSet";
          changeSet.type = CrateChangeType.add;
          changeSet.itemId = ObjectId.fromJson(setId);

          MapSpriteDefault.processList(changeSet, crates).should.equal([ ]);
        });

        it("should not process update changes inside the IconSet", {
          changeSet.model = "IconSet";
          changeSet.type = CrateChangeType.update;
          changeSet.itemId = ObjectId.fromJson(setId);

          MapSpriteDefault.processList(changeSet, crates).should.equal([ ]);
        });

        it("should process changes inside Icon", {
          changeSet.model = "Icon";
          changeSet.itemId = ObjectId.fromString("000000000000000000000001");
          changeSet.type = CrateChangeType.delete_;

          MapSpriteDefault.processList(changeSet, crates).should.equal([]);
        });
      });
    });

    describe("The task", {
      MapSpriteDefault task;
      ubyte[] svgIconContent;
      ubyte[] pngIconContent;

      beforeEach({
        auto set = crates.iconSet.getItem("000000000000000000000001").exec.front;
        set["visibility"]["isDefault"] = true;
        crates.iconSet.updateItem(set);

        task = MapSpriteDefault();

        crateGetters["IconSet"] = &crates.iconSet.getItem;
        crateGetters["Team"] = &crates.team.getItem;
        crateGetters["Picture"] = &crates.picture.getItem;

        IconSettings.files = crates.iconFiles;
        IconSettings.chunks = crates.iconChunks;
        PictureFileSettings.files = crates.pictureFiles;
        PictureFileSettings.chunks = crates.pictureChunks;

        svgIconContent = cast(ubyte[]) (buildPath("tests", "fixtures", "icon.svg").read);
        pngIconContent = cast(ubyte[]) (buildPath("tests", "fixtures", "icon.png").read);

        setMockFile("image/svg+xml", svgIconContent.dup);

        auto icon1 = crates.icon.getItem("000000000000000000000001").and.exec.front;
        icon1["image"]["value"] = "000000000000000000000001";
        icon1["iconSet"] = "000000000000000000000001";
        crates.icon.updateItem(icon1);
      });

      it("builds the default sprite", {
        auto request = MapSpriteDefault.Request(ObjectId.fromString("000000000000000000000001"));

        task.crates = crates;
        task.main(request);

        auto pictures = crates.picture.get
          .where("meta.link.modelId").equal("default").and
          .where("meta.link.model").equal("Map").and
          .where("meta.link.type").equal("sprite").and
          .and.exec.array;

        pictures.length.should.equal(2);
        pictures[0]["meta"]["link"].should.equal(`{
          "type": "sprite",
          "size": 1,
          "modelId": "default",
          "model": "Map"
        }`.parseJsonString);

        pictures[0]["meta"]["data"].should.equal(`{
          "size": { "height": 21, "width": 4096 },
          "index": {
            "group-0": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 21 },
            "group-6": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 147 },
            "group-8": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 189 },
            "group-7": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 168 },
            "group-5": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 126 },
            "000000000000000000000001": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 0 },
            "group-4": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 105 },
            "group-2": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 63 },
            "group-9": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 210 },
            "group-1": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 42 },
            "group-3": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 84 },
            "name1": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 0 }
          }
        }`.parseJsonString);

        pictures[1]["meta"]["link"].should.equal(`{
          "type": "sprite",
          "size": 2,
          "modelId": "default",
          "model": "Map"
        }`.parseJsonString);

        pictures[1]["meta"]["data"].should.equal(`{
          "size": { "height": 42, "width": 4096 },
          "index": {
            "group-0": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 42 },
            "group-6": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 294 },
            "group-8": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 378 },
            "group-7": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 336 },
            "group-5": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 252 },
            "000000000000000000000001": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 0 },
            "group-4": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 210 },
            "group-2": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 126 },
            "group-9": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 420 },
            "group-1": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 84 },
            "group-3": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 168 },
            "name1": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 0 }
          }
        }`.parseJsonString);
      });

      it("clears the sprites before they are regenerated", {
        auto request = MapSpriteDefault.Request(ObjectId.fromString("000000000000000000000001"));

        task.crates = crates;
        task.main(request);
        task.main(request);

        auto pictures = crates.picture.get
          .where("meta.link.modelId").equal("default").and
          .where("meta.link.type").equal("sprite").and
          .and.exec.array;

        pictures.length.should.equal(2);
      });
    });
  });

  describe("The MapSpriteAll task", {
    beforeEach({
      setupTestData();
    });

    describe("Processing list", {
      ChangeSet changeSet;

      beforeEach({
        changeSet = ChangeSet();
        changeSet.model = "Map";
        changeSet.itemId = ObjectId.generate;
      });

      it("should not process add changes inside the Maps", {
        changeSet.model = "Map";
        changeSet.type = CrateChangeType.add;

        MapSpriteAll.processList(changeSet, crates).should.equal([ ]);
      });

      describe("when there is a default icon set", {
        Json setId;

        beforeEach({
          auto set = crates.iconSet.getItem("000000000000000000000001").exec.front;
          set["visibility"]["isDefault"] = true;
          crates.iconSet.updateItem(set);

          setId = set["_id"];
        });

        it("should process add changes inside the IconSet", {
          changeSet.model = "IconSet";
          changeSet.type = CrateChangeType.add;
          changeSet.itemId = ObjectId.fromJson(setId);

          MapSpriteAll.processList(changeSet, crates).should.equal([ changeSet.itemId.toJson ]);
        });

        it("should process update changes inside the IconSet", {
          changeSet.model = "IconSet";
          changeSet.type = CrateChangeType.update;
          changeSet.itemId = ObjectId.fromJson(setId);

          MapSpriteAll.processList(changeSet, crates).should.equal([ changeSet.itemId.toJson ]);
        });

        it("should process changes inside Icon", {
          changeSet.model = "Icon";
          changeSet.itemId = ObjectId.fromString("000000000000000000000001");
          changeSet.type = CrateChangeType.delete_;

          MapSpriteAll.processList(changeSet, crates).should.contain(setId);
        });
      });

      describe("when there is no default icon set", {
        Json setId;

        beforeEach({
          auto set = crates.iconSet.getItem("000000000000000000000001").exec.front;
          set["visibility"]["isDefault"] = false;
          crates.iconSet.updateItem(set);

          setId = set["_id"];
        });

        it("should process add changes inside the IconSet", {
          changeSet.model = "IconSet";
          changeSet.type = CrateChangeType.add;
          changeSet.itemId = ObjectId.fromJson(setId);

          MapSpriteAll.processList(changeSet, crates).should.equal([ changeSet.itemId.toJson ]);
        });

        it("should process update changes inside the IconSet", {
          changeSet.model = "IconSet";
          changeSet.type = CrateChangeType.update;
          changeSet.itemId = ObjectId.fromJson(setId);

          MapSpriteAll.processList(changeSet, crates).should.equal([ changeSet.itemId.toJson ]);
        });

        it("should process changes inside Icon", {
          changeSet.model = "Icon";
          changeSet.itemId = ObjectId.fromString("000000000000000000000001");
          changeSet.type = CrateChangeType.delete_;

          MapSpriteAll.processList(changeSet, crates).should.contain(setId);
        });
      });
    });

    describe("The task", {
      MapSpriteAll task;
      ubyte[] svgIconContent;
      ubyte[] pngIconContent;

      beforeEach({
        auto set = crates.iconSet.getItem("000000000000000000000001").exec.front;
        set["visibility"]["isDefault"] = false;
        crates.iconSet.updateItem(set);

        task = MapSpriteAll();

        crateGetters["IconSet"] = &crates.iconSet.getItem;
        crateGetters["Team"] = &crates.team.getItem;
        crateGetters["Picture"] = &crates.picture.getItem;

        IconSettings.files = crates.iconFiles;
        IconSettings.chunks = crates.iconChunks;
        PictureFileSettings.files = crates.pictureFiles;
        PictureFileSettings.chunks = crates.pictureChunks;

        svgIconContent = cast(ubyte[]) (buildPath("tests", "fixtures", "icon.svg").read);
        pngIconContent = cast(ubyte[]) (buildPath("tests", "fixtures", "icon.png").read);

        setMockFile("image/svg+xml", svgIconContent.dup);

        auto icon1 = crates.icon.getItem("000000000000000000000001").and.exec.front;
        icon1["image"]["value"] = "000000000000000000000001";
        icon1["iconSet"] = "000000000000000000000001";
        crates.icon.updateItem(icon1);
      });

      it("builds the all sprite", {
        auto request = MapSpriteAll.Request(ObjectId.fromString("000000000000000000000001"));

        task.crates = crates;
        task.main(request);

        auto pictures = crates.picture.get
          .where("meta.link.modelId").equal("_").and
          .where("meta.link.model").equal("Map").and
          .where("meta.link.type").equal("sprite").and
          .and.exec.array;

        pictures.length.should.equal(2);
        pictures[0]["meta"]["link"].should.equal(`{
          "type": "sprite",
          "size": 1,
          "modelId": "_",
          "model": "Map"
        }`.parseJsonString);

        pictures[0]["meta"]["data"].should.equal(`{
          "size":{"height":21,"width":4096},
          "index":{
            "group-0":{"height":21,"width":21,"pixelRatio":1,"y":0,"x":0},
            "group-6":{"height":21,"width":21,"pixelRatio":1,"y":0,"x":126},
            "group-8":{"height":21,"width":21,"pixelRatio":1,"y":0,"x":168},
            "group-7":{"height":21,"width":21,"pixelRatio":1,"y":0,"x":147},
            "group-5":{"height":21,"width":21,"pixelRatio":1,"y":0,"x":105},
            "group-4":{"height":21,"width":21,"pixelRatio":1,"y":0,"x":84},
            "group-2":{"height":21,"width":21,"pixelRatio":1,"y":0,"x":42},
            "group-9":{"height":21,"width":21,"pixelRatio":1,"y":0,"x":189},
            "group-1":{"height":21,"width":21,"pixelRatio":1,"y":0,"x":21},
            "group-3":{"height":21,"width":21,"pixelRatio":1,"y":0,"x":63}
          }
        }`.parseJsonString);

        pictures[1]["meta"]["link"].should.equal(`{
          "type": "sprite",
          "size": 2,
          "modelId": "_",
          "model": "Map"
        }`.parseJsonString);
      });

      it("clears the sprites before they are regenerated", {
        auto request = MapSpriteAll.Request(ObjectId.fromString("000000000000000000000001"));

        task.crates = crates;
        task.main(request);
        task.main(request);

        auto pictures = crates.picture.get
          .where("meta.link.modelId").equal("_").and
          .where("meta.link.type").equal("sprite").and
          .and.exec.array;

        pictures.length.should.equal(2);
      });
    });
  });
});