/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.map.MapTilesRequest;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;

import crate.collection.notifications;

import ogm.models.changeset;
import gis_collective.tasks.map.tiles.MapTilesRequest;

import gis_collective.hmq.broadcast.memory;

import geo.xyz;

alias suite = Spec!({
  describe("The MapTilesRequest task", {
    IBroadcast broadcast;

    SysTime now;

    beforeEach({
      setupTestData;
      broadcast = new MemoryBroadcast();

      auto map = crates.map.getItem("000000000000000000000001").exec.front;
      map["info"]["changeIndex"] = 10;
      crates.map.updateItem(map);

      crateGetters[`Map`] = &crates.map.getItem;
      crateGetters[`Team`] = &crates.team.getItem;
      crateGetters[`IconSet`] = &crates.iconSet.getItem;
      crateGetters[`Picture`] = &crates.picture.getItem;

      now = Clock.currTime;
      now.fracSecs = 0.msecs;
    });

    describe("processing list", {
      it("should return the map id", {
        ChangeSet changeSet;
        changeSet.model = "Map";
        changeSet.itemId = ObjectId.fromString("1");

        auto task = new MapTilesRequest(crates, broadcast);

        task.processList(changeSet).should.equal(["000000000000000000000001"]);
      });

      it("should ignore a feature", {
        ChangeSet changeSet;
        changeSet.model = "Feature";
        changeSet.itemId = ObjectId.fromString("1");

        auto task = new MapTilesRequest(crates, broadcast);

        task.processList(changeSet).should.equal([]);
      });
    });

    describe("running the task", {
      describe("for an existing map with points", {
        it("should generate 0 tiles when the map is empty", {
          crates.feature = new MemoryCrate!Feature;

          bool isCalled;
          broadcast.register("MapTiles", (const Json message) {
            isCalled = true;
          });

          auto task = new MapTilesRequest(crates, broadcast);
          task.start("000000000000000000000001");

          isCalled.should.equal(false);
        });

        it("it should generate 37 tiles when the map contains a feature", {
          Json[] requestList;

          broadcast.register("MapTiles", (const Json request) {
            requestList ~= request.clone;
          });

          auto task = new MapTilesRequest(crates, broadcast);
          task.start("000000000000000000000001");

          requestList.length.should.equal(37);
          requestList[0].should.equal(`{ "value": {
            "changeIndex": 10,
            "path": { "z": 0, "y": 0, "x": 0 },
            "mapId": "000000000000000000000001"
          }}`.parseJsonString);
        });

        it("should generate 37 tiles that contain two features with the same coordinates", {
          Json[] requestList;

          broadcast.register("MapTiles", (const Json request) {
            requestList ~= request.clone;
          });

          createFeature(
            Feature(ObjectId.fromString("1"),
                [map1],
                "site1",
                Json("description of site1"),
                GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.5, 1.5] }`.parseJsonString)));

          createFeature(
            Feature(ObjectId.fromString("2"),
                [map1],
                "site1",
                Json("description of site1"),
                GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.5, 1.5] }`.parseJsonString)));

          auto task = new MapTilesRequest(crates, broadcast);
          task.start("000000000000000000000001");

          requestList.length.should.equal(37);
          requestList[0].should.equal(`{ "value": {
            "changeIndex": 10,
            "path": { "z": 0, "y": 0, "x": 0 },
            "mapId": "000000000000000000000001"
          }}`.parseJsonString);
        });

        it("should generate 37 tiles that contain two features with different coordinates", {
          Json[] requestList;

          broadcast.register("MapTiles", (const Json request) {
            requestList ~= request.clone;
          });

          createFeature(
            Feature(ObjectId.fromString("1"),
                [map1],
                "",
                Json(""),
                GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.5, 1.5] }`.parseJsonString)));

          createFeature(
            Feature(ObjectId.fromString("2"),
                [map1],
                "",
                Json(""),
                GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.5, 1.5] }`.parseJsonString)));

          auto task = new MapTilesRequest(crates, broadcast);
          task.start("000000000000000000000001");

          requestList.length.should.equal(37);
          requestList[0].should.equal(`{ "value": {
            "changeIndex": 10,
            "path": { "z": 0, "y": 0, "x": 0 },
            "mapId": "000000000000000000000001"
          }}`.parseJsonString);
        });

        it("should generate 36 tiles when a site at 1.5,1.5", {
          Json[] requestList;

          broadcast.register("MapTiles", (const Json request) {
            requestList ~= request.clone;
          });

          createFeature(
            Feature(ObjectId.fromString("1"),
                [map1],
                "",
                Json(""),
                GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.5, 1.5] }`.parseJsonString)));

          crates.tileLayer.addItem(`{
            "mapId": "000000000000000000000001",
            "teamId": "000000000000000000000001",
            "changeIndex": 10,
            "isReady": true,
            "isPublic": true,
            "extent": 512,
            "geometries": [],
            "properties": [],
            "layerName": "sites",
            "path": { "z": 0, "y": 0, "x": 0 }
          }`.parseJsonString);

          auto task = new MapTilesRequest(crates, broadcast);
          task.start("000000000000000000000001");

          requestList.length.should.equal(36);
        });

        it("should generate a meta record containing the tile count", {
          auto task = new MapTilesRequest(crates, broadcast);
          task.start("000000000000000000000001");

          auto result = crates.meta.get
            .where("type")
            .equal("MapTiles")
            .and.exec.array;

          result.length.should.equal(1);
          result[0].should.equal((`{
            "_id": "000000000000000000000001",
            "type": "MapTiles",
            "model": "Map",
            "itemId": "000000000000000000000001",
            "changeIndex": 10,
            "data": {
              "layer": "sites",
              "tiles": [
                "0.0.0",
                "1.0.0",
                "1.0.1",
                "1.1.0",
                "10.516.507",
                "11.1032.1015",
                "12.2064.2029",
                "12.2064.2030",
                "12.2064.2031",
                "12.2065.2030",
                "13.4130.4061",
                "14.8260.8123",
                "15.16520.16247",
                "16.33040.32493",
                "16.33040.32494",
                "16.33040.32495",
                "16.33041.32494",
                "2.1.0",
                "2.1.1",
                "2.1.2",
                "2.2.1",
                "3.3.2",
                "3.3.3",
                "3.3.4",
                "3.4.3",
                "4.7.6",
                "4.7.7",
                "4.7.8",
                "4.8.7",
                "5.16.15",
                "6.32.31",
                "7.64.63",
                "8.128.125",
                "8.128.126",
                "8.128.127",
                "8.129.126",
                "9.258.253" ],
              "createdOn": "` ~ now.toISOExtString ~ `",
              "processedTileCount": 0
            }
          }`).parseJsonString);
        });

        it("should trigger a MapTilesCheck event", {
          Json receivedMessage;

          broadcast.register("MapTilesCheck", (const Json message) @safe {
            receivedMessage = message.clone;
          });

          auto task = new MapTilesRequest(crates, broadcast);
          task.start("000000000000000000000001");

          receivedMessage.should.equal(`{ "value": "000000000000000000000001" }`.parseJsonString);
        });

        describe("When a meta for the map exists", {
          beforeEach({
            crates.meta.addItem(`{
              "_id": "000000000000000000000001",
              "type": "MapTiles",
              "model": "Map",
              "itemId": "000000000000000000000001",
              "changeIndex": 10,
              "data": {
                "layer": "sites",
                "tiles": [ ],
                "processedTileCount": 0
              }
            }`.parseJsonString);
          });

          it("should replace the previous meta record", {
            auto task = new MapTilesRequest(crates, broadcast);
            task.start("000000000000000000000001");

            auto result = crates.meta.get
              .where("type")
              .equal("MapTiles")
              .and.exec.array;

            result.length.should.equal(1);
            result[0].should.equal((`{
              "_id": "000000000000000000000001",
              "type": "MapTiles",
              "model": "Map",
              "itemId": "000000000000000000000001",
              "changeIndex": 10,
              "data": {
                "layer": "sites",
                "tiles": [
                        "0.0.0",
                        "1.0.0",
                        "1.0.1",
                        "1.1.0",
                        "10.516.507",
                        "11.1032.1015",
                        "12.2064.2029",
                        "12.2064.2030",
                        "12.2064.2031",
                        "12.2065.2030",
                        "13.4130.4061",
                        "14.8260.8123",
                        "15.16520.16247",
                        "16.33040.32493",
                        "16.33040.32494",
                        "16.33040.32495",
                        "16.33041.32494",
                        "2.1.0",
                        "2.1.1",
                        "2.1.2",
                        "2.2.1",
                        "3.3.2",
                        "3.3.3",
                        "3.3.4",
                        "3.4.3",
                        "4.7.6",
                        "4.7.7",
                        "4.7.8",
                        "4.8.7",
                        "5.16.15",
                        "6.32.31",
                        "7.64.63",
                        "8.128.125",
                        "8.128.126",
                        "8.128.127",
                        "8.129.126",
                        "9.258.253"],
                "createdOn": "` ~ now.toISOExtString ~ `",
                "processedTileCount": 0
              }
            }`).parseJsonString);
          });
        });
      });

      describe("for an existing map with a line", {
        beforeEach({
          crates.feature = new MemoryCrate!Feature;

          createFeature(
            Feature(ObjectId.fromString("5"),
                  [map1],
                  "line",
                  Json("description of line"),
                  GeoJsonGeometry.fromJson(`{ "type": "LineString", "coordinates": [[1.5, 1.5], [2,2], [10,10]] }`.parseJsonString),
                  ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z"), 20),
                  [],
                  [], [icon1], VisibilityEnum.Public));
        });

        it("it should generate 6217 tiles", {
          Json[] requestList;

          broadcast.register("MapTiles", (const Json request) {
            requestList ~= request.clone;
          });

          auto task = new MapTilesRequest(crates, broadcast);
          task.start("000000000000000000000001");

          requestList.length.should.equal(6217);
          requestList[0].should.equal(`{ "value": {
            "changeIndex": 10,
            "path": { "z": 0, "y": 0, "x": 0 },
            "mapId": "000000000000000000000001"
          }}`.parseJsonString);
        });
      });

      describe("for an existing map with a multi line", {
        beforeEach({
          crates.feature = new MemoryCrate!Feature;

          createFeature(
            Feature(ObjectId.fromString("5"),
                  [map1],
                  "line",
                  Json("description of line"),
                  GeoJsonGeometry.fromJson(`{ "type": "MultiLineString", "coordinates": [[[1.5, 1.5], [2, 2]], [[9, 9], [11, 11], [10, 10]]] }`.parseJsonString),
                  ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z"), 20),
                  [],
                  [],
                  [icon1], VisibilityEnum.Public));
        });

        it("it should generate 1887 tiles", {
          Json[] requestList;

          broadcast.register("MapTiles", (const Json request) {
            requestList ~= request.clone;
          });

          auto task = new MapTilesRequest(crates, broadcast);
          task.start("000000000000000000000001");

          requestList.length.should.equal(1887);
          requestList[0].should.equal(`{ "value": {
            "changeIndex": 10,
            "path": { "z": 0, "y": 0, "x": 0 },
            "mapId": "000000000000000000000001"
          }}`.parseJsonString);
        });
      });

      describe("for an existing map with a polygon", {
        beforeEach({
          crates.feature = new MemoryCrate!Feature;

          createFeature(
            Feature(ObjectId.fromString("5"),
                  [map1],
                  "polygon",
                  Json("description of polygon"),
                  GeoJsonGeometry.fromJson(`{ "type": "Polygon", "coordinates": [[ [100.0, 0.0], [101.0, 0.0], [101.0, 1.0], [100.0, 1.0], [100.0, 0.0] ]] }`.parseJsonString),
                  ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z"), 20),
                  [],
                  [],
                  [icon1], VisibilityEnum.Public));
        });

        it("should generate 45209 tiles", {
          Json[] requestList;

          broadcast.register("MapTiles", (const Json request) {
            requestList ~= request.clone;
          });

          auto task = new MapTilesRequest(crates, broadcast);
          task.start("000000000000000000000001");

          requestList.length.should.equal(45209);
          requestList[0].should.equal(`{ "value": {
            "changeIndex": 10,
            "path": {"z":10,"y":509,"x":796},
            "mapId": "000000000000000000000001"
          }}`.parseJsonString);
        });
      });

      describe("for an existing map with a multi polygon", {
        beforeEach({
          crates.feature = new MemoryCrate!Feature;

          auto geometry = GeoJsonGeometry.fromJson(`{ "type": "MultiPolygon",
                    "coordinates": [
                      [[[102.0, 2.0], [103.0, 2.0], [103.0, 3.0], [102.0, 3.0], [102.0, 2.0]]],
                      [[[100.0, 0.0], [101.0, 0.0], [101.0, 1.0], [100.0, 1.0], [100.0, 0.0]],
                      [[100.2, 0.2], [100.8, 0.2], [100.8, 0.8], [100.2, 0.8], [100.2, 0.2]]]
                    ]}`.parseJsonString);

          createFeature(
            Feature(ObjectId.fromString("5"),
                  [map1],
                  "polygon",
                  Json("description of polygon"),
                  geometry,
                  ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z"), 20),
                  [],
                  [],
                  [icon1], VisibilityEnum.Public));
        });

        it("should generate 3796 tiles", {
          Json[] requestList;

          broadcast.register("MapTiles", (const Json request) {
            requestList ~= request.clone;
          });

          auto task = new MapTilesRequest(crates, broadcast);
          task.start("000000000000000000000001");

          requestList.length.should.equal(90051);
          requestList[0].should.equal(`{ "value": {
            "changeIndex":10,
            "path":{"z":10,"y":509,"x":796},
            "mapId":"000000000000000000000001"
          }}`.parseJsonString);
        });
      });
    });
  });
});
