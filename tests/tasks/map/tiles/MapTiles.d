/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.map.tiles.MapTiles;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;

import crate.collection.notifications;

import ogm.models.changeset;
import gis_collective.tasks.map.tiles.MapTiles;
import gis_collective.tasks.map.tiles.MapTilesRequest;

import gis_collective.hmq.broadcast.memory;

import geo.xyz;

enum largePolygon = `{ "type": "Polygon", "coordinates": [[
                    [ 13.19958171050189, 52.62282258756287 ], [ 13.21819532784542, 52.62875318054193 ], [ 13.26328275349281, 52.64127528572067 ],
                    [ 13.28099464523561, 52.66113344877758 ], [ 13.31684215815013, 52.66081936184142 ], [ 13.31140471825492, 52.6282985865713 ],
                    [ 13.39762697945031, 52.6485679958746 ], [ 13.4263677331821, 52.63725599531142 ], [ 13.44423360712349, 52.64951053053105 ],
                    [ 13.47530469223895, 52.65657889290628 ], [ 13.45044782414658, 52.66270388266318 ], [ 13.46132270393699, 52.66929906568714 ],
                    [ 13.47705028398211, 52.67221717281421 ], [ 13.51794380515115, 52.64876584290755 ], [ 13.49689662010067, 52.60405039443685 ],
                    [ 13.50742021262591, 52.59169236077352 ], [ 13.52846739767639, 52.59339712439947 ], [ 13.55372401973697, 52.58529890645428 ],
                    [ 13.56845704927231, 52.57378833808249 ], [ 13.58319007880765, 52.57037721887235 ], [ 13.58739951581775, 52.55118472873585 ],
                    [ 13.63958180702201, 52.54250506667728 ], [ 13.63507469725127, 52.53840306481936 ], [ 13.63395876668525, 52.53772431763861 ],
                    [ 13.63183023245748, 52.53813910882931 ], [ 13.63050764808294, 52.53771174814736 ], [ 13.62904040604243, 52.5380511231466 ],
                    [ 13.62821379080834, 52.53761119208829 ], [ 13.6278004831913, 52.53757348350678 ], [ 13.6249899913954, 52.53798827612243 ],
                    [ 13.62583727201034, 52.53420471954405 ], [ 13.62484533372943, 52.53359643387867 ], [ 13.62575461048693, 52.53011421991138 ],
                    [ 13.62608525658056, 52.53035308095343 ], [ 13.62660189110187, 52.53016450655448 ], [ 13.6310862787468, 52.53060451222584 ],
                    [ 13.63201622088515, 52.53059194069638 ], [ 13.63609345951627, 52.53053714700349 ], [ 13.64015760061566, 52.5304728366624 ],
                    [ 13.64068225852252, 52.53042996304936 ], [ 13.64100342128102, 52.53041954892302 ], [ 13.64168850540358, 52.53033468820385 ],
                    [ 13.64272215978724, 52.53015366542795 ], [ 13.64348956985996, 52.52997026001245 ], [ 13.64394837970049, 52.52990530861152 ],
                    [ 13.64502830536291, 52.52981305476098 ], [ 13.64691942304211, 52.52970825094735 ], [ 13.64885664373294, 52.52970624622921 ],
                    [ 13.65695527938649, 52.52988457298881 ], [ 13.65775070241389, 52.52758823195234 ], [ 13.65808473704943, 52.52643525564136 ],
                    [ 13.65824152881714, 52.52616152300666 ], [ 13.65858919751944, 52.52585046112375 ], [ 13.63532797819388, 52.5141709085959 ],
                    [ 13.61265613787833, 52.47347608955019 ], [ 13.6463316339591, 52.47988584419133 ], [ 13.7550754233866, 52.44226861959024 ],
                    [ 13.7375361025112, 52.40419122981879 ], [ 13.64703320679412, 52.36822284643918 ], [ 13.63370332292881, 52.37678940544438 ],
                    [ 13.60564040952817, 52.37293465950694 ], [ 13.59301209849787, 52.39348941681675 ], [ 13.53478155319154, 52.38877979680913 ],
                    [ 13.54039413587167, 52.401194983805 ], [ 13.48005887206028, 52.3960580886509 ], [ 13.46672898819497, 52.42088087571511 ],
                    [ 13.41832046257886, 52.40932717816008 ], [ 13.4288440551041, 52.3866388943145 ], [ 13.42112675391892, 52.37507622656523 ],
                    [ 13.21453510667747, 52.40441763819376 ], [ 13.16136747583096, 52.43279064353038 ], [ 13.14483273696153, 52.45353287424874 ],
                    [ 13.12037854000024, 52.4749500095719 ], [ 13.12228902413784, 52.47704460461514 ], [ 13.16885072980566, 52.50924888504483 ],
                    [ 13.15221891489755, 52.5149254252851 ], [ 13.14536391010435, 52.51738731113721 ], [ 13.1431163675492, 52.51971229889779 ],
                    [ 13.12850734094073, 52.51779761203525 ], [ 13.11715725103722, 52.51690862191347 ], [ 13.12536078136352, 52.54391221567218 ],
                    [ 13.1304177521126, 52.55614382060114 ], [ 13.13264592196, 52.55482494509312 ], [ 13.13348315430145, 52.55401365677227 ],
                    [ 13.13452969472826, 52.5533296177091 ], [ 13.13638730398584, 52.55267738448515 ], [ 13.14141069803452, 52.5524705768045 ],
                    [ 13.14318981676009, 52.55212059235527 ], [ 13.14415786665489, 52.55213650079986 ], [ 13.14460264633628, 52.55253421004082 ],
                    [ 13.14533522463504, 52.55251830174041 ], [ 13.14656490963654, 52.55417273409589 ], [ 13.14693119878592, 52.55426818021323 ],
                    [ 13.14727132442464, 52.5549203897917 ], [ 13.14732365144598, 52.55581119696714 ], [ 13.14693119878592, 52.55702012063469 ],
                    [ 13.14693119878592, 52.55856304061189 ], [ 13.14664340016855, 52.55911975099335 ], [ 13.14586737012126, 52.56059673021849 ],
                    [ 13.14716652282081, 52.56138650665329 ], [ 13.14766619693602, 52.56442397581463 ], [ 13.15006463268903, 52.56746123464563 ],
                    [ 13.14466815224475, 52.58203714942593 ], [ 13.13237616901055, 52.57966889299277 ], [ 13.12649208924056, 52.58582619937295 ],
                    [ 13.16240919102746, 52.59905683535453 ], [ 13.19958171050189, 52.62282258756287 ]]]}`;

enum longLine = `{
  "type": "LineString",
  "coordinates": [
    [ 13.29101945788359, 52.49301881198514 ], [ 13.31331992008564, 52.47979533465406 ], [ 13.31602975607838, 52.47869505312963 ],
    [ 13.31930832308195, 52.47794114064382 ], [ 13.33222186250416, 52.47790038824957 ], [ 13.33727353207088, 52.47851167020042 ],
    [ 13.34145537773869, 52.47875618060283 ], [ 13.34386412084335, 52.47853204611914 ], [ 13.34570413293719, 52.47779850709873 ],
    [ 13.34741032596966, 52.47639252312692 ], [ 13.35008670719706, 52.47457894122215 ], [ 13.3516925359335, 52.47406949465588 ],
    [ 13.35390055044611, 52.47362117679893 ], [ 13.35674420550022, 52.47372306762202 ], [ 13.35955440578899, 52.47429365187168 ],
    [ 13.36079223210666, 52.47437516330339 ], [ 13.36273260849653, 52.47394722660238 ], [ 13.36423807293694, 52.47347852925023 ],
    [ 13.36808537095133, 52.4715629316581 ], [ 13.37493654973817, 52.47006659728271 ], [ 13.39051397397465, 52.47006659728271 ],
    [ 13.40257391531902, 52.46547471733362 ], [ 13.40910638354721, 52.46271935942107 ], [ 13.4282012906758, 52.46118853050459 ],
    [ 13.45583865625665, 52.4648624304792 ], [ 13.46337611959688, 52.4510837246126 ], [ 13.49603846073788, 52.43484994094487 ],
    [ 13.52116333853865, 52.42749682581649 ], [ 13.52417832387475, 52.40941520264553 ], [ 13.51814835320256, 52.39929851880106 ],
    [ 13.54729321145145, 52.37537713400778 ], [ 13.54829820656348, 52.36126357355496 ], [ 13.55684066501574, 52.34223381334812 ],
    [ 13.55432817723567, 52.32227446034955 ], [ 13.5618656405759, 52.28601753844515 ], [ 13.58916930704346, 52.24611153513092 ],
    [ 13.59570177527166, 52.22395415012355 ], [ 13.61228419462016, 52.20270960952899 ], [ 13.62283664329649, 52.17652482989757 ]
  ]
}`;

alias suite = Spec!({
  describe("The MapTiles task", {
    Team team1;
    Icon icon1;
    IconSet iconSet1;
    Map map1;
    Picture cover;

    IBroadcast broadcast;

    SysTime now;

    beforeEach({
      createCrates;

      crateGetters[`Map`] = &crates.map.getItem;
      crateGetters[`Team`] = &crates.team.getItem;
      crateGetters[`IconSet`] = &crates.iconSet.getItem;
      crateGetters[`Picture`] = &crates.picture.getItem;

      now = Clock.currTime;
      now.fracSecs = 0.msecs;

      broadcast = new MemoryBroadcast();

      cover._id = ObjectId.fromString("1");
      cover.picture = new PictureFile;
      crates.picture.addItem(cover.serializeToJson);

      createPicture("1", "default");

      auto team1 = Team(ObjectId.fromString("1"), "team1", Json(""), [], [], [], [], true, cover);
      createTeam(team1);

      iconSet1._id = ObjectId.fromString("1");
      iconSet1.name = "set1";
      iconSet1.visibility.team = team1;
      iconSet1.visibility.isPublic = true;
      iconSet1.visibility.isDefault = true;
      createIconSet(iconSet1);

      icon1._id = ObjectId.fromString("1");
      icon1.iconSet = iconSet1;
      icon1.category = "category";
      icon1.subcategory = "subcategory";
      icon1.name = "name1";
      icon1.image = OptionalIconImage(false, new IconFile());//.fromString("data:text/plain;base64,dGhpcyBpcyBhIHRleHQgZmlsZQ==");
      icon1.attributes = [ IconAttribute("phone", "short text"), IconAttribute("program", "long text"),
                            IconAttribute("max number of people", "integer"), IconAttribute("price", "decimal"),
                            IconAttribute("kids-friendly","boolean"), IconAttribute("type of food","options","vegan,international,asian") ];

      createIcon(icon1);

      map1 = createMap(Map(ObjectId.fromString("1"), "map1", Json(""),
        Visibility(team1, true), Polygon([[[-70, 30], [-70, 50], [-80, 50], [-80, 30], [-70,30]]]),
        cover, IconSets(true, [iconSet1]), BaseMaps(), "", SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-02-01T00:00:00Z"),

        ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z"), 10)
      ));
    });

    describe("running the task", {
      it("should not generate a tile when the request change index is older than the map change index", {
        auto task = new MapTiles(crates, broadcast);

        TileProcessRequest request;
        request.mapId = "000000000000000000000001";

        task.start(request);

        crates.tileLayer.get.size.should.equal(0);
      });

      describe("when there is a valid request", {
        TileProcessRequest request;

        beforeEach({
          request.mapId = "000000000000000000000001";
          request.changeIndex = 10;
          request.path = ZXY(16, 33_029, 32_506);
        });

        it("should generate a tile with a line feature for level 16", {
          auto task = new MapTiles(crates, broadcast);

          createFeature(
            Feature(ObjectId.fromString("1"),
                  [map1],
                  "line",
                  Json("description of line"),
                  GeoJsonGeometry.fromJson(`{ "type": "LineString", "coordinates": [[1.4, 1.4], [1.6, 1.6]] }`.parseJsonString),
                  ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
                  [],
                  [], [icon1], VisibilityEnum.Public));

          task.start(request);

          crates.tileLayer.get.size.should.equal(1);
          auto data = crates.tileLayer.get.exec.front;

          auto tileLayer = data.deserializeJson!TileLayer;
          tileLayer.changeIndex.should.equal(map1.info.changeIndex);
          tileLayer.mapId.should.equal(map1._id.toString);
          tileLayer.isReady.should.equal(true);
          tileLayer.extent.should.equal(4096);
          tileLayer.layerName.should.equal("sites");
          tileLayer.path.z.should.equal(request.path.z);
          tileLayer.path.x.should.equal(request.path.x);
          tileLayer.path.y.should.equal(request.path.y);

          tileLayer.properties.serializeToJson.should.equal(`[
            {"_id":"000000000000000000000001","type":"line","visibility":"1","icons":"000000000000000000000001","name":"line"},
            {"_id":"000000000000000000000001","type":"lineMarker","visibility":"1","icons":"000000000000000000000001","name":"line"},
            {"_id":"000000000000000000000001","type":"lineMarker","visibility":"1","icons":"000000000000000000000001","name":"line"}]`.parseJsonString);

          Json expectedLines = `[
            {"type":"line","points":[[4802,-820],[-820,4803]]},
            {"type":"point","points":[-812,4796]},
            {"type":"point","points":[3243,739]}]`.parseJsonString;

          tileLayer.geometries.should.equal(expectedLines);
        });

        it("should generate a tile with a multi line feature for level 16", {
          auto task = new MapTiles(crates, broadcast);

          createFeature(
            Feature(ObjectId.fromString("1"),
                  [map1],
                  "line",
                  Json("description of line"),
                  GeoJsonGeometry.fromJson(`{ "type": "MultiLineString", "coordinates": [[[1.5, 1.5], [1.25, 1.25]], [[1.26, 1.26], [1.65, 1.65]]] }`.parseJsonString),
                  ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
                  [],
                  [], [icon1], VisibilityEnum.Public));

          task.start(request);

          crates.tileLayer.get.size.should.equal(1);
          auto data = crates.tileLayer.get.exec.front;

          auto tileLayer = data.deserializeJson!TileLayer;
          tileLayer.changeIndex.should.equal(map1.info.changeIndex);
          tileLayer.mapId.should.equal(map1._id.toString);
          tileLayer.isReady.should.equal(true);
          tileLayer.extent.should.equal(4096);
          tileLayer.layerName.should.equal("sites");
          tileLayer.path.z.should.equal(request.path.z);
          tileLayer.path.x.should.equal(request.path.x);
          tileLayer.path.y.should.equal(request.path.y);

          tileLayer.properties.serializeToJson.should.equal(`[
            {"_id":"000000000000000000000001","visibility":"1","type":"lineMarker","icons":"000000000000000000000001","name":"line"},
            {"_id":"000000000000000000000001","visibility":"1","type":"line","icons":"000000000000000000000001","name":"line"},
            {"_id":"000000000000000000000001","visibility":"1","type":"lineMarker","icons":"000000000000000000000001","name":"line"},
            {"_id":"000000000000000000000001","visibility":"1","type":"lineMarker","icons":"000000000000000000000001","name":"line"}]`
            .parseJsonString);

          Json expectedLines = `[
            {"type":"point","points":[767,3217]},
            {"type":"line","points":[[4802,-820],[-820,4803]]},{"type":"point","points":[222,3761]},
            {"type":"point","points":[4277,-295]}]`.parseJsonString;

          tileLayer.geometries.should.equal(expectedLines);
        });

        it("should generate a tile with a polygon feature for level 16", {
          auto task = new MapTiles(crates, broadcast);

          createFeature(
            Feature(ObjectId.fromString("1"),
                  [map1],
                  "polygon",
                  Json("description of polygon"),
                  GeoJsonGeometry.fromJson(`{ "type": "Polygon", "coordinates": [[[1, 1], [2, 2], [3, 1], [1.7, 3], [1, 1]]] }`.parseJsonString),
                  ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
                  [],
                  [], [icon1], VisibilityEnum.Public));

          task.start(request);

          crates.tileLayer.get.size.should.equal(1);
          auto data = crates.tileLayer.get.exec.front;

          auto tileLayer = data.deserializeJson!TileLayer;
          tileLayer.changeIndex.should.equal(map1.info.changeIndex);
          tileLayer.mapId.should.equal(map1._id.toString);
          tileLayer.isReady.should.equal(true);
          tileLayer.extent.should.equal(4096);
          tileLayer.layerName.should.equal("sites");
          tileLayer.path.z.should.equal(request.path.z);
          tileLayer.path.x.should.equal(request.path.x);
          tileLayer.path.y.should.equal(request.path.y);

          tileLayer.properties.serializeToJson.should.equal(`[
            {"_id":"000000000000000000000001","type":"polygonBorder","visibility":"1","icons":"000000000000000000000001","name":"polygon"},
            {"_id":"000000000000000000000001","type":"polygonBorderMarker","visibility":"1","icons":"000000000000000000000001","name":"polygon"}
          ]`.parseJsonString);

          Json expectedLines = `[
            {"type":"line","points":[[4802,-820],[-820,4803]]},
            {"type":"point","points":[984,2999]}
          ]`.parseJsonString;

          tileLayer.geometries.should.equal(expectedLines);
        });

        it("should generate a tile for large polygon feature for level 10", {
          auto task = new MapTiles(crates, broadcast);

          createFeature(
            Feature(ObjectId.fromString("1"),
                  [map1],
                  "polygon",
                  Json("description of polygon"),
                  GeoJsonGeometry.fromJson(largePolygon.parseJsonString),
                  ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
                  [],
                  [], [icon1], VisibilityEnum.Public));

          request.path.z = 10;
          request.path.x = 549;
          request.path.y = 335;
          task.start(request);

          crates.tileLayer.get.size.should.equal(1);
          auto data = crates.tileLayer.get.exec.front;

          auto tileLayer = data.deserializeJson!TileLayer;
          tileLayer.changeIndex.should.equal(map1.info.changeIndex);
          tileLayer.mapId.should.equal(map1._id.toString);
          tileLayer.isReady.should.equal(true);
          tileLayer.extent.should.equal(4096);
          tileLayer.layerName.should.equal("sites");
          tileLayer.path.z.should.equal(request.path.z);
          tileLayer.path.x.should.equal(request.path.x);
          tileLayer.path.y.should.equal(request.path.y);

          tileLayer.properties.serializeToJson.should.equal(`[
            {"_id":"000000000000000000000001","type":"polygonBorder","visibility":"1","icons":"000000000000000000000001","name":"polygon"},
            {"_id":"000000000000000000000001","type":"polygonBorderMarker","visibility":"1","icons":"000000000000000000000001","name":"polygon"}]`.parseJsonString);

          Json expectedLines = `[
            {"type":"line","points":[[-820,-332],[1596,4655],[1311,4245],[1333,4205],[1876,3589],[1682,3480],[1602,3433],[1576,3389],[1406,3425],[1273,3442],[1369,2925],[1428,2691],[1454,2716],[1464,2732],[1476,2745],[1498,2757],[1556,2761],[1577,2768],[1588,2768],[1593,2760],[1602,2760],[1616,2729],[1620,2727],[1624,2714],[1625,2697],[1620,2674],[1620,2644],[1617,2634],[1608,2605],[1623,2590],[1629,2532],[1657,2474],[1594,2194],[1451,2240],[1382,2122],[1801,1868],[2234,1412],[2451,1298],[2976,1058],[3182,676],[3600,682],[3537,1307],[4541,918],[4876,1135],[4915,1091]]},
            {"type":"point","points":[1594,1994]}
          ]`.parseJsonString;

          tileLayer.geometries.should.equal(expectedLines);
        });

        it("should generate a tile for large polygon feature for level 8 137.84", {
          auto task = new MapTiles(crates, broadcast);

          createFeature(
            Feature(ObjectId.fromString("1"),
                  [map1],
                  "polygon",
                  Json("description of polygon"),
                  GeoJsonGeometry.fromJson(largePolygon.parseJsonString),
                  ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
                  [],
                  [], [icon1], VisibilityEnum.Public));

          request.path.z = 8;
          request.path.x = 137;
          request.path.y = 84;
          task.start(request);

          crates.tileLayer.get.size.should.equal(1);
          auto data = crates.tileLayer.get.exec.front;

          auto tileLayer = data.deserializeJson!TileLayer;
          tileLayer.changeIndex.should.equal(map1.info.changeIndex);
          tileLayer.mapId.should.equal(map1._id.toString);
          tileLayer.isReady.should.equal(true);
          tileLayer.extent.should.equal(4096);
          tileLayer.layerName.should.equal("sites");
          tileLayer.path.z.should.equal(request.path.z);
          tileLayer.path.x.should.equal(request.path.x);
          tileLayer.path.y.should.equal(request.path.y);

          tileLayer.properties.serializeToJson.should.equal(`[
            {"_id":"000000000000000000000001","type":"polygonBorder","visibility":"1","icons":"000000000000000000000001","name":"polygon"},
            {"_id":"000000000000000000000001","type":"polygonBorder","visibility":"1","icons":"000000000000000000000001","name":"polygon"},
            {"_id":"000000000000000000000001","type":"polygonBorderMarker","visibility":"1","icons":"000000000000000000000001","name":"polygon"},
            {"_id":"000000000000000000000001","type":"polygonBorderMarker","visibility":"1","icons":"000000000000000000000001","name":"polygon"}]`.parseJsonString);

          Json expectedLines = `[
            {"type":"line","points":[[1332,4905],[1908,-698],[2159,-795],[2243,-741],[2295,-799],[-820,365]]},
            {"type":"line","points":[[4915,1466],[2509,-796],[2448,-581],[2479,-522],[2540,-530],[2614,-491],[2656,-436],[2699,-420],[2712,-328],[2864,-286],[2851,-267],[2847,-263],[2841,-265],[2837,-263],[2833,-265],[2829,-263],[2821,-265],[2824,-247],[2821,-244],[2823,-227],[2826,-227],[2839,-229],[2842,-229],[2854,-229],[2865,-229],[2868,-228],[2873,-227],[2876,-226],[2880,-226],[2885,-225],[2891,-225],[2914,-226],[2917,-215],[2918,-209],[2851,-151],[2785,44],[2883,13],[3200,193],[3149,375],[2885,547],[2847,506],[2765,524],[2728,426],[2558,449],[2575,389],[2399,414],[2360,295],[2219,351],[2250,459],[2227,514],[1626,374],[1471,238],[1423,139],[1351,37],[1357,27],[1493,-127],[1444,-154],[1424,-166],[1418,-177],[1375,-168],[1342,-164],[1366,-293],[1381,-352],[1387,-345],[1390,-341],[1393,-338],[1398,-335],[1413,-334],[1418,-332],[1421,-332],[1424,-334],[1428,-342],[1430,-346],[1430,-350],[1429,-356],[1429,-363],[1428,-366],[1426,-373],[1429,-377],[1431,-391],[1438,-406],[1422,-476],[1386,-464],[1369,-494],[1474,-557],[1582,-671],[1636,-700],[1768,-760],[-820,3967]]},
            {"type":"point","points":[2821,-244]},
            {"type":"point","points":[2226,348]}]`.parseJsonString;

          tileLayer.geometries.should.equal(expectedLines);
        });

        it("should generate a tile for large polygon feature for level 8 137.83", {
          auto task = new MapTiles(crates, broadcast);

          createFeature(
            Feature(ObjectId.fromString("1"),
                  [map1],
                  "polygon",
                  Json("description of polygon"),
                  GeoJsonGeometry.fromJson(largePolygon.parseJsonString),
                  ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
                  [],
                  [], [icon1], VisibilityEnum.Public));

          request.path.z = 8;
          request.path.x = 137;
          request.path.y = 83;
          task.start(request);

          crates.tileLayer.get.size.should.equal(1);
          auto data = crates.tileLayer.get.exec.front;

          auto tileLayer = data.deserializeJson!TileLayer;
          tileLayer.changeIndex.should.equal(map1.info.changeIndex);
          tileLayer.mapId.should.equal(map1._id.toString);
          tileLayer.isReady.should.equal(true);
          tileLayer.extent.should.equal(4096);
          tileLayer.layerName.should.equal("sites");
          tileLayer.path.z.should.equal(request.path.z);
          tileLayer.path.x.should.equal(request.path.x);
          tileLayer.path.y.should.equal(request.path.y);

          tileLayer.properties.serializeToJson.should.equal(`[
            {"_id":"000000000000000000000001","type":"polygonBorder","visibility":"1","icons":"000000000000000000000001","name":"polygon"},
            {"_id":"000000000000000000000001","type":"polygonBorderMarker","visibility":"1","icons":"000000000000000000000001","name":"polygon"},
            {"_id":"000000000000000000000001","type":"polygonBorderMarker","visibility":"1","icons":"000000000000000000000001","name":"polygon"}]`.parseJsonString);

          Json expectedLines = `[
            {"type":"line","points":[[1582,3425],[1636,3396],[1768,3336],[1819,3241],[1924,3242],[1908,3398],[2159,3301],[2243,3355],[2295,3297],[2385,3263],[2313,3233],
              [2344,3202],[2390,3188],[2509,3300],[2448,3515],[2479,3574],[2540,3566],[2614,3605],[2656,3660],[2699,3676],[2712,3768],[2864,3810],[2851,3829],[2847,3833],
              [2841,3831],[2837,3833],[2833,3831],[2829,3833],[2821,3831],[2824,3849],[2821,3852],[2823,3869],[2826,3869],[2839,3867],[2842,3867],[2854,3867],[2865,3867],
              [2868,3868],[2873,3869],[2876,3870],[2880,3870],[2885,3871],[2891,3871],[2914,3870],[2917,3881],[2918,3887],[2851,3945],[2785,4140],[2883,4109],[3200,4289],
              [3149,4471],[2885,4643],[2847,4602],[2765,4620],[2728,4522],[2558,4545],[2575,4485],[2399,4510],[2360,4391],[2219,4447],[2250,4555],[2227,4610],[1626,4470],
              [1471,4334],[1423,4235],[1351,4133],[1357,4123],[1493,3969],[1444,3942],[1424,3930],[1418,3919],[1375,3928],[1342,3932],[1366,3803],[1381,3744],[1387,3751],
              [1390,3755],[1393,3758],[1398,3761],[1413,3762],[1418,3764],[1421,3764],[1424,3762],[1428,3754],[1430,3750],[1430,3746],[1429,3740],[1429,3733],[1428,3730],
              [1426,3723],[1429,3719],[1431,3705],[1438,3690],[1422,3620],[1386,3632],[1369,3602],[1474,3539],[1582,3425]]},
            {"type":"point","points":[2821,3852]},
            {"type":"point","points":[2226,4444]}]`.parseJsonString;

          tileLayer.geometries.should.equal(expectedLines);
        });

        it("should generate a tile for large polygon feature for level 9", {
          auto task = new MapTiles(crates, broadcast);

          createFeature(
            Feature(ObjectId.fromString("1"),
                  [map1],
                  "polygon",
                  Json("description of polygon"),
                  GeoJsonGeometry.fromJson(largePolygon.parseJsonString),
                  ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
                  [],
                  [], [icon1], VisibilityEnum.Public));

          request.path.z = 9;
          request.path.x = 274;
          request.path.y = 167;
          task.start(request);

          crates.tileLayer.get.size.should.equal(1);
          auto data = crates.tileLayer.get.exec.front;

          auto tileLayer = data.deserializeJson!TileLayer;
          tileLayer.changeIndex.should.equal(map1.info.changeIndex);
          tileLayer.mapId.should.equal(map1._id.toString);
          tileLayer.isReady.should.equal(true);
          tileLayer.extent.should.equal(4096);
          tileLayer.layerName.should.equal("sites");
          tileLayer.path.z.should.equal(request.path.z);
          tileLayer.path.x.should.equal(request.path.x);
          tileLayer.path.y.should.equal(request.path.y);

          tileLayer.properties.serializeToJson.should.equal(`[
            {"_id":"000000000000000000000001","type":"polygonBorder","visibility":"1","icons":"000000000000000000000001","name":"polygon"},
            {"_id":"000000000000000000000001","type":"polygonBorder","visibility":"1","icons":"000000000000000000000001","name":"polygon"},
            {"_id":"000000000000000000000001","type":"polygonBorder","visibility":"1","icons":"000000000000000000000001","name":"polygon"},
            {"_id":"000000000000000000000001","type":"polygonBorder","visibility":"1","icons":"000000000000000000000001","name":"polygon"},
            {"_id":"000000000000000000000001","type":"polygonBorderMarker","visibility":"1","icons":"000000000000000000000001","name":"polygon"}]`.parseJsonString);

          Json expectedLines = `[
            {"type":"line","points":[[4915,2871],[4897,2934],[4915,2969]]},
            {"type":"line","points":[[4915,4908],[4915,4908]]},
            {"type":"line","points":[[2928,-825],[4721,4687],[4439,4798],[2859,-825]]},
            {"type":"line","points":[[-820,3896],[3252,4844],[2942,4573],[2846,4375],[2703,4170],[2714,4150],[2986,3842],[2889,3788],[2849,3764],[2836,3742],[2751,3760],[2684,3769],[2732,3510],[2762,3393],[2775,3406],[2780,3414],[2786,3420],[2797,3426],[2826,3428],[2836,3432],[2842,3432],[2844,3428],[2849,3428],[2856,3412],[2860,3405],[2860,3396],[2858,3385],[2858,3370],[2856,3365],[2852,3350],[2859,3343],[2862,3314],[2876,3285],[2845,3145],[2773,3168],[2739,3109],[2948,2982],[3165,2754],[3273,2697],[3536,2577],[3639,2386],[3848,2389],[3816,2701],[4318,2507],[4486,2615],[4590,2498],[4771,2430],[4626,2371],[4689,2308],[4781,2280],[4915,2406]]},
            {"type":"point","points":[2845,3045]}]`.parseJsonString;

          tileLayer.geometries.should.equal(expectedLines);
        });

        it("should generate a tile for a long line for level 16", {
          auto task = new MapTiles(crates, broadcast);

          createFeature(
            Feature(ObjectId.fromString("1"),
                  [map1],
                  "polygon",
                  Json("description of polygon"),
                  GeoJsonGeometry.fromJson(longLine.parseJsonString),
                  ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
                  [],
                  [], [icon1], VisibilityEnum.Public));

          request.path.z = 16;
          request.path.x = 35_205;
          request.path.y = 21_507;

          task.start(request);

          auto tileLayer = crates.tileLayer.get.exec.front.deserializeJson!TileLayer;

          tileLayer.properties.serializeToJson.should.equal(`[
            {"_id":"000000000000000000000001","type":"line","visibility":"1","icons":"000000000000000000000001","name":"polygon"},
            {"_id":"000000000000000000000001","type":"lineMarker","visibility":"1","icons":"000000000000000000000001","name":"polygon"}
          ]`.parseJsonString);

          Json expectedLines = `[{"type":"line","points":[[-820,3276],[2738,3276],[-820,1052]]},{"type":"point","points":[4183,4179]}]`.parseJsonString;
          tileLayer.geometries.should.equal(expectedLines);

        });

        it("should generate a tile with a multi polygon feature for level 16", {
          auto task = new MapTiles(crates, broadcast);

          createFeature(
            Feature(ObjectId.fromString("1"),
                  [map1],
                  "polygon",
                  Json("description of polygon"),
                  GeoJsonGeometry.fromJson(`{ "type": "MultiPolygon", "coordinates": [[[[1.4, 1.4], [1.55, 1.55], [1.56, 1.56], [1.6, 1.6], [1.5, 1.5]]]] }`.parseJsonString),
                  ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
                  [],
                  [], [icon1], VisibilityEnum.Public));

          task.start(request);

          crates.tileLayer.get.size.should.equal(1);
          auto data = crates.tileLayer.get.exec.front;

          auto tileLayer = data.deserializeJson!TileLayer;
          tileLayer.changeIndex.should.equal(map1.info.changeIndex);
          tileLayer.mapId.should.equal(map1._id.toString);
          tileLayer.isReady.should.equal(true);
          tileLayer.extent.should.equal(4096);
          tileLayer.layerName.should.equal("sites");
          tileLayer.path.z.should.equal(request.path.z);
          tileLayer.path.x.should.equal(request.path.x);
          tileLayer.path.y.should.equal(request.path.y);

          tileLayer.properties.serializeToJson.should.equal(`[
            {"_id":"000000000000000000000001","type":"polygonBorder","visibility":"1","icons":"000000000000000000000001","name":"polygon"},
            {"_id":"000000000000000000000001","type":"polygonBorderMarker","visibility":"1","icons":"000000000000000000000001","name":"polygon"},
            {"_id":"000000000000000000000001","type":"polygonBorderMarker","visibility":"1","icons":"000000000000000000000001","name":"polygon"}]`
            .parseJsonString);

          Json expectedLines = `[{"type":"line","points":[[4802,-820],[-820,4803]]},{"type":"point","points":[-812,4796]},{"type":"point","points":[3243,739]}]`.parseJsonString;
          tileLayer.geometries.should.equal(expectedLines);
        });

        it("should generate a tile with a point feature for level 16", {
          auto task = new MapTiles(crates, broadcast);

          createFeature(
            Feature(ObjectId.fromString("1"),
                  [map1],
                  "site 1",
                  Json("description of site1"),
                  GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.4369877865837455, 1.4369697219185866] }`.parseJsonString),
                  ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
                  [],
                  [], [icon1], VisibilityEnum.Public));

          task.start(request);
          crates.tileLayer.get.size.should.equal(1);

          auto tileLayer = crates.tileLayer.get.exec.front.deserializeJson!TileLayer;
          tileLayer.changeIndex.should.equal(map1.info.changeIndex);
          tileLayer.mapId.should.equal(map1._id.toString);
          tileLayer.isReady.should.equal(true);
          tileLayer.extent.should.equal(4096);
          tileLayer.layerName.should.equal("sites");
          tileLayer.path.z.should.equal(request.path.z);
          tileLayer.path.x.should.equal(request.path.x);
          tileLayer.path.y.should.equal(request.path.y);

          tileLayer.properties.serializeToJson.should.equal(`[{
            "_id":"000000000000000000000001",
            "type":"point",
            "name":"site 1",
            "visibility":"1",
            "icons":"000000000000000000000001"
          }]`.parseJsonString);

          tileLayer.geometries[0].should.equal(`{ "points": [2439, 1557], "type": "point" }`.parseJsonString);
        });

        it("should generate a tile with a multi point feature for level 16", {
          auto task = new MapTiles(crates, broadcast);

          createFeature(
            Feature(ObjectId.fromString("1"),
                  [map1],
                  "site",
                  Json("description of site"),
                  GeoJsonGeometry.fromJson(`{ "type": "MultiPoint", "coordinates": [[1.4369877865837455, 1.4369697219185866], [1.4369877865836455, 1.4369697219186866]] }`.parseJsonString),
                  ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
                  [],
                  [], [icon1], VisibilityEnum.Public));

          task.start(request);
          crates.tileLayer.get.size.should.equal(1);

          auto tileLayer = crates.tileLayer.get.exec.front.deserializeJson!TileLayer;
          tileLayer.changeIndex.should.equal(map1.info.changeIndex);
          tileLayer.mapId.should.equal(map1._id.toString);
          tileLayer.isReady.should.equal(true);
          tileLayer.extent.should.equal(4096);
          tileLayer.layerName.should.equal("sites");
          tileLayer.path.z.should.equal(request.path.z);
          tileLayer.path.x.should.equal(request.path.x);
          tileLayer.path.y.should.equal(request.path.y);

          tileLayer.properties.serializeToJson.should.equal(`[{
            "_id":"000000000000000000000001",
            "type":"point",
            "name":"site",
            "visibility":"1",
            "icons":"000000000000000000000001"
          },{
            "_id":"000000000000000000000001",
            "type":"point",
            "name":"site",
            "visibility":"1",
            "icons":"000000000000000000000001"
          }]`.parseJsonString);

          tileLayer.geometries[0].should.equal(`{ "points": [2439, 1557], "type": "point" }`.parseJsonString);
          tileLayer.geometries[1].should.equal(`{ "points": [2439, 1557], "type": "point" }`.parseJsonString);
        });
      });

      describe("when a sites tile with the same path and changeIndex exists", {
        beforeEach({
          crates.tileLayer.addItem(`{
            "mapId": "000000000000000000000001",
            "teamId": "000000000000000000000001",
            "changeIndex": 10,
            "isReady": true,
            "isPublic": true,
            "extent": 512,
            "geometries": [],
            "properties": [],
            "layerName": "sites",
            "path": { "z": 0, "y": 0, "x": 0 }
          }`.parseJsonString);
        });

        it("should ignore the request", {
          bool isCalled;

          broadcast.register("MapTilesCheck", (const Json) @safe {
            isCalled = true;
          });

          TileProcessRequest request;
          request.mapId = "000000000000000000000001";
          request.changeIndex = 10;

          auto task = new MapTiles(crates, broadcast);
          task.start(request);

          isCalled.should.equal(false);
          crates.tileLayer.get.size.should.equal(1);
        });
      });

      describe("when there are no `MapTiles` meta in the db", {
        it("should not trigger a MapTilesCheck event", {
          bool isCalled;

          broadcast.register("MapTilesCheck", (const Json) @safe {
            isCalled = true;
          });

          TileProcessRequest request;
          request.mapId = "000000000000000000000001";
          request.changeIndex = 10;

          auto task = new MapTiles(crates, broadcast);
          task.start(request);

          isCalled.should.equal(false);
        });
      });

      describe("when there is a `MapTiles` meta in the db with waitingCheck = true", {
        beforeEach({
          crates.meta.addItem(`{
            "_id": "000000000000000000000001",
            "model": "Map",
            "type": "MapTiles",
            "itemId": "000000000000000000000001",
            "changeIndex": 10,
            "data": {
              "waitingCheck": true,
              "layer": "sites",
              "tiles": [],
              "processedTileCount": 0
            }
          }`.parseJsonString);
        });
      });

      describe("when there is a `MapTiles` meta in the db with no waitingCheck field", {
        beforeEach({
          crates.meta.addItem(`{
            "_id": "000000000000000000000001",
            "type": "MapTiles",
            "model": "Map",
            "itemId": "000000000000000000000001",
            "changeIndex": 10,
            "data": {
              "layer": "sites",
              "tiles": [],
              "processedTileCount": 0
            }
          }`.parseJsonString);
        });

        it("should trigger a MapTilesCheck event", {
          Json receivedMessage;

          broadcast.register("MapTilesCheck", (const Json message) @safe {
            receivedMessage = message.clone;
          });

          TileProcessRequest request;
          request.mapId = "000000000000000000000001";
          request.changeIndex = 10;

          auto task = new MapTiles(crates, broadcast);
          task.start(request);

          receivedMessage.should.equal(`{ "value": "000000000000000000000001", "uid": "000000000000000000000001" }`.parseJsonString);
        });
      });

      describe("when there is a `MapTiles` meta in the db with waitingCheck = false", {
        beforeEach({
          crates.meta.addItem(`{
            "_id": "000000000000000000000001",
            "type": "MapTiles",
            "model": "Map",
            "itemId": "000000000000000000000001",
            "changeIndex": 10,
            "data": {
              "waitingCheck": false,
              "layer": "sites",
              "tiles": [],
              "processedTileCount": 0
            }
          }`.parseJsonString);
        });

        it("should trigger a MapTilesCheck event", {
          Json receivedMessage;

          broadcast.register("MapTilesCheck", (const Json message) @safe {
            receivedMessage = message.clone;
          });

          TileProcessRequest request;
          request.mapId = "000000000000000000000001";
          request.changeIndex = 10;

          auto task = new MapTiles(crates, broadcast);
          task.start(request);

          receivedMessage.should.equal(`{ "value": "000000000000000000000001", "uid": "000000000000000000000001" }`.parseJsonString);
        });
      });
    });
  });
});
