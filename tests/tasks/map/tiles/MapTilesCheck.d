/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.map.MapTilesCheck;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;

import ogm.models.changeset;
import gis_collective.tasks.map.tiles.MapTilesCheck;

import gis_collective.hmq.broadcast.memory;

import geo.xyz;

alias suite = Spec!({
  describe("The MapTilesCheck task", {
    IBroadcast broadcast;

    beforeEach({
      setupTestData;
      broadcast = new MemoryBroadcast();

      auto map = crates.map.getItem("000000000000000000000001").exec.front;
      map["info"]["changeIndex"] = 10;
      crates.map.updateItem(map);

      crateGetters["Map"] = &crates.map.getItem;
    });

    describe("running the task", {
      describe("when there is no MapTile meta for the map", {
        it("should trigger a MapTilesRequest task", {
          Json receivedMessage;

          broadcast.register("MapTilesRequest", (const Json message) {
            receivedMessage = message.clone;
          });

          auto task = new MapTilesCheck(crates, broadcast);
          task.start("000000000000000000000001");

          receivedMessage.should.equal(`{ "value": "000000000000000000000001" }`.parseJsonString);
        });
      });

      describe("when there is a site MapTile meta for an older changeIndex", {
        beforeEach({
          crates.meta.addItem(`{
            "_id": "000000000000000000000001",
            "type": "MapTiles",
            "model": "Map",
            "itemId": "000000000000000000000001",
            "changeIndex": 9,
            "data": {
              "layer": "sites",
              "tiles": [ ],
              "processedTileCount": 0
            }
          }`.parseJsonString);
        });

        it("should be deleted", {
          crates.meta.get.size.should.equal(1);

          auto task = new MapTilesCheck(crates, broadcast);
          task.start("000000000000000000000001");

          crates.meta.get.size.should.equal(0);
        });
      });

      describe("when there is a MapTile meta for a newer changeIndex", {
        beforeEach({
          crates.meta.addItem(`{
            "_id": "000000000000000000000001",
            "type": "MapTiles",
            "model": "Map",
            "itemId": "000000000000000000000001",
            "changeIndex": 11,
            "data": {
              "layer": "sites",
              "tiles": [ ],
              "processedTileCount": 0
            }
          }`.parseJsonString);
        });

        it("should be deleted", {
          crates.meta.get.size.should.equal(1);

          auto task = new MapTilesCheck(crates, broadcast);
          task.start("000000000000000000000001");

          crates.meta.get.size.should.equal(0);
        });
      });

      describe("when there is a MapTile meta for the last changeIndex", {
        beforeEach({
          crates.meta.addItem(`{
            "_id": "000000000000000000000001",
            "type": "MapTiles",
            "model": "Map",
            "itemId": "000000000000000000000001",
            "changeIndex": 10,
            "data": {
              "layer": "sites",
              "tiles": [ ],
              "processedTileCount": 0
            }
          }`.parseJsonString);
        });

        it("should not delete the meta", {
          crates.meta.get.size.should.equal(1);

          auto task = new MapTilesCheck(crates, broadcast);
          task.start("000000000000000000000001");

          crates.meta.get.size.should.equal(1);
        });

        it("should not trigger a MapTilesRequest task", {
          bool isTriggered;

          broadcast.register("MapTilesRequest", (const Json) {
            isTriggered = true;
          });

          auto task = new MapTilesCheck(crates, broadcast);
          task.start("000000000000000000000001");

          isTriggered.should.equal(false);
        });
      });

      describe("when there is a meta with tiles in the list and it has the last change index", {
        beforeEach({
          crates.meta.addItem(`{
            "_id": "000000000000000000000001",
            "type": "MapTiles",
            "model": "Map",
            "itemId": "000000000000000000000001",
            "changeIndex": 10,
            "data": {
              "layer": "sites",
              "tiles": [
                "0.0.0",
                "1.1.0",
                "10.516.507",
                "11.1032.1015",
                "12.2065.2030",
                "13.4130.4061",
                "14.8260.8123",
                "15.16520.16247",
                "16.33041.32494",
                "2.2.1",
                "3.4.3",
                "4.8.7",
                "5.16.15",
                "6.32.31",
                "7.64.63",
                "8.129.126",
                "9.258.253"],
              "processedTileCount": 0
            }
          }`.parseJsonString);
        });

        it("should not delete a tile if it has the exact change index", {
          crates.tileLayer.addItem(`{
            "mapId": "000000000000000000000001",
            "teamId": "000000000000000000000001",
            "changeIndex": 10,
            "isReady": true,
            "isPublic": true,
            "extent": 512,
            "geometries": [],
            "properties": [],
            "layerName": "sites",
            "path": { "z": 0, "y": 0, "x": 0 }
          }`.parseJsonString);

          auto task = new MapTilesCheck(crates, broadcast);
          task.start("000000000000000000000001");

          crates.tileLayer.get.size.should.equal(1);
        });

        it("should update the processedTileCount meta value", {
          crates.tileLayer.addItem(`{
            "mapId": "000000000000000000000001",
            "teamId": "000000000000000000000001",
            "changeIndex": 10,
            "isReady": true,
            "isPublic": true,
            "extent": 512,
            "geometries": [],
            "properties": [],
            "layerName": "sites",
            "path": { "z": 0, "y": 0, "x": 0 }
          }`.parseJsonString);

          auto task = new MapTilesCheck(crates, broadcast);
          task.start("000000000000000000000001");

          crates.meta.getItem("000000000000000000000001").exec.front["data"].should.equal(`{
              "layer": "sites",
              "waitingCheck": false,
              "tiles": [ "0.0.0",
                "1.1.0",
                "10.516.507",
                "11.1032.1015",
                "12.2065.2030",
                "13.4130.4061",
                "14.8260.8123",
                "15.16520.16247",
                "16.33041.32494",
                "2.2.1",
                "3.4.3",
                "4.8.7",
                "5.16.15",
                "6.32.31",
                "7.64.63",
                "8.129.126",
                "9.258.253" ],
              "processedTileCount": 1
            }`.parseJsonString);
        });

        it("should not delete a tile if it has an older change index", {
          crates.tileLayer.addItem(`{
            "mapId": "000000000000000000000001",
            "teamId": "000000000000000000000001",
            "changeIndex": 9,
            "isReady": true,
            "isPublic": true,
            "extent": 512,
            "geometries": [],
            "properties": [],
            "layerName": "sites",
            "path": { "z": 0, "y": 0, "x": 0 }
          }`.parseJsonString);

          auto task = new MapTilesCheck(crates, broadcast);
          task.start("000000000000000000000001");

          crates.tileLayer.get.size.should.equal(1);
        });

        it("should delete a tile if it is not in the list", {
          crates.tileLayer.addItem(`{
            "mapId": "000000000000000000000001",
            "teamId": "000000000000000000000001",
            "changeIndex": 10,
            "isReady": true,
            "isPublic": true,
            "layerName": "sites",
            "extent": 512,
            "geometries": [],
            "properties": [],
            "path": { "z": 1, "y": 0, "x": 0 }
          }`.parseJsonString);

          auto task = new MapTilesCheck(crates, broadcast);
          task.start("000000000000000000000001");

          crates.tileLayer.get.size.should.equal(0);
        });

        it("should not delete a tile if it is in the list", {
          crates.tileLayer.addItem(`{
            "mapId": "000000000000000000000001",
            "teamId": "000000000000000000000001",
            "changeIndex": 10,
            "isReady": true,
            "isPublic": true,
            "layerName": "sites",
            "extent": 512,
            "geometries": [],
            "properties": [],
            "path": { "z": 0, "y": 0, "x": 0 }
          }`.parseJsonString);

          auto task = new MapTilesCheck(crates, broadcast);
          task.start("000000000000000000000001");

          crates.tileLayer.get.size.should.equal(1);
        });

        it("should not delete a tile if it is not in the list and on other layer", {
          crates.tileLayer.addItem(`{
            "mapId": "000000000000000000000001",
            "changeIndex": 9,
            "isReady": true,
            "isPublic": true,
            "layerName": "other",
            "extent": 512,
            "geometries": [],
            "properties": [],
            "path": { "z": 1, "y": 0, "x": 0 }
          }`.parseJsonString);

          auto task = new MapTilesCheck(crates, broadcast);
          task.start("000000000000000000000001");

          crates.tileLayer.get.size.should.equal(1);
        });

        it("should not delete a tile if it is not in the list and on is on other map", {
          crates.tileLayer.addItem(`{
            "mapId": "000000000000000000000002",
            "changeIndex": 9,
            "isReady": true,
            "isPublic": true,
            "layerName": "sites",
            "extent": 512,
            "geometries": [],
            "properties": [],
            "path": { "z": 0, "y": 0, "x": 0 }
          }`.parseJsonString);

          auto task = new MapTilesCheck(crates, broadcast);
          task.start("000000000000000000000001");

          crates.tileLayer.get.size.should.equal(1);
        });

        it("should delete older tiles", {
          crates.tileLayer.addItem(`{
            "mapId": "000000000000000000000001",
            "teamId": "000000000000000000000001",
            "changeIndex": 9,
            "isReady": true,
            "isPublic": true,
            "layerName": "sites",
            "extent": 512,
            "geometries": [],
            "properties": [],
            "path": { "z": 0, "y": 0, "x": 0 }
          }`.parseJsonString);

          crates.tileLayer.addItem(`{
            "mapId": "000000000000000000000001",
            "teamId": "000000000000000000000001",
            "changeIndex": 10,
            "isReady": true,
            "isPublic": true,
            "layerName": "sites",
            "extent": 512,
            "geometries": [],
            "properties": [],
            "path": { "z": 0, "y": 0, "x": 0 }
          }`.parseJsonString);

          auto task = new MapTilesCheck(crates, broadcast);
          task.start("000000000000000000000001");

          crates.tileLayer.get.size.should.equal(1);
          crates.tileLayer.get.exec.front.should.equal(`{
            "_id": "000000000000000000000002",
            "changeIndex": 10,
            "isReady": true,
            "isPublic": true,
            "mapId": "000000000000000000000001",
            "teamId": "000000000000000000000001",
            "geometries": [],
            "properties": [],
            "extent": 512,
            "layerName": "sites",
            "path": { "z": 0, "y": 0, "x": 0 }
          }`.parseJsonString);
        });

        it("should delete duplicated tiles", {
          crates.tileLayer.addItem(`{
            "mapId": "000000000000000000000001",
            "teamId": "000000000000000000000001",
            "changeIndex": 10,
            "isReady": true,
            "isPublic": true,
            "layerName": "sites",
            "extent": 512,
            "geometries": [],
            "properties": [],
            "path": { "z": 0, "y": 0, "x": 0 }
          }`.parseJsonString);

          crates.tileLayer.addItem(`{
            "mapId": "000000000000000000000001",
            "teamId": "000000000000000000000001",
            "changeIndex": 10,
            "isReady": true,
            "isPublic": true,
            "layerName": "sites",
            "extent": 512,
            "points": [],
            "properties": [],
            "path": { "z": 0, "y": 0, "x": 0 }
          }`.parseJsonString);

          auto task = new MapTilesCheck(crates, broadcast);
          task.start("000000000000000000000001");

          crates.tileLayer.get.size.should.equal(1);
          crates.tileLayer.get.exec.front.should.equal(`{
            "_id": "000000000000000000000001",
            "changeIndex": 10,
            "isReady": true,
            "isPublic": true,
            "mapId": "000000000000000000000001",
            "teamId": "000000000000000000000001",
            "geometries": [],
            "properties": [],
            "extent": 512,
            "layerName": "sites",
            "path": { "z": 0, "y": 0, "x": 0 }
          }`.parseJsonString);
        });

        it("should delete invalid tiles", {
          crates.tileLayer.addItem(`{
            "changeIndex": 10,
            "isReady": true,
            "mapId": "000000000000000000000001",
            "teamId": "000000000000000000000001",
            "layerName": "sites"
          }`.parseJsonString);

          auto task = new MapTilesCheck(crates, broadcast);
          task.start("000000000000000000000001");

          crates.tileLayer.get.size.should.equal(0);
        });
      });

      describe("when the map does exist", {
        beforeEach({
          crates.meta.addItem(`{
            "_id": "000000000000000000000001",
            "type": "MapTiles",
            "model": "Map",
            "itemId": "000000000000000000000001",
            "changeIndex": 10,
            "data": {
              "layer": "sites",
              "tiles": [],
              "processedTileCount": 0
            }
          }`.parseJsonString);

          crates.meta.addItem(`{
            "_id": "000000000000000000000002",
            "type": "MapTiles",
            "model": "Map",
            "itemId": "000000000000000000000020",
            "changeIndex": 10,
            "data": {
              "layer": "sites",
              "tiles": [],
              "processedTileCount": 0
            }
          }`.parseJsonString);

          crates.tileLayer.addItem(`{
            "mapId": "000000000000000000000001",
            "teamId": "000000000000000000000001",
            "changeIndex": 10,
            "isReady": true,
            "isPublic": true,
            "layerName": "sites",
            "extent": 512,
            "geometries": [],
            "properties": [],
            "path": { "z": 0, "y": 0, "x": 0 }
          }`.parseJsonString);

          crates.tileLayer.addItem(`{
            "mapId": "000000000000000000000020",
            "teamId": "000000000000000000000001",
            "changeIndex": 10,
            "isReady": true,
            "isPublic": true,
            "layerName": "sites",
            "extent": 512,
            "geometries": [],
            "properties": [],
            "path": { "z": 0, "y": 0, "x": 0 }
          }`.parseJsonString);
        });

        it("should remove the meta and the tiles", {
          auto task = new MapTilesCheck(crates, broadcast);
          task.start("000000000000000000000020");

          crates.tileLayer.get.size.should.equal(1);
          crates.meta.get.size.should.equal(1);
        });
      });
    });
  });
});
