/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.map.visibility;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;

import crate.collection.notifications;

import ogm.models.changeset;
import gis_collective.tasks.map.visibility;

alias suite = Spec!({
  URLRouter router;

  describe("The Map.visibility task", {
    describe("Processing list", {
      ChangeSet changeSet;

      beforeEach({
        setupTestData();

        changeSet = ChangeSet();
        changeSet.model = "Map";
        changeSet.itemId = ObjectId.fromString("1");
      });

      it("should process any changes in fields inside maps", {
        changeSet.model = "Map";
        changeSet.removed["area"] = "";

        MapVisibility.processList(changeSet, crates).should.equal([changeSet.itemId.toJson]);
      });

      it("should process a map delete", {
        changeSet.model = "Map";
        changeSet.type = CrateChangeType.delete_;

        MapVisibility.processList(changeSet, crates).should.equal([changeSet.itemId.toJson]);
      });

      it("should not process any changes inside features", {
        changeSet.model = "Feature";

        MapVisibility.processList(changeSet, crates).should.contain([]);
      });

      it("should not process changes in the Picture model", {
        changeSet.model = "Picture";

        MapVisibility.processList(changeSet, crates).should.equal([]);
      });
    });

    describe("Metadata", {
      MapVisibility task;

      beforeEach({
        setupTestData();

        task = MapVisibility(crates);
      });

      it("updates the computed visibility of the map features", {
        auto map = crates.map.getItem("000000000000000000000001").and.exec.front;
        map["visibility"]["isPublic"] = false;
        crates.map.updateItem(map);

        task.main(MapVisibility.Message(ObjectId("000000000000000000000001")));

        crates.feature.getItem("000000000000000000000001").exec.front["computedVisibility"].should.equal(`{
          "isDefault":false,
          "isPublic":false,
          "team":"000000000000000000000001"
        }`.parseJsonString);
      });
    });
  });
});