/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.map.icons;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;

import crate.collection.notifications;

import ogm.models.changeset;
import gis_collective.tasks.map.MapIcons;

alias suite = Spec!({
  URLRouter router;

  describe("The Map.icons task", {
    describe("Processing list", {
      const Json empty = Json.emptyObject;
      ChangeSet changeSet;

      beforeEach({
        setupTestData();
        changeSet = ChangeSet();
        changeSet.model = "Map";
        changeSet.itemId = ObjectId.fromString("1");
      });

      it("should process feature maps when the icons are changed", {
        changeSet.model = "Feature";
        changeSet.added["icons"] = Json.emptyObject;
        changeSet.removed["icons"] = parseJsonString(`["1"]`);

        MapIcons.processList(changeSet, crates).should.equal([ Json("000000000000000000000001") ]);
      });

      it("should not process feature maps when the author are changed", {
        changeSet.model = "Feature";
        changeSet.added["info.author"] = `""`.parseJsonString;
        changeSet.removed["info.author"] = `""`.parseJsonString;

        MapIcons.processList(changeSet, crates).should.contain([]);
      });

      it("should not process feature maps when the change is missing", {
        changeSet.model = "Feature";
        changeSet.added["visibility"] = 1;
        changeSet.removed["visibility"] = 0;

        MapIcons.processList(changeSet, crates).should.equal([]);
      });

      it("should not process changes in the Picture model", {
        changeSet.model = "Picture";

        MapIcons.processList(changeSet, crates).should.equal([]);
      });
    });

    describe("Metadata", {
      it("should count all features, public feature and contributors", {
        setupTestData();

        MapIcons(crates).main(MapIcons.MapIconsMessage("000000000000000000000001"));

        crates.meta.get.exec.front.should.equal(`{
          "_id": "000000000000000000000001",
          "type": "Map.icons",
          "itemId": "000000000000000000000001",
          "changeIndex": 0,
          "model": "Map",
          "data": {
            "icons": ["000000000000000000000001"]
          }
        }`.parseJsonString);
      });
    });
  });
});
