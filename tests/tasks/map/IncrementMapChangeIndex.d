/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.map.IncrementMapChangeIndex;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;

import crate.collection.notifications;

import ogm.models.changeset;
import gis_collective.tasks.map.IncrementMapChangeIndex;

import geo.xyz;

alias suite = Spec!({
  describe("The IncrementMapChangeIndex task", {
    Team team1;
    Icon icon1;
    IconSet iconSet1;
    Map map1;
    Picture cover;

    beforeEach({
      createCrates;

      crateGetters[`Map`] = &crates.map.getItem;
      crateGetters[`Team`] = &crates.team.getItem;
      crateGetters[`IconSet`] = &crates.iconSet.getItem;
      crateGetters[`Picture`] = &crates.picture.getItem;

      cover._id = ObjectId.fromString("1");
      cover.picture = new PictureFile;
      crates.picture.addItem(cover.serializeToJson);

      createPicture("1", "default");

      auto team1 = Team(ObjectId.fromString("1"), "team1", Json(""), [], [], [], [], true, cover);
      createTeam(team1);

      iconSet1._id = ObjectId.fromString("1");
      iconSet1.name = "set1";
      iconSet1.visibility.team = team1;
      iconSet1.visibility.isPublic = true;
      iconSet1.visibility.isDefault = true;
      createIconSet(iconSet1);

      map1 = createMap(Map(ObjectId.fromString("1"), "map1", Json(""),
        Visibility(team1, true), Polygon([[[-70, 30], [-70, 50], [-80, 50], [-80, 30], [-70,30]]]),
        cover, IconSets(true, [iconSet1]), BaseMaps(), "", SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-02-01T00:00:00Z"),
        ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z"))));
    });

    describe("Processing list", {
      it("should return the list of the feature maps", {
        createFeature(
          Feature(ObjectId.fromString("1"),
              [map1],
              "site1",
              Json("description of site1"),
              GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.5, 1.5] }`.parseJsonString),
              ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z"))));

        ChangeSet changeSet;
        changeSet.model = "Feature";
        changeSet.itemId = ObjectId.fromString("1");

        auto task = new IncrementMapChangeIndex(crates);

        task.processList(changeSet)
          .should.equal(["000000000000000000000001"]);
      });

      it("should return the list of the removed maps if the item is not found", {
        ChangeSet changeSet;
        changeSet.model = "Feature";
        changeSet.itemId = ObjectId.fromString("1");
        changeSet.removed["maps[1]"] = `"3"`.parseJsonString;
        changeSet.removed["maps[2]"] = `"4"`.parseJsonString;

        auto task = new IncrementMapChangeIndex(crates);

        task.processList(changeSet)
          .should.equal(["3", "4"]);
      });
    });

    describe("Running the task", {
      it("should increment the change index", {
        auto task = new IncrementMapChangeIndex(crates);
        task.start(map1._id.toString);

        crates.map.getItem(map1._id.toString).exec
          .front["info"]["changeIndex"].to!size_t
          .should.equal(1);
      });
    });
  });
});