/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.map.MapMask;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;

import crate.collection.notifications;

import ogm.models.meta;
import ogm.models.changeset;

import gis_collective.tasks.map.MapMask;
import gis_collective.hmq.broadcast.memory;


alias suite = Spec!({
  URLRouter router;

  describe("The Map.Mask task", {
    describe("Processing list", {
      ChangeSet changeSet;

      beforeEach({
        changeSet = ChangeSet();
        changeSet.model = "Map";
        changeSet.itemId = ObjectId.generate;
      });

      it("should process add changes inside Maps", {
        changeSet.model = "Map";
        changeSet.type = CrateChangeType.add;

        MapMask.processList(changeSet).should.equal([ changeSet.itemId.toJson ]);
      });

      it("should not process changes inside features", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.delete_;

        MapMask.processList(changeSet).should.equal([ ]);
      });

      it("should process update changes inside maps", {
        changeSet.model = "Map";
        changeSet.type = CrateChangeType.update;

        MapMask.processList(changeSet).should.equal([ changeSet.itemId.toJson ]);
      });

      it("should not process delete changes inside features", {
        changeSet.model = "Map";
        changeSet.type = CrateChangeType.delete_;

        MapMask.processList(changeSet).should.equal([ ]);
      });
    });

    describe("The task", {
      MapMask task;

      beforeEach({
        setupTestData();

        task = MapMask();
      });

      it("triggers a feature.mask for each feature belonging to the map", {
        auto request = MapMask.Request(ObjectId.fromString("000000000000000000000001"));

        auto broadcast = new MemoryBroadcast();
        task.broadcast = broadcast;
        task.crates = crates;

        Json[] receivedMessages;
        void receiver(const Json message) {
          receivedMessages ~= message;
        }

        task.broadcast.register("Feature.Mask", &receiver);
        task.main(request);

        receivedMessages.length.should.equal(1);
        receivedMessages[0].should.equal(`{ "id": "000000000000000000000001" }`.parseJsonString);
      });
    });
  });
});