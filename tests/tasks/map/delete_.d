/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.map.delete_;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;

import crate.collection.notifications;

import ogm.models.meta;
import ogm.models.changeset;

import gis_collective.tasks.map.delete_;
import gis_collective.hmq.broadcast.memory;


alias suite = Spec!({
  URLRouter router;

  describe("The Map.delete task", {
    describe("Processing list", {
      ChangeSet changeSet;

      beforeEach({
        changeSet = ChangeSet();
        changeSet.model = "Map";
        changeSet.itemId = ObjectId.generate;
      });

      it("should not process add changes inside maps", {
        changeSet.model = "Map";
        changeSet.type = CrateChangeType.add;

        MapDelete.processList(changeSet).should.equal([ ]);
      });

      it("should not process changes inside features", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.delete_;

        MapDelete.processList(changeSet).should.equal([ ]);
      });

      it("should not process update changes inside maps", {
        changeSet.model = "Map";
        changeSet.type = CrateChangeType.update;

        MapDelete.processList(changeSet).should.equal([ ]);
      });

      it("should process delete changes inside maps", {
        changeSet.model = "Map";
        changeSet.type = CrateChangeType.delete_;

        MapDelete.processList(changeSet).should.equal([ changeSet.itemId.toJson ]);
      });
    });

    describe("The task", {
      MapDelete task;

      beforeEach({
        setupTestData();

        task = MapDelete();

        Meta meta;
        meta.model = "Map";
        meta.itemId = "000000000000000000000001";

        crates.meta.addItem(meta.serializeToJson);
      });

      it("should remove all features related to that map", {
        auto request = MapDelete.Request(ObjectId.fromString("000000000000000000000001"));

        task.crates = crates;
        task.broadcast = new MemoryBroadcast;
        task.main(request);

        crates.feature.get.where("_id").equal("000000000000000000000001").and.size.should.equal(0);
        crates.feature.get.where("_id").equal("000000000000000000000002").and.size.should.equal(1);
        crates.feature.get.where("_id").equal("000000000000000000000003").and.size.should.equal(1);
        crates.feature.get.where("_id").equal("000000000000000000000004").and.size.should.equal(1);
      });

      it("should remove all map meta", {
        auto request = MapDelete.Request(ObjectId.fromString("000000000000000000000001"));

        task.crates = crates;
        task.broadcast = new MemoryBroadcast;
        task.main(request);

        crates.meta.get
          .where("model").equal("Map").and
          .where("itemId").equal("000000000000000000000001").and
          .size.should.equal(0);
      });

      it("should trigger a remove feature job", {
        Json message;

        void mockHandler(const Json msg) {
          message = msg.clone;
        }

        auto request = MapDelete.Request(ObjectId.fromString("000000000000000000000001"));

        task.broadcast = new MemoryBroadcast;
        task.broadcast.register("feature.delete", &mockHandler);

        task.crates = crates;
        task.main(request);

        message.should.equal(`{
          "id": "000000000000000000000001"
        }`.parseJsonString);
      });

      describe("when there is a feature on two maps", {
        beforeEach({
          auto feature = crates.feature.getItem("000000000000000000000001").exec.front;

          feature["maps"] ~= Json("000000000000000000000002");

          crates.feature.updateItem(feature);
        });

        it("should remove the deleted map from the map list", {
          auto request = MapDelete.Request(ObjectId.fromString("000000000000000000000001"));

          task.crates = crates;
          task.broadcast = new MemoryBroadcast;
          task.main(request);

          crates.feature.get.where("_id").equal("000000000000000000000001").and.size.should.equal(1);
          crates.feature.getItem("000000000000000000000001").exec.front["maps"].should.equal(`["000000000000000000000002"]`.parseJsonString);
        });
      });
    });
  });
});