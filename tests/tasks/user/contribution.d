/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.user.contribution;

import tests.fixtures;
import gis_collective.tasks.user.Contribution;

alias suite = Spec!({

  describe("The user.contribution task", {
    UserContribution task;

    beforeEach({
      setupTestData();
      task = UserContribution();
      task.crates = crates;
    });

    describe("the processing list", {
      ChangeSet changeSet;

      beforeEach({
        changeSet.type = CrateChangeType.add;
        changeSet.author = "000000000000000000000001";
        changeSet.itemId = "000000000000000000000099";
        changeSet.time = SysTime.fromISOExtString("2020-04-04T06:40:33Z");
      });

      it("should trigger on a site change", {
        changeSet.model = "Site";

        task.processList(changeSet).should.equal([ Json("000000000000000000000001") ]);
      });

      it("should trigger on a feature change", {
        changeSet.model = "Feature";

        task.processList(changeSet).should.equal([ Json("000000000000000000000001") ]);
      });

      it("should trigger on a map change", {
        changeSet.model = "Map";

        task.processList(changeSet).should.equal([ Json("000000000000000000000001") ]);
      });

      it("should trigger on a UserProfile change", {
        changeSet.model = "UserProfile";

        task.processList(changeSet).should.equal([ Json("000000000000000000000099") ]);
      });
    });

    describe("when a profile does not exist", {
      it("should do nothing", {
        task.main(UserContributionRequest("000000000000000000000001"));

        crates.meta.get.size.should.equal(0);
      });

      describe("when a meta for the profile exists", {
        beforeEach({
          crates.meta.addItem(`{
            "type" : "user.contribution",
            "model" : "UserData",
            "itemId" : "000000000000000000000001",
            "changeIndex": 0,
            "data" : {
              "private": {
                "000000000000000000000001": [{
                  "day": 0,
                  "week": 10,
                  "year": 2020,
                  "count": 1
                }]
              }
            }}`.parseJsonString);
        });

        it("should delete the old meta", {
          task.main(UserContributionRequest("000000000000000000000001"));

          crates.meta.get.size.should.equal(0);
        });
      });
    });

    describe("for a profile that wants to hide private contributions", {
      beforeEach({
        auto profile = UserProfile();
        profile.showPrivateContributions = false;

        crates.userProfile.addItem(profile.serializeToJson);

        ChangeSet changeSet;
        changeSet.model = "Site";
        changeSet.type = CrateChangeType.add;
        changeSet.author = "000000000000000000000001";
        changeSet.itemId = "000000000000000000000001";
        changeSet.time = SysTime.fromISOExtString("2020-04-04T06:40:33Z");

        task.time = changeSet.time;

        crates.changeSet.addItem(changeSet.serializeToJson);
      });

      describe("when there is a changeset of a private site", {
        beforeEach({
          auto site1 = crates.feature.getItem("000000000000000000000001").and.exec.front;
          site1["visibility"] = 0;

          crates.feature.updateItem(site1);
        });

        it("should create the contribution chart with a public change", {
          task.main(UserContributionRequest("000000000000000000000001"));

          crates.meta.get.and.exec.array.serializeToJson.should.equal(`[{
            "_id":"000000000000000000000001",
            "type":"user.contribution",
            "changeIndex":0,
            "itemId":"000000000000000000000001",
            "model":"UserModel",
            "data":{
              "private":{
                "000000000000000000000001": [
                  {"year":2020,"count":1,"day":6,"week":14}
                ]
              },
              "public":[]
          }}]`.parseJsonString);
        });
      });

      describe("when there is a changeset of a deleted site", {
        beforeEach({
          crates.feature.deleteItem("000000000000000000000001");
        });

        it("should create the contribution chart with a public change", {
          task.main(UserContributionRequest("000000000000000000000001"));

          crates.meta.get.and.exec.array.serializeToJson.should.equal(`[]`.parseJsonString);
        });
      });

      describe("when there is a changeset of a public site with a missing map", {
        beforeEach({
          crates.map.deleteItem("000000000000000000000001");
        });

        it("should create the contribution chart with a public change", {
          task.main(UserContributionRequest("000000000000000000000001"));

          crates.meta.get.and.exec.array.serializeToJson.should.equal(`[]`.parseJsonString);
        });
      });

      describe("when there is a changeset of a public site from a private map", {
        beforeEach({
          auto map1 = crates.map.getItem("000000000000000000000001").and.exec.front;
          map1["visibility"]["isPublic"] = false;

          crates.map.updateItem(map1);
        });

        it("should create the contribution chart with a public change", {
          task.main(UserContributionRequest("000000000000000000000001"));

          crates.meta.get.and.exec.array.serializeToJson.should.equal(`[{
            "_id":"000000000000000000000001",
            "type":"user.contribution",
            "changeIndex":0,
            "itemId":"000000000000000000000001",
            "model":"UserModel",
            "data":{
              "private":{
                "000000000000000000000001": [
                  {"year":2020,"count":1,"day":6,"week":14}
                ]
              },
              "public":[]
          }}]`.parseJsonString);
        });
      });
    });

    describe("for a profile that wants to show private contributions", {
      beforeEach({
        auto profile = UserProfile();
        profile.showPrivateContributions = true;

        crates.userProfile.addItem(profile.serializeToJson);

        ChangeSet changeSet;
        changeSet.model = "Site";
        changeSet.type = CrateChangeType.add;
        changeSet.author = "000000000000000000000001";
        changeSet.itemId = "000000000000000000000001";
        changeSet.time = SysTime.fromISOExtString("2020-04-04T06:40:33Z");

        task.time = changeSet.time;

        crates.changeSet.addItem(changeSet.serializeToJson);
      });

      it("should create the contribution chart with a public change", {
        task.main(UserContributionRequest("000000000000000000000001"));

        crates.meta.get.and.exec.array.serializeToJson.should.equal(`[{
          "_id":"000000000000000000000001",
          "type":"user.contribution",
          "changeIndex":0,
          "itemId":"000000000000000000000001",
          "model":"UserModel",
          "data":{
            "private":{},
            "public":[
              {"year":2020,"count":1,"day":6,"week":14}
          ]}}]`.parseJsonString);
      });

      describe("when there is a changeset of a private site", {
        beforeEach({

          auto site1 = crates.feature.getItem("000000000000000000000001").and.exec.front;
          site1["visibility"] = 0;

          crates.feature.updateItem(site1);
        });

        it("should create the contribution chart with a public change", {
          task.main(UserContributionRequest("000000000000000000000001"));

          crates.meta.get.and.exec.array.serializeToJson.should.equal(`[{
            "_id":"000000000000000000000001",
            "type":"user.contribution",
            "changeIndex":0,
            "itemId":"000000000000000000000001",
            "model":"UserModel",
            "data":{
              "public":[
                {"year":2020,"count":1,"day":6,"week":14}
              ],
              "private": {}
          }}]`.parseJsonString);
        });
      });

      describe("when there is a changeset of a public site from a private map", {
        beforeEach({
          auto map1 = crates.map.getItem("000000000000000000000001").and.exec.front;
          map1["visibility"]["isPublic"] = false;

          crates.map.updateItem(map1);
        });

        it("should create the contribution chart with a public change", {
          task.main(UserContributionRequest("000000000000000000000001"));

          crates.meta.get.and.exec.array.serializeToJson.should.equal(`[{
            "_id":"000000000000000000000001",
            "type":"user.contribution",
            "changeIndex":0,
            "itemId":"000000000000000000000001",
            "model":"UserModel",
            "data":{
              "public":[
                {"year":2020,"count":1,"day":6,"week":14}
              ],
              "private": {}
          }}]`.parseJsonString);
        });
      });
    });

    describe("for a contribution older than 365 days", {
      beforeEach({
        auto profile = UserProfile();
        profile.showPrivateContributions = true;

        crates.userProfile.addItem(profile.serializeToJson);

        ChangeSet changeSet;
        changeSet.model = "Site";
        changeSet.type = CrateChangeType.add;
        changeSet.author = "000000000000000000000001";
        changeSet.itemId = "000000000000000000000001";
        changeSet.time = SysTime.fromISOExtString("2020-04-04T06:40:33Z");

        task.time = SysTime.fromISOExtString("2021-04-05T06:40:33Z");

        crates.changeSet.addItem(changeSet.serializeToJson);
      });

      it("should should ignore the change", {
        task.main(UserContributionRequest("000000000000000000000001"));

        crates.meta.get.and.exec.array.serializeToJson.should.equal(`[]`.parseJsonString);
      });
    });
  });
});
