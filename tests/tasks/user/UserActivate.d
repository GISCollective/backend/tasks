/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.user.UserActivate;

import tests.fixtures;
import gis_collective.tasks.user.UserActivate;
import gis_collective.hmq.broadcast.memory;
import ogm.calendar;

alias suite = Spec!({

  describe("The user.activate task", {
    UserActivate task;
    Json[] receivedMessages;

    beforeEach({
      setupTestData();
      task = UserActivate();
      task.crates = crates;
      task.broadcast = new MemoryBroadcast();

      receivedMessages = [];
      void receiver(const Json message) {
        receivedMessages ~= message;
      }

      Json name = Json.emptyObject;
      name["name"] = "appearance.name";
      name["value"] = "OGM";
      crates.preference.addItem(name);

      task.broadcast.register("notifications.message", &receiver);
      SysCalendar.instance = new CalendarMock("2022-03-12T12:34:44Z");
    });

    describe("when the article contains a string", {
      beforeEach({
        Article testArticle;
        testArticle.slug = "notification-user-activate";
        testArticle.title = "Welcome to {{serviceName}}";
        testArticle.content = "Hello {{receiverName}},\n\nThanks for signing up to {{serviceName}}!\n\nTo get started, click the link below to confirm your account.\n\n{{serviceUrl}}/login/activate?email={{email}}&token={{token}}";

        createArticle(testArticle);
      });

      it("should do nothing when the user does not exist", {
        task.main(UserActivateRequest("000000000000000000000099"));

        receivedMessages.length.should.equal(0);
      });

      it("should do nothing when the user is activated", {
        auto user = crates.user.get.where("_id").equal("000000000000000000000001").and.exec.front;
        user["isActive"] = true;
        crates.user.updateItem(user);
        task.main(UserActivateRequest("000000000000000000000001"));

        receivedMessages.length.should.equal(0);
      });

      it("should trigger a message when the user with a profile exists", {
        auto user = crates.user.get.where("_id").equal("000000000000000000000001").and.exec.front;
        user["isActive"] = false;
        crates.user.updateItem(user);

        auto profile = UserProfile().serializeToJson;
        profile["_id"] = "000000000000000000000001";
        profile["firstName"] = "John";
        crates.userProfile.addItem(profile);

        task.main(UserActivateRequest("000000000000000000000001"));

        auto tokens = crates.user.get.where("_id").equal("000000000000000000000001").and.exec
          .front["tokens"]
          .byValue
          .filter!(a => a["type"] == "activation")
          .front;
        string token = tokens["name"].to!string;

        receivedMessages.length.should.equal(1);
        receivedMessages[0].should.equal(`{"id":"000000000000000000000001"}`.parseJsonString);

        auto message = crates.message.getItem("000000000000000000000001").and.exec.front;

        message.should.equal((`{
          "html":"<p>Hello John,<\/p>\n\n<p>Thanks for signing up to OGM!<\/p>\n\n<p>To get started, click the link below to confirm your account.<\/p>\n\n<p>/login/activate?email=leader@gmail.com&token=` ~ token ~ `<\/p>\n",
          "isSent":false,
          "actions":{"Confirm email":"/login/activate?email=leader@gmail.com&token=` ~ token ~ `"},
          "text":"Hello John,\n\nThanks for signing up to OGM!\n\nTo get started, click the link below to confirm your account.\n/login/activate?email=leader@gmail.com&token=` ~ token ~ `",
          "to":{"type":"user","value":"000000000000000000000001"},
          "uniqueKey":"notification-user-activate-000000000000000000000001-2022-03-12T12:34:44Z",
          "_id":"000000000000000000000001",
          "type":"message",
          "subject":"Welcome to OGM",
          "sentOn":"0001-01-01T00:00:00+00:00",
          "useGenericTemplate":true
        }`).parseJsonString);
      });
    });
  });
});
