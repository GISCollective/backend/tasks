/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.user.testNotification;

import tests.fixtures;
import gis_collective.tasks.user.TestNotification;
import gis_collective.hmq.broadcast.memory;
import ogm.calendar;

alias suite = Spec!({

  describe("The user.testNotification task", {
    TestNotification task;
    Json[] receivedMessages;

    beforeEach({
      setupTestData();
      task = TestNotification();
      task.crates = crates;
      task.broadcast = new MemoryBroadcast();

      SysCalendar.instance = new CalendarMock("2018-01-01T12:30:10Z");

      receivedMessages = [];
      void receiver(const Json message) {
        receivedMessages ~= message;
      }

      task.broadcast.register("notifications.message", &receiver);
    });

    describe("when the article contains a string", {
      beforeEach({
        Article testArticle;
        testArticle.slug = "notification-test";
        testArticle.title = "Notification test message";
        testArticle.content = "Hello {{receiverName}},\n\nYou're receiving this notification because someone is testing the notification system. Please do not reply.";

        createArticle(testArticle);
      });

      it("should do nothing when the user does not exist", {
        task.main(TestNotificationRequest("000000000000000000000099"));

        receivedMessages.length.should.equal(0);
      });

      it("should trigger a message when the user exists", {
        task.main(TestNotificationRequest("000000000000000000000001"));

        receivedMessages.length.should.equal(2);
        receivedMessages[0].should.equal(`{"id":"000000000000000000000001"}`.parseJsonString);

        auto message = crates.message.getItem("000000000000000000000001").and.exec.front;
        message.should.equal(`{
          "html":"<p>Hello ---,<\/p>\n\n<p>You're receiving this notification because someone is testing the notification system. Please do not reply.<\/p>\n",
          "isSent":false,
          "actions":{},
          "text":"Hello ---,\n\nYou're receiving this notification because someone is testing the notification system. Please do not reply.",
          "to":{"type":"user","value":"000000000000000000000001"},
          "uniqueKey":"testMessage_000000000000000000000001_2018-01-01T12:30:10Z",
          "_id":"000000000000000000000001",
          "type":"message",
          "subject":"Notification test message",
          "sentOn":"0001-01-01T00:00:00+00:00",
          "useGenericTemplate":true
        }`.parseJsonString);
      });

      it("triggers a message when the user with a profile exists", {
        auto profile = UserProfile().serializeToJson;
        profile["_id"] = "000000000000000000000001";
        profile["firstName"] = "John";
        crates.userProfile.addItem(profile);

        task.main(TestNotificationRequest("000000000000000000000001"));

        receivedMessages.length.should.equal(2);
        receivedMessages[0].should.equal(`{"id":"000000000000000000000001"}`.parseJsonString);

        auto message = crates.message.getItem("000000000000000000000001").and.exec.front;
        message.should.equal(`{
          "html":"<p>Hello John,<\/p>\n\n<p>You're receiving this notification because someone is testing the notification system. Please do not reply.<\/p>\n",
          "isSent":false,
          "actions":{},
          "text":"Hello John,\n\nYou're receiving this notification because someone is testing the notification system. Please do not reply.",
          "to":{"type":"user","value":"000000000000000000000001"},
          "uniqueKey":"testMessage_000000000000000000000001_2018-01-01T12:30:10Z",
          "_id":"000000000000000000000001",
          "type":"message",
          "subject":"Notification test message",
          "sentOn":"0001-01-01T00:00:00+00:00",
          "useGenericTemplate":true
        }`.parseJsonString);
      });
    });

    describe("when the article contains blocks", {
      beforeEach({
        Article testArticle;
        testArticle.slug = "notification-test";
        testArticle.title = "Notification test message";
        testArticle.content = `{
          "blocks": [
            {
              "type": "header",
              "data": {
                "level": 1,
                "text": "Notification test message"
              }
            },
            {
              "type": "paragraph",
              "data": {
                "text": "Hello {{receiverName}},"
              }
            },
            {
              "type": "paragraph",
              "data": {
                "text": "You're receiving this notification because someone is testing the notification system. Please do not reply."
              }
            }
          ]
        }`.parseJsonString;

        createArticle(testArticle);
      });

      it("triggers a message when the user with a profile exists", {
        auto profile = UserProfile().serializeToJson;
        profile["_id"] = "000000000000000000000001";
        profile["firstName"] = "John";
        crates.userProfile.addItem(profile);

        task.main(TestNotificationRequest("000000000000000000000001"));

        receivedMessages.length.should.equal(2);
        receivedMessages[0].should.equal(`{"id":"000000000000000000000001"}`.parseJsonString);

        auto message = crates.message.getItem("000000000000000000000001").and.exec.front;
        message.should.equal(`{
          "html":"<p>Hello John,<\/p>\n\n<p>You're receiving this notification because someone is testing the notification system. Please do not reply.<\/p>\n",
          "isSent":false,
          "actions":{},
          "text":"Hello John,\n\nYou're receiving this notification because someone is testing the notification system. Please do not reply.\n",
          "to":{"type":"user","value":"000000000000000000000001"},
          "uniqueKey":"testMessage_000000000000000000000001_2018-01-01T12:30:10Z",
          "_id":"000000000000000000000001",
          "type":"message",
          "subject":"Notification test message",
          "sentOn":"0001-01-01T00:00:00+00:00",
          "useGenericTemplate":true
        }`.parseJsonString);

        message = crates.message.getItem("000000000000000000000002").and.exec.front;
        message.should.equal(`{
          "html":"<p>Hello John,<\/p>\n\n<p>You're receiving this notification because someone is testing the notification system. Please do not reply.<\/p>\n",
          "isSent":false,
          "actions":{},
          "text":"Hello John,\n\nYou're receiving this notification because someone is testing the notification system. Please do not reply.\n",
          "to":{"type":"user","value":"000000000000000000000001"},
          "uniqueKey":"testMessage_000000000000000000000001_2018-01-01T12:30:10Z",
          "_id":"000000000000000000000002",
          "type":"admin",
          "subject":"Notification test message",
          "sentOn":"0001-01-01T00:00:00+00:00",
          "useGenericTemplate":true
        }`.parseJsonString);
      });
    });
  });
});
