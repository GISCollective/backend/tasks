/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.user.forgotPassword;

import tests.fixtures;
import gis_collective.tasks.user.ForgotPassword;
import gis_collective.hmq.broadcast.memory;
import ogm.calendar;

alias suite = Spec!({

  describe("The user.forgot task", {
    ForgotPassword task;
    Json[] receivedMessages;

    beforeEach({
      setupTestData();
      task = ForgotPassword();
      task.crates = crates;
      task.broadcast = new MemoryBroadcast();

      receivedMessages = [];
      void receiver(const Json message) {
        receivedMessages ~= message;
      }

      Json name = Json.emptyObject;
      name["name"] = "appearance.name";
      name["value"] = "OGM";
      crates.preference.addItem(name);

      task.broadcast.register("notifications.message", &receiver);
      SysCalendar.instance = new CalendarMock("2022-03-12T12:34:44Z");
    });

    describe("when the article contains a string", {
      beforeEach({
        Article testArticle;
        testArticle.slug = "notification-user-forgot";
        testArticle.title = "Reset password at {{serviceName}}";
        testArticle.content = "Hello {{receiverName}},\n\nSomeone, hopefully you, has requested to reset the password for your {{serviceName}} account on {{serviceUrl}}.\n\nIf you did not perform this request, you can safely ignore this email. Otherwise, click the link below to complete the process.\n\n{{serviceUrl}}/login/reset?email={{email}}&token={{token}}";

        createArticle(testArticle);
      });

      it("should do nothing when the user does not exist", {
        task.main(ForgotPasswordRequest("000000000000000000000099"));

        receivedMessages.length.should.equal(0);
      });

      it("should trigger a message when the user with a profile exists", {
        auto profile = UserProfile().serializeToJson;
        profile["_id"] = "000000000000000000000001";
        profile["firstName"] = "John";
        crates.userProfile.addItem(profile);

        task.main(ForgotPasswordRequest("000000000000000000000001"));

        auto tokens = crates.user.get.where("_id").equal("000000000000000000000001").and.exec
          .front["tokens"]
          .byValue
          .filter!(a => a["type"] == "passwordReset")
          .front;
        string token = tokens["name"].to!string;

        receivedMessages.length.should.equal(1);
        receivedMessages[0].should.equal((`{"id":"000000000000000000000001"}`).parseJsonString);

        auto message = crates.message.getItem("000000000000000000000001").and.exec.front;

        message.should.equal((`{
          "html":"<p>Hello John,<\/p>\n\n<p>Someone, hopefully you, has requested to reset the password for your OGM account on .<\/p>\n\n<p>If you did not perform this request, you can safely ignore this email. Otherwise, click the link below to complete the process.<\/p>\n\n<p>/login/reset?email=leader@gmail.com&token=` ~ token ~ `<\/p>\n",
          "isSent":false,
          "actions":{"Reset password":"/login/reset?email=leader@gmail.com&token=` ~ token ~ `"},
          "text":"Hello John,\n\nSomeone, hopefully you, has requested to reset the password for your OGM account on .\n\nIf you did not perform this request, you can safely ignore this email. Otherwise, click the link below to complete the process.\n/login/reset?email=leader@gmail.com&token=` ~ token ~ `",
          "to":{"type":"user","value":"000000000000000000000001"},
          "uniqueKey":"notification-user-forgot-000000000000000000000001-2022-03-12T12:34:44Z",
          "_id":"000000000000000000000001",
          "type":"message",
          "subject":"Reset password at OGM",
          "sentOn":"0001-01-01T00:00:00+00:00",
          "useGenericTemplate":true
        }`).parseJsonString);
      });
    });
  });
});
