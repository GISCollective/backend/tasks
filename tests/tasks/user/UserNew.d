/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.user.UserNew;

import tests.fixtures;
import gis_collective.tasks.user.UserNew;
import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({

  describe("The user.new task", {
    UserNew task;
    Json[] receivedMessages;

    beforeEach({
      setupTestData();
      task = UserNew();
      task.crates = crates;
      task.broadcast = new MemoryBroadcast();

      receivedMessages = [];
      void receiver(const Json message) {
        receivedMessages ~= message;
      }

      Json name = Json.emptyObject;
      name["name"] = "appearance.name";
      name["value"] = "OGM";
      crates.preference.addItem(name);

      task.broadcast.register("notifications.message", &receiver);
    });

    describe("when the article contains a string", {
      beforeEach({
        Article testArticle;
        testArticle.slug = "notification-user-new";
        testArticle.title = "There is a new user at {{serviceName}}";
        testArticle.content = "Hello {{receiverName}},\n\n{{personName}} just registered with the email {{userEmail}} at {{serviceName}}. You can check their profile at\n\n{{serviceUrl}}/browse/profiles/{{userId}}";

        createArticle(testArticle);
      });

      it("should do nothing when the user does not exist", {
        task.main(UserNewRequest("000000000000000000000099"));

        receivedMessages.length.should.equal(0);
      });

      it("should trigger a message when the user with a profile exists", {
        auto profile = UserProfile().serializeToJson;
        profile["_id"] = "000000000000000000000001";
        profile["firstName"] = "John";
        crates.userProfile.addItem(profile);

        task.main(UserNewRequest("000000000000000000000001"));

        receivedMessages.length.should.equal(1);
        receivedMessages[0].should.equal(`{"id":"000000000000000000000001"}`.parseJsonString);

        auto message = crates.message.getItem("000000000000000000000001").and.exec.front;

        message.should.equal(`{
          "html":"<p>Hello ---,<\/p>\n\n<p>John just registered with the email leader@gmail.com at OGM. You can check their profile at<\/p>\n\n<p>/browse/profiles/000000000000000000000001<\/p>\n",
          "isSent":false,
          "actions":{"View profile":"/browse/profiles/000000000000000000000001"},
          "text":"Hello ---,\nJohn just registered with the email leader@gmail.com at OGM. You can check their profile at\n/browse/profiles/000000000000000000000001",
          "to":{"type":"","value":""},
          "uniqueKey":"notification-user-new-000000000000000000000001",
          "_id":"000000000000000000000001",
          "type":"admin",
          "subject":"There is a new user at OGM","sentOn":"0001-01-01T00:00:00+00:00",
          "useGenericTemplate":true
        }`.parseJsonString);
      });
    });
  });
});
