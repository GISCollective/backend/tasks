/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.user.changedPassword;

import tests.fixtures;
import gis_collective.tasks.user.ChangedPassword;
import gis_collective.hmq.broadcast.memory;
import ogm.calendar;

alias suite = Spec!({
  describe("The user.changedPassword task", {
    ChangedPassword task;
    Json[] receivedMessages;

    beforeEach({
      setupTestData();
      task = ChangedPassword();
      task.crates = crates;
      task.broadcast = new MemoryBroadcast();

      receivedMessages = [];
      void receiver(const Json message) {
        receivedMessages ~= message;
      }

      Json name = Json.emptyObject;
      name["name"] = "appearance.name";
      name["value"] = "OGM";
      crates.preference.addItem(name);

      task.broadcast.register("notifications.message", &receiver);
      SysCalendar.instance = new CalendarMock("2022-03-12T12:34:44Z");
    });

    describe("when the article contains a string", {
      beforeEach({
        Article testArticle;
        testArticle.slug = "notification-changed-password";
        testArticle.title = "Changed password at {{serviceName}}";
        testArticle.content = "Hello {{receiverName}},\n\nThe password for your {{serviceName}} account on {{serviceUrl}} has successfully been changed. If you did not initiate this change, please contact your administrator immediately.";

        createArticle(testArticle);
      });

      it("should do nothing when the user does not exist", {
        task.main(ChangedPasswordRequest("000000000000000000000099"));

        receivedMessages.length.should.equal(0);
      });

      it("should trigger a message when the user with a profile exists", {
        auto profile = UserProfile().serializeToJson;
        profile["_id"] = "000000000000000000000001";
        profile["firstName"] = "John";
        crates.userProfile.addItem(profile);

        task.main(ChangedPasswordRequest("000000000000000000000001"));

        receivedMessages.length.should.equal(1);
        receivedMessages[0].should.equal((`{
          "id": "000000000000000000000001"
        }`).parseJsonString);

        auto message = crates.message.getItem("000000000000000000000001").and.exec.front;
        message.should.equal((`{
          "html":"<p>Hello John,<\/p>\n\n<p>The password for your OGM account on  has successfully been changed. If you did not initiate this change, please contact your administrator immediately.<\/p>\n",
          "isSent":false,
          "actions":{},
          "text":"Hello John,\n\nThe password for your OGM account on  has successfully been changed. If you did not initiate this change, please contact your administrator immediately.",
          "to":{"type":"user","value":"000000000000000000000001"},
          "uniqueKey":"notification-changed-password-000000000000000000000001-2022-03-12T12:34:44Z",
          "_id":"000000000000000000000001",
          "type":"message",
          "subject":"Changed password at OGM",
          "sentOn":"0001-01-01T00:00:00+00:00",
          "useGenericTemplate":true
        }`).parseJsonString);
      });
    });
  });
});
