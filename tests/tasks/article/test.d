/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.article.test;

import tests.fixtures;

import gis_collective.tasks.article.Test;
import vibeauth.mail.base;
import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;

  describe("The article.test task", {
    ArticleTest task;
    string publicNewsletterId;
    Json[] receivedMessages;

    beforeEach({
      setupTestData();
      task = ArticleTest();
      createTestDefaultSpace();

      crates.newsletter.addItem(`{}`.parseJsonString);

      auto publicNewsletter = crates.newsletter.addItem(`{
        "name": "test",
        "description": "some description",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      publicNewsletterId = publicNewsletter["_id"].to!string;

      crates.article.addItem(`{
        "type": "newsletter-article",
        "relatedId": "000000000000000000000002",
        "status": "pending",
        "content": { "blocks": [{
          "type": "paragraph",
          "data": {
            "text": "If the <a href=\"https://example.com\">information</a> we provide is not useful."
          }
        }]}
      }`.parseJsonString);

      task.broadcast = new MemoryBroadcast();

      receivedMessages = [];
      void receiver(const Json message) {
        receivedMessages ~= message;
      }
      task.broadcast.register("newsletter.email", &receiver);
    });

    it("does not trigger a job when the user id is not found", {
      task.crates = crates;

      ArticleTest.Request message;
      message.id = "000000000000000000000001";
      message.userId = "000000000000000000000090";

      task.main(message);

      receivedMessages.length.should.equal(0);
    });

    it("triggers a newsletter email job when the user is found", {
      task.crates = crates;

      ArticleTest.Request message;
      message.id = "000000000000000000000001";
      message.userId = "000000000000000000000002";

      task.main(message);

      receivedMessages.length.should.equal(1);
      receivedMessages[0].should.equal(`{
        "recipients":["member@gmail.com"],
        "subject":"",
        "isTest":true,
        "content":{
          "blocks":[
            {"type":"paragraph","data":{"text":"If the <a href=\"https://giscollective.com/link-redirect?id=000000000000000000000001\">information<\/a> we provide is not useful."}
          }]
        },
        "articleId":"000000000000000000000001",
        "newsletterId":"000000000000000000000002"
      }`.parseJsonString);
    });

    it("does not ignore an article in the draft state", {
      crates.newsletterEmail.addItem(`{
        "_id": "000000000000000000000001",
        "email": "member@mail.com",
        "list": [{
          "newsletter": "000000000000000000000002",
          "joinedOn": "2023-01-12T16:20:11Z",
        }]
      }`.parseJsonString);

      task.crates = crates;

      ArticleTest.Request message;
      message.id = "000000000000000000000001";
      message.userId = "000000000000000000000002";

      auto article = crates.article.getItem("000000000000000000000001").and.exec.front;
      article["status"] = "draft";
      crates.article.updateItem(article);

      task.main(message);

      receivedMessages.length.should.equal(1);
    });

    it("does not ignore a welcome message article", {
      crates.newsletterEmail.addItem(`{
        "_id": "000000000000000000000001",
        "email": "member@mail.com",
        "list": [{
          "newsletter": "000000000000000000000002",
          "joinedOn": "2023-01-12T16:20:11Z",
        }]
      }`.parseJsonString);

      task.crates = crates;

      ArticleTest.Request message;
      message.id = "000000000000000000000001";
      message.userId = "000000000000000000000002";

      auto article = crates.article.getItem("000000000000000000000001").and.exec.front;
      article["type"] = "newsletter-welcome-message";
      crates.article.updateItem(article);

      task.main(message);

      receivedMessages.length.should.equal(1);
    });

    it("does not updates the article state", {
      crates.newsletterEmail.addItem(`{
        "_id": "000000000000000000000001",
        "email": "member@mail.com",
        "list": [{
          "newsletter": "000000000000000000000002"
        }]
      }`.parseJsonString);

      task.crates = crates;

      ArticleTest.Request message;
      message.id = "000000000000000000000001";
      message.userId = "000000000000000000000002";

      task.main(message);

      auto article = crates.article.getItem("000000000000000000000001").and.exec.front;
      article["status"].to!string.should.equal("pending");
    });
  });
});
