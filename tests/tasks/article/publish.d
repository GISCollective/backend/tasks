/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.article.publish;

import tests.fixtures;

import gis_collective.tasks.article.Publish;
import vibeauth.mail.base;
import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;

  describe("The article.publish task", {
    ArticlePublish task;
    string publicNewsletterId;
    Json[] receivedMessages;

    beforeEach({
      setupTestData();
      task = ArticlePublish();
      createTestDefaultSpace();

      crates.newsletter.addItem(`{}`.parseJsonString);

      auto publicNewsletter = crates.newsletter.addItem(`{
        "name": "test",
        "description": "some description",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      publicNewsletterId = publicNewsletter["_id"].to!string;

      crates.article.addItem(`{
        "type": "newsletter-article",
        "relatedId": "000000000000000000000002",
        "status": "pending",
        "content": { "blocks": [{
          "type": "paragraph",
          "data": {
            "text": "If the <a href=\"https://example.com\">information</a> we provide is not useful."
          }
        }]}
      }`.parseJsonString);

      crates.article.addItem(`{
        "type": "newsletter-article",
        "relatedId": "000000000000000000000002",
        "status": "pending",
        "content": { "blocks": [{
          "type": "paragraph",
          "data": {
            "text": "If the <a href=\"https://example.com\">information</a> we provide is not useful."
          }
        }]}
      }`.parseJsonString);

      task.broadcast = new MemoryBroadcast();

      receivedMessages = [];
      void receiver(const Json message) {
        receivedMessages ~= message;
      }
      task.broadcast.register("newsletter.email", &receiver);
    });

    it("does not trigger a job when there are no subscribers", {
      task.crates = crates;

      ArticlePublish.Request message;
      message.id = "000000000000000000000001";

      task.main(message);

      receivedMessages.length.should.equal(0);
    });

    it("triggers a newsletter email job when there is a subscriber", {
      crates.newsletterEmail.addItem(`{
        "_id": "000000000000000000000001",
        "email": "email@giscollective.com",
        "list": [{
          "newsletter": "000000000000000000000002",
          "joinedOn": "2023-01-12T16:20:11Z",
        }]
      }`.parseJsonString);

      crates.newsletterEmail.addItem(`{
        "_id": "000000000000000000000001",
        "email": "email12@giscollective.com",
        "list": [{
          "newsletter": "000000000000000000000001",
          "joinedOn": "2023-01-12T16:20:11Z",
        }]
      }`.parseJsonString);

      task.crates = crates;

      ArticlePublish.Request message;
      message.id = "000000000000000000000001";

      task.main(message);

      receivedMessages.length.should.equal(1);
      receivedMessages[0].should.equal(`{
        "recipients":["email@giscollective.com"],
        "subject":"",
        "isTest":false,
        "content":{
          "blocks":[{"type":"paragraph","data":{"text":"If the <a href=\"https://giscollective.com/link-redirect?id=000000000000000000000001\">information<\/a> we provide is not useful."}}]
        },
        "articleId":"000000000000000000000001",
        "newsletterId":"000000000000000000000002"
      }`.parseJsonString);
    });

    it("does not trigger a newsletter when the message was already sent", {
      crates.newsletterEmail.addItem(`{
        "_id": "000000000000000000000001",
        "email": "email@giscollective.com",
        "list": [{
          "newsletter": "000000000000000000000002",
          "joinedOn": "2023-01-12T16:20:11Z",
        }]
      }`.parseJsonString);

      crates.newsletterEmail.addItem(`{
        "_id": "000000000000000000000001",
        "email": "email12@giscollective.com",
        "list": [{
          "newsletter": "000000000000000000000001",
          "joinedOn": "2023-01-12T16:20:11Z",
        }]
      }`.parseJsonString);

      crates.metric.addItem(`{
        "labels": {
          "relatedId": "000000000000000000000001",
          "email": "email@giscollective.com",
        },
        "name": "000000000000000000000002",
        "reporter": "",
        "time": "2023-01-12T16:20:11Z",
        "type": "newsletterSent",
        "value": 1,
      }`.parseJsonString);

      task.crates = crates;

      ArticlePublish.Request message;
      message.id = "000000000000000000000002";

      task.main(message);

      receivedMessages.length.should.equal(0);
    });

    it("ignores an article in the draft state", {
      crates.newsletterEmail.addItem(`{
        "_id": "000000000000000000000001",
        "email": "email@giscollective.com",
        "list": [{
          "newsletter": "000000000000000000000002",
          "joinedOn": "2023-01-12T16:20:11Z",
        }]
      }`.parseJsonString);

      task.crates = crates;

      ArticlePublish.Request message;
      message.id = "000000000000000000000001";

      auto article = crates.article.getItem("000000000000000000000001").and.exec.front;
      article["status"] = "draft";
      crates.article.updateItem(article);

      task.main(message);

      receivedMessages.length.should.equal(0);

      article = crates.article.getItem("000000000000000000000001").and.exec.front;
      article["status"].to!string.should.equal("draft");
    });

    it("ignores a welcome message article", {
      crates.newsletterEmail.addItem(`{
        "_id": "000000000000000000000001",
        "email": "email@giscollective.com",
        "list": [{
          "newsletter": "000000000000000000000002",
          "joinedOn": "2023-01-12T16:20:11Z",
        }]
      }`.parseJsonString);

      task.crates = crates;

      ArticlePublish.Request message;
      message.id = "000000000000000000000001";

      auto article = crates.article.getItem("000000000000000000000001").and.exec.front;
      article["type"] = "newsletter-welcome-message";
      crates.article.updateItem(article);

      task.main(message);

      receivedMessages.length.should.equal(0);
    });

    it("updates the article state to sent", {
      crates.newsletterEmail.addItem(`{
        "_id": "000000000000000000000001",
        "email": "email@giscollective.com",
        "list": [{
          "newsletter": "000000000000000000000002"
        }]
      }`.parseJsonString);

      task.crates = crates;

      ArticlePublish.Request message;
      message.id = "000000000000000000000001";

      task.main(message);

      auto article = crates.article.getItem("000000000000000000000001").and.exec.front;
      article["status"].to!string.should.equal("sent");
    });

    it("sends the emails in batches when there are more emails than maxRecipients", {
      task.maxRecipients = 2;

      foreach(i; 0..3) {
        crates.newsletterEmail.addItem((`{
          "_id": "000000000000000000000001",
          "email": "email` ~ i.to!string ~ `@giscollective.com",
          "list": [{
            "newsletter": "000000000000000000000002",
            "joinedOn": "2023-01-12T16:20:11Z",
          }]
        }`).parseJsonString);
      }

      task.crates = crates;

      ArticlePublish.Request message;
      message.id = "000000000000000000000001";

      task.main(message);

      receivedMessages.length.should.equal(2);
      receivedMessages[0].should.equal(`{
        "recipients":["email0@giscollective.com", "email1@giscollective.com"],
        "subject":"",
        "isTest":false,
        "content":{
          "blocks":[{"type":"paragraph","data":{"text":"If the <a href=\"https://giscollective.com/link-redirect?id=000000000000000000000002\">information<\/a> we provide is not useful."}}]
        },
        "articleId":"000000000000000000000001",
        "newsletterId":"000000000000000000000002"
      }`.parseJsonString);

      receivedMessages[1].should.equal(`{
        "recipients":["email2@giscollective.com"],
        "subject":"",
        "isTest":false,
        "content":{
          "blocks":[{"type":"paragraph","data":{"text":"If the <a href=\"https://giscollective.com/link-redirect?id=000000000000000000000002\">information<\/a> we provide is not useful."}}]
        },
        "articleId":"000000000000000000000001",
        "newsletterId":"000000000000000000000002"
      }`.parseJsonString);
    });
  });
});
