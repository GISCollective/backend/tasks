/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.baseMap.BaseMapIconSetExtract;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;
import std.path;
import std.file;

import vibe.core.file;
import vibe.stream.memory;

import crate.collection.notifications;

import ogm.models.meta;
import ogm.models.changeset;

import gis_collective.tasks.baseMap.BaseMapIconSetExtract;
import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;

  // describe("The BaseMapIconSetExtract task", {
  //   beforeEach({ setupTestData(); });

  //   describe("Processing list", {
  //     ChangeSet changeSet;

  //     beforeEach({
  //       changeSet = ChangeSet();
  //       changeSet.model = "IconSet";
  //       changeSet.itemId = ObjectId.generate;
  //     });

  //     it("should process add changes inside BaseMap", {
  //       changeSet.model = "BaseMap";
  //       changeSet.type = CrateChangeType.add;

  //       BaseMapIconSetExtract.processList(changeSet, crates)
  //       .should.equal([changeSet.itemId.toJson]);
  //     });

  //     it("should not process changes inside map", {
  //       changeSet.model = "Map";
  //       changeSet.type = CrateChangeType.delete_;

  //       BaseMapIconSetExtract.processList(changeSet, crates).should.equal([]);
  //     });

  //     it("should process update changes inside BaseMap", {
  //       changeSet.model = "BaseMap";
  //       changeSet.type = CrateChangeType.update;

  //       BaseMapIconSetExtract.processList(changeSet, crates)
  //       .should.equal([changeSet.itemId.toJson]);
  //     });

  //     it("should process delete changes inside BaseMap", {
  //       changeSet.model = "BaseMap";
  //       changeSet.type = CrateChangeType.delete_;

  //       BaseMapIconSetExtract.processList(changeSet, crates)
  //       .should.equal([changeSet.itemId.toJson]);
  //     });
  //   });

  //   describe("IconSetExtract", {
  //     describe("extractIcons", {
  //       // it("does not extract the files when there is an empty json object", {
  //       //   auto iconSetExtract = IconSetExtract();
  //       //   scope(exit) iconSetExtract.clean;

  //       //   iconSetExtract.spritePath = buildPath("tests", "fixtures", "sprite.png");
  //       //   iconSetExtract.workingPath = buildPath(tempDir, "extractTest");
  //       //   iconSetExtract.spriteData = Json.emptyObject;

  //       //   iconSetExtract.extractIcons();

  //       //   auto files = buildPath(tempDir, "extractTest").dirEntries("*.png", SpanMode.depth).map!(a => a.name).array;
  //       //   buildPath(tempDir, "extractTest").exists.should.equal(true);

  //       //   files.should.equal([]);
  //       // });

  //       it("extracts the files when there is an json object with the sprite mapping", {
  //         auto iconSetExtract = IconSetExtract();
  //         scope(exit) iconSetExtract.clean;

  //         iconSetExtract.spritePath = buildPath("tests", "fixtures", "sprite.png");
  //         iconSetExtract.workingPath = buildPath(tempDir, "extractTest");
  //         iconSetExtract.spriteData = buildPath("tests", "fixtures", "sprite.json").readText.parseJsonString;

  //         iconSetExtract.extractIcons();

  //         auto files = buildPath(tempDir, "extractTest").dirEntries("*.png", SpanMode.depth).map!(a => a.name).array;

  //         auto expectedFiles = iconSetExtract.spriteData.byKeyValue.map!(a => buildPath(iconSetExtract.workingPath, a.key ~ ".png")).array;

  //         files.should.containOnly(expectedFiles);
  //       });
  //     });

  //     describe("ensureIconSet", {
  //       it("creates a new icon set when there is none for the basemap", {
  //         auto iconSetExtract = IconSetExtract();
  //         iconSetExtract.crates = crates;
  //         iconSetExtract.baseMap = `{ "_id": "1", "name": "test 1", "icon": "icon", "layers": [], "visibility": { "isDefault": false, "isPublic": true, "team": "1" } }`.parseJsonString;

  //         iconSetExtract.ensureIconSet();

  //         auto record = crates.iconSet.get.where("source.type").equal("BaseMap").and.exec.front;
  //         record["styles"] = Json.emptyObject;
  //         record["cover"] = null;
  //         record["source"]["syncAt"] = "0001-01-01T00:00:00+00:00";
  //         record.remove("sprites");

  //         record.should.equal(`{
  //           "_id": "000000000000000000000005",
  //           "name": "test 1 icons",
  //           "cover": null,
  //           "visibility": {
  //             "isDefault": false,
  //             "isPublic": false,
  //             "team": "1"
  //           },
  //           "source": {
  //             "type": "BaseMap",
  //             "remoteId": "",
  //             "modelId": "1",
  //             "syncAt": "0001-01-01T00:00:00+00:00"
  //           },
  //           "ver": 0,
  //           "styles": { }
  //         }`.parseJsonString);
  //       });

  //       it("does not create a new icon set when there is an iconset for the base map", {
  //         crates.iconSet.addItem(`{
  //           "name": "existing icons",
  //           "source": {
  //             "type": "BaseMap",
  //             "remoteId": "",
  //             "modelId": "1"
  //           }
  //         }`.parseJsonString);

  //         auto iconSetExtract = IconSetExtract();
  //         iconSetExtract.crates = crates;
  //         iconSetExtract.baseMap = `{ "_id": "1", "name": "test 1", "icon": "icon", "layers": [], "visibility": { "isDefault": false, "isPublic": true, "team": "1" } }`.parseJsonString;

  //         iconSetExtract.ensureIconSet();

  //         auto records = crates.iconSet.get.where("source.type").equal("BaseMap").and.size;

  //         records.should.equal(1);
  //       });
  //     });

  //     describe("upsertIcon", {
  //       IconSetExtract iconSetExtract;

  //       beforeEach({
  //         iconSetExtract = IconSetExtract();
  //         iconSetExtract.crates = crates;

  //         iconSetExtract.iconSet = crates.iconSet.getItem("000000000000000000000001").and.exec.front;
  //         iconSetExtract.workingPath = "./tests/fixtures";
  //       });

  //       it("creates a new icon when it does not exist", {
  //         auto icon = iconSetExtract.upsertIcon("test", Json.emptyObject);

  //         icon["name"].to!string.should.equal("test");
  //         icon["measurements"].should.equal(`{
  //           "stretchX": [],
  //           "height": 0,
  //           "width": 0,
  //           "content": [],
  //           "stretchY": []
  //         }`.parseJsonString);
  //       });

  //       it("return an existing icon when it is matched by name and icon set", {
  //         auto icon = iconSetExtract.upsertIcon("name1", Json.emptyObject);

  //         icon["name"].to!string.should.equal("name1");

  //         crates.icon.get.where("name").equal("name1").and.size.should.equal(1);
  //       });

  //       it("copies the placeholder sprite data to the content property", {
  //         auto icon = iconSetExtract.upsertIcon("test", `{ "placeholder": [0, 34, 40, 46] }`.parseJsonString);

  //         icon["name"].to!string.should.equal("test");
  //         icon["measurements"].should.equal(`{
  //           "stretchX": [],
  //           "height": 0,
  //           "width": 0,
  //           "content": [0, 34, 40, 46],
  //           "stretchY": []
  //         }`.parseJsonString);
  //       });

  //       it("copies the content sprite data to the content property", {
  //         auto icon = iconSetExtract.upsertIcon("test", `{ "content": [0, 34, 40, 46] }`.parseJsonString);

  //         icon["name"].to!string.should.equal("test");
  //         icon["measurements"].should.equal(`{
  //           "stretchX": [],
  //           "height": 0,
  //           "width": 0,
  //           "content": [0, 34, 40, 46],
  //           "stretchY": []
  //         }`.parseJsonString);
  //       });

  //       it("copies the width and height when they are set", {
  //         auto icon = iconSetExtract.upsertIcon("test", `{ "width": 40, "height": 80 }`.parseJsonString);

  //         icon["name"].to!string.should.equal("test");
  //         icon["measurements"].should.equal(`{
  //           "stretchX": [],
  //           "height": 80,
  //           "width": 40,
  //           "content": [],
  //           "stretchY": []
  //         }`.parseJsonString);
  //       });

  //       it("copies the stretch properties when they are set", {
  //         auto icon = iconSetExtract.upsertIcon("test", `{ "stretchX": [[1,2],[3,4]], "stretchY": [[5,6],[7,8]] }`.parseJsonString);

  //         icon["name"].to!string.should.equal("test");
  //         icon["measurements"].should.equal(`{
  //           "stretchX": [[1,2],[3,4]],
  //           "stretchY": [[5,6],[7,8]],
  //           "height": 0,
  //           "width": 0,
  //           "content": []
  //         }`.parseJsonString);
  //       });
  //     });
  //   });

  //   // describe("The task", {
  //   //   BaseMapIconSetExtract task;
  //   //   ubyte[] svgIconContent;
  //   //   ubyte[] pngIconContent;

  //   //   beforeEach({
  //   //     task = BaseMapIconSetExtract();

  //   //     crateGetters["IconSet"] = &crates.iconSet.getItem;
  //   //     crateGetters["Team"] = &crates.team.getItem;
  //   //     crateGetters["Picture"] = &crates.picture.getItem;

  //   //     IconSettings.files = crates.iconFiles;
  //   //     IconSettings.chunks = crates.iconChunks;
  //   //     PictureFileSettings.files = crates.pictureFiles;
  //   //     PictureFileSettings.chunks = crates.pictureChunks;

  //   //     svgIconContent = cast(ubyte[]) (buildPath("tests", "fixtures", "icon.svg").read);
  //   //     pngIconContent = cast(ubyte[]) (buildPath("tests", "fixtures", "icon.png").read);

  //   //     setMockFile("image/svg+xml", svgIconContent.dup);

  //   //     auto icon1 = crates.icon.getItem("000000000000000000000001").and.exec.front;
  //   //     icon1["image"]["value"] = "000000000000000000000001";
  //   //     icon1["iconSet"] = "000000000000000000000001";
  //   //     crates.icon.updateItem(icon1);

  //   //     foreach(int i; 1..10) {
  //   //       auto otherIcon = crates.icon.getItem("000000000000000000000002").and.exec.front;
  //   //       otherIcon["image"]["value"] = "000000000000000000000002";
  //   //       otherIcon["iconSet"] = "000000000000000000000001";
  //   //       crates.icon.addItem(otherIcon);
  //   //     }
  //   //   });

  //   //   it("does nothing when there is no icon set", {
  //   //     auto request = BaseMapIconSetExtract.Request(ObjectId.fromString("0000000000000000000000a1"));

  //   //     task.crates = crates;
  //   //     task.main(request);

  //   //     crates.picture.get
  //   //       .where("meta.iconSet").equal("0000000000000000000000a1").and
  //   //       .where("meta.type").equal("sprite").and
  //   //       .and.size.should.equal(0);
  //   //   });

  //   //   it("deletes the sprites of a missing iconset", {
  //   //     auto picture = Picture();
  //   //     picture.meta.link = Json.emptyObject;
  //   //     picture.meta.link["model"] = "iconSet";
  //   //     picture.meta.link["id"] = "0000000000000000000000a1";
  //   //     picture.meta.link["type"] = "sprite";

  //   //     auto request = BaseMapIconSetExtract.Request(ObjectId.fromString("0000000000000000000000a1"));

  //   //     task.crates = crates;
  //   //     task.main(request);

  //   //     crates.picture.get
  //   //       .where("meta.link.id").equal("0000000000000000000000a1").and
  //   //       .where("meta.link.type").equal("sprite").and
  //   //       .and.size.should.equal(0);
  //   //   });

  //   //   it("builds the sprite when the set exists", {
  //   //     auto request = BaseMapIconSetExtract.Request(ObjectId.fromString("000000000000000000000001"));

  //   //     task.crates = crates;
  //   //     task.main(request);

  //   //     auto pictures = crates.picture.get
  //   //       .where("meta.link.id").equal("000000000000000000000001").and
  //   //       .where("meta.link.type").equal("sprite").and
  //   //       .and.exec.array;

  //   //     pictures.length.should.equal(2);
  //   //     pictures[0]["meta"]["link"].should.equal(`{
  //   //       "type": "sprite",
  //   //       "size": 1,
  //   //       "id": "000000000000000000000001",
  //   //       "model": "iconSet"
  //   //     }`.parseJsonString);

  //   //     pictures[0]["meta"]["data"].should.equal(`{
  //   //       "size": { "height": 64, "width": 4096 },
  //   //       "index": {
  //   //         "name1": { "height": 22, "width": 22, "pixelRatio": 1, "y": 21, "x": 21 },
  //   //         "name2": { "height": 22, "width": 22, "pixelRatio": 1, "y": 21, "x": 597 },
  //   //         "000000000000000000000001": { "height": 22, "width": 22, "pixelRatio": 1, "y": 21, "x": 21 },
  //   //         "000000000000000000000005": { "height": 22, "width": 22, "pixelRatio": 1, "y": 21, "x": 85 },
  //   //         "000000000000000000000006": { "height": 22, "width": 22, "pixelRatio": 1, "y": 21, "x": 149 },
  //   //         "000000000000000000000007": { "height": 22, "width": 22, "pixelRatio": 1, "y": 21, "x": 213 },
  //   //         "000000000000000000000008": { "height": 22, "width": 22, "pixelRatio": 1, "y": 21, "x": 277 },
  //   //         "000000000000000000000009": { "height": 22, "width": 22, "pixelRatio": 1, "y": 21, "x": 341 },
  //   //         "000000000000000000000010": { "height": 22, "width": 22, "pixelRatio": 1, "y": 21, "x": 405 },
  //   //         "000000000000000000000011": { "height": 22, "width": 22, "pixelRatio": 1, "y": 21, "x": 469 },
  //   //         "000000000000000000000012": { "height": 22, "width": 22, "pixelRatio": 1, "y": 21, "x": 533 },
  //   //         "000000000000000000000013": { "height": 22, "width": 22, "pixelRatio": 1, "y": 21, "x": 597 }
  //   //       }
  //   //     }`.parseJsonString);

  //   //     pictures[1]["meta"]["link"].should.equal(`{
  //   //       "type": "sprite",
  //   //       "size": 2,
  //   //       "id": "000000000000000000000001",
  //   //       "model": "iconSet"
  //   //     }`.parseJsonString);

  //   //     pictures[1]["meta"]["data"].should.equal(`{
  //   //       "size": { "height": 64, "width": 4096 },
  //   //       "index": {
  //   //         "name1": { "height": 43, "width": 43, "pixelRatio": 1, "y": 10, "x": 10 },
  //   //         "name2": { "height": 43, "width": 43, "pixelRatio": 1, "y": 10, "x": 586 },
  //   //         "000000000000000000000001": { "height": 43, "width": 43, "pixelRatio": 1, "y": 10, "x": 10 },
  //   //         "000000000000000000000005": { "height": 43, "width": 43, "pixelRatio": 1, "y": 10, "x": 74 },
  //   //         "000000000000000000000006": { "height": 43, "width": 43, "pixelRatio": 1, "y": 10, "x": 138 },
  //   //         "000000000000000000000007": { "height": 43, "width": 43, "pixelRatio": 1, "y": 10, "x": 202 },
  //   //         "000000000000000000000008": { "height": 43, "width": 43, "pixelRatio": 1, "y": 10, "x": 266 },
  //   //         "000000000000000000000009": { "height": 43, "width": 43, "pixelRatio": 1, "y": 10, "x": 330 },
  //   //         "000000000000000000000010": { "height": 43, "width": 43, "pixelRatio": 1, "y": 10, "x": 394 },
  //   //         "000000000000000000000011": { "height": 43, "width": 43, "pixelRatio": 1, "y": 10, "x": 458 },
  //   //         "000000000000000000000012": { "height": 43, "width": 43, "pixelRatio": 1, "y": 10, "x": 522 },
  //   //         "000000000000000000000013": { "height": 43, "width": 43, "pixelRatio": 1, "y": 10, "x": 586 }
  //   //       }
  //   //     }`.parseJsonString);
  //   //   });

  //   //   it("clears the sprites before they are regenerated", {
  //   //     auto request = BaseMapIconSetExtract.Request(ObjectId.fromString("000000000000000000000001"));

  //   //     task.crates = crates;
  //   //     task.main(request);
  //   //     task.main(request);

  //   //     auto pictures = crates.picture.get
  //   //       .where("meta.link.id").equal("000000000000000000000001").and
  //   //       .where("meta.link.type").equal("sprite").and
  //   //       .and.exec.array;

  //   //     pictures.length.should.equal(2);
  //   //   });
  //   // });
  // });
});
