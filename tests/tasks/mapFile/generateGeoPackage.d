/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.mapFile.generateGeoPackage;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;
import std.conv;
import std.file;

import crate.collection.notifications;

import ogm.models.changeset;
import gis_collective.tasks.mapFile.generateGeoPackage;


import fluent.asserts;
import ogm.test.fixtures;
import gis_collective.hmq.broadcast.memory;

import ogm.batch.log;
import geo.geopackage.file;
import geo.geopackage.layer;

alias suite = Spec!({
  describe("The map file generate geo package task", {
    GenerateGeoPackage generateGeoPackage;

    beforeEach({
      setupTestData();
      crateGetters["Map"] = &crates.map.getItem;
      crateGetters["Team"] = &crates.team.getItem;
      crateGetters["Picture"] = &crates.picture.getItem;
      crateGetters["IconSet"] = &crates.iconSet.getItem;

      generateGeoPackage = GenerateGeoPackage();
      generateGeoPackage.crates = crates;
    });

    it("should process a request for an existing map", {
      auto request = GenerateGeoPackageRequest(map3._id.toString, "some_id", Clock.currTime.toISOExtString);

      generateGeoPackage.main(request);

      auto mapFile = crates.mapFile.get.exec.front;

      mapFile.should.equal((`{
        "_id": "000000000000000000000001",
        "file": "` ~ mapFile["file"].to!string ~ `",
        "map": "000000000000000000000003",
        "options": {
          "analyzedAt": "0001-01-01T00:00:00+00:00",
          "updateBy": "",
          "destinationMap": "",
          "uuid": "some_id",
          "crs": "",
          "fields": [],
          "extraIcons": []
        }
      }`).parseJsonString);
    });

    it("should create an icon point layer", {
      auto fileName = generateGeoPackage.getUniqName();
      scope(exit) fileName.remove;

      generateGeoPackage.geoPackage = new GeoPackage(fileName);
      scope(exit) generateGeoPackage.geoPackage.close;

      generateGeoPackage.createIconLayer("points", "000000000000000000000001");

      generateGeoPackage.geoPackage.geometryLayers.names.should.equal(["name1_points"]);
    });

    it("should add a site to a point layer", {
      auto fileName = generateGeoPackage.getUniqName();
      scope(exit) fileName.remove;

      generateGeoPackage.geoPackage = new GeoPackage(fileName);
      scope(exit) generateGeoPackage.geoPackage.close;

      generateGeoPackage.addFeature(crates.feature.getItem("000000000000000000000001").exec.front, "000000000000000000000001");

      generateGeoPackage.geoPackage.geometryLayers.names.should.equal(["name1_point"]);

      auto items = generateGeoPackage.geoPackage.geometryLayers.get("name1_point").get().array;

      items.length.should.equal(1);
      items[0].should.equal(`{
        "_id": "000000000000000000000001",
        "description": "description of site1",
        "geometry": { "type": "Point", "coordinates": [ 1.5, 1.5 ] },
        "fid": 1,
        "name": "site1",
        "program": "Luni-Vineri:9-18",
        "type of food": "international",
        "phone": "123456",
        "visibility": "1",
        "kids-friendly": "true",
        "description": "description of site1",
        "price": 11.4,
        "max number of people": 23
      }`.parseJsonString);
    });

    it("should add a site with extra fields to a point layer", {
      auto fileName = generateGeoPackage.getUniqName();
      scope(exit) fileName.remove;

      generateGeoPackage.geoPackage = new GeoPackage(fileName);
      scope(exit) generateGeoPackage.geoPackage.close;

      auto feature = crates.feature.getItem("000000000000000000000001").exec.front;
      feature["attributes"]["name1"]["other"] = "should be ignored";

      generateGeoPackage.addFeature(feature, "000000000000000000000001");
      generateGeoPackage.geoPackage.geometryLayers.names.should.equal(["name1_point"]);

      auto items = generateGeoPackage.geoPackage.geometryLayers.get("name1_point").get().array;

      items.length.should.equal(1);
      items[0].should.equal(`{
        "_id": "000000000000000000000001",
        "description": "description of site1",
        "geometry": { "type": "Point", "coordinates": [ 1.5, 1.5 ] },
        "fid": 1,
        "name": "site1",
        "program": "Luni-Vineri:9-18",
        "type of food": "international",
        "phone": "123456",
        "kids-friendly": "true",
        "visibility": "1",
        "description": "description of site1",
        "price": 11.4,
        "max number of people": 23
      }`.parseJsonString);
    });

    it("should add attributes matched using other names property", {
      auto fileName = generateGeoPackage.getUniqName();
      scope(exit) fileName.remove;

      generateGeoPackage.geoPackage = new GeoPackage(fileName);
      scope(exit) generateGeoPackage.geoPackage.close;

      auto feature = crates.feature.getItem("000000000000000000000001").exec.front;
      feature["attributes"]["name1"] = Json.emptyObject;
      feature["attributes"]["other name 1"] = Json.emptyObject;
      feature["attributes"]["other name 1"]["program"] = "other program";

      generateGeoPackage.addFeature(feature, "000000000000000000000001");
      generateGeoPackage.geoPackage.geometryLayers.names.should.equal(["name1_point"]);

      auto items = generateGeoPackage.geoPackage.geometryLayers.get("name1_point").get().array;

      items.length.should.equal(1);
      items[0].should.equal(`{
        "_id": "000000000000000000000001",
        "description": "description of site1",
        "geometry": { "type": "Point", "coordinates": [ 1.5, 1.5 ] },
        "fid": 1,
        "name": "site1",
        "program": "other program",
        "type of food": "",
        "phone": "",
        "kids-friendly": "",
        "visibility": "1",
        "description": "description of site1",
        "price": "",
        "max number of people": ""
      }`.parseJsonString);
    });

    it("should add a multi line to a multi line layer", {
      auto fileName = generateGeoPackage.getUniqName();
      scope(exit) fileName.remove;

      generateGeoPackage.geoPackage = new GeoPackage(fileName);
      scope(exit) generateGeoPackage.geoPackage.close;

      generateGeoPackage.addFeature(`{
        "info":{"changeIndex":4,"createdOn":"2020-08-04T17:29:15Z","lastChangeOn":"2020-08-04T17:32:12Z","originalAuthor":"5c0441d688b62de4032b5297","author":"5c0441d688b62de4032b5297"},
        "contributors":["5c0441d688b62de4032b5297"],
        "maps":["5eee4f362f7a94010095ea45"],
        "name":"Central Park Bike Route",
        "_id":"5f299aebf8916f01003bab54",
        "visibility": 1,
        "icons":["5ca7bfcdecd8490100cab9a6","5ca7bfe8ecd8490100cab9ef"],
        "description":"",
        "attributes":{},
        "position":{"type":"MultiLineString","coordinates":[[[-73.979966,40.768902],[-73.97984,40.76865000000001],[-73.97975,40.76853],[-73.97974000000001,40.76956000000001],[-73.97978999999999,40.76949999999999],[-73.97996999999999,40.76914999999997],[-73.97996000000001,40.76893999999999]]]},
        "pictures":["5f299b35f8916f01003bab5b"]
      }`.parseJsonString, "000000000000000000000001");

      generateGeoPackage.geoPackage.geometryLayers.names.should.equal(["name1_multilinestring"]);

      auto items = generateGeoPackage.geoPackage.geometryLayers.get("name1_multilinestring").get().array;

      items.length.should.equal(1);
      items[0].should.equal(`{
        "_id": "5f299aebf8916f01003bab54",
        "description": "",
        "geometry":{"type":"MultiLineString","coordinates":[[[-73.979966,40.768902],[-73.97984,40.76865000000001],[-73.97975,40.76853],[-73.97974000000001,40.76956000000001],[-73.97978999999999,40.76949999999999],[-73.97996999999999,40.76914999999997],[-73.97996000000001,40.76893999999999]]]},
        "fid": 1,
        "name": "Central Park Bike Route",
        "program": "",
        "type of food": "",
        "phone": "",
        "kids-friendly": "",
        "visibility": "1",
        "description": "",
        "price": "",
        "max number of people": ""
      }`.parseJsonString);
    });
  });
});
