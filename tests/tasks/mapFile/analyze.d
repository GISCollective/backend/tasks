/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.mapFile.analyze;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;
import std.conv;

import crate.collection.notifications;

import ogm.models.changeset;
import gis_collective.tasks.mapFile.analyze;

import fluent.asserts;
import ogm.test.fixtures;
import gis_collective.hmq.broadcast.memory;

import ogm.batch.log;
import geo.xyz;

alias suite = Spec!({
  describe("The map file analyze task", {
    Json receivedValue;
    bool isReceived;
    MapFileAnalyze mapFileAnalyze;

    beforeEach({
      receivedValue = Json();
      isReceived = false;

      void handler(const Json value) @safe {
        receivedValue = value;
        isReceived = true;
      }

      setupTestData();
      crateGetters["Map"] = &crates.map.getItem;
      crateGetters["Team"] = &crates.team.getItem;
      crateGetters["Picture"] = &crates.picture.getItem;
      crateGetters["IconSet"] = &crates.iconSet.getItem;

      crates.mapFile.addItem(`{
        "map": "000000000000000000000001",
        "file": "000000000000000000000001"
      }`.parseJsonString);

      mapFileAnalyze = MapFileAnalyze();
      mapFileAnalyze.log = new MemoryBatchLog;
      mapFileAnalyze.crates = crates;
    });

    describe("Processing list", {
      ChangeSet changeSet;

      beforeEach({
        changeSet = ChangeSet();
        changeSet.itemId = ObjectId.generate;
      });

      it("should process an add change inside map files", {
        changeSet.model = "MapFile";
        changeSet.type = CrateChangeType.add;

        mapFileAnalyze.processList(changeSet).should.equal([changeSet.itemId.toJson]);
      });

      it("should not process an add change inside maps", {
        changeSet.model = "Map";
        changeSet.type = CrateChangeType.add;

        mapFileAnalyze.processList(changeSet).should.equal([]);
      });

      it("should not process a delete change inside map files", {
        changeSet.model = "MapFile";
        changeSet.type = CrateChangeType.delete_;

        mapFileAnalyze.processList(changeSet).should.equal([]);
      });

      it("should not process an update change inside map files", {
        changeSet.model = "MapFile";
        changeSet.type = CrateChangeType.update;

        mapFileAnalyze.processList(changeSet).should.equal([]);
      });
    });
  });
});
