/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.mapFile.import_;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;
import std.conv;

import crate.collection.notifications;

import ogm.models.changeset;
import ogm.batch.log;

import gis_collective.tasks.mapFile.import_;

import fluent.asserts;
import ogm.test.fixtures;
import gis_collective.hmq.broadcast.memory;

import geo.xyz;

alias suite = Spec!({
  describe("The import task", {
    Json receivedValue;
    bool isReceived;
    MapFileImport mapFileImport;
    string now;

    beforeEach({
      receivedValue = Json();
      isReceived = false;

      void handler(const Json value) @safe {
        receivedValue = value;
        isReceived = true;
      }

      auto n = Clock.currTime;
      n.fracSecs = 0.msecs;
      now = n.toISOExtString;

      setupTestData();
      crateGetters["Map"] = &crates.map.getItem;
      crateGetters["Team"] = &crates.team.getItem;
      crateGetters["Picture"] = &crates.picture.getItem;
      crateGetters["IconSet"] = &crates.iconSet.getItem;

      crates.mapFile.addItem(`{
        "map": "000000000000000000000001",
        "file": "000000000000000000000001"
      }`.parseJsonString);

      mapFileImport = MapFileImport();

      mapFileImport.broadcast = new MemoryBroadcast();
      mapFileImport.broadcast.register("mapFile.analyze", &handler);
      mapFileImport.crates = crates;
    });

    describe("using a geoJson file", {
      beforeEach({
        BatchJob job;
        job.name = "mapFile.import 000000000000000000000001";
        job.visibility = map1.visibility;

        crates.batchJob.addItem(job.serializeToJson);
      });

      it("should import a feature from a GeoJson FeatureCollection", {
        string jsonData = `{ "type": "FeatureCollection", "features": [{
            "type": "Feature",
            "properties": {
              "_id": "000000000000000000000011",
              "name": "new site",
              "info": {
                "changeIndex": 0,
                "createdOn": "2015-01-01T00:00:00Z",
                "lastChangeOn": "2015-01-01T00:00:00Z",
                "originalAuthor": "",
                "author": "00000000000000000000005"
              },
              "contributors": [],
              "maps": [ "000000000000000000000001" ],
              "visibility": 1,
              "icons": [ "000000000000000000000001" ],
              "description": "description of site1",
              "attributes": { },
              "position": {
                "type": "Point",
                "coordinates": [ 1.5, 1.5 ]
              },
              "pictures": []
            },
            "geometry": {
              "type": "Point",
              "coordinates": [
                1.5,1.5
              ]
            }
          }
        ]}`;

        setMockFile("application/geo+json", jsonData);

        auto importMessage = ImportMessage(
          "000000000000000000000001",
          "00000000000000000000005",
          "000000000000000000000001");

        mapFileImport.main(importMessage);

        string strJson = `{
          "info": {
            "changeIndex": 0,
            "createdOn": "` ~ now ~ `",
            "lastChangeOn": "` ~ now ~ `",
            "originalAuthor": "00000000000000000000005",
            "author": "00000000000000000000005"
          },
          "contributors": [],
          "maps": [ "000000000000000000000001" ],
          "name": "new site",
          "_id": "000000000000000000000005",
          "visibility": 1,
          "icons": [ "000000000000000000000001" ],
          "description": "description of site1",
          "attributes": { },
          "position": {
            "type": "Point",
            "coordinates": [ 1.5, 1.5 ]
          },
          "pictures": [],
          "sounds": []
        }`;

        crates.feature.getItem("000000000000000000000005").exec.front.should.equal(strJson.parseJsonString);
      });
    });

    describe("using a csv file", {
      string newCsvData;
      string existingCsvData;
      string nameMatchCsvData;
      ImportMessage importMessage;

      beforeEach({
        newCsvData = `_id,maps,name,description,position.lon,position.lat,info.createdOn,info.lastChangeOn,info.changeIndex,info.author,info.originalAuthor,pictures,icons,visibility,contributors,attributes` ~ "\n" ~
          `,map1,new site,description of site5,1.5,1.5,2015-01-01T00:00:00Z,2015-01-01T00:00:00Z,0,,,,name1,1,,` ~ "\n";

        existingCsvData = `_id,maps,name,description,position.lon,position.lat,info.createdOn,info.lastChangeOn,info.changeIndex,info.author,info.originalAuthor,pictures,icons,visibility,contributors,attributes` ~ "\n" ~
          `000000000000000000000001,map1,new name,new description,1.5,1.5,2015-01-01T00:00:00Z,2015-01-01T00:00:00Z,0,,,,name1,1,,` ~ "\n";

        nameMatchCsvData = `_id,maps,name,description,position.lon,position.lat,info.createdOn,info.lastChangeOn,info.changeIndex,info.author,info.originalAuthor,pictures,icons,visibility,contributors,attributes` ~ "\n" ~
          `,map1,site1,new description,1.5,1.5,2015-01-01T00:00:00Z,2015-01-01T00:00:00Z,0,,,,name1,1,,` ~ "\n";

        importMessage = ImportMessage(
          "000000000000000000000001",
          "00000000000000000000005",
          "000000000000000000000001");
      });

      it("should trigger an analyze task if the batchjob does not exist", {
        setMockFile("text/csv", newCsvData);
        mapFileImport.main(importMessage);

        isReceived.should.equal(true);
        receivedValue.should.equal(`{ "id": "000000000000000000000001" }`.parseJsonString);
      });

      describe("when a batchjob with a scheduled run exists", {
        beforeEach({
          BatchJob job;
          job.name = "mapFile.import 000000000000000000000001";
          job.visibility = map1.visibility;

          RunEntry run;
          run.status = BatchJobStatus.scheduled;

          job.runHistory ~= run;

          crates.batchJob.addItem(job.serializeToJson);
        });

        it("should update the existing run entry", {
          setMockFile("text/csv", newCsvData);
          mapFileImport.main(importMessage);

          crates.batchJob.getItem("000000000000000000000001").exec.front["runHistory"].length.should.equal(1);
          auto firstRun = crates.batchJob.getItem("000000000000000000000001").exec.front["runHistory"][0];

          now = firstRun["time"].to!string;
          firstRun.should.equal(parseJsonString(`{
              "time": "` ~ now ~ `",
              "ping": "` ~ now ~ `",
              "ignored": 0,
              "total": 1,
              "errors": 0,
              "sent": 0,
              "duration": 0,
              "processed": 1,
              "runId": "",
              "status": "success"
            }`));
        });
      });

      describe("when a batchjob with a success run exists", {
        beforeEach({
          BatchJob job;
          job.name = "mapFile.import 000000000000000000000001";
          job.visibility = map1.visibility;

          RunEntry run;
          run.status = BatchJobStatus.success;

          job.runHistory ~= run;

          crates.batchJob.addItem(job.serializeToJson);
        });

        it("should update the existing run entry", {
          setMockFile("text/csv", newCsvData);
          mapFileImport.main(importMessage);

          crates.batchJob.getItem("000000000000000000000001").exec.front["runHistory"].length.should.equal(2);
          crates.batchJob.getItem("000000000000000000000001").exec.front["runHistory"][1]
            .should.equal(parseJsonString(`{
              "time": "` ~ now ~ `",
              "ping": "` ~ now ~ `",
              "ignored": 0,
              "total": 1,
              "errors": 0,
              "sent": 0,
              "duration": 0,
              "processed": 1,
              "runId": "000000000000000000000001.1",
              "status": "success"
            }`));
        });
      });

      describe("when a batchjob with a running run exists", {
        beforeEach({
          BatchJob job;
          job.name = "mapFile.import 000000000000000000000001";
          job.visibility = map1.visibility;

          RunEntry run;
          run.status = BatchJobStatus.running;

          job.runHistory ~= run;

          crates.batchJob.addItem(job.serializeToJson);
        });

        it("should throw an exception", {
          setMockFile("text/csv", newCsvData);

          should({
            mapFileImport.main(importMessage);
          }).throwAnyException.withMessage("The job is already running");

          crates.batchJob.getItem("000000000000000000000001").exec.front["runHistory"].length.should.equal(1);
          crates.batchJob.getItem("000000000000000000000001").exec.front["runHistory"][0]
            .should.equal(parseJsonString(`{
              "time": "0001-01-01T00:00:00+00:00",
              "ping": "0001-01-01T00:00:00+00:00",
              "ignored": 0,
              "total": 0,
              "errors": 0,
              "sent": 0,
              "duration": 0,
              "processed": 0,
              "runId": "",
              "status": "running"
            }`));
        });
      });

      describe("when a batchjob with no runs exists", {
        beforeEach({
          BatchJob job;
          job.name = "mapFile.import 000000000000000000000001";
          job.visibility = map1.visibility;

          crates.batchJob.addItem(job.serializeToJson);
        });

        it("should add a job jun to the batch job", {
          setMockFile("text/csv", newCsvData);
          mapFileImport.main(importMessage);

          auto expectedJson = parseJsonString(`{
              "time": "",
              "ping": "",
              "ignored": 0,
              "total": 1,
              "errors": 0,
              "sent": 0,
              "duration": 0,
              "processed": 1,
              "runId": "000000000000000000000001.0",
              "status": "success"
            }`);

          auto runHistory = crates.batchJob.getItem("000000000000000000000001").exec.front["runHistory"][0];

          expectedJson["time"] = runHistory["time"];
          expectedJson["ping"] = runHistory["ping"];

          runHistory.should.equal(expectedJson);
        });

        it("should import a row when the id is not set", {
          setMockFile("text/csv", newCsvData);

          mapFileImport.main(importMessage);

          auto expected = `{
            "_id": "000000000000000000000005",
            "info": {
              "changeIndex": 0,
              "createdOn": "",
              "lastChangeOn": "",
              "originalAuthor": "00000000000000000000005",
              "author": "00000000000000000000005"
            },
            "maps": [ "000000000000000000000001" ],
            "contributors": [],
            "name": "new site",
            "visibility": 1,
            "icons": [ "000000000000000000000001" ],
            "description": "description of site5",
            "attributes": {},
            "pictures": [],
            "sounds": [],
            "position": {
              "type": "Point",
              "coordinates": [ 1.5, 1.5 ]
            }
          }`.parseJsonString;

          auto item = crates.feature.getItem("000000000000000000000005").exec.front;

          expected["info"]["createdOn"] = item["info"]["createdOn"];
          expected["info"]["lastChangeOn"] = item["info"]["lastChangeOn"];

          item.should.equal(expected);
        });

        it("should import a row when the id is set and replace the existing record", {
          setMockFile("text/csv", existingCsvData);

          mapFileImport.main(importMessage);

          string strJson = `{
            "_id": "000000000000000000000001",
            "info": {
              "changeIndex": 0,
              "createdOn": "",
              "lastChangeOn": "",
              "originalAuthor": "000000000000000000000001",
              "author": "00000000000000000000005"
            },
            "maps": [ "000000000000000000000001" ],
            "contributors": [],
            "name": "new name",
            "visibility": 1,
            "icons": [ "000000000000000000000001" ],
            "description": "new description",
            "attributes": {},
            "pictures": [],
            "sounds": [],
            "position": {
              "type": "Point",
              "coordinates": [ 1.5, 1.5 ]
            }
          }`;

          auto expectedFeature = strJson.parseJsonString;
          auto feature = crates.feature.getItem("000000000000000000000001").exec.front;

          expectedFeature["info"]["createdOn"] = feature["info"]["createdOn"];
          expectedFeature["info"]["lastChangeOn"] = feature["info"]["lastChangeOn"];

          feature.should.equal(expectedFeature);
        });

        it("replace the existing record on name match", {
          setMockFile("text/csv", nameMatchCsvData);
          mapFileImport.main(importMessage);

          auto item = crates.feature.getItem("000000000000000000000001").exec.front;

          auto expected = `{
            "_id": "000000000000000000000001",
            "info": {
              "changeIndex": 0,
              "createdOn": "2015-01-01T00:00:00Z",
              "lastChangeOn": "",
              "originalAuthor": "000000000000000000000001",
              "author": "00000000000000000000005"
            },
            "maps": [ "000000000000000000000001" ],
            "contributors": [],
            "name": "site1",
            "visibility": 1,
            "icons": [ "000000000000000000000001" ],
            "description": "new description",
            "attributes": {},
            "pictures": [],
            "sounds": [],
            "position": {
              "type": "Point",
              "coordinates": [ 1.5, 1.5 ]
            }
          }`.parseJsonString;

          expected["info"]["lastChangeOn"] = item["info"]["lastChangeOn"];

          item.should.equal(expected);
        });
      });
    });
  });
});
