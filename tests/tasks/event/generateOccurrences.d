/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.event.generateOccurrences;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;

import crate.collection.notifications;
import gis_collective.tasks.event.generateOccurrences;
import ogm.calendar;

alias suite = Spec!({
  URLRouter router;

  describe("The Event.generateOccurrences task", {
    EventGenerateOccurrences task;

    beforeEach({
      setupTestData();

      SysCalendar.instance = new CalendarMock("2018-01-01T12:30:10Z");

      task = EventGenerateOccurrences(crates);
    });

    it("updates the allOccurrences field", {
      crates.event.addItem(`{
        "name": "test 1",
        "location": { "type": "Text", "value": "" },
        "calendar": "000000000000000000000001",
        "entries": [{
          "begin": "2018-01-01T12:30:10Z",
          "end": "2018-01-01T13:30:10Z",
          "intervalEnd": "2018-02-01T13:30:10Z",
          "repetition": "monthly",
          "timezone": "UTC"
        }],
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      task.main(EventGenerateOccurrences.Message("000000000000000000000001"));

      auto result = crates.event.getItem("000000000000000000000001").exec.front;

      result["allOccurrences"].should.equal(`[{
        "begin": "2018-01-01T12:30:10+00:00",
        "dayOfWeek": 0,
        "end": "2018-01-01T13:30:10+00:00"
      }, {
        "begin": "2018-02-01T12:30:10+00:00",
        "dayOfWeek": 3,
        "end": "2018-02-01T13:30:10+00:00"
      }]`.parseJsonString);
    });

    it("updates the nextOccurrence", {
      crates.event.addItem(`{
        "name": "test 1",
        "location": { "type": "Text", "value": "" },
        "calendar": "000000000000000000000001",
        "entries": [{
          "begin": "2018-01-01T12:30:10Z",
          "end": "2018-01-01T13:30:10Z",
          "intervalEnd": "2018-02-01T13:30:10Z",
          "repetition": "monthly",
          "timezone": "UTC"
        }],
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      task.main(EventGenerateOccurrences.Message("000000000000000000000001"));

      crates.event.getItem("000000000000000000000001").exec.front["nextOccurrence"].should.equal(`{
        "begin": "2018-01-01T12:30:10+00:00",
        "dayOfWeek": 0,
        "end": "2018-01-01T13:30:10+00:00"
      }`.parseJsonString);
    });
  });
});