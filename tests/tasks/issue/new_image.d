/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.issue.new_image;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;

import crate.collection.notifications;

import ogm.models.changeset;
import gis_collective.tasks.issue.new_image;
import gis_collective.tasks.error;

alias suite = Spec!({
  URLRouter router;

  describe("The Issue.newImageStatus task", {
    describe("Processing list", {
      const Json empty = Json.emptyObject;
      ChangeSet changeSet;

      beforeEach({
        changeSet = ChangeSet();
        changeSet.itemId = ObjectId.generate;
        changeSet.model = "Issue";
      });

      it("should ignore changesets with undefined changes", {
        IssueStatus.processList(changeSet).should.equal([]);
      });

      it("should process any resolved change", {
        changeSet.added["status"] = "resolved";
        changeSet.removed["status"] = "opened";

        IssueStatus.processList(changeSet).should.equal([changeSet.itemId.toJson]);
      });

      it("should ignore any processed change", {
        changeSet.added["processed"] = Json(true);
        changeSet.added["status"] = "resolved";

        IssueStatus.processList(changeSet).should.equal([]);
      });

      it("should ignore any processed change when it's a string flag", {
        changeSet.added["processed"] = Json("true");
        changeSet.added["status"] = "resolved";

        IssueStatus.processList(changeSet).should.equal([]);
      });

      it("should ignore map changes", {
        changeSet.model = "Map";
        IssueStatus.processList(changeSet).should.equal([]);
      });
    });

    describe("Procesing an issue", {
      beforeEach({
          setupTestData();

          IssueFileSettings.files = crates.issueFiles;
          IssueFileSettings.chunks = crates.issueChunks;
      });

      it("should ignore opened issues", {
        auto issue = Issue(ObjectId.fromString("000000000000000000000001"), "000000000000000000000001");
        createIssue(issue);

        IssueStatus("000000000000000000000001", crates).start;

        crates.issue.get.exec.front.should.equal(`{
          "_id": "000000000000000000000001",
          "feature": "000000000000000000000001",
          "creationDate": "0001-01-01T00:00:00+00:00",
          "other": "",
          "title": "",
          "attributions": "",
          "assignee": "",
          "resolveDate": "0001-01-01T00:00:00+00:00",
          "processed": false,
          "description": "",
          "type": "none",
          "author": "",
          "status": "open"
        }`.parseJsonString);
      });

      it("should throw a validation exception for rejected issues with no files", {
        auto issue = Issue(ObjectId.fromString("000000000000000000000001"), "000000000000000000000001");
        issue.type = Issue.Type.newImage;
        issue.status = Issue.Status.rejected;
        createIssue(issue);

        IssueStatus("000000000000000000000001", crates).start
          .should.throwException!TaskValidationException
          .withMessage("Tried to process issue `000000000000000000000001` with no file.");

        crates.issue.get.exec.front.should.equal(`{
          "_id": "000000000000000000000001",
          "feature": "000000000000000000000001",
          "creationDate": "0001-01-01T00:00:00+00:00",
          "other": "",
          "title": "",
          "attributions": "",
          "assignee": "",
          "resolveDate": "0001-01-01T00:00:00+00:00",
          "processed": true,
          "description": "",
          "type": "newImage",
          "author": "",
          "status": "rejected"
        }`.parseJsonString);

        crates.feature.getItem("000000000000000000000001").exec.front["pictures"].length.should.equal(0);
      });

      it("should copy the issue file to the site if the issue is resolved and not processed", {
        auto issue = Issue(ObjectId.fromString("000000000000000000000001"), "000000000000000000000001");
        issue.type = Issue.Type.newImage;
        issue.attributions = "some attributions";
        issue.title = "some name";
        issue.file = new IssueFile("000000000000000000000002");
        issue.status = Issue.Status.resolved;

        createIssue(issue);
        auto beforePictureCount = crates.picture.get.size;

        IssueStatus("000000000000000000000001", crates).start;

        auto dbIssue = crates.issue.get.exec.front;
        auto expectedIssue = `{
          "_id": "000000000000000000000001",
          "feature": "000000000000000000000001",
          "creationDate": "0001-01-01T00:00:00+00:00",
          "other": "",
          "title": "some name",
          "attributions": "some attributions",
          "assignee": "",
          "resolveDate": "0001-01-01T00:00:00+00:00",
          "processed": true,
          "description": "",
          "type": "newImage",
          "author": "",
          "status": "resolved"
        }`.parseJsonString;

        expectedIssue["file"] = issue.file.id;
        dbIssue.should.equal(expectedIssue);

        auto feature = crates.feature.getItem("000000000000000000000001").exec.front;
        feature["pictures"].length.should.equal(1);
        crates.picture.get.size.should.equal(beforePictureCount + 1);

        auto picture = crates.picture.getItem(feature["pictures"][0].to!string).and.exec.front;
        picture["name"].to!string.should.equal("some name");
        picture["meta"]["attributions"].to!string.should.equal("some attributions");
      });

      it("should ignore an issue if the site is missing", {
        auto issue = Issue(ObjectId.fromString("000000000000000000000001"), "00000000000000000000000a");
        issue.type = Issue.Type.newImage;
        issue.file = new IssueFile("000000000000000000000002");
        issue.status = Issue.Status.resolved;

        createIssue(issue);
        IssueStatus("000000000000000000000001", crates).start
          .should.throwException!TaskValidationException
          .withMessage("Feature `00000000000000000000000a` referenced by issue `000000000000000000000001` does not exist.");

        auto dbIssue = crates.issue.get.exec.front;
        auto expectedIssue = `{
          "_id": "000000000000000000000001",
          "feature": "00000000000000000000000a",
          "creationDate": "0001-01-01T00:00:00+00:00",
          "other": "",
          "title": "",
          "assignee": "",
          "resolveDate": "0001-01-01T00:00:00+00:00",
          "processed": true,
          "attributions": "",
          "description": "",
          "type": "newImage",
          "author": "",
          "status": "resolved"
        }`.parseJsonString;
        expectedIssue["file"] = issue.file.id;

        dbIssue.should.equal(expectedIssue);
      });
    });
  });
});