/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.searchMeta.searchMetaTask;

import gis_collective.tasks.searchMeta.SearchMetaTask;
import tests.fixtures;

import fluent.asserts;
import gis_collective.hmq.broadcast.memory;
import gis_collective.tasks.newsletter.Subscribe;
import ogm.calendar;
import vibeauth.mail.base;

alias suite = Spec!({
  URLRouter router;

  describe("searchMeta.searchMetaTask", {
    SearchMetaTask task;
    string publicNewsletterId;
    Json[] receivedMessages;

    beforeEach({
      setupTestData();
      task = SearchMetaTask();
      task.crates = crates;
      SysCalendar.instance = new CalendarMock("2022-03-12T12:34:44Z");

      crateGetters["Team"] = &crates.team.getItem;
      crateGetters["Picture"] = &crates.picture.getItem;
      crateGetters["Icon"] = &crates.icon.getItem;
      crateGetters["IconSet"] = &crates.iconSet.getItem;
      crateGetters["Map"] = &crates.map.getItem;
      crateGetters["Feature"] = &crates.feature.getItem;
      crateGetters["Campaign"] = &crates.campaign.getItem;
      crateGetters["Article"] = &crates.article.getItem;
    });

    describe("removeMeta", {
      it("removes a record by id", {
        crates.searchMeta.addItem(`{
          "relatedId": "000000000000000000000001",
          "relatedModel": "modelName",
        }`.parseJsonString);

        crates.searchMeta.addItem(`{
          "relatedId": "000000000000000000000002",
          "relatedModel": "modelName",
        }`.parseJsonString);


        task.removeMeta("modelName", ObjectId.fromString("000000000000000000000001"));

        crates.searchMeta.get.size.should.equal(1);
        crates.searchMeta.get.where("relatedId").equal("000000000000000000000002").and.size.should.equal(1);
      });

      it("does not remove a record if the model does not match", {
        crates.searchMeta.addItem(`{
          "relatedId": "000000000000000000000001",
          "relatedModel": "other modelName",
        }`.parseJsonString);

        crates.searchMeta.addItem(`{
          "relatedId": "000000000000000000000002",
          "relatedModel": "modelName",
        }`.parseJsonString);

        task.removeMeta("modelName", ObjectId.fromString("000000000000000000000001"));

        crates.searchMeta.get.size.should.equal(2);
      });
    });

    describe("getArticleKeywords", {
      it("returns 2 keyrowords for a string with 2 keywords", {
        auto keywords = task.getArticleKeywords(Json("Some Keywords"));

        keywords.should.equal(["some", "keyword"]);
      });

      it("extracts keywords from an article json", {
        auto keywords = task.getArticleKeywords(`{ "blocks": [ {
          "type": "header",
          "data": { "level": 1, "text": "New title" }
        }, {
          "type": "image",
          "data": {
            "withBorder": false,
            "file": {
              "url": "https://new.opengreenmap.org/api-v1/pictures/63cbe94ea4ebd801002db3e8/picture/lg"
            },
            "stretched": false,
            "withBackground": false,
            "caption": ""
          }
        }, {
          "type": "paragraph",
          "data": {
            "text": "New paragraph"
          }
        }, {
          "type": "table",
          "data": {
            "withHeadings": false,
            "content": [ [ "ssdf", "ssd" ], [ "ssdf", "sas" ] ]
          }
        } ] }`.parseJsonString);
        keywords.should.equal(["new", "titl", "paragraph", "ssd", "sas", "ssdf"]);
      });
    });

    describe("updateMeta", {
      describe("using features", {
        it("removes existing search meta for the record", {
          auto record = crates.feature.addItem(`{
            "name": "Gibbs Hill Lighthouse Bermuda",
            "description": {
              "blocks": [
                {
                  "type": "header",
                  "data": {
                    "level": 1,
                    "text": "Gibbs Hill Lighthouse Bermuda"
                  }
                },
                {
                  "type": "paragraph",
                  "data": {
                    "text": "other text"
                  }
                }
              ]
            },
            "icons": ["000000000000000000000001", "000000000000000000000002"],
            "maps": ["000000000000000000000001"],
            "pictures": ["000000000000000000000009"],
            "position": {
              "type": "Polygon",
              "coordinates": [[[0, 0], [0, 4], [4, 4], [4, 0], [0, 0]]]
            },
            "attributes": {
              "name1": {
                "phone": "private phone",
              },
              "Chance Brothers Lighthouse": {
                "climate_change_impact_observations": null,
                "lighthouse_markings": "36 metre white tapered cylindrical cast iron tower with balcony and lantern.",
              }
            }
          }`.parseJsonString);

          task.updateMeta("Feature", record["_id"].to!string);

          auto searchRecord = `{
            "relatedId": "",
            "relatedModel": "Feature",
            "categories": []
          }`.parseJsonString;
          searchRecord["relatedId"] = record["_id"];
          crates.searchMeta.addItem(searchRecord);

          task.updateMeta("Feature", record["_id"].to!string);

          auto searchMetaCount = crates.searchMeta.get
            .where("relatedModel").equal("Feature").and
            .where("relatedId").equal(record["_id"].to!string).and
            .size;

          searchMetaCount.should.equal(1);
        });

        it("creates the search keywords for a feature", {
          auto record = crates.feature.addItem(`{
            "name": "Gibbs Hill Lighthouse Bermuda",
            "description": {
              "blocks": [
                {
                  "type": "header",
                  "data": {
                    "level": 1,
                    "text": "Gibbs Hill Lighthouse Bermuda"
                  }
                },
                {
                  "type": "paragraph",
                  "data": {
                    "text": "other text"
                  }
                }
              ]
            },
            "icons": ["000000000000000000000001", "000000000000000000000002"],
            "maps": ["000000000000000000000001"],
            "pictures": ["000000000000000000000009"],
            "position": {
              "type": "Polygon",
              "coordinates": [[[0, 0], [0, 4], [4, 4], [4, 0], [0, 0]]]
            },
            "attributes": {
              "name1": {
                "phone": "private phone",
              },
              "Chance Brothers Lighthouse": {
                "climate_change_impact_observations": null,
                "lighthouse_markings": "36 metre white tapered cylindrical cast iron tower with balcony and lantern.",
              }
            }
          }`.parseJsonString);

          task.updateMeta("Feature", record["_id"].to!string);

          auto searchMeta = crates.searchMeta.get
            .where("relatedModel").equal("Feature").and
            .where("relatedId").equal(record["_id"].to!string).and
            .exec.front;

          searchMeta.should.equal(`{
            "feature":{"centroid":{"type":"Point","coordinates":[2,2]},"distance":1778034,"area":198111444408,"maps":["000000000000000000000001"],"isGeoCoding":false},
            "visibility":{"isDefault":false,"isPublic":true,"team":"000000000000000000000001"},
            "lastChangeOn":"2022-03-12T12:34:44Z",
            "lastDuplicateCheckOn": "0001-01-01T00:00:00+00:00",
            "categories":[],
            "title":"Gibbs Hill Lighthouse Bermuda",
            "_id":"000000000000000000000001",
            "cover":"000000000000000000000009",
            "relatedId":"000000000000000000000005",
            "icons":["000000000000000000000001","000000000000000000000002"],
            "description":"other text",
            "relatedModel":"Feature",
            "keywords":["gibb","hill","lighthous","bermuda","other","text","privat","phone","metr","white","taper","cylindr","cast","iron","tower","with","balconi","and","lantern","name","map"]
          }`.parseJsonString);
        });

        it("ignores private attributes", {
          auto icon = crates.icon.getItem(`000000000000000000000001`).and.exec.front;
          icon["attributes"] = `[{"name": "phone", "isPrivate": true }]`.parseJsonString;
          crates.icon.updateItem(icon);

          auto record = crates.feature.addItem(`{
            "name": "Gibbs Hill Lighthouse Bermuda",
            "description": {},
            "icons": ["000000000000000000000001"],
            "maps": ["000000000000000000000001"],
            "pictures": [],
            "position": {
              "type": "Polygon",
              "coordinates": [[[0, 0], [0, 4], [4, 4], [4, 0], [0, 0]]]
            },
            "attributes": {
              "name1": {
                "phone": "private phone",
              },
            }
          }`.parseJsonString);

          task.updateMeta("Feature", record["_id"].to!string);

          auto searchMeta = crates.searchMeta.get
            .where("relatedModel").equal("Feature").and
            .where("relatedId").equal(record["_id"].to!string).and
            .exec.front;

          searchMeta["keywords"].should.equal(`[
            "gibb","hill","lighthous","bermuda","name","map"
          ]`.parseJsonString);
        });

        it("sets isPublic=true for a public feature on a public map", {
          task.updateMeta("Feature", "000000000000000000000001");
          auto searchMeta = crates.searchMeta.get
            .where("relatedModel").equal("Feature").and
            .where("relatedId").equal("000000000000000000000001").and
            .exec.front;

            searchMeta["visibility"].should.equal(`{"isDefault":false,"isPublic":true,"team":"000000000000000000000001"}`.parseJsonString);
        });

        it("sets isPublic=false for a pending feature on a public map", {
          auto record = crates.feature.getItem("000000000000000000000001").and.exec.front;
          record["visibility"] = -1;
          crates.feature.updateItem(record);

          task.updateMeta("Feature", "000000000000000000000001");

          auto searchMeta = crates.searchMeta.get
            .where("relatedModel").equal("Feature").and
            .where("relatedId").equal("000000000000000000000001").and
            .exec.front;

            searchMeta["visibility"].should.equal(`{"isDefault":false,"isPublic":false,"team":"000000000000000000000001"}`.parseJsonString);
        });

        it("sets isPublic=false for a private feature on a public map", {
          auto record = crates.feature.getItem("000000000000000000000001").and.exec.front;
          record["visibility"] = 0;
          crates.feature.updateItem(record);

          task.updateMeta("Feature", "000000000000000000000001");

          auto searchMeta = crates.searchMeta.get
            .where("relatedModel").equal("Feature").and
            .where("relatedId").equal("000000000000000000000001").and
            .exec.front;

            searchMeta["visibility"].should.equal(`{"isDefault":false,"isPublic":false,"team":"000000000000000000000001"}`.parseJsonString);
        });

        it("sets isPublic=false for a public feature on a private map", {
          auto record = crates.map.getItem("000000000000000000000001").and.exec.front;
          record["visibility"]["isPublic"] = false;
          crates.map.updateItem(record);

          task.updateMeta("Feature", "000000000000000000000001");

          auto searchMeta = crates.searchMeta.get
            .where("relatedModel").equal("Feature").and
            .where("relatedId").equal("000000000000000000000001").and
            .exec.front;

            searchMeta["visibility"].should.equal(`{"isDefault":false,"isPublic":false,"team":"000000000000000000000001"}`.parseJsonString);
        });

        it("sets isGeoCoding=true for a public feature on a public map", {
          auto map = crates.map.getItem("000000000000000000000001").and.exec.front;
          map["isIndex"] = true;
          crates.map.updateItem(map);

          task.updateMeta("Feature", "000000000000000000000001");

          auto searchMeta = crates.searchMeta.get
            .where("relatedModel").equal("Feature").and
            .where("relatedId").equal("000000000000000000000001").and
            .exec.front;

            searchMeta["feature"].should.equal(`{"centroid":{"type":"Point","coordinates":[1.5,1.5]},"distance":0,"area":0,"maps":["000000000000000000000001"],"isGeoCoding":true}`.parseJsonString);
        });
      });

      describe("using maps", {
        it("creates the search keywords for a public map", {
          task.updateMeta("Map", "000000000000000000000001");

          auto searchMeta = crates.searchMeta.get
            .where("relatedModel").equal("Map").and
            .where("relatedId").equal("000000000000000000000001").and
            .exec.front;

          searchMeta.should.equal(`{
            "feature":{"centroid":{"type":"Point","coordinates":[-75,40]},"distance":6124681,"area":1888944712729,"maps":["000000000000000000000001"],"isGeoCoding":false},
            "visibility":{"isDefault":false,"isPublic":true,"team":"000000000000000000000001"},
            "lastChangeOn":"2015-01-01T00:00:00Z",
            "categories":[],
            "title":"map1",
            "lastDuplicateCheckOn": "0001-01-01T00:00:00+00:00",
            "_id":"000000000000000000000001",
            "cover":"000000000000000000000002",
            "relatedId":"000000000000000000000001",
            "icons":[],
            "description":"",
            "relatedModel":"Map",
            "keywords":["map"]
          }`.parseJsonString);
        });

        it("creates the search keywords for a private map", {
          task.updateMeta("Map", "000000000000000000000002");

          auto searchMeta = crates.searchMeta.get
            .where("relatedModel").equal("Map").and
            .where("relatedId").equal("000000000000000000000002").and
            .exec.front;

          searchMeta.should.equal(`{
            "feature":{"centroid":{"type":"Point","coordinates":[-7.5,4]},"distance":666594,"area":24722430149,"maps":["000000000000000000000002"],"isGeoCoding":false},
            "visibility":{"isDefault":false,"isPublic":false,"team":"000000000000000000000002"},
            "lastChangeOn":"2015-01-01T00:00:00Z",
            "categories":[],
            "lastDuplicateCheckOn": "0001-01-01T00:00:00+00:00",
            "title":"map2",
            "_id":"000000000000000000000001",
            "cover":"000000000000000000000002",
            "relatedId":"000000000000000000000002",
            "icons":[],
            "description":"",
            "relatedModel":"Map",
            "keywords":["map"]
          }`.parseJsonString);
        });
      });

      describe("using teams", {
        it("creates the search keywords for a public team", {
          auto record = crates.team.getItem("000000000000000000000001").and.exec.front;
          record["isPublic"] = true;
          record["about"] = "some stuff about my team";
          record["logo"] = "000000000000000000000003";
          crates.team.updateItem(record);

          task.updateMeta("Team", "000000000000000000000001");

          auto searchMeta = crates.searchMeta.get
            .where("relatedModel").equal("Team").and
            .where("relatedId").equal("000000000000000000000001").and
            .exec.front;

          searchMeta.should.equal(`{
            "visibility":{"isDefault":false,"isPublic":true, "team":"000000000000000000000001"},
            "title":"team1",
            "icons":[],
            "_id":"000000000000000000000001",
            "relatedId":"000000000000000000000001",
            "description":"some stuff about my team",
            "relatedModel":"Team",
            "lastDuplicateCheckOn": "0001-01-01T00:00:00+00:00",
            "lastChangeOn":"2022-03-12T12:34:44Z",
            "categories":[],
            "cover":"000000000000000000000003",
            "feature":{"area":0,"distance":0,"maps":[],"isGeoCoding":false},
            "keywords":["team","some","stuff","about","my"]
          }`.parseJsonString);
        });

        it("creates the search keywords for a private team", {
          auto record = crates.team.getItem("000000000000000000000001").and.exec.front;
          record["isPublic"] = false;
          crates.team.updateItem(record);

          task.updateMeta("Team", "000000000000000000000001");

          auto searchMeta = crates.searchMeta.get
            .where("relatedModel").equal("Team").and
            .where("relatedId").equal("000000000000000000000001").and
            .exec.front;

          searchMeta.should.equal(`{
            "visibility":{"isDefault":false,"isPublic":false, "team":"000000000000000000000001"},
            "title":"team1",
            "icons":[],
            "lastDuplicateCheckOn": "0001-01-01T00:00:00+00:00",
            "_id":"000000000000000000000001",
            "relatedId":"000000000000000000000001",
            "description":"",
            "cover":"000000000000000000000002",
            "categories":[],
            "relatedModel":"Team",
            "lastChangeOn":"2022-03-12T12:34:44Z",
            "feature":{"area":0,"distance":0,"maps":[],"isGeoCoding":false},
            "keywords":["team"]
          }`.parseJsonString);
        });
      });

      describe("using campaigns", {
        it("creates the search keywords for a public campaign", {
          auto record = crates.campaign.getItem("000000000000000000000001").and.exec.front;
          record["visibility"]["isPublic"] = true;
          record["article"] = "some stuff about my campaign";

          crates.campaign.updateItem(record);

          task.updateMeta("Campaign", "000000000000000000000001");

          auto searchMeta = crates.searchMeta.get
            .where("relatedModel").equal("Campaign").and
            .where("relatedId").equal("000000000000000000000001").and
            .exec.front;

          searchMeta.should.equal(`{
            "feature":{"centroid":{"type":"Point","coordinates":[-75,40]},"distance":6124681,"area":1888944712729,"maps":["000000000000000000000001"],"isGeoCoding":false},
            "visibility":{"isDefault":false,"isPublic":true,"team":"000000000000000000000001"},
            "lastChangeOn":"2015-01-01T00:00:00Z",
            "categories":[],
            "title":"Campaign 1",
            "lastDuplicateCheckOn": "0001-01-01T00:00:00+00:00",
            "_id":"000000000000000000000001",
            "cover":"000000000000000000000002",
            "relatedId":"000000000000000000000001",
            "icons":[],
            "description":"some stuff about my campaign",
            "relatedModel":"Campaign",
            "keywords":["campaign","some","stuff","about","my"]
          }`.parseJsonString);
        });

        it("creates the search keywords for a private campaign", {
          auto record = crates.campaign.getItem("000000000000000000000001").and.exec.front;
          record["visibility"]["isPublic"] = false;
          record["article"] = "some stuff about my campaign";

          crates.campaign.updateItem(record);

          task.updateMeta("Campaign", "000000000000000000000001");

          auto searchMeta = crates.searchMeta.get
            .where("relatedModel").equal("Campaign").and
            .where("relatedId").equal("000000000000000000000001").and
            .exec.front;

          searchMeta.should.equal(`{
            "feature":{"centroid":{"type":"Point","coordinates":[-75,40]},"distance":6124681,"area":1888944712729,"maps":["000000000000000000000001"],"isGeoCoding":false},
            "visibility":{"isDefault":false,"isPublic":false,"team":"000000000000000000000001"},
            "lastChangeOn":"2015-01-01T00:00:00Z",
            "categories":[],
            "lastDuplicateCheckOn": "0001-01-01T00:00:00+00:00",
            "title":"Campaign 1",
            "_id":"000000000000000000000001",
            "cover":"000000000000000000000002",
            "relatedId":"000000000000000000000001",
            "icons":[],
            "description":"some stuff about my campaign",
            "relatedModel":"Campaign",
            "keywords":["campaign","some","stuff","about","my"]
          }`.parseJsonString);
        });

        it("adds the map centroid when there is one attached", {
          auto record = crates.campaign.getItem("000000000000000000000001").and.exec.front;
          record["visibility"]["isPublic"] = false;
          record["article"] = "some stuff about my campaign";
          record["map"]["isEnabled"] = true;
          record["map"]["map"] = "000000000000000000000001";

          crates.campaign.updateItem(record);

          task.updateMeta("Campaign", "000000000000000000000001");

          auto searchMeta = crates.searchMeta.get
            .where("relatedModel").equal("Campaign").and
            .where("relatedId").equal("000000000000000000000001").and
            .exec.front;

          searchMeta.should.equal(`{
            "feature":{"centroid":{"type":"Point","coordinates":[-75,40]},"distance":6124681,"area":1888944712729,"maps":["000000000000000000000001"],"isGeoCoding":false},
            "visibility":{"isDefault":false,"isPublic":false,"team":"000000000000000000000001"},
            "lastChangeOn":"2015-01-01T00:00:00Z",
            "categories":[],
            "lastDuplicateCheckOn": "0001-01-01T00:00:00+00:00",
            "title":"Campaign 1",
            "_id":"000000000000000000000001",
            "cover":"000000000000000000000002",
            "relatedId":"000000000000000000000001",
            "icons":[],
            "description":"some stuff about my campaign",
            "relatedModel":"Campaign",
            "keywords":["campaign","some","stuff","about","my"]
          }`.parseJsonString);
        });
      });

      describe("using icon sets", {
        it("creates the search keywords for a public icon set", {
          auto record = crates.iconSet.getItem("000000000000000000000001").and.exec.front;
          record["visibility"]["isPublic"] = true;
          record["description"] = "some stuff about my set";

          crates.iconSet.updateItem(record);

          task.updateMeta("IconSet", "000000000000000000000001");

          auto searchMeta = crates.searchMeta.get
            .where("relatedModel").equal("IconSet").and
            .where("relatedId").equal("000000000000000000000001").and
            .exec.front;

          searchMeta.should.equal(`{
            "feature":{"distance":0,"area":0,"maps":[],"isGeoCoding":false},
            "visibility":{"isDefault":true,"isPublic":true,"team":"000000000000000000000001"},
            "lastChangeOn":"2022-03-12T12:34:44Z",
            "categories":[],
            "title":"set1",
            "lastDuplicateCheckOn": "0001-01-01T00:00:00+00:00",
            "_id":"000000000000000000000001",
            "relatedId":"000000000000000000000001",
            "icons":[],
            "description":"some stuff about my set",
            "relatedModel":"IconSet",
            "keywords":["set","some","stuff","about","my"]
          }`.parseJsonString);
        });
      });

      describe("using icons", {
        it("creates the search keywords for a public icon", {
          auto setRecord = crates.iconSet.getItem("000000000000000000000001").and.exec.front;
          setRecord["visibility"]["isPublic"] = true;
          crates.iconSet.updateItem(setRecord);

          auto record = crates.icon.getItem("000000000000000000000001").and.exec.front;
          record["description"] = "some stuff about my icon";
          crates.icon.updateItem(record);

          task.updateMeta("Icon", "000000000000000000000001");

          auto searchMeta = crates.searchMeta.get
            .where("relatedModel").equal("Icon").and
            .where("relatedId").equal("000000000000000000000001").and
            .exec.front;

          searchMeta.should.equal(`{
            "feature":{"distance":0,"area":0,"maps":[],"isGeoCoding":false},
            "visibility":{"isDefault":true,"isPublic":true,"team":"000000000000000000000001"},
            "lastChangeOn":"2022-03-12T12:34:44Z",
            "categories":[],
            "lastDuplicateCheckOn": "0001-01-01T00:00:00+00:00",
            "title":"name1",
            "_id":"000000000000000000000001",
            "relatedId":"000000000000000000000001",
            "icons":[],
            "description":"some stuff about my icon",
            "relatedModel":"Icon",
            "keywords":["name","some","stuff","about","my","icon"]
          }`.parseJsonString);
        });

        it("creates the search keywords for a private icon", {
          auto setRecord = crates.iconSet.getItem("000000000000000000000001").and.exec.front;
          setRecord["visibility"]["isPublic"] = false;
          crates.iconSet.updateItem(setRecord);

          auto record = crates.icon.getItem("000000000000000000000001").and.exec.front;
          record["description"] = "some stuff about my icon";
          crates.icon.updateItem(record);

          task.updateMeta("Icon", "000000000000000000000001");

          auto searchMeta = crates.searchMeta.get
            .where("relatedModel").equal("Icon").and
            .where("relatedId").equal("000000000000000000000001").and
            .exec.front;

          searchMeta.should.equal(`{
            "feature":{"distance":0,"area":0,"maps":[],"isGeoCoding":false},
            "visibility":{"isDefault":true,"isPublic":true,"team":"000000000000000000000001"},
            "lastChangeOn":"2022-03-12T12:34:44Z",
            "categories":[],
            "lastDuplicateCheckOn": "0001-01-01T00:00:00+00:00",
            "title":"name1",
            "_id":"000000000000000000000001",
            "relatedId":"000000000000000000000001",
            "icons":[],
            "description":"some stuff about my icon",
            "relatedModel":"Icon",
            "keywords":["name","some","stuff","about","my","icon"]
          }`.parseJsonString);
        });
      });

      describe("using articles", {
        Json article;

        beforeEach({
          article = crates.article.addItem(`{
            "visibility": {
              "isDefault": false,
              "isPublic": true,
              "team": "000000000000000000000001"
            },
            "title": "WELCOME TO OGM2",
            "content": {
              "time": 1660614435846,
              "blocks": [
                {
                  "type": "paragraph",
                  "data": {
                    "text": "OGM2 is <a href=\"http://GreenMap.org\">Green Map System</a>'s second Open Green Map platform. Designed for community mapping, this platform \nis ready for all kinds of mapmakers - from students to professionals - who \nwant to draw attention to sustainability in their \nown city or town. "
                  }
                }
              ]
            }
          }`.parseJsonString);
        });

        it("creates the search keywords for a public article", {
          article["visibility"]["isPublic"] = true;
          article["categories"] = `["a", "b"]`.parseJsonString;
          crates.article.updateItem(article);

          task.updateMeta("Article", "000000000000000000000001");

          auto searchMeta = crates.searchMeta.get
            .where("relatedModel").equal("Article").and
            .where("relatedId").equal("000000000000000000000001").and
            .exec.front;

          searchMeta.should.equal(`{
            "feature":{"distance":0,"area":0,"maps":[],"isGeoCoding":false},
            "visibility":{"isDefault":false,"isPublic":true,"team":"000000000000000000000001"},
            "lastChangeOn":"2022-03-12T12:34:44Z",
            "categories":["a", "b"],
            "title":"WELCOME TO OGM2",
            "lastDuplicateCheckOn": "0001-01-01T00:00:00+00:00",
            "_id":"000000000000000000000001",
            "relatedId":"000000000000000000000001",
            "icons":[],
            "description":"OGM2 is <a href=\"http://GreenMap.org\">Green Map System<\/a>'s second Open Green Map platform. Designed for community mapping, this platform \nis ready for all kinds of mapmakers - from students to professionals - who \nwant to draw attention to sustainability in their \nown city or town. ",
            "relatedModel":"Article",
            "keywords":["ogm","is","a","href","http","greenmap","org","green","map","system","s","second","open","platform","design","for","communiti","this","readi","all","kind","of","mapmak","from","student","to","profession","who","want","draw","attent","sustain","in","their","own","citi","or","town"]
          }`.parseJsonString);
        });
      });
    });
  });

  describe("UpdateSearchMetaTask", {
    describe("processList", {
      ChangeSet changeSet;

      beforeEach({
        changeSet = ChangeSet();
        changeSet.itemId = ObjectId.generate;
      });

      it("returns the id when there is an add Team change", {
        UpdateSearchMetaTask task;

        changeSet.model = "Team";
        changeSet.type = CrateChangeType.add;

        auto result = task.processList(changeSet);

        result.map!(a => a.to!string).array.should.equal([ changeSet.itemId.to!string ]);
      });

      it("returns nothing when there is a change in an unsupoprted model", {
        UpdateSearchMetaTask task;

        changeSet.model = "Other";
        changeSet.type = CrateChangeType.add;

        auto result = task.processList(changeSet);

        result.should.equal([ ]);
      });
    });
  });
});
