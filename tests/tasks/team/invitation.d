/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.team.invitation;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;

import crate.collection.notifications;

import ogm.models.meta;
import ogm.models.changeset;

import gis_collective.tasks.team.invitation;
import gis_collective.hmq.broadcast.memory;


alias suite = Spec!({
  URLRouter router;
  Json message;

  describe("The Team.invitation task", {
    describe("The task", {
      TeamInvitation task;

      beforeEach({
        setupTestData();

        task = TeamInvitation();
      });

      it("should send a notification", {
        auto request = TeamInvitation.Request(
          "test", ObjectId("000000000000000000000001"), Invitation("test@test.com", "members")
        );

        task.crates = crates;
        task.broadcast = new MemoryBroadcast;

        message = Json();
        void mockHandler(const Json msg) {
          message = msg.clone;
        }

        task.broadcast.register("notifications.message", &mockHandler);

        task.main(request);

        message.should.equal(`{"id":"000000000000000000000001"}`.parseJsonString);
        crates.message.getItem("000000000000000000000001").exec.front.should.equal(`{
          "html":"<h1>Welcome aboard!<\/h1><p>You're invited to join our <b>members<\/b> of the <b>team1<\/b> team.<\/p>",
          "isSent":false,
          "text":"Welcome aboard!\n\nYou're invited to join our members of the team1 team.",
          "actions":{
            "Register now to get access":"/login/register"
          },
          "to":{"type":"email","value":"test@test.com"},
          "uniqueKey":"team.invitation.test@test.com.000000000000000000000001",
          "_id":"000000000000000000000001",
          "type":"message",
          "subject":"Invitation to join team1",
          "sentOn":"0001-01-01T00:00:00+00:00",
          "useGenericTemplate":true
        }`.parseJsonString);
      });
    });
  });
});