/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.team.delete_;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;

import crate.collection.notifications;

import ogm.models.meta;
import ogm.models.changeset;

import gis_collective.tasks.team.delete_;
import gis_collective.hmq.broadcast.memory;


alias suite = Spec!({
  URLRouter router;

  describe("The Team.delete task", {
    describe("Processing list", {
      ChangeSet changeSet;

      beforeEach({
        changeSet = ChangeSet();
        changeSet.model = "Team";
        changeSet.itemId = ObjectId.generate;
      });

      it("should not process add changes inside teams", {
        changeSet.model = "Team";
        changeSet.type = CrateChangeType.add;

        TeamDelete.processList(changeSet).should.equal([ ]);
      });

      it("should not process changes inside features", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.delete_;

        TeamDelete.processList(changeSet).should.equal([ ]);
      });

      it("should not process update changes inside teams", {
        changeSet.model = "Team";
        changeSet.type = CrateChangeType.update;

        TeamDelete.processList(changeSet).should.equal([ ]);
      });

      it("should process delete changes inside teams", {
        changeSet.model = "Team";
        changeSet.type = CrateChangeType.delete_;

        TeamDelete.processList(changeSet).should.equal([ changeSet.itemId.toJson ]);
      });
    });

    describe("The task", {
      TeamDelete task;

      beforeEach({
        setupTestData();

        task = TeamDelete();

        Meta meta;
        meta.model = "Team";
        meta.itemId = "000000000000000000000001";

        crates.meta.addItem(meta.serializeToJson);
      });

      it("should remove all maps related to that team", {
        auto request = TeamDelete.Request(ObjectId.fromString("000000000000000000000001"));

        task.crates = crates;
        task.broadcast = new MemoryBroadcast;
        task.main(request);

        crates.map.get.where("_id").equal("000000000000000000000001").and.size.should.equal(0);
        crates.map.get.where("_id").equal("000000000000000000000002").and.size.should.equal(1);
        crates.map.get.where("_id").equal("000000000000000000000003").and.size.should.equal(1);
        crates.map.get.where("_id").equal("000000000000000000000004").and.size.should.equal(1);
      });

      it("should remove all map meta", {
        auto request = TeamDelete.Request(ObjectId.fromString("000000000000000000000001"));

        task.crates = crates;
        task.broadcast = new MemoryBroadcast;
        task.main(request);

        crates.meta.get
          .where("model").equal("Map").and
          .where("itemId").equal("000000000000000000000001").and
          .size.should.equal(0);
      });

      it("should trigger a remove map job", {
        Json message;

        void mockHandler(const Json msg) {
          message = msg.clone;
        }

        auto request = TeamDelete.Request(ObjectId.fromString("000000000000000000000001"));

        task.broadcast = new MemoryBroadcast;
        task.broadcast.register("map.delete", &mockHandler);

        task.crates = crates;
        task.main(request);

        message.should.equal(`{
          "id": "000000000000000000000001"
        }`.parseJsonString);
      });
    });
  });
});