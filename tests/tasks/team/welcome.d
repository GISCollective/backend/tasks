/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.team.welcome;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;

import crate.collection.notifications;

import ogm.models.meta;
import ogm.models.changeset;

import gis_collective.tasks.team.welcome;
import gis_collective.hmq.broadcast.memory;


alias suite = Spec!({
  URLRouter router;
  Json message;

  describe("The Team.welcome task", {
    describe("The task", {
      TeamWelcome task;

      beforeEach({
        setupTestData();

        task = TeamWelcome();
      });

      it("should send a notification", {
        auto request = TeamWelcome.Request(
          "test", "member", ObjectId("000000000000000000000001"), ObjectId("000000000000000000000001")
        );

        task.crates = crates;
        task.broadcast = new MemoryBroadcast;

        message = Json();
        void mockHandler(const Json msg) {
          message = msg.clone;
        }

        task.broadcast.register("notifications.message", &mockHandler);

        task.main(request);

        message.should.equal(`{"id":"000000000000000000000001"}`.parseJsonString);

        crates.message.getItem("000000000000000000000001").exec.front.should.equal(`{
          "html":"<h1>Welcome aboard!<\/h1><p>You were added to the <b>member<\/b> list of the <b>team1<\/b> team.<\/p>",
          "isSent":false,
          "actions":{},
          "text":"Welcome aboard!\n\nYou were added to the member list of the team1 team.",
          "to":{"type":"user","value":"000000000000000000000001"},
          "uniqueKey":"team.welcome.000000000000000000000001.000000000000000000000001",
          "_id":"000000000000000000000001",
          "type":"message",
          "subject":"Welcome to team1",
          "sentOn":"0001-01-01T00:00:00+00:00",
          "useGenericTemplate":true
        }`.parseJsonString);
      });
    });
  });
});