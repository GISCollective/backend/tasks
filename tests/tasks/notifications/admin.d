/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.notifications.admin;

import tests.fixtures;

import gis_collective.tasks.notifications.admin;
import vibeauth.mail.base;
import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;

  describe("The notifications.admin task", {
    NotificationsAdmin task;
    Json[] receivedMessages;

    beforeEach({
      setupTestData();
      task = NotificationsAdmin();
      createTestDefaultSpace();

      task.broadcast = new MemoryBroadcast();

      receivedMessages = [];
      void receiver(const Json message) {
        receivedMessages ~= message;
      }
      task.broadcast.register("notifications.message", &receiver);
    });

    it("sends a message when a map is created", {
      task.crates = crates;

      NotificationsAdmin.Request message;
      message.id = "000000000000000000000001";
      message.teamId = "000000000000000000000001";
      message.author = "000000000000000000000001";
      message.name = "test map";
      message.model = "Map";
      message.type = "add";

      task.main(message);

      auto result = crates.message.getItem(receivedMessages[0]["id"].to!string).and.exec.front;
      result.should.equal(`{
        "html":"Someone added a new map: test map to team1.",
        "isSent":false,
        "actions":{
          "View profile":"/browse/profiles/000000000000000000000001",
          "View team dashboard":"/manage/dashboards/000000000000000000000001"
        },
        "text":"Someone added a new map: test map to team1.",
        "to":{"type":"","value":""},
        "uniqueKey":"notification-admin-Map-add-000000000000000000000001",
        "_id":"000000000000000000000001",
        "type":"admin",
        "subject":"There is a new map!",
        "sentOn":"0001-01-01T00:00:00+00:00",
        "useGenericTemplate":true
      }`.parseJsonString);
    });

    it("sends a message when a map is deleted", {
      task.crates = crates;

      NotificationsAdmin.Request message;
      message.id = "000000000000000000000001";
      message.teamId = "000000000000000000000001";
      message.author = "000000000000000000000001";
      message.name = "test map";
      message.model = "Map";
      message.type = "delete";

      task.main(message);

      auto result = crates.message.getItem(receivedMessages[0]["id"].to!string).and.exec.front;
      result.should.equal(`{
        "html":"Someone deleted a map: test map from team1.",
        "isSent":false,
        "actions":{
          "View profile":"/browse/profiles/000000000000000000000001",
          "View team dashboard":"/manage/dashboards/000000000000000000000001"
        },
        "text":"Someone deleted a map: test map from team1.",
        "to":{"type":"","value":""},
        "uniqueKey":"notification-admin-Map-delete-000000000000000000000001",
        "_id":"000000000000000000000001",
        "type":"admin",
        "subject":"A map was deleted!",
        "sentOn":"0001-01-01T00:00:00+00:00",
        "useGenericTemplate":true
      }`.parseJsonString);
    });

    it("sends a message when a survey is created", {
      task.crates = crates;

      NotificationsAdmin.Request message;
      message.id = "000000000000000000000001";
      message.teamId = "000000000000000000000001";
      message.author = "000000000000000000000001";
      message.name = "test survey";
      message.model = "Campaign";
      message.type = "add";

      task.main(message);

      auto result = crates.message.getItem(receivedMessages[0]["id"].to!string).and.exec.front;
      result.should.equal(`{
        "html":"Someone added a new survey: test survey to team1.",
        "isSent":false,
        "actions":{
          "View profile":"/browse/profiles/000000000000000000000001",
          "View team dashboard":"/manage/dashboards/000000000000000000000001"
        },
        "text":"Someone added a new survey: test survey to team1.",
        "to":{"type":"","value":""},
        "uniqueKey":"notification-admin-Campaign-add-000000000000000000000001",
        "_id":"000000000000000000000001",
        "type":"admin",
        "subject":"There is a new survey!",
        "sentOn":"0001-01-01T00:00:00+00:00",
        "useGenericTemplate":true
      }`.parseJsonString);
    });

    it("sends a message when an icon set is created", {
      task.crates = crates;

      NotificationsAdmin.Request message;
      message.id = "000000000000000000000001";
      message.teamId = "000000000000000000000001";
      message.author = "000000000000000000000001";
      message.name = "test icon set";
      message.model = "IconSet";
      message.type = "add";

      task.main(message);

      auto result = crates.message.getItem(receivedMessages[0]["id"].to!string).and.exec.front;
      result.should.equal(`{
        "html":"Someone added a new icon set: test icon set to team1.",
        "isSent":false,
        "actions":{
          "View profile":"/browse/profiles/000000000000000000000001",
          "View team dashboard":"/manage/dashboards/000000000000000000000001"
        },
        "text":"Someone added a new icon set: test icon set to team1.",
        "to":{"type":"","value":""},
        "uniqueKey":"notification-admin-IconSet-add-000000000000000000000001",
        "_id":"000000000000000000000001",
        "type":"admin",
        "subject":"There is a new icon set!",
        "sentOn":"0001-01-01T00:00:00+00:00",
        "useGenericTemplate":true
      }`.parseJsonString);
    });

    it("sends a message when a Calendar is created", {
      task.crates = crates;

      NotificationsAdmin.Request message;
      message.id = "000000000000000000000001";
      message.teamId = "000000000000000000000001";
      message.author = "000000000000000000000001";
      message.name = "test calendar";
      message.model = "Calendar";
      message.type = "add";

      task.main(message);

      auto result = crates.message.getItem(receivedMessages[0]["id"].to!string).and.exec.front;
      result.should.equal(`{
        "html":"Someone added a new calendar: test calendar to team1.",
        "isSent":false,
        "actions":{
          "View profile":"/browse/profiles/000000000000000000000001",
          "View team dashboard":"/manage/dashboards/000000000000000000000001"
        },
        "text":"Someone added a new calendar: test calendar to team1.",
        "to":{"type":"","value":""},
        "uniqueKey":"notification-admin-Calendar-add-000000000000000000000001",
        "_id":"000000000000000000000001",
        "type":"admin",
        "subject":"There is a new calendar!",
        "sentOn":"0001-01-01T00:00:00+00:00",
        "useGenericTemplate":true
      }`.parseJsonString);
    });

    it("sends a message when a Newsletter is created", {
      task.crates = crates;

      NotificationsAdmin.Request message;
      message.id = "000000000000000000000001";
      message.teamId = "000000000000000000000001";
      message.author = "000000000000000000000001";
      message.name = "test newsletter";
      message.model = "Newsletter";
      message.type = "add";

      task.main(message);

      auto result = crates.message.getItem(receivedMessages[0]["id"].to!string).and.exec.front;
      result.should.equal(`{
        "html":"Someone added a new newsletter: test newsletter to team1.",
        "isSent":false,
        "actions":{
          "View profile":"/browse/profiles/000000000000000000000001",
          "View team dashboard":"/manage/dashboards/000000000000000000000001"
        },
        "text":"Someone added a new newsletter: test newsletter to team1.",
        "to":{"type":"","value":""},
        "uniqueKey":"notification-admin-Newsletter-add-000000000000000000000001",
        "_id":"000000000000000000000001",
        "type":"admin",
        "subject":"There is a new newsletter!",
        "sentOn":"0001-01-01T00:00:00+00:00",
        "useGenericTemplate":true
      }`.parseJsonString);
    });

    it("sends a message when a Space is created", {
      task.crates = crates;

      NotificationsAdmin.Request message;
      message.id = "000000000000000000000001";
      message.teamId = "000000000000000000000001";
      message.author = "000000000000000000000001";
      message.name = "test space";
      message.model = "Space";
      message.type = "add";

      task.main(message);

      auto result = crates.message.getItem(receivedMessages[0]["id"].to!string).and.exec.front;
      result.should.equal(`{
        "html":"Someone added a new space: test space to team1.",
        "isSent":false,
        "actions":{
          "View profile":"/browse/profiles/000000000000000000000001",
          "View team dashboard":"/manage/dashboards/000000000000000000000001"
        },
        "text":"Someone added a new space: test space to team1.",
        "to":{"type":"","value":""},
        "uniqueKey":"notification-admin-Space-add-000000000000000000000001",
        "_id":"000000000000000000000001",
        "type":"admin",
        "subject":"There is a new space!",
        "sentOn":"0001-01-01T00:00:00+00:00",
        "useGenericTemplate":true
      }`.parseJsonString);
    });


    it("sends a message when a Team is created", {
      task.crates = crates;

      NotificationsAdmin.Request message;
      message.id = "000000000000000000000001";
      message.teamId = "";
      message.author = "000000000000000000000001";
      message.name = "test team";
      message.model = "Team";
      message.type = "add";

      task.main(message);

      auto result = crates.message.getItem(receivedMessages[0]["id"].to!string).and.exec.front;
      result.should.equal(`{
        "html":"Someone added a new team: test team.",
        "isSent":false,
        "actions":{
          "View profile":"/browse/profiles/000000000000000000000001",
          "View team dashboard":"/manage/dashboards/000000000000000000000001"
        },
        "text":"Someone added a new team: test team.",
        "to":{"type":"","value":""},
        "uniqueKey":"notification-admin-Team-add-000000000000000000000001",
        "_id":"000000000000000000000001",
        "type":"admin",
        "subject":"There is a new team!",
        "sentOn":"0001-01-01T00:00:00+00:00",
        "useGenericTemplate":true
      }`.parseJsonString);
    });
  });
});
