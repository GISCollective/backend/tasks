/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.newsletter.subscribe;

import tests.fixtures;

import gis_collective.tasks.newsletter.Subscribe;
import vibeauth.mail.base;
import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;

  describe("The newsletter.subscribe task", {
    NewsletterSubscribe task;
    string publicNewsletterId;
    Json[] receivedMessages;

    beforeEach({
      setupTestData();
      task = NewsletterSubscribe();
      createTestDefaultSpace();

      auto publicNewsletter = crates.newsletter.addItem(`{
        "name": "test",
        "isTest":false,
        "description": "some description",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      publicNewsletterId = publicNewsletter["_id"].to!string;

      crates.article.addItem(`{
        "type": "newsletter-welcome-message",
        "relatedId": "000000000000000000000001",
        "content": { "blocks": [{
          "type": "paragraph",
          "data": {
            "text": "If the <a href=\"https://example.com\">information</a> we provide is not useful."
          }
        }]}
      }`.parseJsonString);

      task.broadcast = new MemoryBroadcast();

      receivedMessages = [];
      void receiver(const Json message) {
        receivedMessages ~= message;
      }
      task.broadcast.register("newsletter.email", &receiver);
    });

    it("triggers an newsletter email job when there is a subscriber", {
      task.crates = crates;

      NewsletterSubscribe.Request message;
      message.id = publicNewsletterId;
      message.email = "test@giscollective.com";

      task.main(message);

      receivedMessages[0].should.equal(`{
        "recipients":["test@giscollective.com"],
        "subject":"",
        "isTest":false,
        "content":{
          "blocks":[
            {"type":"paragraph","data":{"text":"If the <a href=\"https://giscollective.com/link-redirect?id=000000000000000000000001\">information<\/a> we provide is not useful."}}
          ]
        },
        "articleId":"000000000000000000000001",
        "newsletterId":"000000000000000000000001"
      }`.parseJsonString);
    });
  });
});
