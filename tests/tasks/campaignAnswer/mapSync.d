/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.campaignAnswer.mapSync;

import ogm.calendar;
import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;

import crate.collection.notifications;

import ogm.models.meta;
import ogm.models.changeset;

import gis_collective.tasks.campaignAnswer.mapSync;
import gis_collective.hmq.broadcast.memory;


alias suite = Spec!({
  URLRouter router;

  describe("The CampaignAnswer.MapSync task", {
    before({
      SysCalendar.instance = new CalendarMock("2022-03-12T12:34:44Z");
      CampaignAnswerMapSync.serviceUrl = "https://giscollective.com";
    });

    describe("Processing list", {
      ChangeSet changeSet;

      beforeEach({
        changeSet = ChangeSet();
        changeSet.model = "CampaignAnswer";
        changeSet.itemId = ObjectId.generate;
      });

      it("should process add changes inside campaign answers", {
        changeSet.model = "CampaignAnswer";
        changeSet.type = CrateChangeType.add;

        CampaignAnswerMapSync.processList(changeSet).should.equal([ changeSet.itemId.toJson ]);
      });

      it("should not process changes inside maps", {
        changeSet.model = "Map";
        changeSet.type = CrateChangeType.delete_;

        CampaignAnswerMapSync.processList(changeSet).should.equal([ ]);
      });

      it("should not process update changes inside campaign answers", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.update;

        CampaignAnswerMapSync.processList(changeSet).should.equal([ ]);
      });

      it("should not process delete changes inside features", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.delete_;

        CampaignAnswerMapSync.processList(changeSet).should.equal([ ]);
      });
    });

    describe("The task", {
      CampaignAnswerMapSync task;
      Json[] receivedMessages;
      Json[] receivedChanges;
      Json answer;

      beforeEach({
        setupTestData();

        answer = `{
          "_id" : "000000000000000000000001",
          "campaign": "000000000000000000000001",
          "attributes" : {
            "group" : {
              "value": 1
            }
          },
          "info" : {
            "changeIndex" : 0,
            "originalAuthor" : "5df626b89e13420100359ee8",
            "author" : "5df626b89e13420100359ee8",
            "createdOn" : "2021-02-12T19:07:58Z",
            "lastChangeOn" : "2021-02-12T19:07:58Z"
          },
          "position" : {
            "type" : "Point",
            "coordinates" : [1, 2]
          },
          "icons" : [ "5fe87b68e0e9940100eabbe6", "5fe87adbbb57ee0100f94ea8" ],
          "pictures" : [ "6026d20ebbb726b71545ac5a", "6026d20ebbb726b71545ac57" ]
        }`.parseJsonString;

        task = CampaignAnswerMapSync();
        task.broadcast = new MemoryBroadcast();

        receivedMessages = [];
        receivedChanges = [];
        void changesReceiver(const Json message) {
          receivedChanges ~= message;
        }

        void messageReceiver(const Json message) {
          receivedMessages ~= message;
        }

        task.broadcast.register("Feature.change", &changesReceiver);
        task.broadcast.register("notifications.message", &messageReceiver);
      });

      describe("for a campaign without a map", {
        size_t featureCount;

        beforeEach({
          auto campaign = crates.campaign.getItem("000000000000000000000001").and.exec.front;
          campaign["map"]["isEnabled"] = false;
          crates.campaign.updateItem(campaign);

          crates.campaignAnswer.addItem(answer);
          featureCount = crates.feature.get.size;
        });

        it("does not copy the answer", {
          auto request = CampaignAnswerMapSync.Request("000000000000000000000001");

          task.crates = crates;
          task.main(request);

          crates.feature.get.size.should.equal(featureCount);
        });
      });

      describe("for a campaign with a map", {
        size_t featureCount;

        beforeEach({
          auto campaign = crates.campaign.getItem("000000000000000000000001").and.exec.front;
          campaign["map"]["isEnabled"] = true;
          crates.campaign.updateItem(campaign);

          crates.campaignAnswer.addItem(answer);
          featureCount = crates.feature.get.size;
        });

        it("copies the answer", {
          auto request = CampaignAnswerMapSync.Request("000000000000000000000001");

          task.crates = crates;
          task.main(request);

          crates.feature.get.size.should.equal(featureCount + 1);
          auto newFeature = crates.feature.get.where("name").equal("Answer 000000000000000000000001").and.exec.front;
          newFeature.should.equal((`{
            "info": {
              "changeIndex": 0,
              "originalAuthor": "5df626b89e13420100359ee8",
              "author": "5df626b89e13420100359ee8",
              "createdOn": "2021-02-12T19:07:58Z",
              "lastChangeOn": "2021-02-12T19:07:58Z" },
            "computedVisibility":{"isDefault":false,"isPublic":false},
            "contributors":["5df626b89e13420100359ee8"],
            "isLarge":false,
            "name":"Answer 000000000000000000000001",
            "_id": "000000000000000000000005",
            "maps":["000000000000000000000001"],
            "description": "",
            "visibility": 0,
            "icons": [ "5fe87b68e0e9940100eabbe6", "5fe87adbbb57ee0100f94ea8" ],
            "position" : {
              "type" : "Point",
              "coordinates" : [1., 2.]
            },
            "pictures": [ "6026d20ebbb726b71545ac5a", "6026d20ebbb726b71545ac57" ],
            "source": {
              "modelId":"000000000000000000000001",
              "remoteId":"000000000000000000000001",
              "type":"Campaign",
              "syncAt": "2022-03-12T12:34:44Z"
            },
            "decorators": {
              "showAsLineAfterZoom": 99,
              "useDefault": true,
              "changeIndex": 0,
              "maxZoom": 20,
              "minZoom": 0,
              "showLineMarkers": false,
              "keepWhenSmall": true,
              "center": []
            },
            "attributes":{
              "group":{"value":1}
            },
            "sounds":[]
          }`).parseJsonString);
        });

        it("sends a message that a new feature was created", {
          auto request = CampaignAnswerMapSync.Request("000000000000000000000001");
          task.crates = crates;
          task.main(request);

          receivedChanges.length.should.equal(1);
          receivedChanges[0]["modelName"].to!string.should.equal("Feature");
          receivedChanges[0]["type"].to!string.should.equal("add");
          receivedChanges[0]["after"].should.equal((`{
            "info":{
              "changeIndex":0,
              "createdOn":"2021-02-12T19:07:58Z",
              "lastChangeOn":"2021-02-12T19:07:58Z",
              "originalAuthor":"5df626b89e13420100359ee8",
              "author":"5df626b89e13420100359ee8"
            },
            "decorators":{"showAsLineAfterZoom":99,"useDefault":true,"changeIndex":0,"maxZoom":20,"minZoom":0,"showLineMarkers":false,"keepWhenSmall":true,"center":[]},
            "visibility":0,
            "contributors":["5df626b89e13420100359ee8"],
            "computedVisibility":{"isDefault":false,"isPublic":false},
            "isLarge":false,
            "name":"Answer 000000000000000000000001",
            "_id":"000000000000000000000005",
            "maps":["000000000000000000000001"],
            "icons":["5fe87b68e0e9940100eabbe6","5fe87adbbb57ee0100f94ea8"],
            "description":"",
            "source": {
              "type": "Campaign",
              "remoteId": "000000000000000000000001",
              "modelId": "000000000000000000000001",
              "syncAt": "2022-03-12T12:34:44Z"
            },
            "attributes":{"group":{"value":1}},
            "position":{"type":"Point","coordinates":[1.,2.]},
            "pictures":["6026d20ebbb726b71545ac5a","6026d20ebbb726b71545ac57"],
            "sounds":[]
          }`).parseJsonString);
        });
      });

      describe("for a campaign with a map and custom name prefix", {
        size_t featureCount;

        beforeEach({
          auto campaign = crates.campaign.getItem("000000000000000000000001").and.exec.front;
          campaign["map"]["isEnabled"] = true;
          campaign["options"] = `{
            "featureNamePrefix": " some prefix "
          }`.parseJsonString;

          crates.campaign.updateItem(campaign);

          crates.campaignAnswer.addItem(answer);
          featureCount = crates.feature.get.size;
        });

        it("copies the answer", {
          auto request = CampaignAnswerMapSync.Request("000000000000000000000001");

          task.crates = crates;
          task.main(request);

          crates.feature.get.size.should.equal(featureCount + 1);
          auto newFeature = crates.feature.getItem("000000000000000000000005").and.exec.front;
          newFeature["name"].to!string.should.equal("some prefix 000000000000000000000001");
        });
      });

      describe("for a campaign with a map and custom name prefix", {
        size_t featureCount;

        beforeEach({
          auto map = crates.map.getItem("000000000000000000000001").and.exec.front;
          map["addFeaturesAsPending"] = true;
          crates.map.updateItem(map);

          auto campaign = crates.campaign.getItem("000000000000000000000001").and.exec.front;
          campaign["map"]["isEnabled"] = true;
          campaign["map"]["map"] = "000000000000000000000001";
          crates.campaign.updateItem(campaign);

          crates.campaignAnswer.addItem(answer);
          featureCount = crates.feature.get.size;
        });

        it("copies the answer with pending visibility`", {
          auto request = CampaignAnswerMapSync.Request("000000000000000000000001");

          task.crates = crates;
          task.main(request);

          crates.feature.get.size.should.equal(featureCount + 1);
          auto newFeature = crates.feature.getItem("000000000000000000000005").and.exec.front;
          newFeature["visibility"].to!int.should.equal(-1);
        });

        it("updates the answer", {
          auto request = CampaignAnswerMapSync.Request("000000000000000000000001");

          task.crates = crates;
          task.main(request);

          auto answer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;
          answer["status"].should.equal(1);
          answer["featureId"].to!string.should.equal("000000000000000000000005");
        });
      });

      describe("for answer with name and description", {
        size_t featureCount;

        beforeEach({
          auto campaign = crates.campaign.getItem("000000000000000000000001").and.exec.front;
          campaign["map"]["isEnabled"] = true;
          campaign["options"] = Json.emptyObject;

          crates.campaign.updateItem(campaign);

          answer["attributes"]["about"] = `{
            "name": "some name",
            "description": "some description",
          }`.parseJsonString;

          crates.campaignAnswer.addItem(answer);
          featureCount = crates.feature.get.size;
        });

        it("copies the answer", {
          auto request = CampaignAnswerMapSync.Request("000000000000000000000001");

          task.crates = crates;
          task.main(request);

          crates.feature.get.size.should.equal(featureCount + 1);
          auto newFeature = crates.feature.getItem("000000000000000000000005").and.exec.front;
          newFeature["name"].to!string.should.equal("some name");
          newFeature["description"].to!string.should.equal("some description");
          newFeature["attributes"].should.equal(`{
            "group":{"value":1}
          }`.parseJsonString);

          newFeature["source"].should.equal((`{
            "modelId":"000000000000000000000001",
            "remoteId":"000000000000000000000001",
            "type":"Campaign",
            "syncAt": "` ~ newFeature["source"]["syncAt"].to!string ~ `"
          }`).parseJsonString);
        });
      });

      describe("for answer that is already synced", {
        size_t featureCount;

        beforeEach({
          auto campaign = crates.campaign.getItem("000000000000000000000001").and.exec.front;
          campaign["map"]["isEnabled"] = true;
          campaign["options"] = Json.emptyObject;

          crates.campaign.updateItem(campaign);

          answer["attributes"]["about"] = `{
            "name": "some name",
            "description": "some description",
          }`.parseJsonString;

          crates.campaignAnswer.addItem(answer);

          auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
          feature["source"] = `{
            "modelId":"000000000000000000000001",
            "remoteId":"000000000000000000000001",
            "type":"Campaign"
          }`.parseJsonString;

          crates.feature.updateItem(feature);

          featureCount = crates.feature.get.size;
        });

        it("does nothing", {
          auto request = CampaignAnswerMapSync.Request("000000000000000000000001");

          task.crates = crates;
          task.main(request);

          crates.feature.get.size.should.equal(featureCount);
        });
      });

      describe("for an answer with an overflowed position with positive numbers", {
        size_t featureCount;

        beforeEach({
          auto campaign = crates.campaign.getItem("000000000000000000000001").and.exec.front;
          campaign["map"]["isEnabled"] = true;
          campaign["options"] = Json.emptyObject;

          crates.campaign.updateItem(campaign);

          answer["attributes"]["about"] = `{
            "name": "some name",
            "description": "some description"
          }`.parseJsonString;

          answer["position"] = `{ "type" : "Point", "coordinates" : [ 243.8058646701426, 31.05680725475381 ] }`.parseJsonString;

          crates.campaignAnswer.addItem(answer);
          featureCount = crates.feature.get.size;
        });

        it("copies the answer", {
          auto request = CampaignAnswerMapSync.Request("000000000000000000000001");

          task.crates = crates;
          task.main(request);

          crates.feature.get.size.should.equal(featureCount + 1);
          auto newFeature = crates.feature.getItem("000000000000000000000005").and.exec.front;
          newFeature["position"]["coordinates"].to!string.should.equal("[-116.1941353298574,31.05680725475381]");
        });
      });

      describe("for an answer with an overflowed position with negative numbers", {
        size_t featureCount;

        beforeEach({
          auto campaign = crates.campaign.getItem("000000000000000000000001").and.exec.front;
          campaign["map"]["isEnabled"] = true;
          campaign["options"] = Json.emptyObject;

          crates.campaign.updateItem(campaign);

          answer["attributes"]["about"] = `{
            "name": "some name",
            "description": "some description"
          }`.parseJsonString;

          answer["position"] = `{ "type" : "Point", "coordinates" : [ -243.8058646701426, -31.05680725475381 ] }`.parseJsonString;

          crates.campaignAnswer.addItem(answer);
          featureCount = crates.feature.get.size;
        });

        it("copies the answer", {
          auto request = CampaignAnswerMapSync.Request("000000000000000000000001");

          task.crates = crates;
          task.main(request);

          crates.feature.get.size.should.equal(featureCount + 1);
          auto newFeature = crates.feature.getItem("000000000000000000000005").and.exec.front;
          newFeature["position"]["coordinates"].to!string.should.equal("[116.1941353298574,-31.05680725475381]");
        });
      });

      describe("for a campaign with an add notification", {
        beforeEach({
          auto article = `{
            "title": "Campaign Notification",
            "slug": "notification-campaign-add-000000000000000000000001",
            "content": "Hello {{receiverName}},\n\nYou're receiving this notification because someone is testing the notification system. Please do not reply."
          }`.parseJsonString;

          crates.article.addItem(article);
          auto newAnswer = answer.clone;
          newAnswer["info"]["author"] = "000000000000000000000001";
          crates.campaignAnswer.addItem(newAnswer);
          receivedMessages = [];
        });

        it("trigger a notification when the author is a leader", {
          auto newAnswer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;
          newAnswer["info"]["author"] = userId["leader"];
          newAnswer["info"]["originalAuthor"] = userId["leader"];
          crates.campaignAnswer.updateItem(newAnswer);

          auto request = CampaignAnswerMapSync.Request("000000000000000000000001");

          task.crates = crates;
          task.main(request);

          receivedMessages.length.should.equal(2);
        });

        it("triggers a notification when the author is an owner", {
          auto newAnswer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;
          newAnswer["info"]["author"] = userId["owner"];
          newAnswer["info"]["originalAuthor"] = userId["owner"];
          crates.campaignAnswer.updateItem(newAnswer);

          auto request = CampaignAnswerMapSync.Request("000000000000000000000001");

          task.crates = crates;
          task.main(request);

          receivedMessages.length.should.equal(2);
        });

        it("triggers a notification when the author is an member", {
          auto newAnswer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;
          newAnswer["info"]["author"] = userId["member"];
          newAnswer["info"]["originalAuthor"] = userId["member"];
          crates.campaignAnswer.updateItem(newAnswer);

          auto request = CampaignAnswerMapSync.Request("000000000000000000000001");

          task.crates = crates;
          task.main(request);

          receivedMessages.length.should.equal(2);
        });

        it("triggers a notification when the author is a guest", {
          auto newAnswer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;
          newAnswer["info"]["author"] = userId["guest"];
          newAnswer["info"]["originalAuthor"] = userId["guest"];
          crates.campaignAnswer.updateItem(newAnswer);

          auto request = CampaignAnswerMapSync.Request("000000000000000000000001");

          task.crates = crates;
          task.main(request);

          receivedMessages[0].should.equal(`{ "id": "000000000000000000000001" }`.parseJsonString);

          auto message = crates.message.getItem("000000000000000000000001").and.exec.front;

          message.should.equal((`{
            "html":"<p>Hello there,<\/p>\n\n<p>You're receiving this notification because someone is testing the notification system. Please do not reply.<\/p>\n",
            "isSent":false,
            "actions":{},
            "text":"Hello there,\n\nYou're receiving this notification because someone is testing the notification system. Please do not reply.",
            "to":{"type":"email","value":"guest@gmail.com"},
            "uniqueKey":"notification-campaign-add-000000000000000000000001-000000000000000000000001",
            "_id":"000000000000000000000001",
            "type":"message",
            "subject":"Campaign Notification",
            "sentOn":"0001-01-01T00:00:00+00:00",
            "useGenericTemplate":true
          }`).parseJsonString);
        });

        it("triggers a notification when the contributor email is set", {
          auto newAnswer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;
          newAnswer["info"]["author"] = userId["guest"];
          newAnswer["info"]["originalAuthor"] = userId["guest"];
          newAnswer["contributor"] = `{
            "email": "contributor@a.b"
          }`.parseJsonString;
          crates.campaignAnswer.updateItem(newAnswer);

          auto request = CampaignAnswerMapSync.Request("000000000000000000000001");

          task.crates = crates;
          task.main(request);

          receivedMessages[0].should.equal(`{ "id": "000000000000000000000001" }`.parseJsonString);

          auto message = crates.message.getItem("000000000000000000000001").and.exec.front;
          message.should.equal((`{
            "html":"<p>Hello there,<\/p>\n\n<p>You're receiving this notification because someone is testing the notification system. Please do not reply.<\/p>\n",
            "isSent":false,
            "actions":{},
            "text":"Hello there,\n\nYou're receiving this notification because someone is testing the notification system. Please do not reply.",
            "to":{"type":"email","value":"contributor@a.b"},
            "uniqueKey":"notification-campaign-add-000000000000000000000001-000000000000000000000001",
            "_id":"000000000000000000000001",
            "type":"message",
            "subject":"Campaign Notification",
            "sentOn":"0001-01-01T00:00:00+00:00",
            "useGenericTemplate":true
          }`).parseJsonString);
        });

        it("triggers a notification when the contributor email is an invalid email", {
          auto newAnswer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;
          newAnswer["info"]["author"] = userId["guest"];
          newAnswer["info"]["originalAuthor"] = userId["guest"];
          newAnswer["contributor"] = `{
            "email": "invalid"
          }`.parseJsonString;
          crates.campaignAnswer.updateItem(newAnswer);

          auto request = CampaignAnswerMapSync.Request("000000000000000000000001");

          task.crates = crates;
          task.main(request);

          receivedMessages[0].should.equal(`{ "id": "000000000000000000000001" }`.parseJsonString);

          auto message = crates.message.getItem("000000000000000000000001").and.exec.front;

          message.should.equal((`{
            "html":"<p>Hello there,<\/p>\n\n<p>You're receiving this notification because someone is testing the notification system. Please do not reply.<\/p>\n",
            "isSent":false,
            "actions":{},
            "text":"Hello there,\n\nYou're receiving this notification because someone is testing the notification system. Please do not reply.",
            "to":{"type":"email","value":"guest@gmail.com"},
            "uniqueKey":"notification-campaign-add-000000000000000000000001-000000000000000000000001",
            "_id":"000000000000000000000001",
            "type":"message",
            "subject":"Campaign Notification",
            "sentOn":"0001-01-01T00:00:00+00:00",
            "useGenericTemplate":true
          }`).parseJsonString);
        });

        it("does not trigger a contributor notification when the author is anonymous and the contributor has no email", {
          auto newAnswer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;
          newAnswer["info"]["author"] = "@anonymous";
          newAnswer["info"]["originalAuthor"] = "@anonymous";
          crates.campaignAnswer.updateItem(newAnswer);

          newAnswer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;

          auto request = CampaignAnswerMapSync.Request("000000000000000000000001");

          task.crates = crates;
          task.main(request);

          receivedMessages.length.should.equal(1);

          auto message = crates.message.getItem("000000000000000000000001").and.exec.front;
          message["uniqueKey"].to!string.should.equal("notification-add-answer-team-000000000000000000000001");
        });
      });

      describe("for a campaign with an add notification that has all variables", {
        beforeEach({
          auto article = `{
            "title": "Campaign Notification",
            "slug": "notification-campaign-add-000000000000000000000001",
            "content": "{{receiverName}}; {{reviewUrl}}; {{serviceName}}; {{serviceUrl}}; {{campaignName}};"
          }`.parseJsonString;

          crates.article.addItem(article);
          auto newAnswer = answer.clone;
          newAnswer["info"]["author"] = "000000000000000000000001";
          crates.campaignAnswer.addItem(newAnswer);
          receivedMessages = [];
        });

        it("fills in all the data", {
          auto newAnswer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;
          newAnswer["info"]["author"] = userId["leader"];
          newAnswer["info"]["originalAuthor"] = userId["leader"];
          crates.campaignAnswer.updateItem(newAnswer);

          auto request = CampaignAnswerMapSync.Request("000000000000000000000001");

          task.crates = crates;
          task.main(request);

          receivedMessages.length.should.equal(2);

          auto message = crates.message.getItem("000000000000000000000001").and.exec.front;
          message["text"].to!string.should.equal("there; https://giscollective.com/manage/survey-answers/000000000000000000000001; ---; https://giscollective.com; Campaign 1;");
        });
      });

      describe("the team notification", {
        beforeEach({
          auto article = `{
            "title": "Campaign Notification",
            "slug": "notification-add-answer-team",
            "content": "{{receiverName}}; {{reviewUrl}}; {{serviceName}}; {{serviceUrl}}; {{campaignName}};"
          }`.parseJsonString;

          crates.article.addItem(article);
          auto newAnswer = answer.clone;
          newAnswer["info"]["author"] = "000000000000000000000001";
          crates.campaignAnswer.addItem(newAnswer);
          receivedMessages = [];
        });

        it("trigger a notification to the team", {
          auto request = CampaignAnswerMapSync.Request("000000000000000000000001");

          task.crates = crates;
          task.main(request);

          receivedMessages[1].should.equal(`{ "id": "000000000000000000000002" }`.parseJsonString);

          auto message = crates.message.getItem("000000000000000000000002").and.exec.front;

          message.should.equal((`{
            "html":"<p>team1; https://giscollective.com/manage/survey-answers/000000000000000000000001; ---; https://giscollective.com; Campaign 1;<\/p>\n",
            "isSent":false,
            "actions":{"Review it now":"https://giscollective.com/manage/survey-answers/000000000000000000000001"},
            "text":"team1; https://giscollective.com/manage/survey-answers/000000000000000000000001; ---; https://giscollective.com; Campaign 1;",
            "to":{"type":"team","value":"000000000000000000000001"},
            "uniqueKey":"notification-add-answer-team-000000000000000000000001",
            "_id":"000000000000000000000002",
            "type":"message",
            "subject":"Campaign Notification",
            "sentOn":"0001-01-01T00:00:00+00:00",
            "useGenericTemplate":true
          }`).parseJsonString);
        });
      });
    });
  });
});