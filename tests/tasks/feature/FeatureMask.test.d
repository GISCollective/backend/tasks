/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.feature.FeatureMask;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;

import crate.collection.notifications;

import ogm.models.meta;
import ogm.models.changeset;

import gis_collective.tasks.feature.FeatureMask;
import gis_collective.hmq.broadcast.memory;


alias suite = Spec!({
  URLRouter router;

  describe("The Feature.Mask task", {
    describe("Processing list", {
      ChangeSet changeSet;

      beforeEach({
        changeSet = ChangeSet();
        changeSet.model = "Feature";
        changeSet.itemId = ObjectId.generate;
      });

      it("should process add changes inside features", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.add;

        FeatureMask.processList(changeSet).should.equal([ changeSet.itemId.toJson ]);
      });

      it("should not process changes inside maps", {
        changeSet.model = "Map";
        changeSet.type = CrateChangeType.delete_;

        FeatureMask.processList(changeSet).should.equal([ ]);
      });

      it("should process update changes inside features", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.update;

        FeatureMask.processList(changeSet).should.equal([ changeSet.itemId.toJson ]);
      });

      it("should not process delete changes inside features", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.delete_;

        FeatureMask.processList(changeSet).should.equal([ ]);
      });
    });

    describe("The task", {
      FeatureMask task;

      beforeEach({
        setupTestData();

        task = FeatureMask();
      });

      describe("for a site that has a masked map", {
        beforeEach({
          auto map = crates.map.getItem("000000000000000000000001").and.exec.front;
          map["mask"] = `{
            "isEnabled": true,
            "map": "000000000000000000000002"
          }`.parseJsonString;

          crates.map.updateItem(map);
        });

        it("removes the decimal part of the position when there is no mask match and unmasked is not set", {
          auto request = FeatureMask.Request(ObjectId.fromString("000000000000000000000001"));

          task.crates = crates;
          task.main(request);

          auto feature = crates.feature.get
            .where("_id").equal("000000000000000000000001")
            .and.exec
              .front;

          feature["position"].should.equal(`{
            "type": "Point",
            "coordinates": [ 1.0, 1.0 ]
          }`.parseJsonString);

          feature["unmasked"].should.equal(`{
            "type": "Point",
            "coordinates": [ 1.5, 1.5 ]
          }`.parseJsonString);

          feature["attributes"]["mask"]["type"].to!string.should.equal("0");
        });

        it("removes the decimal part of the unmasked field when there is no mask match", {
          auto feature = crates.feature.get
            .where("_id").equal("000000000000000000000001")
            .and.exec
              .front;
          feature["unmasked"] = `{
            "type": "Point",
            "coordinates": [10.44, 20.55]
          }`.parseJsonString;

          crates.feature.updateItem(feature);

          auto request = FeatureMask.Request(ObjectId.fromString("000000000000000000000001"));

          task.crates = crates;
          task.main(request);

          feature = crates.feature.get
            .where("_id").equal("000000000000000000000001")
            .and.exec
              .front;

          feature["position"].should.equal(`{
            "type": "Point",
            "coordinates": [ 10.0, 20.0 ]
          }`.parseJsonString);

          feature["unmasked"].should.equal(`{
            "type": "Point",
            "coordinates": [10.44, 20.55]
          }`.parseJsonString);

          feature["attributes"]["mask"]["type"].to!string.should.equal("0");
        });

        it("updates the precision of the decimal part of the unmasked field when there is no mask match", {
          auto feature = crates.feature.get
            .where("_id").equal("000000000000000000000001")
            .and.exec
              .front;
          feature["unmasked"] = `{
            "type": "Point",
            "coordinates": [10.44, 20.55]
          }`.parseJsonString;

          crates.feature.updateItem(feature);

          auto request = FeatureMask.Request(ObjectId.fromString("000000000000000000000001"));

          task.crates = crates;
          task.main(request);

          crates.preference.addItem(`{
              "name" : "locationServices.maskingPrecision",
              "value" : "1"
            }`.parseJsonString);

          task.crates = crates;
          task.main(request);

          feature = crates.feature.get
            .where("_id").equal("000000000000000000000001")
            .and.exec
              .front;

          feature["position"].should.equal(`{
            "type": "Point",
            "coordinates": [ 10.4, 20.5 ]
          }`.parseJsonString);

          feature["unmasked"].should.equal(`{
            "type": "Point",
            "coordinates": [10.44, 20.55]
          }`.parseJsonString);

          feature["attributes"]["mask"]["type"].to!string.should.equal("1");
        });

        describe("when the precision preference is set to 1 decimal", {
          beforeEach({
            crates.preference.addItem(`{
              "name" : "locationServices.maskingPrecision",
              "value" : "1"
            }`.parseJsonString);
          });

          it("removes all but the first decimal when there is no mask match", {
            auto feature = crates.feature.get
              .where("_id").equal("000000000000000000000001")
              .and.exec
                .front;
            feature["unmasked"] = `{
              "type": "Point",
              "coordinates": [10.44, 20.57346]
            }`.parseJsonString;

            crates.feature.updateItem(feature);

            auto request = FeatureMask.Request(ObjectId.fromString("000000000000000000000001"));

            task.crates = crates;
            task.main(request);

            feature = crates.feature.get
              .where("_id").equal("000000000000000000000001")
              .and.exec
                .front;

            feature["position"].should.equal(`{
              "type": "Point",
              "coordinates": [ 10.4, 20.5 ]
            }`.parseJsonString);

            feature["unmasked"].should.equal(`{
              "type": "Point",
              "coordinates": [10.44, 20.57346]
            }`.parseJsonString);

          feature["attributes"]["mask"]["type"].to!string.should.equal("1");
          });
        });

        describe("when the precision preference is set to 5 decimals", {
          beforeEach ({
            crates.preference.addItem(`{
              "name" : "locationServices.maskingPrecision",
              "value" : "5"
            }`.parseJsonString);
          });

          it("removes all but the first 5 decimals", {
            auto feature = crates.feature.get
              .where("_id").equal("000000000000000000000001")
              .and.exec
                .front;
            feature["unmasked"] = `{
              "type": "Point",
              "coordinates": [10.44, 20.573462]
            }`.parseJsonString;

            crates.feature.updateItem(feature);

            auto request = FeatureMask.Request(ObjectId.fromString("000000000000000000000001"));

            task.crates = crates;
            task.main(request);

            feature = crates.feature.get
              .where("_id").equal("000000000000000000000001")
              .and.exec
                .front;

            feature["position"].to!string.parseJsonString.should.equal(`{
              "type": "Point",
              "coordinates": [ 10.43999, 20.57346 ]
            }`.parseJsonString);

            feature["unmasked"].should.equal(`{
              "type": "Point",
              "coordinates": [10.44, 20.573462]
            }`.parseJsonString);

          feature["attributes"]["mask"]["type"].to!string.should.equal("5");
          });
        });

        it("uses the centroid of a mask when is matched", {
          createFeature(
          Feature(ObjectId.fromString("a"),
            [map2],
            "site1",
            Json("description of site1"),
            GeoJsonGeometry.fromJson(`{
              "type": "Polygon",
              "coordinates": [[[0, 0], [20, 0],[20, 20], [0, 20], [0, 0]]]
            }`.parseJsonString),
            ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"),SysTime.fromISOExtString("2015-01-01T00:00:00Z"),0,"","000000000000000000000001")));

          auto request = FeatureMask.Request(ObjectId.fromString("000000000000000000000001"));

          task.crates = crates;
          task.main(request);

          auto feature = crates.feature.get
            .where("_id").equal("000000000000000000000001")
            .and.exec
              .front;

          feature["position"].should.equal(`{
            "type": "Point",
            "coordinates": [ 10.0, 10.0 ]
          }`.parseJsonString);

          feature["unmasked"].should.equal(`{
            "type": "Point",
            "coordinates": [ 1.5, 1.5 ]
          }`.parseJsonString);

          feature["attributes"]["mask"]["type"].to!string.should.equal("index map");
        });

        it("adds the box polygons", {
          createFeature(
          Feature(ObjectId.fromString("a"),
            [map2],
            "site1",
            Json("description of site1"),
            GeoJsonGeometry.fromJson(`{
              "type": "Polygon",
              "coordinates": [[[0, 0], [20, 0],[20, 20], [0, 20], [0, 0]]]
            }`.parseJsonString),
            ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"),SysTime.fromISOExtString("2015-01-01T00:00:00Z"),0,"","000000000000000000000001")));

          auto request = FeatureMask.Request(ObjectId.fromString("000000000000000000000001"));

          task.crates = crates;
          task.main(request);

          auto feature = crates.feature.get
            .where("_id").equal("000000000000000000000001")
            .and.exec
              .front;

          feature["positionBox"].should.equal(`{"type":"Point","coordinates":[10,10]}`.parseJsonString);
          feature["unmaskedBox"].should.equal(`{"type":"Point","coordinates":[1.5,1.5]}`.parseJsonString);
        });
      });

      describe("for a site that has a no masked map", {
        it("replaces the unmasked value with position when is set", {
          auto feature = crates.feature.get
            .where("_id").equal("000000000000000000000001")
            .and.exec
              .front;
          feature["unmasked"] = `{
            "type": "Point",
            "coordinates": [10.44, 20.55]
          }`.parseJsonString;
          crates.feature.updateItem(feature);

          auto request = FeatureMask.Request(ObjectId.fromString("000000000000000000000001"));

          task.crates = crates;
          task.main(request);

          feature = crates.feature.get
            .where("_id").equal("000000000000000000000001")
            .and.exec
              .front;

          feature["position"].should.equal(`{
            "type": "Point",
            "coordinates": [10.44, 20.55]
          }`.parseJsonString);

          feature["unmasked"].to!string.should.equal("null");
          feature["unmaskedBox"].to!string.should.equal(`null`);
          feature["positionBox"].should.equal(`{"type":"Point","coordinates":[10.44,20.55]}`.parseJsonString);
          feature["attributes"]["mask"].to!string.should.equal("null");
        });

        it("ignores a feature that has unmasked null", {
          auto beforeFeature = crates.feature.get
            .where("_id").equal("000000000000000000000001")
            .and.exec
              .front;

          auto request = FeatureMask.Request(ObjectId.fromString("000000000000000000000001"));

          task.crates = crates;
          task.main(request);

          auto feature = crates.feature.get
            .where("_id").equal("000000000000000000000001")
            .and.exec
              .front;

          feature.should.equal(beforeFeature);

          feature["unmasked"].to!string.should.equal("null");
          feature["unmaskedBox"].to!string.should.equal(`null`);
          feature["positionBox"].should.equal(`{"type":"Point","coordinates":[1.5,1.5]}`.parseJsonString);
        });

        it("does ignores a feature that has no unmasked field", {
          auto feature = crates.feature.get
            .where("_id").equal("000000000000000000000001")
            .and.exec
              .front;
          feature["unmasked"] = Json(null);
          crates.feature.updateItem(feature);

          auto beforeFeature = crates.feature.get
            .where("_id").equal("000000000000000000000001")
            .and.exec
              .front;

          auto request = FeatureMask.Request(ObjectId.fromString("000000000000000000000001"));

          task.crates = crates;
          task.main(request);

          feature = crates.feature.get
            .where("_id").equal("000000000000000000000001")
            .and.exec
              .front;

          feature.should.equal(beforeFeature);

          feature["unmasked"].to!string.should.equal("null");
          feature["unmaskedBox"].to!string.should.equal(`null`);
          feature["positionBox"].should.equal(`{"type":"Point","coordinates":[1.5,1.5]}`.parseJsonString);
        });
      });
    });
  });
});
