/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.feature.delete_;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;

import crate.collection.notifications;

import ogm.models.meta;
import ogm.models.changeset;

import gis_collective.tasks.feature.delete_;
import gis_collective.hmq.broadcast.memory;


alias suite = Spec!({
  URLRouter router;

  describe("The Feature.delete task", {
    describe("Processing list", {
      ChangeSet changeSet;

      beforeEach({
        changeSet = ChangeSet();
        changeSet.model = "Feature";
        changeSet.itemId = ObjectId.generate;
      });

      it("should not process add changes inside features", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.add;

        FeatureDelete.processList(changeSet).should.equal([ ]);
      });

      it("should not process changes inside maps", {
        changeSet.model = "Map";
        changeSet.type = CrateChangeType.delete_;

        FeatureDelete.processList(changeSet).should.equal([ ]);
      });

      it("should not process update changes inside features", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.update;

        FeatureDelete.processList(changeSet).should.equal([ ]);
      });

      it("should process delete changes inside features", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.delete_;

        FeatureDelete.processList(changeSet).should.equal([ changeSet.itemId.toJson ]);
      });
    });

    describe("The task", {
      FeatureDelete task;

      beforeEach({
        setupTestData();

        task = FeatureDelete();

        Meta meta;
        meta.model = "Feature";
        meta.itemId = "000000000000000000000001";

        crates.meta.addItem(meta.serializeToJson);
      });

      it("should remove all feature meta", {
        auto request = FeatureDelete.Request(ObjectId.fromString("000000000000000000000001"));

        task.crates = crates;
        task.main(request);

        crates.meta.get
          .where("model").equal("Feature").and
          .where("itemId").equal("000000000000000000000001").and
          .size.should.equal(0);
      });
    });
  });
});