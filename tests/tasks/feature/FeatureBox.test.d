/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.feature.FeatureBox;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.file;
import std.datetime;

import crate.collection.notifications;

import ogm.models.meta;
import ogm.models.changeset;

import gis_collective.tasks.feature.FeatureBox;
import gis_collective.hmq.broadcast.memory;


alias suite = Spec!({
  URLRouter router;

  describe("The Feature.Box task", {
    describe("Processing list", {
      ChangeSet changeSet;

      beforeEach({
        changeSet = ChangeSet();
        changeSet.model = "Feature";
        changeSet.itemId = ObjectId.generate;
      });

      it("should process add changes inside features", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.add;

        FeatureBox.processList(changeSet).should.equal([ changeSet.itemId.toJson ]);
      });

      it("should not process changes inside maps", {
        changeSet.model = "Map";
        changeSet.type = CrateChangeType.delete_;

        FeatureBox.processList(changeSet).should.equal([ ]);
      });

      it("should process update changes inside features", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.update;

        FeatureBox.processList(changeSet).should.equal([ changeSet.itemId.toJson ]);
      });

      it("should not process delete changes inside features", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.delete_;

        FeatureBox.processList(changeSet).should.equal([ ]);
      });
    });

    describe("The task", {
      FeatureBox task;

      beforeEach({
        setupTestData();

        task = FeatureBox();
      });

      describe("for a site that has a masked map", {
        beforeEach({
          auto map = crates.map.getItem("000000000000000000000001").and.exec.front;
          map["mask"] = `{
            "isEnabled": true,
            "map": "000000000000000000000002"
          }`.parseJsonString;

          crates.map.updateItem(map);
        });

        it("does not add box properties", {
          auto request = FeatureBox.Request(ObjectId.fromString("000000000000000000000001"));

          auto feature = crates.feature.get
            .where("_id").equal("000000000000000000000001")
            .and.exec
              .front;

          feature.remove("positionBox");
          crates.feature.updateItem(feature);

          task.crates = crates;
          task.main(request);

          feature = crates.feature.get
            .where("_id").equal("000000000000000000000001")
            .and.exec
              .front;

          feature["positionBox"].to!string.should.equal(`null`);
          feature["unmaskedBox"].to!string.should.equal(`null`);
        });
      });

      describe("for a site that has a no masked map", {
        it("adds the position box", {
          auto request = FeatureBox.Request(ObjectId.fromString("000000000000000000000001"));

          task.crates = crates;
          task.main(request);

          auto feature = crates.feature.get
            .where("_id").equal("000000000000000000000001")
            .and.exec
              .front;

          feature["positionBox"].should.equal(`{
            "type":"Point",
            "coordinates":[1.5,1.5]
          }`.parseJsonString);
        });
      });
    });
  });
});
