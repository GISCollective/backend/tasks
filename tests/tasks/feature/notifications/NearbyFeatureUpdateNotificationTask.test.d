/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.feature.notifications.NearbyFeatureUpdateNotificationTask;

import tests.fixtures;

import gis_collective.tasks.feature.notifications.NearbyFeatureUpdateNotificationTask;
import gis_collective.hmq.broadcast.memory;


alias suite = Spec!({
  URLRouter router;

  describe("The NearbyFeatureUpdateNotification task", {
    NearbyFeatureUpdateNotificationTask task;
    Json[] receivedMessages;

    beforeEach({
      setupTestData();

      auto map = crates.map.getItem("000000000000000000000002").exec.front;
      map["area"] = `{
        "type": "Polygon",
        "coordinates": [ [
            [ -10, -20 ],
            [ 16, -20 ],
            [ 16, 2 ],
            [ -10, 2 ],
            [ -10, -20 ]
          ]
        ]
      }`.parseJsonString;

      crates.map.updateItem(map);

      receivedMessages = [];

      task = NearbyFeatureUpdateNotificationTask();
      task.crates = crates;

      task.broadcast = new MemoryBroadcast();

      void receiver(const Json message) {
        receivedMessages ~= message;
      }

      task.broadcast.register("notifications.message", &receiver);
    });

    it("should trigger the email notification task for each appropriate user Id", {
      task.id = "000000000000000000000001";
      task.start();

      receivedMessages.length.should.equal(4);
      receivedMessages[0].should.equal(`{
        "subject": "Updates in the area of your map",
        "textMessage": "Here's what has updated: site1",
        "userId": "000000000000000000000004"
      }`.parseJsonString);
    });
  });
});
