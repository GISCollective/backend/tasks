/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.feature.SoundsVisibility;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;

import crate.collection.notifications;

import ogm.models.meta;
import ogm.models.changeset;

import gis_collective.tasks.feature.SoundsVisibility;
import gis_collective.hmq.broadcast.memory;


alias suite = Spec!({
  URLRouter router;

  describe("The Feature.SoundsVisibility task", {
    describe("Processing list", {
      ChangeSet changeSet;

      beforeEach({
        changeSet = ChangeSet();
        changeSet.model = "Feature";
        changeSet.itemId = ObjectId.generate;
      });

      it("should process add changes inside features", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.add;

        SoundsVisibility.processList(changeSet).should.equal([ changeSet.itemId.toJson ]);
      });

      it("should not process changes inside maps", {
        changeSet.model = "Map";
        changeSet.type = CrateChangeType.delete_;

        SoundsVisibility.processList(changeSet).should.equal([ ]);
      });

      it("should process update changes inside features", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.update;

        SoundsVisibility.processList(changeSet).should.equal([ changeSet.itemId.toJson ]);
      });

      it("should not process delete changes inside features", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.delete_;

        SoundsVisibility.processList(changeSet).should.equal([ ]);
      });
    });

    describe("The task", {
      SoundsVisibility task;

      beforeEach({
        setupTestData();

        task = SoundsVisibility();

        auto info = ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z"));
        createSound(Sound(ObjectId.fromString("1"), "name4", VisibilityOptional(Team(ObjectId.fromString("4")), true), info, new SoundFile));

        auto feature = crates.feature.getItem("000000000000000000000001").exec.front;
        feature["sounds"] = ["000000000000000000000001"].serializeToJson;

        crates.feature.updateItem(feature);
      });

      it("should update sounds visibility attached to the feature", {
        auto request = SoundsVisibility.Request(ObjectId.fromString("000000000000000000000001"));

        task.crates = crates;
        task.main(request);

        auto sound = crates.sound.get
          .where("_id").equal("000000000000000000000001").and.exec.front;

        sound.should.equal(`{
          "_id": "000000000000000000000001",
          "visibility": {
            "isDefault": false,
            "isPublic": true,
            "team": "000000000000000000000001"
          },
          "feature": "",
          "info": {
            "changeIndex": 0,
            "createdOn": "2015-01-01T00:00:00Z",
            "lastChangeOn": "2015-01-01T00:00:00Z",
            "originalAuthor": "",
            "author": ""
          },
          "sourceUrl": "",
          "sound": "",
          "name": "name4",
          "campaignAnswer":"",
          "attributions": ""
        }`.parseJsonString);
      });
    });
  });
});