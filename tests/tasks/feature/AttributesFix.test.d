/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.feature.AttributesFix;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;

import crate.collection.notifications;

import ogm.models.meta;
import ogm.models.changeset;

import gis_collective.tasks.feature.AttributesFix;
import gis_collective.hmq.broadcast.memory;


alias suite = Spec!({
  URLRouter router;

  describe("The Feature.AttributesFix task", {
    describe("Processing list", {
      ChangeSet changeSet;

      beforeEach({
        setupTestData();
        changeSet = ChangeSet();
        changeSet.model = "Feature";
        changeSet.itemId = ObjectId.fromString("000000000000000000000001");
      });

      it("should not process changes when icons are added", {
        changeSet.model = "Icon";
        changeSet.type = CrateChangeType.add;

        AttributesFix.processList(changeSet, crates).should.equal([ ]);
      });

      it("should not process changes when icons are deleted", {
        changeSet.model = "Icon";
        changeSet.type = CrateChangeType.delete_;

        AttributesFix.processList(changeSet, crates)
          .should.equal([
            Json("000000000000000000000001"),
            Json("000000000000000000000002"),
            Json("000000000000000000000003"),
            Json("000000000000000000000004") ]);
      });

      it("should not process changes when icons are updated", {
        changeSet.model = "Icon";
        changeSet.type = CrateChangeType.update;

        AttributesFix.processList(changeSet, crates)
          .should.equal([
            Json("000000000000000000000001"),
            Json("000000000000000000000002"),
            Json("000000000000000000000003"),
            Json("000000000000000000000004") ]);
      });

      it("should not process changes inside maps", {
        changeSet.model = "Map";
        changeSet.type = CrateChangeType.delete_;

        AttributesFix.processList(changeSet, crates).should.equal([ ]);
      });

      it("should process add changes inside Features", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.add;

        AttributesFix.processList(changeSet, crates).should.equal([ changeSet.itemId.toJson ]);
      });

      it("should process update changes inside Features", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.update;

        AttributesFix.processList(changeSet, crates).should.equal([ changeSet.itemId.toJson ]);
      });

      it("should not process delete changes inside features", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.delete_;

        AttributesFix.processList(changeSet, crates).should.equal([ ]);
      });
    });

    describe("The task", {
      AttributesFix task;
      Json feature;
      Json icon;

      beforeEach({
        setupTestData();
        feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
        icon = crates.icon.getItem("000000000000000000000001").and.exec.front;
        task = AttributesFix();
      });

      describe("when an icon is deleted", {
        beforeEach({
          crates.icon.deleteItem("000000000000000000000001");
        });

        it("removes the icon from the list", {
          auto request = AttributesFix.Request(ObjectId.fromString("000000000000000000000001"));

          task.crates = crates;
          task.main(request);

          feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
          feature["icons"].length.should.equal(0);
        });
      });

      describe("when an icon allows many instances", {
        beforeEach({
          icon["allowMany"] = true;
          crates.icon.updateItem(icon);
        });

        it("converts the object attributes to a list when they are an object", {
          auto request = AttributesFix.Request(ObjectId.fromString("000000000000000000000001"));

          task.crates = crates;
          task.main(request);

          feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
          feature["attributes"].should.equal(`{ "name1": [{
            "program": "Luni-Vineri:9-18",
            "phone": "123456",
            "kids-friendly": "true",
            "price": "11.4",
            "type of food": "international",
            "max number of people": "23"
          }]}`.parseJsonString);
        });

        it("does nothing when the attributes are already a list", {
          feature["attributes"] = `{ "name1": [{
            "program": "Luni-Vineri:9-18",
            "phone": "123456",
            "kids-friendly": "true",
            "price": "11.4",
            "type of food": "international",
            "max number of people": "23"
          }]}`.parseJsonString;
          crates.feature.updateItem(feature);

          auto request = AttributesFix.Request(ObjectId.fromString("000000000000000000000001"));

          task.crates = crates;
          task.main(request);

          feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
          feature["attributes"].should.equal(`{ "name1": [{
            "program": "Luni-Vineri:9-18",
            "phone": "123456",
            "kids-friendly": "true",
            "price": "11.4",
            "type of food": "international",
            "max number of people": "23"
          }]}`.parseJsonString);
        });

        it("does nothing when the attributes are invalid", {
          feature["attributes"] = `invalid`;
          crates.feature.updateItem(feature);

          auto request = AttributesFix.Request(ObjectId.fromString("000000000000000000000001"));

          task.crates = crates;
          task.main(request);

          feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
          feature["attributes"].toString.should.equal(`{}`);
        });
      });

      describe("when an icon does not allow many instances", {
        beforeEach({
          icon["allowMany"] = false;
          crates.icon.updateItem(icon);
        });

        it("does nothing when the attributes are an object", {
          auto request = AttributesFix.Request(ObjectId.fromString("000000000000000000000001"));

          task.crates = crates;
          task.main(request);

          feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
          feature["attributes"].should.equal(`{ "name1": {
            "program": "Luni-Vineri:9-18",
            "phone": "123456",
            "kids-friendly": "true",
            "price": "11.4",
            "type of food": "international",
            "max number of people": "23"
          }}`.parseJsonString);
        });

        it("converts the list to an object when it has one element", {
          feature["attributes"] = `{ "name1": [{
            "program": "Luni-Vineri:9-18",
            "phone": "123456",
            "kids-friendly": "true",
            "price": "11.4",
            "type of food": "international",
            "max number of people": "23"
          }]}`.parseJsonString;
          crates.feature.updateItem(feature);

          auto request = AttributesFix.Request(ObjectId.fromString("000000000000000000000001"));

          task.crates = crates;
          task.main(request);

          feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
          feature["attributes"].should.equal(`{ "name1": {
            "program": "Luni-Vineri:9-18",
            "phone": "123456",
            "kids-friendly": "true",
            "price": "11.4",
            "type of food": "international",
            "max number of people": "23"
          }}`.parseJsonString);
        });

        it("converts the list to an object when it has one valid and one invalid element", {
          feature["attributes"] = `{ "name1": ["invalid", {
            "program": "Luni-Vineri:9-18",
            "phone": "123456",
            "kids-friendly": "true",
            "price": "11.4",
            "type of food": "international",
            "max number of people": "23"
          }]}`.parseJsonString;
          crates.feature.updateItem(feature);

          auto request = AttributesFix.Request(ObjectId.fromString("000000000000000000000001"));

          task.crates = crates;
          task.main(request);

          feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
          feature["attributes"].should.equal(`{ "name1": {
            "program": "Luni-Vineri:9-18",
            "phone": "123456",
            "kids-friendly": "true",
            "price": "11.4",
            "type of food": "international",
            "max number of people": "23"
          }}`.parseJsonString);
        });

        it("does nothing when it has 2 valid elements", {
          feature["attributes"] = `{ "name1": [{}, {
            "program": "Luni-Vineri:9-18",
            "phone": "123456",
            "kids-friendly": "true",
            "price": "11.4",
            "type of food": "international",
            "max number of people": "23"
          }]}`.parseJsonString;
          crates.feature.updateItem(feature);

          auto request = AttributesFix.Request(ObjectId.fromString("000000000000000000000001"));

          task.crates = crates;
          task.main(request);

          feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
          feature["attributes"].should.equal(`{ "name1": [{}, {
            "program": "Luni-Vineri:9-18",
            "phone": "123456",
            "kids-friendly": "true",
            "price": "11.4",
            "type of food": "international",
            "max number of people": "23"
          }]}`.parseJsonString);
        });
      });

      describe("when an icon has other names", {
        beforeEach({
          icon["otherNames"] = ["other name"].serializeToJson;
          crates.icon.updateItem(icon);
        });

        it("does nothing when the icon name is matched to an attribute", {
          auto request = AttributesFix.Request(ObjectId.fromString("000000000000000000000001"));

          task.crates = crates;
          task.main(request);

          feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
          feature["attributes"].should.equal(`{ "name1": {
            "program": "Luni-Vineri:9-18",
            "phone": "123456",
            "kids-friendly": "true",
            "price": "11.4",
            "type of food": "international",
            "max number of people": "23"
          }}`.parseJsonString);
        });

        it("renames the attribute name when is using the other name", {
          feature["attributes"] = `{ "other name": {
            "program": "Luni-Vineri:9-18",
            "phone": "123456",
            "kids-friendly": "true",
            "price": "11.4",
            "type of food": "international",
            "max number of people": "23"
          }}`.parseJsonString;
          crates.feature.updateItem(feature);

          auto request = AttributesFix.Request(ObjectId.fromString("000000000000000000000001"));

          task.crates = crates;
          task.main(request);

          feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
          feature["attributes"].should.equal(`{ "name1": {
            "program": "Luni-Vineri:9-18",
            "phone": "123456",
            "kids-friendly": "true",
            "price": "11.4",
            "type of food": "international",
            "max number of people": "23"
          }}`.parseJsonString);
        });

        it("ignores invalid attributes", {
          feature["attributes"] = `{ "other name": "invalid" }`.parseJsonString;
          crates.feature.updateItem(feature);

          auto request = AttributesFix.Request(ObjectId.fromString("000000000000000000000001"));

          task.crates = crates;
          task.main(request);

          feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
          feature["attributes"].toString.should.equal(`{}`);
        });
      });
    });
  });
});