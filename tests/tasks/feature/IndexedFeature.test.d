/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.feature.IndexedFeature;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;
import std.string;

import crate.collection.notifications;

import ogm.models.meta;
import ogm.models.changeset;

import gis_collective.tasks.feature.IndexedFeature;
import gis_collective.hmq.broadcast.memory;


alias suite = Spec!({
  URLRouter router;

  describe("The Feature.Indexed task", {
    describe("Processing list", {
      ChangeSet changeSet;

      beforeEach({
        changeSet = ChangeSet();
        changeSet.model = "Feature";
        changeSet.itemId = ObjectId.generate;
      });

      it("should process add changes inside features", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.add;

        IndexedFeature.processList(changeSet).should.equal([ changeSet.itemId.toJson ]);
      });

      it("should not process changes inside maps", {
        changeSet.model = "Map";
        changeSet.type = CrateChangeType.delete_;

        IndexedFeature.processList(changeSet).should.equal([ ]);
      });

      it("should process update changes inside features", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.update;

        IndexedFeature.processList(changeSet).should.equal([ changeSet.itemId.toJson ]);
      });

      it("should process delete changes inside features", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.delete_;

        IndexedFeature.processList(changeSet).should.equal([ changeSet.itemId.toJson ]);
      });

      it("should include the deleted maps", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.delete_;
        changeSet.removed["maps[0]"] = "map 1";
        changeSet.removed["maps[1]"] = "map 2";

        auto list = IndexedFeature.processList(changeSet);
        list[0].should.equal((`{
          "id": "` ~ changeSet.itemId.to!string ~ `",
          "removedMaps": ["map 2", "map 1"]
        }`).parseJsonString);
      });
    });

    describe("The task", {
      IndexedFeature task;
      MemoryBroadcast broadcast;
      Json[] receivedMessages;

      beforeEach({
        setupTestData();

        task = IndexedFeature();

        broadcast = new MemoryBroadcast();
        task.broadcast = broadcast;
        task.crates = crates;

        receivedMessages = [];
        void receiver(const Json message) {
          receivedMessages ~= message;
        }

        broadcast.register("Map.Mask", &receiver);
      });

      it("does nothing when a non index feature is changed", {
        auto request = IndexedFeature.Request("000000000000000000000001");
        task.main(request);

        receivedMessages.length.should.equal(0);
      });

      describe("for an index feature", {
        beforeEach({
          auto map = crates.map.getItem("000000000000000000000001").and.exec.front;
          map["isIndex"] = true;
          crates.map.updateItem(map);
        });

        it("does nothing when it is not asigned to a map", {
          auto request = IndexedFeature.Request("000000000000000000000001");
          task.main(request);

          receivedMessages.length.should.equal(0);
        });

        it("triggers a Map.Mask task when it is asigned", {
          auto map = crates.map.getItem("000000000000000000000002").and.exec.front;
          map["mask"] = `{
            "isEnabled": true,
            "map": "000000000000000000000001"
          }`.parseJsonString;
          crates.map.updateItem(map);

          auto request = IndexedFeature.Request("000000000000000000000001");
          task.main(request);

          receivedMessages.length.should.equal(1);
          receivedMessages[0].should.equal(`{
            "id": "000000000000000000000002"
          }`.parseJsonString);
        });

        it("triggers a Map.Mask task when an indexed map was removed", {
          auto map = crates.map.getItem("000000000000000000000002").and.exec.front;
          map["mask"] = `{
            "isEnabled": true,
            "map": "000000000000000000000004"
          }`.parseJsonString;
          crates.map.updateItem(map);

          auto request = IndexedFeature.Request("000000000000000000000001", ["000000000000000000000004"]);
          task.main(request);

          receivedMessages.length.should.equal(1);
          receivedMessages[0].should.equal(`{
            "id": "000000000000000000000002"
          }`.parseJsonString);
        });
      });
    });
  });
});