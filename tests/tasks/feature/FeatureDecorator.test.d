/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.feature.FeatureDecorator;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;

import crate.collection.notifications;

import ogm.models.meta;
import ogm.models.changeset;

import gis_collective.tasks.feature.FeatureDecorator;
import gis_collective.hmq.broadcast.memory;


alias suite = Spec!({
  URLRouter router;

  describe("The Feature.decorators task", {
    describe("Processing list", {
      ChangeSet changeSet;

      beforeEach({
        setupTestData();

        changeSet = ChangeSet();
        changeSet.model = "Feature";
        changeSet.itemId = ObjectId.generate;
      });

      it("should process add changes inside features", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.add;

        FeatureDecorator.processList(changeSet, crates).should.equal([ changeSet.itemId.toJson ]);
      });

      it("should process update changes inside features", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.update;

        FeatureDecorator.processList(changeSet, crates).should.equal([ changeSet.itemId.toJson ]);
      });

      it("should not process delete changes inside features", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.delete_;

        FeatureDecorator.processList(changeSet, crates).should.equal([ ]);
      });

      it("should ignore add changes inside icons", {
        changeSet.model = "Icon";
        changeSet.type = CrateChangeType.add;

        FeatureDecorator.processList(changeSet, crates).should.equal([ ]);
      });

      it("should ignore delete changes inside icons", {
        changeSet.model = "Icon";
        changeSet.type = CrateChangeType.delete_;

        FeatureDecorator.processList(changeSet, crates).should.equal([ ]);
      });

      it("should ignore update changes inside icons that are not assigned to features", {
        changeSet.model = "Icon";
        changeSet.type = CrateChangeType.update;

        FeatureDecorator.processList(changeSet, crates).should.equal([ ]);
      });

      it("should trigger a task for a feature when one of its icons is updated", {
        changeSet.model = "Icon";
        changeSet.type = CrateChangeType.update;
        changeSet.itemId = icon2._id;

        FeatureDecorator.processList(changeSet, crates).should.equal([ Json("000000000000000000000002"), Json("000000000000000000000003"), Json("000000000000000000000004") ]);
      });
    });

    describe("The task", {
      FeatureDecorator task;

      beforeEach({
        setupTestData();

        task = FeatureDecorator();
        task.crates = crates;
      });


      describe("for a point feature", {
        beforeEach({
          auto icon = crates.icon.getItem("000000000000000000000001").and.exec.front;
          icon["minZoom"] = 1;
          icon["maxZoom"] = 10;
          crates.icon.updateItem(icon);
        });

        it("sets the zoom options", {
          auto request = FeatureDecorator.Request("000000000000000000000001");
          task.main(request);

          auto item = crates.feature.getItem("000000000000000000000001").and.exec.front;

          item["decorators"].should.equal(`{
            "useDefault": true,
            "changeIndex": 0,
            "center": [],
            "showLineMarkers": true,
            "keepWhenSmall": true,
            "showAsLineAfterZoom": 99,
            "maxZoom": 10,
            "minZoom": 1,
          }`.parseJsonString);
        });

        it("does nothing when the feature has custom decorators", {
          auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
          feature["decorators"] = `{
            "useDefault": false,
            "changeIndex": 0,
            "center": [],
            "showLineMarkers": true,
            "keepWhenSmall": true,
            "showAsLineAfterZoom": 99,
            "maxZoom": 9,
            "minZoom": 2,
          }`.parseJsonString;
          crates.feature.updateItem(feature);

          auto request = FeatureDecorator.Request("000000000000000000000001");
          task.main(request);

          auto item = crates.feature.getItem("000000000000000000000001").and.exec.front;

          item["decorators"].should.equal(`{
            "useDefault": false,
            "changeIndex": 0,
            "center": [],
            "showLineMarkers": true,
            "keepWhenSmall": true,
            "showAsLineAfterZoom": 99,
            "maxZoom": 9,
            "minZoom": 2,
          }`.parseJsonString);
        });
      });

      describe("for a line feature", {
        beforeEach({
          auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
          feature["position"] = `{ "type": "LineString", "coordinates": [[1.5, 1.5], [2,2], [5,5]] }`.parseJsonString;
          crates.feature.updateItem(feature);

          auto icon = crates.icon.getItem("000000000000000000000001").and.exec.front;
          icon["minZoom"] = 1;
          icon["maxZoom"] = 10;
          icon["keepWhenSmall"] = false;
          icon["styles"]["hasCustomStyle"] = true;
          icon["styles"]["types"]["lineMarker"]["isVisible"] = false;
          crates.icon.updateItem(icon);
        });

        it("adds the 'showLineMarkers' and 'keepWhenSmall' decorators", {
          auto request = FeatureDecorator.Request("000000000000000000000001");
          task.main(request);

          auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;

          feature["decorators"].should.equal(`{
            "useDefault": true,
            "changeIndex": 0,
            "center": [],
            "showLineMarkers": false,
            "keepWhenSmall": false,
            "showAsLineAfterZoom": 99,
            "minZoom": 1,
            "maxZoom": 10,
          }`.parseJsonString);
        });

        it("ignores invalid icon properties", {
          auto icon = crates.icon.getItem("000000000000000000000001").and.exec.front;
          icon["minZoom"] = "a";
          icon["maxZoom"] = "b";
          icon["keepWhenSmall"] = "c";
          icon["styles"]["hasCustomStyle"] = "d";
          icon["styles"]["types"]["lineMarker"]["isVisible"] = "e";
          crates.icon.updateItem(icon);

          auto request = FeatureDecorator.Request("000000000000000000000001");
          task.main(request);

          auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;

          feature["decorators"].should.equal(`{
            "showAsLineAfterZoom": 99,
            "useDefault": true,
            "changeIndex": 0,
            "maxZoom": 20,
            "minZoom": 0,
            "showLineMarkers": false,
            "keepWhenSmall": true,
            "center": []
          }`.parseJsonString);
        });
      });

      describe("for a polygon feature", {
        beforeEach({
          auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
          feature["position"] = `{ "type": "Polygon", "coordinates": [[[-70.25, 30.1], [-70.25, 50.1], [-80.1, 50.1], [-80.1, 30.1], [-70.25,30.1]]] }`.parseJsonString;
          crates.feature.updateItem(feature);

          auto icon = crates.icon.getItem("000000000000000000000001").and.exec.front;
          icon["minZoom"] = 4;
          icon["maxZoom"] = 16;
          icon["keepWhenSmall"] = true;
          icon["styles"]["hasCustomStyle"] = true;
          icon["styles"]["types"]["lineMarker"]["isVisible"] = false;
          icon["styles"]["types"]["polygonMarker"]["isVisible"] = true;
          icon["styles"]["types"]["polygon"]["showAsLineAfterZoom"] = 11;
          crates.icon.updateItem(icon);
        });

        it("adds all decorators", {
          auto request = FeatureDecorator.Request("000000000000000000000001");
          task.main(request);

          auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;

          feature["decorators"].should.equal(`{
            "useDefault": true,
            "changeIndex": 0,
            "center": [ -75.175, 35.025 ],
            "showLineMarkers": false,
            "keepWhenSmall": true,
            "showAsLineAfterZoom": 11,
            "minZoom": 4,
            "maxZoom": 16,
          }`.parseJsonString);
        });

        it("does not add the center when the polygon marker and label are hidden", {
          auto icon = crates.icon.getItem("000000000000000000000001").and.exec.front;
          icon["styles"]["types"]["polygonMarker"]["isVisible"] = false;
          icon["styles"]["types"]["polygonLabel"]["isVisible"] = false;
          crates.icon.updateItem(icon);
          auto request = FeatureDecorator.Request("000000000000000000000001");
          task.main(request);

          auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;

          feature["decorators"]["center"].should.equal(`[]`.parseJsonString);
        });
      });



      describe("for a multiPolygon feature", {
        beforeEach({
          auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
          feature["position"] = `{ "type": "MultiPolygon", "coordinates": [[[[-70.25, 30.1], [-70.25, 50.1], [-80.1, 50.1], [-80.1, 30.1], [-70.25,30.1]]]] }`.parseJsonString;
          crates.feature.updateItem(feature);

          auto icon = crates.icon.getItem("000000000000000000000001").and.exec.front;
          icon["minZoom"] = 4;
          icon["maxZoom"] = 16;
          icon["keepWhenSmall"] = true;
          icon["styles"]["hasCustomStyle"] = true;
          icon["styles"]["types"]["lineMarker"]["isVisible"] = false;
          icon["styles"]["types"]["polygonMarker"]["isVisible"] = true;
          icon["styles"]["types"]["polygon"]["showAsLineAfterZoom"] = 11;
          crates.icon.updateItem(icon);
        });

        it("adds all decorators", {
          auto request = FeatureDecorator.Request("000000000000000000000001");
          task.main(request);

          auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;

          feature["decorators"].should.equal(`{
            "useDefault": true,
            "changeIndex": 0,
            "center": [ -75.175, 35.025 ],
            "showLineMarkers": false,
            "keepWhenSmall": true,
            "showAsLineAfterZoom": 11,
            "minZoom": 4,
            "maxZoom": 16,
          }`.parseJsonString);
        });

        it("does not add the center when the polygon marker and label are hidden", {
          auto icon = crates.icon.getItem("000000000000000000000001").and.exec.front;
          icon["styles"]["types"]["polygonMarker"]["isVisible"] = false;
          icon["styles"]["types"]["polygonLabel"]["isVisible"] = false;
          crates.icon.updateItem(icon);
          auto request = FeatureDecorator.Request("000000000000000000000001");
          task.main(request);

          auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;

          feature["decorators"]["center"].should.equal(`[]`.parseJsonString);
        });
      });


    });
  });
});
