/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.feature.FeatureSimplify.test;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.file;
import std.datetime;

import crate.collection.notifications;

import ogm.models.meta;
import ogm.models.changeset;

import gis_collective.tasks.feature.FeatureSimplify;
import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;

  describe("The Feature.Simplify task", {
    describe("Processing list", {
      ChangeSet changeSet;

      beforeEach({
        changeSet = ChangeSet();
        changeSet.model = "Feature";
        changeSet.itemId = ObjectId.generate;
      });

      it("should process add changes inside features", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.add;

        FeatureSimplify.processList(changeSet).should.equal([ changeSet.itemId.toJson ]);
      });

      it("should not process changes inside maps", {
        changeSet.model = "Map";
        changeSet.type = CrateChangeType.delete_;

        FeatureSimplify.processList(changeSet).should.equal([ ]);
      });

      it("should process update changes inside features", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.update;

        FeatureSimplify.processList(changeSet).should.equal([ changeSet.itemId.toJson ]);
      });

      it("should not process delete changes inside features", {
        changeSet.model = "Feature";
        changeSet.type = CrateChangeType.delete_;

        FeatureSimplify.processList(changeSet).should.equal([ ]);
      });
    });

    describe("simplify", {
      Json[] messages;
      FeatureSimplify task;

      beforeEach({
        setupTestData();

        task = FeatureSimplify();
        task.broadcast = new MemoryBroadcast();

        task.crates = crates;

        void messageReceiver(const Json message) {
          messages ~= message;
        }

        messages = [];
        task.broadcast.register("Feature.Cut", &messageReceiver);
      });

      it("simplifies the carlow polygon and passes it to the cut job", {
        auto feature = `{ "_id": "aaa" }`.parseJsonString;

        feature["position"] = readText("tests/fixtures/carlow.json").parseJsonString["geometries"][0];

        task.simplify(feature);

        crates.simpleFeature.get.exec.array.length.should.equal(112);
      });

      it("simplifies the carlow multi polygon and passes it to the cut job", {
        auto feature = `{ "_id": "aaa" }`.parseJsonString;

        feature["position"] = readText("tests/fixtures/carlow_multi.json").parseJsonString["geometries"][0];

        task.simplify(feature);

        crates.simpleFeature.get.exec.array.length.should.equal(112);
      });

      it("ignores a feature on an indexed map", {
        auto map = crates.map.getItem("000000000000000000000001").and.exec.front;
        map["isIndex"] = true;
        crates.map.updateItem(map);

        auto feature = `{ "_id": "aaa", "maps": ["000000000000000000000001"] }`.parseJsonString;
        feature["position"] = readText("tests/fixtures/carlow.json").parseJsonString["geometries"][0];

        feature = crates.feature.addItem(feature);

        task.main(FeatureSimplify.Request(ObjectId(feature["_id"])));

        crates.simpleFeature.get.exec.array.length.should.equal(0);
      });
    });
  });
});
