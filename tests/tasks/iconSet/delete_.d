/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.iconSet.delete_;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;

import crate.collection.notifications;

import ogm.models.meta;
import ogm.models.changeset;

import gis_collective.tasks.iconSet.delete_;
import gis_collective.hmq.broadcast.memory;


alias suite = Spec!({
  URLRouter router;

  describe("The IconSet.delete task", {
    describe("Processing list", {
      ChangeSet changeSet;

      beforeEach({
        changeSet = ChangeSet();
        changeSet.model = "IconSet";
        changeSet.itemId = ObjectId.generate;
      });

      it("should not process add changes inside IconSet", {
        changeSet.model = "IconSet";
        changeSet.type = CrateChangeType.add;

        IconSetDelete.processList(changeSet).should.equal([ ]);
      });

      it("should not process changes inside map", {
        changeSet.model = "Map";
        changeSet.type = CrateChangeType.delete_;

        IconSetDelete.processList(changeSet).should.equal([ ]);
      });

      it("should not process update changes inside IconSet", {
        changeSet.model = "IconSet";
        changeSet.type = CrateChangeType.update;

        IconSetDelete.processList(changeSet).should.equal([ ]);
      });

      it("should process delete changes inside IconSet", {
        changeSet.model = "IconSet";
        changeSet.type = CrateChangeType.delete_;

        IconSetDelete.processList(changeSet).should.equal([ changeSet.itemId.toJson ]);
      });
    });

    describe("The task", {
      IconSetDelete task;

      beforeEach({
        setupTestData();

        task = IconSetDelete();
        crates.iconSet.deleteItem("000000000000000000000001");
      });

      it("should remove all icons related to the icon set", {
        auto request = IconSetDelete.Request(ObjectId.fromString("000000000000000000000001"));

        task.crates = crates;
        task.main(request);

        crates.icon.get
          .where("_id").equal("000000000000000000000001")
          .and.size.should.equal(0);
      });
    });
  });
});