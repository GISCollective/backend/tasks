/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.iconSet.IconSetSprite;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;
import std.path;
import std.file;

import vibe.core.file;
import vibe.stream.memory;

import crate.collection.notifications;

import ogm.models.meta;
import ogm.models.icon;
import ogm.models.changeset;

import gis_collective.tasks.iconSet.IconSetSprite;
import gis_collective.hmq.broadcast.memory;


alias suite = Spec!({
  URLRouter router;

  describe("The IconSet.sprite task", {
    beforeEach({
      setupTestData();
    });

    describe("SpriteIconPaths", {
      SpriteIconPaths paths;

      beforeEach({
        paths = SpriteIconPaths("/tmp", 1, 2, "image/svg+xml");
      });

      it("has strSize set to @2", {
        paths.strSize.should.equal("@2");
      });

      it("has tmpFilePath set to /tmp/1.tmp@2.svg", {
        paths.tmpFilePath.should.equal("/tmp/1.tmp@2.svg");
      });

      it("has svgShapePath set to /tmp/1.shape@2.svg", {
        paths.svgShapePath.should.equal("/tmp/1.shape@2.svg");
      });

      it("has pngShapePath set to /tmp/1.shape@2.png", {
        paths.pngShapePath.should.equal("/tmp/1.shape@2.png");
      });

      it("has resultFilePath set to /tmp/1@2.png", {
        paths.resultFilePath.should.equal("/tmp/1@2.png");
      });
    });

    describe("SpriteIcon", {
      it("uses the sprite size when the measurements are not set", {
        auto sprite = SpriteIcon("/tmp", icon1, 1, 64, 21, 21, 1);

        sprite.indexData.should.equal(`{
          "height": 21,
          "width": 21,
          "pixelRatio": 1,
          "y": 21,
          "x": 21
        }`.parseJsonString);
      });

      it("uses the icon measurements when they are set", {
        icon1.measurements = IconMeasurements(20, 24);

        auto sprite = SpriteIcon("/tmp", icon1, 1, 64, 21, 21, 1);

        sprite.indexData.should.equal(`{
          "height": 24,
          "width": 20,
          "pixelRatio": 1,
          "y": 21,
          "x": 21
        }`.parseJsonString);
      });

      it("limits the image size to the slot size", {
        icon1.styles.types.site.size = 100;
        icon1.measurements = IconMeasurements(0, 0);

        auto sprite = SpriteIcon("/tmp", icon1, 1, 64, 21, 21, 1);

        sprite.indexData.should.equal(`{
          "height": 64,
          "width": 64,
          "pixelRatio": 1,
          "y": 21,
          "x": 21
        }`.parseJsonString);
      });
    });

    describe("Processing list", {
      ChangeSet changeSet;

      beforeEach({
        changeSet = ChangeSet();
        changeSet.model = "IconSet";
        changeSet.itemId = ObjectId.generate;
      });

      it("should not process add changes inside IconSet", {
        changeSet.model = "IconSet";
        changeSet.type = CrateChangeType.add;

        IconSetSprite.processList(changeSet, crates).should.equal([ changeSet.itemId.toJson ]);
      });

      it("should not process changes inside map", {
        changeSet.model = "Map";
        changeSet.type = CrateChangeType.delete_;

        IconSetSprite.processList(changeSet, crates).should.equal([ ]);
      });

      it("should process update changes inside IconSet", {
        changeSet.model = "IconSet";
        changeSet.type = CrateChangeType.update;

        IconSetSprite.processList(changeSet, crates).should.equal([ changeSet.itemId.toJson ]);
      });

      it("should process delete changes inside IconSet", {
        changeSet.model = "IconSet";
        changeSet.type = CrateChangeType.delete_;

        IconSetSprite.processList(changeSet, crates).should.equal([ changeSet.itemId.toJson ]);
      });

      it("should process changes inside icons from the set", {
        changeSet.model = "Icon";
        changeSet.itemId = ObjectId.fromString("000000000000000000000001");
        changeSet.type = CrateChangeType.update;

        IconSetSprite.processList(changeSet, crates).should.equal([ Json("000000000000000000000001") ]);
      });

      it("should process changes inside icons from the before and after fields", {
        changeSet.model = "Icon";
        changeSet.itemId = ObjectId.fromString("000000000000000000000001");
        changeSet.type = CrateChangeType.update;
        changeSet.added["iconSet"] = "000000000000000000000002";
        changeSet.removed["iconSet"] = "000000000000000000000003";

        IconSetSprite.processList(changeSet, crates).should.equal([
          Json("000000000000000000000001"),
          Json("000000000000000000000002"),
          Json("000000000000000000000003") ]);
      });

      it("should process changes once inside icons from the before and after fields", {
        changeSet.model = "Icon";
        changeSet.itemId = ObjectId.fromString("000000000000000000000001");
        changeSet.type = CrateChangeType.update;
        changeSet.added["iconSet"] = "000000000000000000000002";
        changeSet.removed["iconSet"] = "000000000000000000000001";

        IconSetSprite.processList(changeSet, crates).should.equal([ Json("000000000000000000000001"), Json("000000000000000000000002") ]);
      });
    });

    describe("The task", {
      IconSetSprite task;
      ubyte[] svgIconContent;
      ubyte[] pngIconContent;

      beforeEach({
        task = IconSetSprite();

        crateGetters["IconSet"] = &crates.iconSet.getItem;
        crateGetters["Team"] = &crates.team.getItem;
        crateGetters["Picture"] = &crates.picture.getItem;

        IconSettings.files = crates.iconFiles;
        IconSettings.chunks = crates.iconChunks;
        PictureFileSettings.files = crates.pictureFiles;
        PictureFileSettings.chunks = crates.pictureChunks;

        svgIconContent = cast(ubyte[]) (buildPath("tests", "fixtures", "icon.svg").read);
        pngIconContent = cast(ubyte[]) (buildPath("tests", "fixtures", "icon.png").read);

        setMockFile("image/svg+xml", svgIconContent.dup);

        auto icon1 = crates.icon.getItem("000000000000000000000001").and.exec.front;
        icon1["image"]["value"] = "000000000000000000000001";
        icon1["iconSet"] = "000000000000000000000001";
        crates.icon.updateItem(icon1);

        foreach(int i; 1..10) {
          auto otherIcon = crates.icon.getItem("000000000000000000000002").and.exec.front;
          otherIcon["image"]["value"] = "000000000000000000000002";
          otherIcon["iconSet"] = "000000000000000000000001";
          crates.icon.addItem(otherIcon);
        }
      });

      it("does nothing when there is no icon set", {
        auto request = IconSetSprite.Request(ObjectId.fromString("0000000000000000000000a1"));

        task.crates = crates;
        task.main(request);

        crates.picture.get
          .where("meta.iconSet").equal("0000000000000000000000a1").and
          .where("meta.type").equal("sprite").and
          .and.size.should.equal(0);
      });

      it("deletes the sprites of a missing iconset", {
        auto picture = Picture();
        picture.meta.link = Json.emptyObject;
        picture.meta.link["model"] = "IconSet";
        picture.meta.link["modelId"] = "0000000000000000000000a1";
        picture.meta.link["type"] = "sprite";

        auto request = IconSetSprite.Request(ObjectId.fromString("0000000000000000000000a1"));

        task.crates = crates;
        task.main(request);

        crates.picture.get
          .where("meta.link.modelId").equal("0000000000000000000000a1").and
          .where("meta.link.type").equal("sprite").and
          .and.size.should.equal(0);
      });

      it("builds the sprite when the set exists", {
        auto request = IconSetSprite.Request(ObjectId.fromString("000000000000000000000001"));

        task.crates = crates;
        task.main(request);

        auto pictures = crates.picture.get
          .where("meta.link.modelId").equal("000000000000000000000001").and
          .where("meta.link.type").equal("sprite").and
          .and.exec.array;

        pictures.length.should.equal(2);
        pictures[0]["meta"]["link"].should.equal(`{
          "type": "sprite",
          "size": 1,
          "modelId": "000000000000000000000001",
          "model": "IconSet"
        }`.parseJsonString);

        pictures[0]["meta"]["data"].should.equal(`{
          "size": { "height": 21, "width": 4096 },
          "index": {
            "name1": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 0 },
            "000000000000000000000011": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 147 },
            "000000000000000000000005": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 21 },
            "000000000000000000000008": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 84 },
            "name2": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 189 },
            "000000000000000000000012": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 168 },
            "000000000000000000000001": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 0 },
            "000000000000000000000006": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 42 },
            "000000000000000000000010": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 126 },
            "000000000000000000000013": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 189 },
            "000000000000000000000009": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 105 },
            "000000000000000000000007": { "height": 21, "width": 21, "pixelRatio": 1, "y": 0, "x": 63 }
        }
        }`.parseJsonString);

        pictures[1]["meta"]["link"].should.equal(`{
          "type": "sprite",
          "size": 2,
          "modelId": "000000000000000000000001",
          "model": "IconSet"
        }`.parseJsonString);

        pictures[1]["meta"]["data"].should.equal(`{
          "size": { "height": 42, "width": 4096 },
          "index": {
            "name1": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 0 },
            "000000000000000000000011": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 294 },
            "000000000000000000000005": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 42 },
            "000000000000000000000008": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 168 },
            "name2": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 378 },
            "000000000000000000000012": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 336 },
            "000000000000000000000001": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 0 },
            "000000000000000000000006": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 84 },
            "000000000000000000000010": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 252 },
            "000000000000000000000013": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 378 },
            "000000000000000000000009": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 210 },
            "000000000000000000000007": { "height": 42, "width": 42, "pixelRatio": 1, "y": 0, "x": 126 }
          }
        }`.parseJsonString);
      });

      it("clears the sprites before they are regenerated", {
        auto request = IconSetSprite.Request(ObjectId.fromString("000000000000000000000001"));

        task.crates = crates;
        task.main(request);
        task.main(request);

        auto pictures = crates.picture.get
          .where("meta.link.modelId").equal("000000000000000000000001").and
          .where("meta.link.type").equal("sprite").and
          .and.exec.array;

        pictures.length.should.equal(2);
      });

      it("sets the sprite pictures to the icon set", {
        auto request = IconSetSprite.Request(ObjectId.fromString("000000000000000000000001"));

        task.crates = crates;
        task.main(request);

        auto iconSet = crates.iconSet.getItem("000000000000000000000001").and.exec.front;

        iconSet["sprites"].should.equal(`{ "small": "000000000000000000000003", "large": "000000000000000000000004" }`.parseJsonString);
      });
    });
  });
});