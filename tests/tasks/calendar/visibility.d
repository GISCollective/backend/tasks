/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tasks.calendar.visibility;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;

import crate.collection.notifications;

import ogm.models.changeset;
import gis_collective.tasks.calendar.visibility;

alias suite = Spec!({
  URLRouter router;

  describe("The Calendar.eventVisibility task", {
    describe("Processing list", {
      ChangeSet changeSet;

      beforeEach({
        setupTestData();

        changeSet = ChangeSet();
        changeSet.model = "Calendar";
        changeSet.itemId = ObjectId.fromString("1");
      });

      it("should process an updated calendar", {
        changeSet.type = CrateChangeType.update;

        CalendarEventsVisibility.processList(changeSet, crates).should.equal([changeSet.itemId.toJson]);
      });

      it("should not process a deleted calendar", {
        changeSet.type = CrateChangeType.delete_;

        CalendarEventsVisibility.processList(changeSet, crates).should.equal([]);
      });

      it("should not process any changes inside features", {
        changeSet.model = "Feature";

        CalendarEventsVisibility.processList(changeSet, crates).should.contain([]);
      });

      it("should not process changes in the Picture model", {
        changeSet.model = "Picture";

        CalendarEventsVisibility.processList(changeSet, crates).should.equal([]);
      });
    });

    describe("the visibility field", {
      CalendarEventsVisibility task;

      beforeEach({
        setupTestData();

        task = CalendarEventsVisibility(crates);
      });

      it("updates the computed visibility of the map features", {
        crates.event.addItem(`{
          "name": "test 1",
          "location": { "type": "Text", "value": "" },
          "calendar": "000000000000000000000001",
          "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
          "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
        }`.parseJsonString);


        auto calendar = crates.calendar.getItem("000000000000000000000001").and.exec.front;
        calendar["visibility"]["isPublic"] = false;
        crates.calendar.updateItem(calendar);

        task.main(CalendarEventsVisibility.Message(ObjectId("000000000000000000000001")));

        crates.event.getItem("000000000000000000000001").exec.front["visibility"].should.equal(`{
          "isDefault":false,
          "isPublic":false,
          "team":"000000000000000000000001"
        }`.parseJsonString);
      });
    });
  });
});