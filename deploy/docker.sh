#!/bin/bash

echo "Deploy app: $2"
echo "with docker image: $1"
echo "========================================"

sudo docker pull $1
sudo docker stop $2 || true
sudo docker rm $2 || true

sudo docker stop $2-1 || true
sudo docker rm $2-1 || true

sudo docker stop $2-2 || true
sudo docker rm $2-2 || true

sudo docker run -p 5001:80 \
  --network gis-collective \
  -e mongoHost=gis-mongo \
  -v /srv/$2-1/config:/app/config:Z \
  -v /srv/$2-1/logs:/app/logs:Z \
  -d --restart=always \
  --name=$2-1 \
  $1

sudo docker run -p 5002:80 \
  --network gis-collective \
  -e mongoHost=gis-mongo \
  -v /srv/$2-2/config:/app/config:Z \
  -v /srv/$2-2/logs:/app/logs:Z \
  -d --restart=always \
  --name=$2-2 \
  $1
