# GISCollective Tasks Server

GISCollective is an open source platform for easy map-making.

This repository contains the code for running tasks when certain events are detected. It is based on the [vibe.d](https://vibed.org/) web server and it uses the [HMQ](https://gitlab.com/GISCollective/backend/hmq) client to subscribe and publish messages.

You can check the `source/gis_collective/tasks/api/api.d` file for the complete list of tasks that are implemented in this repository.

You can read more about the architecture at [https://guide.giscollective.com/en/develop/architecture/](https://guide.giscollective.com/en/develop/architecture/) or find more about our platform at
 [https://giscollective.com](https://giscollective.com/).

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](https://git-scm.com/)
* A [DLang](https://dlang.org/) compiler. We recommend using [DMD](https://dlang.org/download.html) for development and [LDC](https://github.com/ldc-developers/ldc#installation) for production releases.
* The `openssl`, `ImageMagick`, `libsqlite3`, `libevent` and `librsvg2-tools` libraries.

    For fedora/redhat:
    ```
      > dnf install -y openssl ImageMagick librsvg2-tools
    ```

    For ubuntu systems:
    ```
      > apt-get install -y git libssl-dev libevent-dev libsqlite3-dev
    ```
* The [Trial](http://trial.szabobogdan.com/) test runner
* A mongo db server running
* Optionally, you can install and start [HMQ](https://gitlab.com/GISCollective/backend/hmq), [Tasks](https://gitlab.com/GISCollective/backend/tasks) and the tasks from [Node](https://gitlab.com/GISCollective/backend/node/).

## Installation

* `git clone --recurse-submodules -j8 <repository-url>` this repository with its submodules
* `cd tasks`
* create an app `config/configuration.js` file based on `config/configuration.model.js`
* create a db `config/db` folder based on `config/db.model`
* make sure `mongo` db is running
* `dub`

## Running / Development

You can build and start the server by running `dub`.

### Writing Tests

We use [Trial](http://trial.szabobogdan.com/) for running tests and [FluentAsserts](http://fluentasserts.szabobogdan.com/) for writing assertions.

* `trial` to run all tests
* `trial -s "suite name"` to run just one suite
* `trial -t "test name"` to run tests that contain `test name` in the test name

### Building

We use [dub](https://dub.pm/commandline.html) for running and building this server:

* `dub` (development)
* `dub build --build release` (production)

For production environments we recommend building a docker container:

* `dub build --config="executable-ssl11" --build release`
* `cp gis-collective-tasks deploy/service/gis-collective-tasks-fedora-36`
* `cd deploy`
* `docker build --build-arg APP_NAME=gis-collective-tasks --no-cache=true -t $IMAGE_TAG .`

### Deploying

This app can be deployed using `*.rpm`, `*.deb` packages, by downloading the artifacts from this repo, or using a container from https://gitlab.com/GISCollective/backend/tasks/container_registry/583321

## Contributing

You can find our contribution guide at [https://guide.giscollective.com/en/develop/CONTRIBUTING/](https://guide.giscollective.com/en/develop/CONTRIBUTING/)

## Code of conduct

You can find our code of conduct at [https://guide.giscollective.com/en/develop/CODE_OF_CONDUCT/](https://guide.giscollective.com/en/develop/CODE_OF_CONDUCT/)
